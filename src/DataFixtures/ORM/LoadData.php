<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 13/02/2017
 * Time: 17:12
 */

namespace App\DataFixtures\ORM;

use App\Entity\Board;
use App\Entity\Column;
use App\Entity\Comment;
use App\Entity\Community;
use App\Entity\CommunityCandidacy;
use App\Entity\CommunityInvitation;
use App\Entity\CommunityMembership;
use App\Entity\CommunityRole;
use App\Entity\Document;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\Enums\ProjectPermissions;
use App\Entity\Feedback;
use App\Entity\Idea;
use App\Entity\Message;
use App\Entity\Organization;
use App\Entity\Post;
use App\Entity\Project;
use App\Entity\ProjectCandidacy;
use App\Entity\ProjectInvitation;
use App\Entity\ProjectJob;
use App\Entity\ProjectMembership;
use App\Entity\ProjectRole;
use App\Entity\Room;
use App\Entity\Session;
use App\Entity\Skill;
use App\Entity\Tag;
use App\Entity\Task;
use App\Entity\User;
use App\Entity\Vote;
use DateTime;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;

class LoadData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {

        //Create communities
        $community = new Community();
        $community->setName('Trantor');
        $community->setDescription('Center of the galaxy');
        $community->setLogoUrl('https://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg');
        $community->setVisible(true);
        $community->setOpen(true);
        $community->setHash('ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf');

        $community1 = new Community();
        $community1->setName('Terminus');
        $community1->setDescription('Capital planet of the first foundation');
        $community1->setLogoUrl('https://orig00.deviantart.net/21f2/f/2009/009/3/a/terminus_by_guitfiddle.jpg');
        $community1->setVisible(false);
        $community1->setOpen(false);
        $community1->setHash('gez3g5hergf6vcx1vbvghyuo7ythrtg');

        $community2 = new Community();
        $community2->setName('Gaia');
        $community2->setDescription('Origin world of the planetary intelligence known as Gaia.');
        $community2->setLogoUrl('https://aliberk15.files.wordpress.com/2013/10/37393-original.jpg');
        $community2->setVisible(true);
        $community2->setOpen(false);
        $community2->setHash('gedhgrsdv68c1xvb68tyjk8t9j4hfgfh');

        // CREATE USERS

        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setFirstName('Ad');
        $userAdmin->setLastName('Min');
        $userAdmin->setPlainPassword('admin');
        $userAdmin->setEmail('admin@Treviz.com');
        $userAdmin->addRole('ROLE_ADMIN');
        $userAdmin->setEnabled(true);

        $user1 = new User();
        $user1->setUsername('gdornick');
        $user1->setFirstName('Gaal');
        $user1->setLastName('Dornick');
        $user1->setEmail('gaal.dornick@acme.org');
        $user1->setPlainPassword('gdornick');
        $user1->setEnabled(true);

        $user2 = new User();
        $user2->setUsername('hseldon');
        $user2->setFirstName('Hari');
        $user2->setLastName('Seldon');
        $user2->setEmail('hari.seldon@acme.org');
        $user2->setPlainPassword('hseldon');
        $user2->setEnabled(true);

        $user3 = new User();
        $user3->setUsername('jpelorat');
        $user3->setFirstName('Janov');
        $user3->setLastName('Pelorat');
        $user3->setEmail('janov.pelorat@acme.org');
        $user3->setPlainPassword('jpelorat');
        $user3->setEnabled(true);

        $user4 = new User();
        $user4->setUsername('shardin');
        $user4->setFirstName('Salvor');
        $user4->setLastName('Hardin');
        $user4->setEmail('salvor.hardin@acme.org');
        $user4->setPlainPassword('shardin');
        $user4->setEnabled(true);

        $user5 = new User();
        $user5->setUsername('dolivaw');
        $user5->setFirstName('Daneel');
        $user5->setLastName('Olivaw');
        $user5->setEmail('daneel.olivaw@acme.org');
        $user5->setPlainPassword('dolivaw');
        $user5->setEnabled(true);

        $user6 = new User();
        $user6->setUsername('hmallow');
        $user6->setFirstName('Hober');
        $user6->setLastName('Mallow');
        $user6->setEmail('hober.mallow@acme.org');
        $user6->setPlainPassword('hmallow');
        $user6->setEnabled(true);
        $user6->setPasswordRequestedAt(new DateTime());
        $user6->setConfirmationToken('someConfirmationToken');

        $userToEnable = new User();
        $userToEnable->setUsername('the_mule');
        $userToEnable->setFirstName('The');
        $userToEnable->setLastName('Mule');
        $userToEnable->setEmail('the.mule@acme.org');
        $userToEnable->setPlainPassword('the_mule');
        $userToEnable->setConfirmationToken('someT0k3n');
        $userToEnable->setPasswordRequestedAt(new DateTime());
        $userToEnable->setEnabled(false);

        $organization = new Organization();
        $organization->setName('First Empire');
        $user1->setOrganization($organization);

        // CREATE SKILLS

        $skill1 = new Skill();
        $skill2 = new Skill();
        $skill3 = new Skill();
        $skill4 = new Skill();
        $skill5 = new Skill();
        $skill6 = new Skill();
        $skill7 = new Skill();
        $skill8 = new Skill();
        $skill9 = new Skill();
        $skill10 = new Skill();

        $skill1->setName('Mécanique');
        $skill2->setName('Science des matériaux');
        $skill3->setName('Développement Web');
        $skill4->setName('Développement Mobile');
        $skill5->setName('Big Data');
        $skill6->setName('Machine Learning');
        $skill7->setName('Design');
        $skill8->setName('Pitch');
        $skill9->setName('Comptabilité');
        $skill10->setName('Relativité générale');

        // CREATE TAGS

        $tag1 = new Tag();
        $tag1->setName('Environnement');

        $tag2 = new Tag();
        $tag2->setName('Energie');

        $tag3 = new Tag();
        $tag3->setName('Numérique');

        $tag4= new Tag();
        $tag4->setName('Transports');

        $tag5 = new Tag();
        $tag5->setName('Nourriture');

        $tag6 = new Tag();
        $tag6->setName('Divertissement');

        $tag7 = new Tag();
        $tag7->setName('Santé');

        // CREATE PROJECTS

        $project1 = new Project();
        $project1->setName('Encyclopedia Galactica');
        $project1->setShortDescription('Encyclopédie regroupant l\'intégralité des connaissances humaines, aka Wikipedia');
        $project1->setDescription('Encyclopédie regroupant l\'intégralité des connaissances humaines, aka Wikipedia');
        $project1->addSkill($skill2);
        $project1->addSkill($skill3);
        $project1->addSkill($skill4);
        $project1->addTag($tag3);
        $project1->addTag($tag6);
        $project1->setVisible(true);
        $project1->addCommunity($community);
        $project1->addCommunity($community1);
        $project1->setHash('nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3');

        $project2 = new Project();
        $project2->setName('Psychohistoire');
        $project2->setShortDescription('Science permettant de prédire le comportement de larges groupes de personnes');
        $project2->setDescription('Science combinant histoire, sociologie et statistiques, permettant de prédire le comportement de larges groupes de personnes');
        $project2->addSkill($skill2);
        $project2->addSkill($skill3);
        $project2->addSkill($skill4);
        $project2->addTag($tag3);
        $project2->addTag($tag6);
        $project2->addTag($tag1);
        $project2->addTag($tag2);
        $project2->setVisible(true);
        $project2->addCommunity($community);
        $project2->addCommunity($community2);
        $project2->setHash('gtr57hujtyuk61jh5gcsx1cxw21gtr45j6t684651d231596');

        $project3 = new Project();
        $project3->setName('FTL');
        $project3->setShortDescription('Création d\'un moteur permettant de voyager plus vite que la lumière');
        $project3->setDescription('Création d\'un moteur permettant de voyager plus vite que la lumière');
        $project3->addSkill($skill1);
        $project3->addSkill($skill10);
        $project3->addTag($tag2);
        $project3->setVisible(false);
        $project3->setHash('z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19');

        // CREATE PROJECT ROLES;

        $roleCreator = new ProjectRole();
        $roleCreator->setName('Creator');
        $roleCreator->setPermissions(ProjectPermissions::getAvailablePermissions());
        $roleCreator->setHash('z7g684h9re4h68464f56g62e1r315');
        $roleCreator->setGlobal(true);
        $roleCreator->setDefaultCreator(true);
        $roleCreator->setDefaultMember(false);

        // CREATE PROJECT MEMBERSHIPS;

        // Memberships for the first Project (Encyclopedia Galactica)
        $membership1 = new ProjectMembership();
        $membership1->setRole($roleCreator);
        $membership1->setUser($user1);
        $membership1->setProject($project1);
        $membership1->setHash('5678g68rze4gh86zeg4ze68gze');

        $membership2 = new ProjectMembership();
        $membership2->setRole($project1->getRoles()[0]);
        $membership2->setUser($user2);
        $membership2->setProject($project1);
        $membership2->setHash('32058h29tug68zi3tjegnie4gh86zeg4ze');

        // Memberships for the second project (Psychohistory)
        $membership3 = new ProjectMembership();
        $membership3->setRole($roleCreator);
        $membership3->setUser($user2);
        $membership3->setProject($project2);
        $membership3->setHash('h4df984bngyt94lkyu1s65g');

        // Memberships for the second project (FTL)
        $membership4 = new ProjectMembership();
        $membership4->setRole($roleCreator);
        $membership4->setUser($user3);
        $membership4->setProject($project3);
        $membership4->setHash('g96z8h4E98H4RE9HEZ654GH');

        // CREATE COMMUNITY ROLES;

        $cRoleCreator = new CommunityRole();
        $cRoleCreator->setName('Creator');
        $cRoleCreator->setDefaultCreator(true);
        $cRoleCreator->setGlobal(true);
        $cRoleCreator->setHash('nignzeg619ez1gz4eg1fd321');
        $cRoleCreator->setPermissions(CommunityPermissions::getAvailablePermissions());

        $cRoleMember = new CommunityRole();
        $cRoleMember->setName('Member');
        $cRoleMember->setHash('h6ty84ki1k2c1qsf1ze53g1s');
        $cRoleMember->addPermission(CommunityPermissions::MANAGE_POST);
        $cRoleMember->setGlobal(true);
        $cRoleMember->setDefaultMember(true);

        $specificCommunityRole = new CommunityRole();
        $specificCommunityRole->setName('Admin');
        $specificCommunityRole->setHash('erhez6t1ze65rhj16gz18r98e');
        $specificCommunityRole->addPermission(CommunityPermissions::MANAGE_MEMBERSHIP);
        $specificCommunityRole->addPermission(CommunityPermissions::MANAGE_INVITATIONS);
        $specificCommunityRole->addPermission(CommunityPermissions::MANAGE_CANDIDACIES);
        $specificCommunityRole->setGlobal(false);
        $specificCommunityRole->setDefaultMember(false);
        $specificCommunityRole->setCommunity($community2);

        $specificProjectRole = new ProjectRole();
        $specificProjectRole->setName('Admin');
        $specificProjectRole->setHash('erhez6t1ze65rhj16gz18r98e');
        $specificProjectRole->addPermission(ProjectPermissions::MANAGE_MEMBERSHIP);
        $specificProjectRole->addPermission(ProjectPermissions::MANAGE_INVITATIONS);
        $specificProjectRole->addPermission(ProjectPermissions::MANAGE_CANDIDACIES);
        $specificProjectRole->setGlobal(false);
        $specificProjectRole->setDefaultMember(false);
        $specificProjectRole->setProject($project1);

        // CREATE COMMUNITY MEMBERSHIPS;

        $cMembership1 = new CommunityMembership();
        $cMembership1->setRole($cRoleCreator);
        $cMembership1->setUser($user1);
        $cMembership1->setCommunity($community);
        $cMembership1->setHash('5678g68rze4gh86zeg4ze68gze');

        $cMembership2 = new CommunityMembership();
        $cMembership2->setRole($cRoleCreator);
        $cMembership2->setUser($user2);
        $cMembership2->setCommunity($community1);
        $cMembership2->setHash('h4df984bngyt94lkyu1s65g');

        $cMembership3 = new CommunityMembership();
        $cMembership3->setRole($cRoleCreator);
        $cMembership3->setUser($user3);
        $cMembership3->setCommunity($community2);
        $cMembership3->setHash('e5jh47trj7rtjtr8j91zef');

        $cMembership4 = new CommunityMembership();
        $cMembership4->setRole($cRoleMember);
        $cMembership4->setUser($user4);
        $cMembership4->setCommunity($community);
        $cMembership4->setHash('j7ty4kl869lk1g6n1ty68jsdbf');

        $cMembership5 = new CommunityMembership();
        $cMembership5->setRole($cRoleMember);
        $cMembership5->setUser($user5);
        $cMembership5->setCommunity($community1);
        $cMembership5->setHash('yuoliu57o7ezaf1sd6vb5df4h9re');

        $cMembership6 = new CommunityMembership();
        $cMembership6->setRole($cRoleMember);
        $cMembership6->setUser($user6);
        $cMembership6->setCommunity($community2);
        $cMembership6->setHash('j687ty4j6h1fa68ze4a5a45e');

        // ADD CANDIDACIES
        $projectCandidacy = new ProjectCandidacy();
        $projectCandidacy->setUser($user3);
        $projectCandidacy->setProject($project1);
        $projectCandidacy->setHash('e56htrj9r1eg86qs1ghg6zre1ge6');
        $projectCandidacy->setMessage('Hello World');

        $communityCandidacy = new CommunityCandidacy();
        $communityCandidacy->setUser($user3);
        $communityCandidacy->setCommunity($community1);
        $communityCandidacy->setHash('zjnziugb4eiu71funiuezh8G7365');
        $communityCandidacy->setMessage('Hello World');

        // ADD INVITATIONS
        $projectInvitation = new ProjectInvitation();
        $projectInvitation->setUser($user4);
        $projectInvitation->setProject($project1);
        $projectInvitation->setHash('1hre68h1e98z1gze8ghre198h1e8');
        $projectInvitation->setMessage('Hello world');

        $communityInvitation = new CommunityInvitation();
        $communityInvitation->setUser($user4);
        $communityInvitation->setCommunity($community2);
        $communityInvitation->setHash('235noiN53f2362yryhrz0FEZG9');
        $communityInvitation->setMessage('Hello world');

        // ADD INTERESTS FOR USERS

        $user1->addSkill($skill3);
        $user1->addSkill($skill4);
        $user2->addSkill($skill2);
        $user2->addSkill($skill3);
        $user3->addSkill($skill2);
        $user3->addSkill($skill4);
        $user4->addSkill($skill2);
        $user4->addSkill($skill3);
        $user5->addSkill($skill1);
        $user5->addSkill($skill3);
        $user6->addSkill($skill1);
        $user6->addSkill($skill4);

        $user1->addInterest($tag1);
        $user1->addInterest($tag2);
        $user1->addInterest($tag3);
        $user2->addInterest($tag4);
        $user2->addInterest($tag5);
        $user2->addInterest($tag3);
        $user3->addInterest($tag4);
        $user3->addInterest($tag6);
        $user3->addInterest($tag7);

        // Create boards and columns for a private and a public project.

        $board1 = new Board();
        $board1->setProject($project1);
        $board1->setName('1st Edition');
        $board1->setDescription('1st Edition');
        $board1->setHash('ipuhneh6rj16rtjr896tj');
        $board1->setArchived(false);
        $board1FirstColumn = new Column();
        $board1FirstColumn->setName('Pending');
        $board1FirstColumn->setHash('ehr4hgsd86v16z8r4ge9g4azeg');
        $board1FirstColumn->setBoard($board1);
        $board1SecondColumn = new Column();
        $board1SecondColumn->setName('Doing');
        $board1SecondColumn->setHash('tyki98zf41c8qv4be9h4zs8g');
        $board1SecondColumn->setBoard($board1);
        $board1ThirdColumn = new Column();
        $board1ThirdColumn->setName('Done');
        $board1ThirdColumn->setHash('djhsr9y84zqC9SF45U3K126');
        $board1ThirdColumn->setBoard($board1);

        $board2 = new Board();
        $board2->setProject($project1);
        $board2->setName('2nd Edition');
        $board2->setDescription('2nd Edition');
        $board2->setHash('ztga869f41sd6vqsdf');
        $board2->setArchived(false);
        $board2FirstColumn = new Column();
        $board2FirstColumn->setName('Review');
        $board2FirstColumn->setHash('ryik1o9_8p4lkjh6g1dqs5f1s6');
        $board2FirstColumn->setBoard($board1);

        $board3 = new Board();
        $board3->setProject($project3);
        $board3->setName('Blueprints');
        $board3->setDescription('Blueprints');
        $board3->setHash('rtj3sj68n4fdhbr86tj4');
        $board3->setArchived(false);
        $board3FirstColumn = new Column();
        $board3FirstColumn->setName('Todo');
        $board3FirstColumn->setHash('ze63g4ze68hr4eh8er4hj9tj4rtj');
        $board3FirstColumn->setBoard($board3);

        $task1 = new Task();
        $task1->setHash('oeh51erg16her76her68her');
        $task1->setName('A test task');
        $task1->setSupervisor($user1);
        $task1->setAssignee($user2);
        $task1->setColumn($board1FirstColumn);
        $task1->setPosition(0);

        $task2 = new Task();
        $task2->setHash('rtj4r9t4rj6r1jrj161sd23vgs');
        $task2->setName('A test task');
        $task2->setSupervisor($user2);
        $task2->setAssignee($user1);
        $task2->setColumn($board1FirstColumn);
        $task2->setPosition(1000);

        $task3 = new Task();
        $task3->setHash('ertuzg4z816htrj94arfd');
        $task3->setName('A test task');
        $task3->setSupervisor($user3);
        $task3->setColumn($board3FirstColumn);
        $task3->setPosition(0);

        $feedback = new Feedback();
        $feedback->setGiver($user2);
        $feedback->setReceiver($user1);
        $feedback->setTask($task2);
        $feedback->setPraise(false);
        $feedback->setFeedback('Nice !');
        $feedback->setHash('51feziuzebggne32095');

        // Documents
        $document1 = new Document();
        $document1->setCommunity($community);
        $document1->setOwner($user1);
        $document1->setName('Report');
        $document1->setExtension('odt');
        $document1->setHash('z1h68jty1t8k9u');
        $document1->setFileUrl('test-document.acme.org');
        $document1->setSize(2000);

        $document2 = new Document();
        $document2->setCommunity($community1);
        $document2->setOwner($user2);
        $document2->setName('Report alt');
        $document2->setExtension('odt');
        $document2->setHash('kjgbeziubzib763T2582');
        $document2->setFileUrl('test-document2.acme.org');
        $document2->setSize(3000);

        // Jobs
        $job1 = new ProjectJob();
        $job1->setProject($project3);
        $job1->setContact($user3);
        $job1->setHolder($user3);
        $job1->setName('Mayor');
        $job1->setDescription('Curabitur placerat tellus id lectus sagittis, ut eleifend felis laoreet. Donec vitae mi nec dolor varius molestie. ');
        $job1->setHash('rk541tkl8ty4kty68lk');

        $job2 = new ProjectJob();
        $job2->setProject($project1);
        $job2->setContact($user1);
        $job2->setName('Encyclopedist');
        $job2->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
        $job2->setHash('ziuegneziugezniug214ijb');

        // Posts

        $post1 = new Post();
        $post1->setAuthor($user1);
        $post1->setMessage('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
        $post1->setHash('eh4jr6n1fg6j4ty86k4ty');
        $post1->setPublicationDate(new DateTime('2019-04-01'));

        $post2 = new Post();
        $post2->setAuthor($user2);
        $post2->setMessage('Vestibulum a consequat leo. Nullam vel viverra enim');
        $post2->setHash('6w4ve9br8h4r9e');
        $post2->setPublicationDate(new DateTime('2019-04-02'));

        $post3 = new Post();
        $post3->setAuthor($user1);
        $post3->setMessage('Sed a enim est. Ut sed urna nibh');
        $post3->setHash('vs5x3w4v9zreh41r9eh');
        $post3->setProject($project1);
        $post3->setPublicationDate(new DateTime('2019-04-03'));

        $post4 = new Post();
        $post4->setAuthor($user2);
        $post4->setMessage('Sed a enim est. Ut sed urna nibh');
        $post4->setHash('35s4vge9r8h1z6eg');
        $post4->setProject($project1);
        $post4->setPublicationDate(new DateTime('2019-04-04'));

        $post5 = new Post();
        $post5->setAuthor($user3);
        $post5->setMessage('Sed sit amet dui et nisl sodales vehicula.');
        $post5->setHash('dez68g4hre9hq');
        $post5->setProject($project3);
        $post5->setPublicationDate(new DateTime('2019-04-05'));

        $post6 = new Post();
        $post6->setAuthor($user4);
        $post6->setMessage('Interdum et malesuada fames ac ante ipsum primis in faucibus');
        $post6->setHash('bfgh5j4er9hrz');
        $post6->setCommunity($community);
        $post6->setPublicationDate(new DateTime('2019-04-06'));

        $post7 = new Post();
        $post7->setAuthor($user2);
        $post7->setMessage('Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas');
        $post7->setHash('sty61eh498reh');
        $post7->setCommunity($community1);
        $post7->setPublicationDate(new DateTime('2019-04-07'));

        $post8 = new Post();
        $post8->setAuthor($user2);
        $post8->setMessage('Aenean facilisis lacinia faucibus. Curabitur tellus velit.');
        $post8->setHash('fgk1ui9m41hg6s1rge8');
        $post8->setDocument($document1);
        $post8->setPublicationDate(new DateTime('2019-04-08'));

        $post9 = new Post();
        $post9->setAuthor($user3);
        $post9->setMessage('Aenean facilisis lacinia faucibus. Curabitur tellus velit.');
        $post9->setHash('hgjfd68sg84zer8j4t9j');
        $post9->setJob($job1);
        $post9->setPublicationDate(new DateTime('2019-04-09'));

        $post10 = new Post();
        $post10->setAuthor($user1);
        $post10->setMessage('Aenean facilisis lacinia faucibus. Curabitur tellus velit.');
        $post10->setHash('tdkty35k4t86hj4rtjk');
        $post10->setTask($task1);
        $post10->setPublicationDate(new DateTime('2019-04-10'));

        $post11 = new Post();
        $post11->setAuthor($user3);
        $post11->setMessage('Nullam tellus eros, maximus vel nulla eget, pharetra sollicitudin ante.');
        $post11->setHash('rjyt8y4k9uy84l');
        $post11->setTask($task3);
        $post11->setPublicationDate(new DateTime('2019-04-11'));

        $post12 = new Post();
        $post12->setAuthor($user2);
        $post12->setMessage('Nunc eget semper risus. Nullam semper quis justo non iaculis.');
        $post12->setHash('neziugnz983223ijifnzeige');
        $post12->setDocument($document2);
        $post12->setPublicationDate(new DateTime('2019-04-12'));

        $comment1 = new Comment();
        $comment1->setAuthor($user2);
        $comment1->setMessage("Vivamus varius non lorem id dictum");
        $comment1->setHash("iuzebfgz29Abuogé");
        $post1->addComment($comment1);

        $comment2 = new Comment();
        $comment2->setAuthor($user3);
        $comment2->setMessage("Pellentesque sed sagittis sapien");
        $comment2->setHash("zeoignez0912non124iuB124");
        $post5->addComment($comment2);

        // Brainstorming sessions and ideas
        $session1 = new Session();
        $session1->setCommunity($community);
        $session1->setHash('iuabauribi21412NiunIU2UB12I4');
        $session1->setName('Brainstorming 1');
        $session1->setDescription('The first brainstorming session');

        $session1Idea1 = new Idea();
        $session1Idea1->setSession($session1);
        $session1Idea1->setHash('niuniu12onoivryy698fQFQS');
        $session1Idea1->setAuthor($user1);
        $session1Idea1->setContent('A first idea');
        $session1Idea1->addLiked($user4);
        $session1Idea1->addForked($project1);

        $session1Idea2 = new Idea();
        $session1Idea2->setSession($session1);
        $session1Idea2->setHash('ubgfiyezbgeziug35oin2341ubuaz09');
        $session1Idea2->setAuthor($user4);
        $session1Idea2->setContent('A second idea');

        $session2 = new Session();
        $session2->setCommunity($community);
        $session2->setHash('1egkzg1b2zafa2bnç2hRII21TH98');
        $session2->setName('Brainstorming 2');
        $session2->setDescription('A closed brainstorming session');
        $session2->setOpen(false);

        $session2Idea1 = new Idea();
        $session2Idea1->setSession($session2);
        $session2Idea1->setHash('oibdsgs01ijnu214iun6');
        $session2Idea1->setAuthor($user4);
        $session2Idea1->setContent('An idea in a closed session');

        $vote1 = new Vote();
        $vote1->setHash('zeugnize091Onzogze234');
        $vote1->setIdea($session1Idea1);
        $vote1->setScore(1);
        $vote1->setUser($user4);

        // Chat room and messages
        $room = new Room();
        $room->setHash('iub21oi96Fezfzegezg8zezgb');
        $room->setName('The Room');
        $room->setDescription('This is the room');
        $room->setCommunity($community);
        $room->addUser($user1);

        $room1 = new Room();
        $room1->setHash('ongnzeig2R124unifueg98');
        $room1->setName('First Room');
        $room1->setDescription('This is the first room');
        $room1->setCommunity($community);
        $room1->addUser($user1);

        $roomMessage = new Message();
        $roomMessage->setHash('ozegbe04124njnOU532iubi53');
        $roomMessage->setOwner($user1);
        $roomMessage->setRoom($room);
        $roomMessage->setText('This is a message');

        // PERSIS ENTITIES

        $manager->persist($tag1);
        $manager->persist($tag2);
        $manager->persist($tag3);
        $manager->persist($tag4);
        $manager->persist($tag5);
        $manager->persist($tag6);
        $manager->persist($tag7);
        $manager->persist($skill1);
        $manager->persist($skill2);
        $manager->persist($skill3);
        $manager->persist($skill4);
        $manager->persist($skill5);
        $manager->persist($skill6);
        $manager->persist($skill7);
        $manager->persist($skill8);
        $manager->persist($skill9);
        $manager->persist($skill10);
        $manager->persist($userAdmin);
        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);
        $manager->persist($user5);
        $manager->persist($user6);
        $manager->persist($userToEnable);
        $manager->persist($community);
        $manager->persist($community1);
        $manager->persist($community2);
        $manager->persist($organization);
        $manager->persist($project1);
        $manager->persist($project2);
        $manager->persist($project3);
        $manager->persist($roleCreator);
        $manager->persist($membership1);
        $manager->persist($membership2);
        $manager->persist($membership3);
        $manager->persist($membership4);
        $manager->persist($cRoleMember);
        $manager->persist($cRoleCreator);
        $manager->persist($cMembership1);
        $manager->persist($cMembership2);
        $manager->persist($cMembership3);
        $manager->persist($cMembership4);
        $manager->persist($cMembership5);
        $manager->persist($cMembership6);
        $manager->persist($board1);
        $manager->persist($board2);
        $manager->persist($board3);
        $manager->persist($feedback);
        $manager->persist($document1);
        $manager->persist($document2);
        $manager->persist($job1);
        $manager->persist($job2);
        $manager->persist($post1);
        $manager->persist($post2);
        $manager->persist($post3);
        $manager->persist($post4);
        $manager->persist($post5);
        $manager->persist($post6);
        $manager->persist($post7);
        $manager->persist($post8);
        $manager->persist($post9);
        $manager->persist($post10);
        $manager->persist($post11);
        $manager->persist($post12);
        $manager->persist($projectCandidacy);
        $manager->persist($projectInvitation);
        $manager->persist($communityCandidacy);
        $manager->persist($communityInvitation);
        $manager->persist($session1);
        $manager->persist($session1Idea1);
        $manager->persist($session1Idea2);
        $manager->persist($session2);
        $manager->persist($session2Idea1);
        $manager->persist($vote1);
        $manager->persist($room);
        $manager->persist($room1);
        $manager->persist($roomMessage);

        $manager->flush();
    }
}
