<?php

namespace App\Repository;

use App\Entity\Enums\CommunityPermissions;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * CommunityCandidacyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CommunityCandidacyRepository extends EntityRepository
{
    /**
     * @param User $user User to fetch the candidacies from
     * @param User $currentUser User who is making the request
     * @return mixed
     */
    public function getUserCandidacies(User $user, User $currentUser)
    {
        return $this->createQueryBuilder('cc')
            ->andWhere('cc.user = :user')
            ->andWhere('EXISTS (
                                SELECT cm
                                FROM App:CommunityMembership cm
                                JOIN cm.role r
                                WHERE cm.community = cc.community
                                AND cm.user = :current_user
                                AND r.permissions LIKE :read_candidacies
                              )')
            ->setParameter('user', $user)
            ->setParameter('current_user', $currentUser)
            ->setParameter('read_candidacies', '%' . CommunityPermissions::MANAGE_CANDIDACIES . '%')
            ->getQuery()
            ->getResult();
    }
}
