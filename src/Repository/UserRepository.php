<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{

    public static $SEARCH_KEY_NAME = 'name';
    public static $SEARCH_KEY_ORGANIZATION = 'organization';
    public static $SEARCH_KEY_INTERESTS = 'interests';
    public static $SEARCH_KEY_SKILLS = 'skills';
    public static $SEARCH_KEY_START = 'start';
    public static $SEARCH_KEY_OFFSET = 'offset';
    public static $SEARCH_KEY_NB = 'nb';
    public static $SEARCH_KEY_SHOW_DISABLED = 'showDisabled';

    public function searchUser($params)
    {
        $s_name = $params[self::$SEARCH_KEY_NAME];
        $s_organization = $params[self::$SEARCH_KEY_ORGANIZATION];
        $s_interests = $params[self::$SEARCH_KEY_INTERESTS];
        $s_skills = $params[self::$SEARCH_KEY_SKILLS];
        $s_start = $params[self::$SEARCH_KEY_OFFSET];
        $s_nb = $params[self::$SEARCH_KEY_NB];
        $s_show_disabled = $params[self::$SEARCH_KEY_SHOW_DISABLED];

        $qb = $this->createQueryBuilder('u');
        /*
         * If a name is specified, we look for a user whose first name, last name or username matches.
         */
        if ($s_name){

            $qb->andWhere("UPPER(CONCAT(u.firstName, ' ', u.lastName, ' ', u.username)) LIKE :name
                        OR UPPER(CONCAT(u.lastName, ' ', u.firstName, ' ', u.username)) LIKE :name
                        OR UPPER(CONCAT(u.firstName, ' ', u.username, ' ', u.lastName)) LIKE :name
                        OR UPPER(CONCAT(u.firstName, ' ', u.username, ' ', u.lastName)) LIKE :name
                        OR UPPER(CONCAT(u.lastName, ' ', u.username, ' ', u.firstName)) LIKE :name
                        OR UPPER(CONCAT(u.username, ' ', u.firstName, ' ', u.lastName)) LIKE :name
                        OR UPPER(CONCAT(u.username, ' ', u.lastName, ' ', u.firstName)) LIKE :name
                        ");
            $qb->setParameter('name', '%'.strtoupper($s_name).'%');

        }

        if ($s_organization) {
            $qb->join('u.organization', 'org');
            $qb->andWhere('org.name = :organization');
            $qb->setParameter('organization', $s_organization);
        }

        if ($s_interests){
            foreach ($s_interests as $key=>$s_interest_name){
                $qb->andWhere("EXISTS(
                                SELECT tag$key
                                FROM App:Tag tag$key
                                WHERE tag$key.name = :tag$key
                                AND tag$key MEMBER OF u.interests
                              )");
                $qb->setParameter('tag'.$key, $s_interest_name);
            }
        }

        if ($s_skills){
            foreach ($s_skills as $key=>$s_skill_name){
                $qb->andWhere("EXISTS(
                                SELECT skill$key
                                FROM App:Skill skill$key
                                WHERE skill$key.name = :skill$key
                                AND skill$key MEMBER OF u.skills
                              )");
                $qb->setParameter('skill'.$key, $s_skill_name);
            }
        }

        if (isset($s_show_disabled)) {
            $qb->andWhere('u.enabled = true');
        }

        if (isset($s_nb)){
            $qb->setMaxResults($s_nb);
        }
        else {
            $qb->setMaxResults(10);
        }

        if (isset($s_start)){
            $qb->setFirstResult($s_start);
        }

        return $qb->getQuery()->getResult();
    }

}
