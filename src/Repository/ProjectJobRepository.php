<?php

namespace App\Repository;

use App\Entity\User;
use \Doctrine\ORM\EntityRepository;

/**
 * JobRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProjectJobRepository extends EntityRepository
{

    public function searchJobs(User $currentUser,
                               string $holder = null,
                               string $name = null,
                               string $candidate = null,
                               string $project = null,
                               array $tags = null,
                               array $skills = null,
                               string $task = null,
                               string $attributed = null,
                               int $nb = 10,
                               int $offset = 0)
    {
        $qb = $this
            ->createQueryBuilder('job')
            ->join('job.project', 'project');

        if (null !== $holder) {
            $qb->join('job.holder', 'holder')
                ->andWhere('holder.username = :holder')
                ->setParameter('holder', $holder);
        }

        if (null !== $name) {
            $qb->andWhere('job.name LIKE :name')
                ->setParameter('name', '%'. $name .'%');
        }

        if (null !== $candidate) {
            $qb->andWhere(
                'EXISTS(
                    SELECT pc
                    FROM App:ProjectCandidacy pc
                    WHERE pc.user = :candidate
                    AND pc.job = job
                  )'
            )
                ->setParameter('candidate', $candidate);
        }

        if (null !== $project) {
            $qb->andWhere('project.hash ! :project')
                ->setParameter('project', $project);
        }

        if (null !== $task) {
            $task = $this
                ->getEntityManager()
                ->getRepository('App:Task')
                ->findOneByHash($task);
            if ($task) {
                $qb->andWhere(':task MEMBER OF job.tasks')
                    ->setParameter('task', $task);
            }
        }

        if (null !== $tags) {
            foreach ($tags as $key=>$tag){
                $qb->andWhere("EXISTS(
                                SELECT tag$key
                                FROM App:Tag tag$key
                                WHERE tag$key.name = :tag$key
                                AND tag$key MEMBER OF job.tags
                              )");
                $qb->setParameter('tag'.$key, $tag);
            }
        }

        if (null !== $skills) {
            foreach ($skills as $key=>$skill){
                $qb->andWhere("EXISTS(
                                SELECT skill$key
                                FROM App:Skill skill$key
                                WHERE skill$key.name = :skill$key
                                AND skill$key MEMBER OF job.skills
                              )");
                $qb->setParameter('skill'.$key, $skill);
            }
        }

        if ('true' == $attributed) {
            $qb->andWhere('job.holder IS NOT NULL');
        } elseif ('false' === $attributed) {
            $qb->andWhere('job.holder IS NULL');
        }

        $qb->andWhere(
            'project.visible = TRUE
                        OR EXISTS (
                            SELECT pm
                            FROM App:ProjectMembership pm
                            WHERE pm.user = :user
                            AND pm.project = project
                        )
                        OR EXISTS (
                            SELECT cm
                            FROM App:CommunityMembership cm
                            WHERE cm.user = :user
                            AND cm.community MEMBER OF project.communities
                        )'
        );
        $qb->setParameter('user', $currentUser);

        return $qb->setMaxResults($nb)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();

    }

}
