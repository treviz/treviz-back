<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\User;
use App\Entity\Column;

/**
 * TaskRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TaskRepository extends EntityRepository
{

    /**
     * @param $assignee
     * @param $supervisor
     * @param $to_approve
     * @param User $currentUser
     *
     * @return mixed
     */
    function findTasks($assignee, $supervisor, $to_approve, $currentUser) {
        $qb = $this->createQueryBuilder('task')
            ->join('task.column', 'column')
            ->join('column.board', 'board')
            ->join('board.project', 'project');

        if (!$currentUser->isAdmin()) {
            $qb
                ->join('project.memberships', 'memberships')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->eq('memberships.user', ':currentUser'),
                        $qb->expr()->eq('project.visible', 'TRUE')
                    )
                )
                ->setParameter('currentUser', $currentUser);
        }

        if ($assignee) {
            $qb->join('task.assignee', 'assignee');
            $qb->andWhere('assignee.username = :assignee')
                ->setParameter('assignee', $assignee);
        }

        if ($supervisor) {
            $qb->join('task.supervisor', 'supervisor');
            $qb->andWhere('supervisor.username = :supervisor')
                ->setParameter('supervisor', $supervisor);
        }

        if ($to_approve) {
            $qb->andWhere('task.pendingApproval = true');
        }

        return $qb
            ->andWhere('task.archived = false')
            ->getQuery()
            ->getResult();
    }

    function findTasksByColumn(Column $column) {
        return $this->createQueryBuilder("task")
            ->andWhere("task.column = :column")
            ->setParameter("column", $column)
            ->getQuery()
            ->getResult();
    }

    function findNotArchivedTasksByColumn(Column $column) {
        return $this->createQueryBuilder("task")
            ->andWhere("task.column = :column")
            ->andWhere("task.archived = false")
            ->setParameter("column", $column)
            ->getQuery()
            ->getResult();
    }
}
