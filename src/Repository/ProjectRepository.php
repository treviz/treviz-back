<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * ProjectRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProjectRepository extends EntityRepository
{

    public function searchProject(User $currentUser,
                                  string $name = null,
                                  string $community = null,
                                  array $tags = null,
                                  array $skills = null,
                                  string $user = null,
                                  string $role = null,
                                  int $offset = 0,
                                  int $nbResult = 20)
    {


        /*
         * We only return the public projects, the private ones who share at least one community with the
         * current user, those in which he is member or invited.
         */
        $qb = $this
            ->createQueryBuilder('project')
            ->andWhere("project.visible = true 
                           OR EXISTS(
                            SELECT pm
                            FROM App:ProjectMembership pm
                            WHERE pm.user = :currentUser
                            AND pm.project = project
                           )
                           OR EXISTS(
                            SELECT cm
                            FROM App:CommunityMembership cm
                            JOIN cm.community c
                            WHERE cm.user = :currentUser
                            AND c MEMBER OF project.communities
                           )
                           OR EXISTS(
                            SELECT inv
                            FROM App:ProjectInvitation inv
                            WHERE inv.user = :currentUser
                            AND inv.project = project
                           )")
            ->setParameter("currentUser", $currentUser);

        if ($name != null) {
            $qb->andWhere("project.name LIKE :name");
            $qb->setParameter('name', '%'.$name.'%');
        }

        /*
         * If a community is specified, only return the projects of this community.
         */
        if ($community != null) {
            $qb->andWhere('EXISTS (
                SELECT linkedCommunity
                FROM App:Community linkedCommunity
                WHERE linkedCommunity MEMBER OF project.communities
                AND linkedCommunity.hash = :community_hash
            )')
                ->setParameter('community_hash', $community);

        }

        if ($tags != null) {
            foreach ($tags as $key=>$tag){
                $qb->andWhere("EXISTS(
                                SELECT tag$key
                                FROM App:Tag tag$key
                                WHERE tag$key.name = :tag$key
                                AND tag$key MEMBER OF project.tags
                              )");
                $qb->setParameter('tag'.$key, $tag);
            }
        }

        if ($skills != null) {
            foreach ($skills as $key=>$skill){
                $qb->andWhere("EXISTS(
                                SELECT skill$key
                                FROM App:Skill skill$key
                                WHERE skill$key.name = :skill$key
                                AND skill$key MEMBER OF project.skills
                              )");
                $qb->setParameter('skill'.$key, $skill);
            }
        }

        if ($user!= null) {

            if ($role != null) {
                $qb->andWhere("EXISTS (
                                    SELECT pm2
                                    FROM App:ProjectMembership pm2
                                    JOIN pm2.role r
                                    JOIN pm2.user user2
                                    WHERE pm2.project = project
                                    AND user2.username = :user
                                    AND r.name = :role
                                   )")
                    ->setParameter("user", $user)
                    ->setParameter("role", $role);
            } else {
                $qb->andWhere("EXISTS (
                                    SELECT pm2
                                    FROM App:ProjectMembership pm2
                                    JOIN pm2.user user2
                                    WHERE pm2.project = project
                                    AND user2.username = :user
                                   )")
                    ->setParameter("user", $user);
            }
        }

        return $qb->setMaxResults($nbResult)
            ->setFirstResult($offset)
            ->orderBy("project.creationDate", 'DESC')
            ->getQuery()
            ->getResult();

    }

}
