<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:36
 */

namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Community;
use App\Entity\CommunityMembership;
use App\Repository\CommunityMembershipRepository;
use App\Entity\User;
use App\Exception\ForbiddenActionException;


class CommunityMembershipService
{

    /**
     * @var CommunityMembershipRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository('App:CommunityMembership');
    }

    /**
     * @param User $user
     * @param Community $community
     * @return null|CommunityMembership
     */
    public function getUserMembership(User $user, Community $community): ?CommunityMembership
    {
        /** @var CommunityMembership $membership */
        $membership = $this
            ->repository
            ->findOneBy(array(
                'community' => $community,
                'user' => $user
            ));

        return $membership;
    }

    /**
     * @param User $user
     * @param Community $community
     * @return bool
     */
    public function isUserMember(User $user, Community $community): bool
    {
        return $this->getUserMembership($user, $community) !== null;
    }

    public function checkUserRights(User $user, Community $community, ?string $role = null): void
    {
        if ($user->isAdmin()) {
            return;
        }

        $membership = $this->getUserMembership($user, $community);
        if (null === $membership) {
            throw new ForbiddenActionException();
        }

        if (null !== $role && !$membership->hasRole($role)) {
            throw new ForbiddenActionException();
        }
    }

}
