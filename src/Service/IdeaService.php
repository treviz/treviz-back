<?php


namespace App\Service;


use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Idea;
use App\Exception\IdeaNotFoundException;
use App\Entity\User;
use App\Exception\ForbiddenActionException;

class IdeaService
{

    /** @var EntityManagerInterface */
    private $manager;
    /** @var SessionService */
    private $sessionService;

    public function __construct(EntityManagerInterface $manager,
                                SessionService $sessionService)
    {
        $this->manager = $manager;
        $this->sessionService = $sessionService;
    }

    public function tryToGetIdeaAndCheckRights(string $hash, User $user, bool $write = false, bool $post = false): Idea
    {
        $idea = $this->tryToGetIdea($hash);
        $this->checkRights($idea, $user, $write, $post);
        return $idea;
    }

    public function tryToGetIdea(string $hash): Idea
    {
        /** @var Idea $idea */
        $idea = $this
            ->manager
            ->getRepository('App:Idea')
            ->findOneBy(array('hash' => $hash));

        if (null === $idea) {
            throw new IdeaNotFoundException($hash);
        }

        return $idea;
    }

    public function checkRights(Idea $idea, User $user, bool $write = false, bool $post = false): void
    {
        if ($user->isAdmin()) {
            return;
        }

        if (($write || $post) && !$idea->getSession()->isOpen()) {
            throw new ForbiddenActionException();
        }

        if ($idea->getAuthor() === $user) {
            return;
        }

        if ($write) {
            $this->sessionService->checkUserCanManageIdeas($idea->getSession(), $user);
        } else {
            $this->sessionService->checkUserCanAccessSession($idea->getSession(), $user);
        }
    }

    public function deleteIdea(Idea $idea)
    {
        /** @var Project[] $relatedProject */
        $relatedProject = $this
            ->manager
            ->getRepository('App:Project')
            ->findBy(array(
                'idea' => $idea
            ));
        foreach ($relatedProject as $project) {
            $project->setIdea(null);
        }
        $this->manager->remove($idea);
        $this->manager->flush();
    }
}
