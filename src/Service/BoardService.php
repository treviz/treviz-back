<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\Board;
use App\Exception\BoardNotFoundException;
use App\Repository\BoardRepository;
use App\Entity\Enums\ProjectPermissions;

class BoardService
{

    /** @var ProjectMembershipService $projetMembershipService */
    private $projetMembershipService;

    /** @var BoardRepository $repository */
    private $repository;

    public function __construct(ProjectMembershipService $projetMembershipService,
                                EntityManagerInterface $manager)
    {
        $this->projetMembershipService = $projetMembershipService;
        $this->repository = $manager->getRepository('App:Board');
    }

    public function tryToGetBoardAndCheckAccess(string $hash, User $user, bool $write = false)
    {
        $board = $this->tryToGetBoard($hash);
        $this->checkUserCanAccessBoard($board, $user, $write);
        return $board;
    }

    public function tryToGetBoard(string $hash): Board
    {
        /** @var Board $board */
        $board = $this
            ->repository
            ->findOneBy(array('hash' => $hash));

        if (null === $board) {
            throw new BoardNotFoundException($hash);
        }

        return $board;
    }

    public function checkUserCanAccessBoard(Board $board, User $user, bool $write = false): void
    {
        if (!$write && $board->getProject()->isVisible()) {
            return;
        }

        $this
            ->projetMembershipService
            ->checkUserRights(
                $user,
                $board->getProject(),
                $write ? ProjectPermissions::MANAGE_KANBAN : null
            );
    }

}
