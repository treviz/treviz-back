<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 30/12/2018
 * Time: 16:15
 */

namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class AccountService
{

    /** EntityManagerInterface $manager */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function getUserInfo(User $user)
    {

        $messages = $this
            ->manager
            ->getRepository('App:Message')
            ->findBy(array(
                'owner' => $user
            ));

        $chatrooms = $user->getRooms();

        $posts = $this
            ->manager
            ->getRepository('App:Post')
            ->findBy(array(
                'author' => $user
            ));

        $projectMemberships = $user->getProjectsMemberships();
        $projectInvitations = $user->getProjectsInvitations();
        $projectCandidacies = $user->getProjectsCandidacies();

        $communityMemberships = $user->getCommunitiesMemberships();
        $communityInvitations = $user->getCommunitiesInvitations();
        $communityCandidacies = $user->getCommunitiesCandidacies();

        $ideas = $user->getIdeas();

        $enhancements = $user->getIdeasEnhancements();

        $documents = $this
            ->manager
            ->getRepository('App:Document')
            ->findBy(array(
                'owner' => $user
            ));

        $affectedTasks = $this
            ->manager
            ->getRepository('App:Task')
            ->findBy(array(
                'assignee' => $user
            ));

        $supervisedTasks = $this
            ->manager
            ->getRepository('App:Task')
            ->findBy(array(
                'supervisor' => $user
            ));

        $supervisedJobs = $this
            ->manager
            ->getRepository('App:ProjectJob')
            ->findBy(array(
                'contact' => $user
            ));

        $assignedJobs = $this
            ->manager
            ->getRepository('App:ProjectJob')
            ->findBy(array(
                'holder' => $user
            ));

        return array(
            "user" => $user,
            "projects" => array(
                "memberships" => $projectMemberships,
                "candidacies" => $projectCandidacies,
                "invitations" => $projectInvitations,
            ),
            "communities" => array(
                "memberships" => $communityMemberships,
                "candidacies" => $communityCandidacies,
                "invitations" => $communityInvitations,
            ),
            "brainstorming" => array(
                "ideas" => $ideas,
                "enhancements" => $enhancements
            ),
            "chat" => array(
                "chatrooms" => $chatrooms,
                "messages" => $messages,
            ),
            "documents" => $documents,
            "posts" => $posts,
            "tasks" => array(
                "affected" => $affectedTasks,
                "supervised" => $supervisedTasks,
            ),
            "jobs" => array(
                "supervised" => $supervisedJobs,
                "holded" => $assignedJobs,
            )
        );
    }

}
