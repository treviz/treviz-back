<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Entity\Idea;
use App\Entity\Community;
use App\Entity\Enums\CommunityPermissions;
use App\Exception\CommunityNotFoundException;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Entity\Project;

class CommunityService
{

    /**
     * @var EntityManagerInterface manager
     */
    private $manager;

    /** @var CommunityMembershipService $communityMembershipService */
    private $communityMembershipService;

    public function __construct(EntityManagerInterface $entityManager,
                                CommunityMembershipService $communityMembershipService)
    {
        $this->manager = $entityManager;
        $this->communityMembershipService = $communityMembershipService;
    }

    public function tryToGetCommunity(string $hash): Community
    {
        /** @var Community $community */
        $community = $this
            ->manager
            ->getRepository('App:Community')
            ->findOneBy(array('hash' => $hash));

        if (null === $community) {
            throw new CommunityNotFoundException($hash);
        }

        return $community;
    }

    public function tryToGetCommunityAndCheckRights(string $hash, User $user, string $role): Community
    {
        $community = $this->tryToGetCommunity($hash);
        $this->communityMembershipService
            ->checkUserRights($user, $community, $role);
        return $community;
    }

    /*
     * If the community is public, return its memberships.
     * Otherwise, check if the user has access to it (i.e the user is member or invited).
     */
    public function checkCommunityVisibleForUser(Community $community, User $user): void
    {
        if ($community->isVisible() || $user->isAdmin()) {
            return;
        }

        $membership = $this
            ->communityMembershipService
            ->getUserMembership($user, $community);

        if (null === $membership) {
            $invitation = $this
                ->manager
                ->getRepository('App:CommunityInvitation')
                ->findOneBy(array(
                    'user' => $user,
                    'community' => $community
                ));

            if (null === $invitation) {
                throw new ForbiddenActionException();
            }
        }
    }

    /**
     * @param $hash
     * @param $currentUser
     *
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteCommunity($hash, $currentUser): bool
    {
        $community = $this
            ->tryToGetCommunity($hash);

        $this
            ->communityMembershipService->checkUserRights($currentUser, $community, CommunityPermissions::DELETE_COMMUNITY);

        $this->unlinkAllIdeas($community);
        $this->unlinkAllProjects($community);
        $this->manager->remove($community);
        $this->manager->flush();

        return true;
    }

    /**
     * @param Community $community
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function unlinkAllIdeas(Community $community): void
    {
        foreach ($community->getBrainstormingSessions() as $session) {
            /** @var Idea $idea */
            foreach ($session->getIdeas() as $idea) {
                if ($idea->getForked()->count()) {
                    $session->removeIdea($idea);
                } else {
                    $this->manager->remove($idea);
                }
            }
        }
        $this->manager->flush();
    }

    /**
     * @param Community $community
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function unlinkAllProjects(Community $community): void
    {
        /** @var Project $project */
        foreach ($community->getProjects() as $project) {
            $project->removeCommunity($community);
        }
        $this->manager->flush();
    }
}
