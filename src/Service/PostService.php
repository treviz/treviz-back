<?php


namespace App\Service;


use App\Entity\Comment;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\Enums\ProjectPermissions;
use App\Entity\Post;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PostService
{

    /** @var PostRepository */
    private $repository;
    /** @var ProjectService */
    private $projectService;
    /** @var ProjectMembershipService */
    private $projectMembershipService;
    /** @var CommunityMembershipService */
    private $communityMembershipService;

    public function __construct(EntityManagerInterface $entityManager,
                                ProjectService $projectService,
                                ProjectMembershipService $projectMembershipService,
                                CommunityMembershipService $communityMembershipService)
    {
        $this->repository = $entityManager->getRepository('App:Post');
        $this->projectMembershipService = $projectMembershipService;
        $this->communityMembershipService = $communityMembershipService;
        $this->projectService = $projectService;
    }

    /**
     * Checks the user can access the post with read/write rights.
     * @param Post $post
     * @param User $currentUser
     */
    public function checkUserCanAccessPost(Post $post, User $currentUser): void
    {
        if ($currentUser->isAdmin()) {
            return;
        }

        $project = $post->getRelatedProject();
        if (null !== $project && !$project->isVisible()) {
            $this->projectService->checkProjectVisibleForUser($project, $currentUser);
            return;
        } elseif ($community = $post->getRelatedCommunity()) {
            if (null !== $community && !$community->isVisible()) {
                $membership = $this
                    ->communityMembershipService
                    ->getUserMembership($currentUser, $community);

                if ($membership === null) {
                    throw new AccessDeniedHttpException('You cannot access this post');
                }
            }
        }
    }

    public function checkCurrentUserManagePost(Post $post, User $currentUser): void
    {
        if ($currentUser !== $post->getAuthor()) {
            $this->checkProjectOrCommunityPermissions($post, $currentUser);
        }
    }

    public function checkCurrentUserManageComment(Comment $comment, User $currentUser): void
    {
        if ($currentUser !== $comment->getAuthor()) {
            $post = $comment->getPost();
            $this->checkProjectOrCommunityPermissions($post, $currentUser);
        }
    }

    public function checkProjectOrCommunityPermissions(Post $post, User $currentUser)
    {
        if ($community = $post->getRelatedCommunity()) {
            $this
                ->communityMembershipService
                ->checkUserRights($currentUser, $community, CommunityPermissions::MANAGE_POST);
        } elseif ($project = $post->getRelatedProject()) {
            $this
                ->projectMembershipService
                ->checkUserRights($currentUser, $project, ProjectPermissions::MANAGE_POST);
        } else if (!$currentUser->isAdmin()) {
            throw new ForbiddenActionException();
        }
    }
}
