<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 12/05/2019
 * Time: 12:57
 */

namespace App\Service;


use App\Repository\CommunityRoleRepository;
use App\Utils\HashGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Community;
use App\Entity\CommunityRole;

class CommunityRoleService
{

    /**
     * @var EntityManagerInterface manager
     */
    private $manager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->manager = $entityManager;
    }

    public function getOrCreateDefaultMemberRole(Community $community): CommunityRole
    {
        /** @var CommunityRoleRepository $repository */
        $repository = $this
            ->manager
            ->getRepository('App:CommunityRole');

        /** @var CommunityRole $role */
        $role = $repository
            ->findOneBy(array(
                'community' => $community,
                'defaultMember' => true
            ));
        if (null !== $role) {
            return $role;
        }

        /** @var CommunityRole $globalRole */
        $globalRole = $repository
            ->findOneBy(array(
                'global' => true,
                'defaultMember' => true
            ));
        if (null !== $globalRole) {
            return $globalRole;
        }

        return $this->generateDefaultMemberRole($community);
    }

    /**
     * Generates the default role for new users of the community.
     * This role does not have any specific permission.
     *
     * @param Community $community
     * @return CommunityRole
     */
    public function generateDefaultMemberRole(Community $community): CommunityRole
    {
        $role = new CommunityRole();
        $repository = $this
            ->manager
            ->getRepository('App:CommunityRole');

        $roleHash = HashGeneratorService::generateHash($repository);
        $role->setHash($roleHash);
        $role->setName('Member');
        $role->setCommunity($community);
        $role->setDefaultMember(true);
        $this->manager->persist($role);
        return $role;
    }


}
