<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 28/04/2019
 * Time: 15:52
 */

namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use App\Entity\Idea;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\UserNotFoundException;
use App\Entity\Project;

class UserService
{

    /** EntityManagerInterface $entityManager */
    private $entityManager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->entityManager = $manager;
    }

    /**
     * Tries to fetch a user from the database and checks if he or she can be updated.
     * If not, throws errors.
     * @param string $username Username of the user to fetch
     * @param User $currentUser User making the request
     * @return User
     */
    public function tryToGetUpdatableUser(string $username, User $currentUser): User
    {
        /** @var User $user */
        $user = $this
            ->entityManager
            ->getRepository('App:User')
            ->findOneBy(array('username' => $username));

        if ($user === null) {
            throw new UserNotFoundException();
        }

        if ($user !== $currentUser && !$currentUser->isAdmin()) {
            throw new ForbiddenActionException();
        }

        return $user;
    }

    /**
     * Deletes a user:
     * - Delete all tasks that this user is supervising.
     * - Remove him/her from all tasks assignees.
     * - Delete all the posts of the user
     * - Delete all the messages sent in chat rooms
     *
     * @param User $user User to delete
     * @throws ORMException
     */
    public function deleteUser(User $user)
    {
        $supervisedTasks = $this
            ->entityManager
            ->getRepository('App:Task')
            ->findBy(array('supervisor' => $user));
        foreach ($supervisedTasks as $task) {
            $this
                ->entityManager
                ->remove($task);
        }

        $assignedTasks = $this
            ->entityManager
            ->getRepository('App:Task')
            ->findBy(array('assignee' => $user));
        foreach ($assignedTasks as $task) {
            $task->setAssignee(null);
        }

        $posts = $this
            ->entityManager
            ->getRepository('App:Post')
            ->findBy(array('author' => $user));
        foreach ($posts as $post) {
            $this
                ->entityManager
                ->remove($post);
        }

        $messages = $this
            ->entityManager
            ->getRepository('App:Message')
            ->findBy(array('owner' => $user));
        foreach ($messages as $message) {
            $this
                ->entityManager
                ->remove($message);
        }

        $managedJobs = $this
            ->entityManager
            ->getRepository('App:ProjectJob')
            ->findBy(array('contact' => $user));
        foreach ($managedJobs as $job) {
            $this
                ->entityManager
                ->remove($job);
        }

        $holdJobs = $this
            ->entityManager
            ->getRepository('App:ProjectJob')
            ->findBy(array('holder' => $user));
        foreach ($holdJobs as $job) {
            $job->setHolder(null);
        }

        $ideas = $user->getIdeas();
        /** @var Idea $idea */
        foreach ($ideas as $idea) {
            /** @var Project $project */
            foreach ($idea->getForked() as $project) {
                $project->setIdea(null);
            }
            $this
                ->entityManager
                ->remove($idea);
        }

        $givenFeedback = $this
            ->entityManager
            ->getRepository('App:Feedback')
            ->findBy(array(
                'giver' => $user
            ));
        foreach ($givenFeedback as $feedback) {
            $this->entityManager
                ->remove($feedback);
        }

        $receivedFeedback = $this
            ->entityManager
            ->getRepository('App:Feedback')
            ->findBy(array(
                'receiver' => $user
            ));
        foreach ($receivedFeedback as $feedback) {
            $this->entityManager
                ->remove($feedback);
        }

        $documents = $this
            ->entityManager
            ->getRepository('App:Document')
            ->findBy(array('owner' => $user));
        foreach ($documents as $document) {
            $this
                ->entityManager
                ->remove($document);
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

}
