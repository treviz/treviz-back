<?php


namespace App\Service;


use App\Entity\CommunityRole;
use App\Repository\CommunityRoleRepository;
use App\Repository\ProjectRoleRepository;
use App\Utils\HashGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use App\Entity\Project;
use App\Entity\ProjectRole;

class ProjectRoleService
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $manager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->manager = $entityManager;
    }

    public function getOrCreateDefaultRole(Project $project): ProjectRole
    {
        /** @var ProjectRoleRepository $repository */
        $repository = $this
            ->manager
            ->getRepository('App:ProjectRole');

        /** @var ProjectRole $role */
        $role = $repository
            ->findOneBy(array(
                'project' => $project,
                'defaultMember' => true
            ));
        if (null !== $role) {
            return $role;
        }

        /** @var ProjectRole $globalRole */
        $globalRole = $repository
            ->findOneBy(array(
                'global' => true,
                'defaultMember' => true
            ));
        if (null !== $globalRole) {
            return $globalRole;
        }

        return $this->generateDefaultMemberRole($project);
    }

    /**
     * Generates the default role for new users of the community.
     * This role does not have any specific permission.
     *
     * @param Project $project
     * @return ProjectRole
     */
    public function generateDefaultMemberRole(Project $project): ProjectRole
    {
        $role = new ProjectRole();
        $repository = $this
            ->manager
            ->getRepository('App:ProjectRole');

        $roleHash = HashGeneratorService::generateHash($repository);
        $role->setHash($roleHash);
        $role->setName('Member');
        $role->setProject($project);
        $role->setDefaultMember(true);
        $this->manager->persist($role);
        return $role;
    }

}
