<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;


class MailerService {

    private $mailer;
    private $translator;
    private $contact;
    private $locale;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(MailerInterface $mailer,
                                TranslatorInterface $translator,
                                LoggerInterface $logger,
                                String $mailerContact,
                                String $locale) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->contact = $mailerContact;
        $this->locale = $locale;
        $this->logger = $logger;
    }

    /**
     * @param User $user
     * @param String $url
     * @throws TransportExceptionInterface
     */
    public function sendRegistrationMail(User $user, String $url): void
    {
        $mail = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('signup.welcome'))
            ->htmlTemplate('mails/registration.html.twig')
            ->context(array(
                'lang' => $this->locale,
                'url' => $url,
                'contact' => $this->contact
            ));
        $this->logger->info("Send Registration mail to user " . $user->getUsername());
        $this->mailer->send($mail);
    }

    /**
     * @param User $user
     * @param $url
     * @throws TransportExceptionInterface
     */
    public function sendResetPasswordMail(User $user, $url): void
    {
        $mail = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('reset_pwd.subject'))
            ->htmlTemplate('mails/reset.html.twig')
            ->context(array(
                    'lang' => $this->locale,
                    'url' => $url,
                    'name' => $user->getFirstName(),
                    'contact' => $this->contact
            ));
        $this->logger->info("Send Reset password mail to user " . $user->getUsername());
        $this->mailer->send($mail);
    }

    /**
     * @param User $user User to send the message to
     * @param string $subject Subject of the mail
     * @param string $message Message to send
     * @param string $action Text to display in the 'action' button (link to the project or community)
     * @param string $url Action url allowing the user to quickly navigate to the update's location
     * @param string|null $prefsUrl Url used to manage the notification preferences
     * @throws TransportExceptionInterface
     */
    public function sendUpdateMail(User $user,
                                   string $subject,
                                   string $message,
                                   string $action,
                                   string $url,
                                   ?string $prefsUrl = null): void
    {
        $preferencesUrl = $prefsUrl ?? "$url#preferences";
        $mail = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($subject)
            ->htmlTemplate( 'mails/update.html.twig')
            ->context(array(
                'lang' => $this->locale,
                'url' => $url,
                'prefsUrl' => $preferencesUrl,
                'action' => $action,
                'message' => $message
            ));
        $this->mailer->send($mail);
    }

}
