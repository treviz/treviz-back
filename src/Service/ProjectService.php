<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Entity\Project;
use App\Exception\ProjectNotFoundException;

class ProjectService
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var ProjectMembershipService $projectMembershipService
     */
    private $projectMembershipService;

    public function __construct(EntityManagerInterface $manager, ProjectMembershipService $projectMembershipService)
    {
        $this->entityManager = $manager;
        $this->projectMembershipService = $projectMembershipService;
    }

    public function tryToFetchProject(string $hash): Project
    {
        /** @var Project $project */
        $project = $this
            ->entityManager
            ->getRepository('App:Project')
            ->findOneBy(array(
                'hash' => $hash
            ));

        if (null === $project) {
            throw new ProjectNotFoundException($hash);
        }

        return $project;
    }

    public function checkProjectVisibleForUser(Project $project, User $user): void
    {
        if ($project->isVisible() || $user->isAdmin()) {
            return;
        }

        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        if (null === $membership) {
            $sharedCommunities = $this
                ->entityManager
                ->getRepository('App:Community')
                ->findSharedCommunities($user, $project);

            if (sizeof($sharedCommunities) == 0) {

                $invitation = $this
                    ->entityManager
                    ->getRepository('App:ProjectInvitation')
                    ->findOneBy(array(
                        'user' => $user,
                        'project' => $project
                    ));

                if (null === $invitation) {
                    throw new ForbiddenActionException();
                }
            }
        }
    }
}
