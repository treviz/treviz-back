<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Session;
use App\Exception\SessionNotFoundException;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Entity\Enums\ProjectPermissions;

class SessionService
{
    /** @var EntityManagerInterface */
    private $manager;
    /** @var ProjectMembershipService */
    private $projectMembershipService;
    /** @var CommunityMembershipService */
    private $communityMembershipService;

    public function __construct(EntityManagerInterface $manager,
                                ProjectMembershipService $projectMembershipService,
                                CommunityMembershipService $communityMembershipService)
    {
        $this->manager = $manager;
        $this->projectMembershipService = $projectMembershipService;
        $this->communityMembershipService = $communityMembershipService;
    }

    public function tryToGetSessionAndCheckRights(string $hash, User $user, bool $write = false, bool $post = false): Session
    {
        /** @var Session $session */
        $session = $this
            ->manager
            ->getRepository('App:Session')
            ->findOneBy(array('hash' => $hash));

        if (null === $session) {
            throw new SessionNotFoundException($hash);
        }

        if ($write) {
            $this->checkUserCanWriteSession($session, $user);
        } else {
            $this->checkUserCanAccessSession($session, $user);
        }

        if ($post && !$session->isOpen()) {
            throw new ForbiddenActionException();
        }

        return $session;
    }

    public function checkUserCanWriteSession(Session $session, User $user)
    {
        $this->checkUserHasRole(
            $session,
            $user,
            CommunityPermissions::MANAGE_BRAINSTORMING_SESSION,
            ProjectPermissions::MANAGE_BRAINSTORMING_SESSION
        );
    }

    private function checkUserHasRole(Session $session, User $user, string $communityRole, string $projectRole)
    {
        if ($user->isAdmin()) {
            return;
        }

        if ($community = $session->getCommunity()) {
            $this
                ->communityMembershipService
                ->checkUserRights($user, $community, $communityRole);
        }

        if ($project = $session->getProject()) {
            $this
                ->projectMembershipService
                ->checkUserRights($user, $project, $projectRole);
        }
    }

    public function checkUserCanAccessSession(Session $session, User $user)
    {
        if ($user->isAdmin()) {
            return;
        }

        if ($community = $session->getCommunity()) {
            if (!$this->communityMembershipService->isUserMember($user, $community)) {
                throw new ForbiddenActionException();
            }
        } elseif ($project = $session->getProject()) {
            if (!$this->projectMembershipService->isUserMember($user, $project)) {
                throw new ForbiddenActionException();
            }
        }
    }

    public function checkUserCanManageIdeas(Session $session, User $user)
    {
        $this->checkUserHasRole(
            $session,
            $user,
            CommunityPermissions::MANAGE_BRAINSTORMING_IDEAS,
            ProjectPermissions::MANAGE_BRAINSTORMING_IDEAS
        );
    }

}
