<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\Column;
use App\Exception\ColumnNotFoundException;
use App\Repository\ColumnRepository;
use App\Entity\Enums\ProjectPermissions;

class ColumnService
{

    /** @var ProjectMembershipService $projetMembershipService */
    private $projetMembershipService;

    /** @var ColumnRepository $repository */
    private $repository;

    public function __construct(ProjectMembershipService $projetMembershipService,
                                EntityManagerInterface $manager)
    {
        $this->projetMembershipService = $projetMembershipService;
        $this->repository = $manager->getRepository('App:Column');
    }

    public function tryToGetColumnAndCheckAccess(string $hash, User $user, bool $write = false)
    {
        $column = $this->tryToGetColumn($hash);
        $this->checkUserCanAccessColumn($column, $user, $write);
        return $column;
    }

    public function tryToGetColumn(string $hash): Column
    {
        /** @var Column $column */
        $column = $this
            ->repository
            ->findOneBy(array('hash' => $hash));

        if (null === $column) {
            throw new ColumnNotFoundException($hash);
        }

        return $column;
    }

    public function checkUserCanAccessColumn(Column $column, User $user, bool $write = false): void
    {
        if (!$write && $column->getBoard()->getProject()->isVisible()) {
            return;
        }

        $this
            ->projetMembershipService
            ->checkUserRights(
                $user,
                $column->getBoard()->getProject(),
                $write ? ProjectPermissions::MANAGE_KANBAN : null
            );
    }

}
