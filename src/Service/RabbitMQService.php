<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 03/08/2017
 * Time: 22:50
 */

namespace App\Service;


use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Entity\User;

class RabbitMQService
{

    /** @var AMQPChannel $channel */
    private $channel;
    /** @var boolean $connectionEstablished */
    private $connectionEstablished;

    private const NOTIFICATION_EXCHANGE = 'treviz.notification';

    /**
     * RabbitMQService constructor.
     *
     * @param $host String Host of the RabbitMQ instance
     * @param $port Int Port of the RabbitMQ instance
     * @param $user String Username to use to connect to Rabbitmq
     * @param $password String Password to use to connect to Rabbitmq
     */
    public function __construct(String $host, Int $port, String $user, String $password)
    {
        try {
            $connection = new AMQPStreamConnection($host, $port, $user, $password);
            $this->channel = $connection->channel();
            $this->channel->exchange_declare(
                self::NOTIFICATION_EXCHANGE,
                'direct',
                false,
                true,
                false
            );
            $this->connectionEstablished = true;
        } catch (Exception $exception) {
            $this->connectionEstablished = false;
        }
    }

    public function sendNotificationToUser(User $user, String $notification): void
    {
        if ($this->connectionEstablished) {
            $msg = new AMQPMessage($notification, array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
            $this->channel
                ->queue_declare(
                    'notification.' . $user->getUsername(),
                    false,
                    true,
                    false,
                    false
                );
            $this
                ->channel
                ->basic_publish(
                    $msg,
                    self::NOTIFICATION_EXCHANGE,
                    'notification.' . $user->getUsername()
                );
        }
    }

}
