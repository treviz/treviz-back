<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:36
 */

namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Entity\Project;
use App\Entity\ProjectMembership;
use App\Repository\ProjectMembershipRepository;

class ProjectMembershipService
{

    /**
     * @var ProjectMembershipRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository('App:ProjectMembership');
    }

    /**
     * @param User $user
     * @param Project $project
     * @return null|ProjectMembership
     */
    public function getUserMembership(User $user, Project $project): ?ProjectMembership
    {
        /** @var ProjectMembership $membership */
        $membership = $this->repository->findOneBy(array(
            'project' => $project,
            'user' => $user
        ));
        return $membership;
    }

    /**
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function isUserMember(User $user, Project $project): bool
    {
        return $this->getUserMembership($user, $project) !== null;
    }

    /**
     * Checks if a user is member of a specific project, and if he or she has a specific role if provided.
     * Throws an exception if the user has not.
     *
     * @param User $user
     * @param Project $project
     * @param string|null $role Role to check; defaults to null
     */
    public function checkUserRights(User $user, Project $project, ?string $role = null): void
    {
        if ($user->isAdmin()) {
            return;
        }

        $membership = $this->getUserMembership($user, $project);
        if (null === $membership) {
            throw new ForbiddenActionException();
        }

        if (null !== $role && !$membership->hasRole($role)) {
            throw new ForbiddenActionException();
        }
    }

}
