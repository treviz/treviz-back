<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LabelNotFoundException extends NotFoundHttpException
{
    /**
     * @param ?string     $hash  The hash of the label that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No label could be found with hash : $hash"
        );
    }
}
