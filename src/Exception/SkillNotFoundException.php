<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SkillNotFoundException extends NotFoundHttpException
{
    /**
     * @param ?string     $name  The name of the skill that could not be found
     */
    public function __construct($name = null)
    {
        parent::__construct(
            "No skill could be found with name : $name"
        );
    }
}
