<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectCandidacyNotFoundException extends NotFoundHttpException
{

    /**
     * ProjectNotFoundException constructor.
     *
     * @param string $hash Hash of the project Candidacy that was not found
     */
    public function __construct($hash) {
        parent::__construct(
            "No candidacy was found with hash $hash"
        );
    }

}
