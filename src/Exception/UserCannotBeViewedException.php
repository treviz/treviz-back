<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserCannotBeViewedException extends AccessDeniedHttpException
{

    public function __construct()
    {
        parent::__construct(
            "You cannot view this user"
        );
    }

}
