<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 12/05/2019
 * Time: 13:13
 */

namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MessageNotFoundException extends NotFoundHttpException
{

    /**
     * @param ?string $hash The hash of the candidacy that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No message could be found with hash : $hash"
        );
    }
}
