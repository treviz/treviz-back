<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class JobNotFoundException extends NotFoundHttpException
{

    /**
     * JobNotFoundException constructor.
     *
     * @param string $hash Hash of the job that was not found
     */
    public function __construct($hash) {
        parent::__construct(
            "No job was found with hash $hash"
        );
    }

}
