<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ColumnNotFoundException extends NotFoundHttpException
{
    /**
     * @param string     $hash  The hash of the column that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No column could be found with hash : $hash"
        );
    }
}
