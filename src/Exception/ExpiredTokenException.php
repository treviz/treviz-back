<?php


namespace App\Exception;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExpiredTokenException extends HttpException
{

    public function __construct() {
        parent::__construct(
            Response::HTTP_REQUEST_TIMEOUT,
            'Token has expired. Please try resetting your password.'
        );
    }

}
