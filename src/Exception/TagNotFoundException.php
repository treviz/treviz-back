<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TagNotFoundException extends NotFoundHttpException
{
    /**
     * @param ?string     $name  The name of the tag that could not be found
     */
    public function __construct($name = null)
    {
        parent::__construct(
            "No tag could be found with name : $name"
        );
    }
}
