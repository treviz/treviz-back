<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SessionNotFoundException extends NotFoundHttpException
{
    /**
     * @param string|null $hash  The hash of the idea that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No session could be found with hash : $hash"
        );
    }
}
