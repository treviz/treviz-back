<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BoardNotFoundException extends NotFoundHttpException
{
    /**
     * @param string     $hash  The hash of the board that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No board could be found with hash : $hash"
        );
    }
}
