<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostNotFoundException extends NotFoundHttpException
{
    /**
     * @param string     $hash  The hash of the post that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No post could be found with hash : $hash"
        );
    }

}
