<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class MailDomainAlreadyExistsException extends ConflictHttpException
{

    public function __construct() {
        parent::__construct('A domain with this name already exists');
    }

}
