<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserNotFoundException extends NotFoundHttpException
{
    /**
     * @param string     $username The username of the user that was not found
     */
    public function __construct($username = null)
    {
        parent::__construct(
            "No user could be found with username/email: $username"
        );
    }

}
