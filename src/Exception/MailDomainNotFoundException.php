<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MailDomainNotFoundException extends NotFoundHttpException
{
    /**
     * @param string     $domainName The name of the mail domain to find
     */
    public function __construct($domainName = null)
    {
        parent::__construct(
            "No domain could be found for : $domainName"
        );
    }

}
