<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EnhancementNotFoundException extends NotFoundHttpException
{
    /**
     * @param string|null $hash The hash of the enhancement that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No enhancement could be found with hash : $hash"
        );
    }
}
