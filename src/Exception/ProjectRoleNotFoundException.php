<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectRoleNotFoundException extends NotFoundHttpException
{

    /**
     * ProjectRoleNotFoundException constructor.
     *
     * @param string $hash Hash of the role that was not found
     */
    public function __construct($hash)
    {
        parent::__construct(
            "No project role was found with hash $hash"
        );
    }

}
