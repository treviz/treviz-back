<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ForbiddenActionException extends AccessDeniedHttpException
{

    public function __construct() {
        parent::__construct('You do not have the rights to perform this action');
    }

}
