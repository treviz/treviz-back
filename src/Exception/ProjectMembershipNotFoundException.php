<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectMembershipNotFoundException extends NotFoundHttpException
{

    /**
     * ProjectNotFoundException constructor.
     *
     * @param string $hash Hash of the membership that was not found
     */
    public function __construct($hash) {
        parent::__construct(
            "No membership was found with hash $hash"
        );
    }

}
