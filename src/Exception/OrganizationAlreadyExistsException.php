<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class OrganizationAlreadyExistsException extends ConflictHttpException
{

    public function __construct() {
        parent::__construct('An organization with the same name already exists');
    }

}
