<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class UserAlreadyEnabledException extends ConflictHttpException
{

    public function __construct() {
        parent::__construct('User has already been enabled');
    }

}
