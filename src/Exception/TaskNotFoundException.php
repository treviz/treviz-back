<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskNotFoundException extends NotFoundHttpException
{
    /**
     * @param ?string     $hash  The hash of the comment that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No task could be found with hash : $hash"
        );
    }
}
