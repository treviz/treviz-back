<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class UserAlreadyExistsException extends ConflictHttpException
{

    public function __construct() {
        parent::__construct("Email or Username already exists in database");
    }

}
