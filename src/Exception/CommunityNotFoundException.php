<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 12/05/2019
 * Time: 13:13
 */

namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommunityNotFoundException extends NotFoundHttpException
{

    /**
     * @param string|null $hash The hash of the community that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No community could be found with hash : $hash"
        );
    }
}
