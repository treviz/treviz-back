<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectNotFoundException extends NotFoundHttpException
{

    /**
     * ProjectNotFoundException constructor.
     *
     * @param string $hash Hash of the project that was not found
     */
    public function __construct($hash) {
        parent::__construct(
            "No project was found with hash $hash"
        );
    }

}
