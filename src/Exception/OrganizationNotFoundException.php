<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrganizationNotFoundException extends NotFoundHttpException
{
    /**
     * @param string     $name The name of the organization that was not found
     */
    public function __construct($name = null)
    {
        parent::__construct(
            "No organization with the name '$name' was found"
        );
    }

}
