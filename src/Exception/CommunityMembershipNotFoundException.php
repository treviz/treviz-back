<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 12/05/2019
 * Time: 13:13
 */

namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommunityMembershipNotFoundException extends NotFoundHttpException
{

    /**
     * @param string $hash The hash of the invitation that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No membership could be found with hash : $hash"
        );
    }
}
