<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InvalidTokenException extends AccessDeniedHttpException
{

    public function __construct() {
        parent::__construct(
            'Invalid Token'
        );
    }

}
