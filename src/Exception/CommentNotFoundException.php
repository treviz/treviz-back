<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentNotFoundException extends NotFoundHttpException
{
    /**
     * @param ?string     $hash  The hash of the comment that could not be found
     */
    public function __construct($hash = null)
    {
        parent::__construct(
            "No comment could be found with hash : $hash"
        );
    }
}
