<?php


namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectInvitationNotFoundException extends NotFoundHttpException
{

    /**
     * ProjectNotFoundException constructor.
     *
     * @param string $hash Hash of the project invitation that was not found
     */
    public function __construct($hash) {
        parent::__construct(
            "No invitation was found with hash $hash"
        );
    }

}
