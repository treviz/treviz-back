<?php

namespace App\Controller;

use App\Entity\Idea;
use App\Entity\User;
use App\Form\IdeaType;
use App\Service\IdeaService;
use App\Service\SessionService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class IdeaController
 * @package App\Controller
 */
class BrainstormingIdeaController extends AbstractFOSRestController
{

    /** @var SessionService */
    private $sessionService;
    /** @var IdeaService */
    private $ideaService;

    public function __construct(SessionService $sessionService, IdeaService $ideaService)
    {
        $this->sessionService = $sessionService;
        $this->ideaService = $ideaService;
    }


    /**
     * Fetches the ideas of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of the ideas of a specific brainstorming session",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/{hash}/ideas")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getIdeasAction(string $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $session = $this
            ->sessionService
            ->tryToGetSessionAndCheckRights($hash, $user);

        return $this->view($session->getIdeas(), Response::HTTP_OK);
    }

    /**
     * Fetch a specific idea.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when an idea was successfully fetched",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/ideas/{hash}")
     * @Rest\View(serializerGroups={"Default", "idea"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getIdeaAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this
            ->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user);

        return $this->view($idea, Response::HTTP_OK);
    }

    /**
     * Adds a new idea to a brainstorming session.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when an idea was successfully created",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Post("/brainstorming-sessions/{hash}/ideas")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     * @throws Exception
     */
    public function postIdeaAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $session = $this
            ->sessionService
            ->tryToGetSessionAndCheckRights($hash, $user, false, true);

        $idea = new Idea();
        $form = $this->createForm(IdeaType::class, $idea);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $idea->setSession($session);
        $idea->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:Idea')
            )
        );
        $idea->setAuthor($user);

        $entityManager->persist($idea);
        $entityManager->flush();

        return $this->view($idea, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing idea.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when an idea was successfully updated",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Put("/brainstorming-sessions/ideas/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putIdeaAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this
            ->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user, false);

        $form = $this->createForm(IdeaType::class, $idea);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this
            ->getDoctrine()
            ->getManager()
            ->flush();
        return $this->view($idea, Response::HTTP_OK);
    }

    /**
     * Deletes an existing idea.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when an idea was successfully deleted",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Delete("/brainstorming-sessions/ideas/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function deleteIdeaAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this
            ->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user, true);

        $this->ideaService->deleteIdea($idea);
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
