<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 18/03/2017
 * Time: 14:45
 */

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\User;
use App\Exception\CommentNotFoundException;
use App\Exception\PostNotFoundException;
use App\Form\CommentType;
use App\Service\PostService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class CommentController
 * @package App\Controller
 */
class PostCommentController extends AbstractFOSRestController
{
    /** @var PostService $postService  */
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Create a new comment for a specific post
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a comment was successfully created",
     * )
     *
     * @SWG\Tag(name="comment")
     *
     * @Rest\Post("/posts/{hash}/comments")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param string  $hash Hash of the post to which add a comment
     *
     * @return View
     * @throws Exception
     * @internal param $hash
     */
    public function postPostCommentAction(Request $request, string $hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $post = $entityManager
            ->getRepository('App:Post')
            ->findOneBy(array('hash' => $hash));

        if (null === $post) {
            throw new PostNotFoundException($hash);
        }

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $this->postService->checkUserCanAccessPost($post, $currentUser);

        $comment->setPost($post);
        $comment->setAuthor($currentUser);
        $comment->setHash(
            HashGeneratorService::generateHash($entityManager->getRepository('App:Comment'))
        );

        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->view($comment, Response::HTTP_CREATED);
    }

    /**
     * Updates a comment.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a comment was successfully updated",
     * )
     *
     * @SWG\Tag(name="comment")
     *
     * @Rest\Put("/posts/comments/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putPostCommentAction(Request $request, string $hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Comment $post */
        $comment = $entityManager
            ->getRepository('App:Comment')
            ->findOneBy(array('hash' => $hash));

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $this->postService->checkCurrentUserManageComment($comment, $currentUser);

        if (null === $comment) {
            throw new CommentNotFoundException($hash);
        }

        $form = $this->createForm(CommentType::class, $comment);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager->flush();
        return $this->view($comment, Response::HTTP_OK);
    }

    /**
     * Deletes a comment.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a comment was successfully deleted",
     * )
     *
     * @SWG\Tag(name="comment")
     *
     * @Rest\Delete("/posts/comments/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function deletePostCommentAction($hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Comment $post */
        $comment = $entityManager
            ->getRepository('App:Comment')
            ->findOneBy(array('hash' => $hash));

        /** @var User $user */
        $user = $this->getUser();
        $this->postService->checkCurrentUserManageComment($comment, $user);

        if (null === $comment) {
            throw new CommentNotFoundException($hash);
        }

        $entityManager->remove($comment);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

}
