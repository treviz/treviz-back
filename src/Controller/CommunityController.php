<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/03/2017
 * Time: 15:03
 */

namespace App\Controller;


use App\Entity\Community;
use App\Entity\CommunityMembership;
use App\Entity\CommunityRole;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\User;
use App\Form\CommunityType;
use App\Service\CommunityMembershipService;
use App\Service\CommunityService;
use App\Utils\HashGeneratorService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1/communities")
 *
 * Class CommunityController
 * @package App\Controller
 */
class CommunityController extends AbstractFOSRestController
{
    private $kernelProjectDir;
    private $backendUrl;
    private $communityLogoPath;
    private $communityBackgroundPath;
    /** @var CommunityService */
    private $communityService;
    /** @var CommunityMembershipService */
    private $communityMembershipService;

    /**
     * CommunityController constructor.
     *
     * @param String $kernelProjectDir
     * @param String $backendUrl
     * @param String $communityLogoPath
     * @param String $communityBackgroundPath
     * @param CommunityService $communityService
     * @param CommunityMembershipService $communityMembershipService
     */
    public function __construct(string $kernelProjectDir,
                                string $backendUrl,
                                string $communityLogoPath,
                                string $communityBackgroundPath,
                                CommunityService $communityService,
                                CommunityMembershipService $communityMembershipService)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        $this->backendUrl = $backendUrl;
        $this->communityLogoPath = $communityLogoPath;
        $this->communityBackgroundPath = $communityBackgroundPath;
        $this->communityService = $communityService;
        $this->communityMembershipService = $communityMembershipService;
    }

    /**
     * Fetches a community specified by its hash, as long as the user can see it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns a community identified by its hash",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/{hash}", requirements={"hash"="^(?!(memberships|candidacies|invitations)).\w+"})
     *
     * @param $hash
     * @return View
     */
    public function getCommunityAction($hash): View
    {
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        /** @var User $user */
        $user = $this->getUser();
        $this->communityService
            ->checkCommunityVisibleForUser($community, $user);

        return $this->view($community, Response::HTTP_OK);
    }

    /**
     * Fetches specific communities, according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of communities matching the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("")
     *
     * @Rest\QueryParam(name="name", requirements="[\p{L}\p{N}_\s]+", description="search  by name", nullable=true)
     * @Rest\QueryParam(name="offset", description="pagination", nullable=true)
     * @Rest\QueryParam(name="nb", description="pagination", nullable=true)
     * @Rest\QueryParam(name="user", description="username of the user who must take part in the community", nullable=true)
     * @Rest\QueryParam(name="exclude", description="username of a user who must not take part in the community", nullable=true)
     * @Rest\QueryParam(name="role", description="name of role of the user in those communities", nullable=true)
     * @Rest\QueryParam(name="project", description="hash of the project", nullable=true)
     *
     * @Rest\View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getCommunitiesAction(ParamFetcher $paramFetcher): View
    {
        $offset = $paramFetcher->get('offset');
        $nb = $paramFetcher->get('nb');
        /** @var User $user */
        $user = $this->getUser();
        $communities = $this
            ->getDoctrine()
            ->getRepository('App:Community')
            ->searchCommunities(
                $user,
                $paramFetcher->get('name'),
                $paramFetcher->get('user'),
                $paramFetcher->get('exclude'),
                $paramFetcher->get('role'),
                $paramFetcher->get('project'),
                $offset ?? 0,
                $nb ?? 20
            );

        return $this->view($communities, Response::HTTP_OK);
    }

    /**
     * Creates a new Community and returns it
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created community"
     * )
     *
     * @SWG\Tag(name="community")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function postCommunityAction(Request $request): View
    {
        $community = new Community();
        $form = $this->createForm(CommunityType::class, $community);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->handleLogoAndBackground(
            $community,
            $request->files->get('logo'),
            $request->files->get('background')
        );

        /** @var User $user */
        $user = $this->getUser();
        $membership = new CommunityMembership();
        $membership->setCommunity($community);
        $membership->setUser($user);

        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $defaultCreatorRole = $entityManager
            ->getRepository('App:CommunityRole')
            ->findOneBy(array('defaultCreator' => true));

        if ($defaultCreatorRole === null) {
            $defaultCreatorRole = new CommunityRole();
            $defaultCreatorRole->setName('Creator');
            $defaultCreatorRole->setDefaultCreator(true);
            $defaultCreatorRole->setDefaultMember(false);
            $defaultCreatorRole->setGlobal(true);
            $defaultCreatorRole->setPermissions(CommunityPermissions::getAvailablePermissions());
            $defaultCreatorRole->setHash(hash('sha256', random_bytes(256)));
        }

        $membership->setRole($defaultCreatorRole);
        $community->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:Community')
            )
        );

        $membership->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:CommunityMembership')
            )
        );

        $user->addRoom($community->getRooms()->first());

        $entityManager->persist($community);
        $entityManager->persist($membership);
        $entityManager->flush();

        return $this->view($community, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated community"
     * )
     *
     * @SWG\Tag(name="community")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/{hash}", requirements={"hash"="^(?!(memberships|candidacies|invitations)).\w+"})
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putCommunityAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $community = $this
            ->communityService
            ->tryToGetCommunityAndCheckRights($hash, $user, CommunityPermissions::UPDATE_COMMUNITY);

        /*
         * Set the community logo an background with the actual files so that the form builder does not crash.
         */
        if ($logoFileName = $community->getLogo()) {
            $community->setLogo(new File($this->kernelProjectDir . '/public' . $this->communityLogoPath . $logoFileName));
        }

        if ($backgroundLogoName = $community->getBackgroundImage()) {
            $community->setBackgroundImage(new File($this->kernelProjectDir . '/public' . $this->communityBackgroundPath . $backgroundLogoName));
        }

        $form = $this->createForm(CommunityType::class, $community);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->handleLogoAndBackground(
            $community,
            $request->files->get('logo'),
            $request->files->get('background')
        );
        $this
            ->getDoctrine()
            ->getManager()
            ->flush();
        return $this->view($community, Response::HTTP_OK);
    }

    /**
     * Deletes an existing community.
     * All brainstorming sessions and ideas will automatically be deleted, but we want to keep the ideas that were
     * forked into projects.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the community was successfully deleted"
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Delete("/{hash}", requirements={"hash"="^(?!(memberships|candidacies|invitations)).\w+"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @param $hash
     *
     * @return View
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteCommunityAction($hash): View
    {
        $this->communityService
            ->deleteCommunity($hash, $this->getUser());
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Posts a new logo for the community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated community with a new logo"
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/{hash}/logo")
     * @Rest\FileParam(name="image", image=true, nullable=false)
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @param $hash
     *
     * @return View|Response
     */
    public function postCommunityLogoAction(ParamFetcher $paramFetcher, $hash)
    {
        /** @var User $user */
        $user = $this->getUser();
        $community = $this
            ->communityService
            ->tryToGetCommunityAndCheckRights($hash, $user, CommunityPermissions::UPDATE_COMMUNITY);

        /** @var File $logo */
        $logo = $paramFetcher->get('image');
        $this->handleLogo($community, $logo);

        $this->getDoctrine()
            ->getManager()
            ->flush();

        return $this->view($community, Response::HTTP_OK);
    }

    /**
     * Posts a new background for the community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated community with a new background image"
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/{hash}/background")
     * @Rest\FileParam(name="image", image=true, nullable=false)
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @param $hash
     *
     * @return View|Response
     */
    public function postCommunityBackgroundAction(ParamFetcher $paramFetcher, $hash)
    {
        /** @var User $user */
        $user = $this->getUser();
        $community = $this
            ->communityService
            ->tryToGetCommunityAndCheckRights($hash, $user, CommunityPermissions::UPDATE_COMMUNITY);

        /** @var File $background */
        $background = $paramFetcher->get('image');
        $this->handleBackground($community, $background);

        $this->getDoctrine()
            ->getManager()
            ->flush();

        return $this->view($community, Response::HTTP_OK);
    }

    private function handleLogoAndBackground(Community $community, ?File $logo, ?File $background)
    {
        if (null !== $logo) {
            $this->handleLogo($community, $logo);
        }

        if (null !== $background) {
            $this->handleBackground($community, $background);
        }
    }

    private function handleLogo(Community $community, File $logo)
    {
        $logoName = HashGeneratorService::generateFileHashName($logo);
        $logo->move($this->kernelProjectDir . '/public' . $this->communityLogoPath, $logoName);
        $community->setLogo($logoName);
        $community->setLogoUrl($this->backendUrl . $this->communityLogoPath . $logoName);
    }

    private function handleBackground(Community $community, File $background)
    {
        $backgroundName = HashGeneratorService::generateFileHashName($background);
        $background->move($this->kernelProjectDir . '/public' . $this->communityBackgroundPath, $backgroundName);
        $community->setBackgroundImage($backgroundName);
        $community->setBackgroundImageUrl($this->backendUrl . $this->communityBackgroundPath . $backgroundName);
    }

}
