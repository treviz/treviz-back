<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 23:05
 */

namespace App\Controller;

use App\Form\UserPreferencesType;
use App\Service\UserService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;


/**
 * Class UserPreferencesController
 *
 * @package App\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 */
class UserPreferencesController extends AbstractFOSRestController
{
    /** @var UserService */
    private $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * Retrieves the preferences of a user.
     *
     * @Rest\Get("/users/{username}/preferences")
     *
     * @param $username
     *
     * @return View
     */
    public function getPreferencesAction($username): View
    {
        $user = $this
            ->userService
            ->tryToGetUpdatableUser($username, $this->getUser());

        return $this->view($user->getPreferences(), Response::HTTP_OK);
    }

    /**
     * Updates the preferences of a user.
     *
     * @Rest\Put("/users/{username}/preferences")
     *
     * @param Request $request
     * @param         $username
     *
     * @return View
     */
    public function updatePreferencesAction(Request $request, $username): View
    {
        $user = $this
            ->userService
            ->tryToGetUpdatableUser($username, $this->getUser());

        $preferences = $user->getPreferences();
        $form = $this->createForm(UserPreferencesType::class, $preferences);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this
            ->getDoctrine()
            ->getManager()
            ->flush();
        return $this->view($preferences, Response::HTTP_OK);
    }

}
