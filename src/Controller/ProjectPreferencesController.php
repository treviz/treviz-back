<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:33
 */

namespace App\Controller;


use App\Entity\ProjectMembership;
use App\Entity\ProjectNotificationPreferences;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\ProjectMembershipNotFoundException;
use App\Form\ProjectNotificationPreferencesType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class ProjectController
 * @package App\Controller
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 */
class ProjectPreferencesController extends AbstractFOSRestController
{

    /**
     * Return the notification preferences of a user for a project
     *
     * @Rest\Get("/projects/memberships/{hash}/preferences")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the notification preferences of a user regarding a project",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has not the rights to perform this action",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No project membership could be found for this membership",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @param string $hash
     * @return View
     */
    public function getProjectPreferencesAction(string $hash): View
    {
        return $this->view(
            $this->tryToGetPreferences($hash),
            Response::HTTP_OK
        );
    }

    /**
     * Updates the preferences of a project membership.
     *
     * @Rest\Put("/projects/memberships/{hash}/preferences")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the notification preferences of a user regarding a project",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has not the rights to perform this action",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No project membership could be found for this membership",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     */
    public function updateProjectMembershipPreferencesAction(Request $request, string $hash): View
    {
        $preferences = $this->tryToGetPreferences($hash);
        $form = $this->createForm(ProjectNotificationPreferencesType::class, $preferences);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return $this->view($preferences, Response::HTTP_OK);
    }

    private function tryToGetPreferences($hash): ProjectNotificationPreferences
    {
        /** @var ProjectMembership $membership*/
        $membership = $this
            ->getDoctrine()
            ->getRepository('App:ProjectMembership')
            ->findOneByHash($hash);

        if (null === $membership) {
            throw new ProjectMembershipNotFoundException($hash);
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (!$currentUser->isAdmin() && $membership->getUser() !== $currentUser) {
            throw new ForbiddenActionException();
        }

        return $membership->getPreferences();
    }

}
