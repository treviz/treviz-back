<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 18/03/2017
 * Time: 14:44
 */

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Exception\PostNotFoundException;
use App\Form\PostType;
use App\Message\PostCreatedMessage;
use App\Service\PostService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class PostController
 * @package App\Controller
 *
 * @Rest\Route("/v1")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class PostController extends AbstractFOSRestController
{
    /** @var PostService $postService */
    private $postService;
    /** @var MessageBusInterface $messageBus */
    private $messageBus;

    /**
     * PostController constructor.
     *
     * @param PostService $postService
     * @param MessageBusInterface $messageBus
     */
    public function __construct(PostService $postService, MessageBusInterface $messageBus)
    {
        $this->postService = $postService;
        $this->messageBus = $messageBus;
    }

    /**
     * Fetches the posts of a project, a community, according to query parameters
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of posts matching the query",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @Rest\Get("/posts")
     * @Rest\QueryParam(name="community", description="Hash of a community", nullable=true)
     * @Rest\QueryParam(name="project", description="Hash of a project", nullable=true)
     * @Rest\QueryParam(name="task", description="Hash of a task", nullable=true)
     * @Rest\QueryParam(name="document", description="Hash of a document", nullable=true)
     * @Rest\QueryParam(name="job", description="Hash of a job", nullable=true)
     * @Rest\QueryParam(name="offset", description="Offset", nullable=true)
     * @Rest\QueryParam(name="limit", description="Max number of results", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getPostsAction(ParamFetcher $paramFetcher): View
    {
        $s_project      = $paramFetcher->get('project');
        $s_community    = $paramFetcher->get('community');
        $s_task         = $paramFetcher->get('task');
        $s_document     = $paramFetcher->get('document');
        $s_job          = $paramFetcher->get('job');

        $posts = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->searchPosts(
                $this->getUser(),
                $s_community,
                $s_project,
                $s_task,
                $s_job,
                $s_document,
                $paramFetcher->get('offset'),
                $paramFetcher->get('limit')
            );

        /*
         * Specify the context for serialization.
         * If no search filter was given, adding the "GeneralPosts" group will allow for project, community... serialization.
         */
        $context = new Context();
        $context->enableMaxDepth();
        if (!$s_project && !$s_community && !$s_task && !$s_document && !$s_job) {
            $context->setGroups(['Default', 'GeneralPosts']);
        } else {
            $context->setGroups(['Default']);
        }

        $view = $this->view($posts, Response::HTTP_OK);
        $view->setContext($context);

        return $view;
    }


    /**
     * Fetches A specific post, identified by its hash.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the post",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No post was found for this hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="You do not have the rights to fetch this post",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @Rest\Get("/posts/{hash}")
     *
     * @param string $hash Hash of the post to fetch
     * @return View
     */
    public function getPostAction(string $hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Post $post */
        $post = $entityManager
            ->getRepository('App:Post')
            ->findOneBy(array('hash' => $hash));

        if (null === $post) {
            throw new PostNotFoundException($hash);
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $this->postService->checkUserCanAccessPost($post, $currentUser);

        return $this->view($post, Response::HTTP_OK);
    }

    /**
     * Creates a new post
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a post was successfully created",
     * )
     *
     * @SWG\Tag(name="posts")
     * @Rest\Post("/posts")
     *
     * @param Request $request
     *
     * @return View
     * @internal param ParamFetcher $paramFetcher
     * @throws Exception
     */
    public function postPostAction(Request $request): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /** @var User $user */
        $user = $this->getUser();
        $post->setAuthor($user);
        $post->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:Post'),
                $post->getMessage() . $post->getPublicationDate()->getTimestamp()
            )
        );

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $this->postService->checkUserCanAccessPost($post, $currentUser);

        $entityManager->persist($post);
        $entityManager->flush();

        $this->messageBus->dispatch(new PostCreatedMessage($post->getId()));

        return $this->view($post, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing post.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a post was successfully updated",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put ("/posts/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putPostAction(Request $request, $hash): View
    {
        $post = $this->tryToGetPost($hash);
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $this->postService->checkCurrentUserManagePost($post, $currentUser);
        $form = $this->createForm(PostType::class, $post);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->getDoctrine()->getManager()->flush();
        return $this->view($post, Response::HTTP_OK);
    }

    /**
     * Deletes a post.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a post was successfully deleted",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @Rest\Delete("/posts/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deletePostAction($hash): View
    {
        $post = $this->tryToGetPost($hash);
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $this->postService->checkCurrentUserManagePost($post, $currentUser);

        $entityManger = $this->getDoctrine()->getManager();
        $entityManger->remove($post);
        $entityManger->flush();
        return $this->view($post, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetPost($hash): Post
    {
        /** @var Post $post */
        $post = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->findOneBy(array('hash' => $hash));

        if (null === $post) {
            throw new PostNotFoundException($hash);
        }

        return $post;
    }

}
