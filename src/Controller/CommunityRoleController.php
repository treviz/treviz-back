<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace App\Controller;

use App\Entity\Community;
use App\Entity\CommunityMembership;
use App\Entity\CommunityRole;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\User;
use App\Exception\CommunityRoleNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Form\CommunityRoleType;
use App\Service\CommunityMembershipService;
use App\Service\CommunityService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class CommunityRoleController
 * @package App\Controller
 */
class CommunityRoleController extends AbstractFOSRestController
{
    /** @var CommunityMembershipService */
    private $communityMembershipService;
    /** @var CommunityService */
    private $communityService;

    /**
     * CommunityRoleController constructor.
     *
     * @param CommunityMembershipService $communityMembershipService
     * @param CommunityService $communityService
     */
    public function __construct(CommunityMembershipService $communityMembershipService,
                                CommunityService $communityService)
    {
        $this->communityMembershipService = $communityMembershipService;
        $this->communityService = $communityService;
    }


    /**
     * Fetches the roles of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the roles of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @param $hash
     *
     * @Rest\Get("/communities/{hash}/roles")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @return View
     */
    public function getCommunityRolesAction($hash): View
    {
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        $roles = $community->getRoles();

        $globalRoles = $this
            ->getDoctrine()
            ->getRepository('App:CommunityRole')
            ->findBy(array('global' => true));

        foreach ($globalRoles as $globalRole) {
            $roles->add($globalRole);
        }

        return $this->view($roles, Response::HTTP_OK);
    }

    /**
     * Adds a new role to a community.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created role",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Post ("/communities/{hash}/roles")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postCommunityRoleAction(Request $request, $hash): View
    {
        $em = $this->getDoctrine()->getManager();
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        /** @var User $user */
        $user = $this->getUser();

        $this
            ->communityMembershipService
            ->checkUserRights($user, $community, CommunityPermissions::MANAGE_MEMBERSHIP);

        $role = new CommunityRole();
        $form = $this->createForm(CommunityRoleType::class, $role);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $role->setCommunity($community);
        $role->setDefaultCreator(false);
        $role->setGlobal(false);
        $role->setHash(
            HashGeneratorService::generateHash($em->getRepository('App:CommunityRole'))
        );

        if ($role->isDefaultMember()) {
            $this->removePreviousDefaultRole($role->getCommunity());
        }

        $em->persist($role);
        $em->flush();

        return $this->view($role, Response::HTTP_CREATED);
    }

    /**
     * Add a new role that can be used in all communities.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the newly created global role",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Rest\Post("/communities/roles")
     *
     * @param Request $request
     *
     * @return View
     * @throws Exception
     */

    public function postCommunitiesRoleAction(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();
        $role = new CommunityRole();
        $form = $this->createForm(CommunityRoleType::class, $role);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }
        $role->setHash(
            HashGeneratorService::generateHash($em->getRepository('App:CommunityRole'))
        );

        $em->persist($role);
        $em->flush();

        return $this->view($role, Response::HTTP_OK);
    }

    /**
     * Updates an existing role of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated role",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Put("/communities/roles/{hash}")
     *
     * @param Request $request
     * @param $hash
     *
     * @return View
     */

    public function putCommunityRoleAction(Request $request, $hash): View
    {
        $role = $this->tryToGetRole($hash);
        /** @var User $user */
        $user = $this->getUser();

        /** @var CommunityMembership $userMembership */
        $userMembership = $this
            ->communityMembershipService
            ->getUserMembership($user, $role->getCommunity());

        if ($userMembership === null || !$userMembership->canManageRoles()) {
            throw new ForbiddenActionException();
        }

        $form = $this->createForm(CommunityRoleType::class, $role);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        if ($role->isDefaultMember()) {
            $this->removePreviousDefaultRole($role->getCommunity());
        }

        $this->getDoctrine()->getManager()->flush();
        return $this->view($role, Response::HTTP_OK);
    }

    /**
     * Deletes an existing role of a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the role was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Delete("/communities/roles/{hash}")
     *
     * @param $hash
     *
     * @return View
     */
    public function deleteCommunityRoleAction($hash): View
    {
        $role = $this->tryToGetRole($hash);

        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        if ($role->isGlobal() && $user->isAdmin()) {
            $em->remove($role);
            $em->flush();
            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        if (!$role->isGlobal()) {

            $userMembership = $this
                ->communityMembershipService
                ->getUserMembership($user, $role->getCommunity());

            /*
             * Checks if the user has the rights to delete the membership.
             */
            if ($userMembership !== null
                && in_array(
                    CommunityPermissions::MANAGE_ROLE,
                    $userMembership->getRole()->getPermissions(),
                    true
                )
            ) {

                $em->remove($role);
                $em->flush();

                return $this->view(null, Response::HTTP_NO_CONTENT);
            }
        }

        throw new ForbiddenActionException();
    }

    private function tryToGetRole(string $hash): CommunityRole
    {
        /** @var CommunityRole $role */
        $role = $this
            ->getDoctrine()
            ->getRepository('App:CommunityRole')
            ->findOneBy(array('hash' => $hash));

        if ($role === null) {
            throw new CommunityRoleNotFoundException($hash);
        }

        return $role;
    }

    private function removePreviousDefaultRole(Community $community) {
        $previousRole = $this
            ->getDoctrine()
            ->getRepository('App:CommunityRole')
            ->findOneBy(array(
                'community' => $community,
                'defaultMember' => true
            ));
        if ($previousRole != null) {
            $previousRole->setDefaultMember(false);
        }
    }
}
