<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 26/12/2017
 * Time: 21:10
 */

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class DefaultController extends AbstractFOSRestController
{
    private $platformName;
    private $termsOfServiceUrl;
    private $privacyNoticeUrl;
    private $openOrganization;

    public function __construct(string $platformName,
                                string $termsOfServiceUrl,
                                string $privacyNoticeUrl,
                                string $isOrganizationOpen)
    {
        $this->platformName = $platformName;
        $this->termsOfServiceUrl = $termsOfServiceUrl;
        $this->privacyNoticeUrl = $privacyNoticeUrl;
        $this->openOrganization = $isOrganizationOpen;
    }

    /**
     * Basic method that handles GET '' requests.
     * Returns the name of the organization, as well as the API version and route for the documentation.
     *
     * @Rest\Get("/")
     * @Rest\View(statusCode=200)
     *
     * @return array
     */
    public function getAction(): array
    {
        return [
            'host'           => $this->platformName,
            'termsOfService' => $this->termsOfServiceUrl,
            'privacyNotice'  => $this->privacyNoticeUrl,
            'open'           => $this->openOrganization,
            'documentation'  => '/v1/doc',
            'version'        => '0.8.2'
        ];
    }

}
