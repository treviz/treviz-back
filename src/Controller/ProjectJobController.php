<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 11:30
 */

namespace App\Controller;

use App\Entity\Enums\ProjectPermissions;
use App\Entity\ProjectJob;
use App\Entity\User;
use App\Exception\JobNotFoundException;
use App\Form\ProjectJobType;
use App\Service\ProjectMembershipService;
use App\Service\ProjectService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;


/**
 * Class ProjectJobController
 * @package App\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 */
class ProjectJobController extends AbstractFOSRestController
{

    /** @var ProjectService $projetService */
    private $projetService;

    /** @var ProjectMembershipService $projetMembershipService */
    private $projetMembershipService;

    public function __construct(ProjectService $projetService, ProjectMembershipService $projetMembershipService)
    {
        $this->projetService = $projetService;
        $this->projetMembershipService = $projetMembershipService;
    }

    /**
     * Fetches the jobs of a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the jobs should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the jobs of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectJob::class, groups={"Default"})
     *     )
     * ),
     *
     * @SWG\Response(
     *     response=404,
     *     description="No project was found for the specified hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to fetch the jobs of this project",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/{hash}/jobs")
     * @Rest\View(serializerGroups={"Default", "jobs"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getProjectJobsAction($hash): View
    {
        $project = $this
            ->projetService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();
        $this->projetService
            ->checkProjectVisibleForUser($project, $user);

        return $this->view($project->getJobs(), Response::HTTP_OK);
    }

    /**
     * Fetches the jobs as specified by the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the jobs specified by the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectJob::class, groups={"Default", "jobs"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/jobs")
     * @Rest\QueryParam(name="holder", description="username of the user who currently has the job", nullable=true)
     * @Rest\QueryParam(name="candidate", description="username of a user who is candidate", nullable=true)
     * @Rest\QueryParam(name="tags", description="tags the job must have", nullable=true)
     * @Rest\QueryParam(name="skills", description="skills required for the job", nullable=true)
     * @Rest\QueryParam(name="project", description="project the job must be linked to", nullable=true)
     * @Rest\QueryParam(name="task", description="hash of the task the job must be linked to", nullable=true)
     * @Rest\QueryParam(name="attributed", description="Is the job attributed or not. Defaults to 0.", nullable=true)
     * @Rest\QueryParam(name="name", description="name of the job", nullable=true)
     * @Rest\QueryParam(name="nb", description="Number of results to fetch", nullable=true)
     * @Rest\QueryParam(name="offset", description="Pagination", nullable=true)
     *
     * @Rest\View(serializerGroups={"Default", "jobs"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getProjectsJobsAction(ParamFetcher $paramFetcher): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $jobs = $this
            ->getDoctrine()
            ->getRepository('App:ProjectJob')
            ->searchJobs(
                $user,
                $paramFetcher->get('holder'),
                $paramFetcher->get('name'),
                $paramFetcher->get('candidate'),
                $paramFetcher->get('project'),
                $paramFetcher->get('tags'),
                $paramFetcher->get('skills'),
                $paramFetcher->get('task'),
                $paramFetcher->get('attributed'),
                $paramFetcher->get('nb') ?? 20,
                $paramFetcher->get('offset') ?? 0
            );

        return $this->view($jobs, Response::HTTP_OK);
    }


    /**
     * Fetches a job identified by its hash.
     *
     * @SWG\Response(
     *     response=200,
     *     description="The job was successfully fetched",
     *     @Model(type=ProjectJob::class, groups={"Default", "job"})
     * ),
     *
     * @SWG\Response(
     *     response=404,
     *     description="No project was found for the specified hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to fetch this job",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/jobs/{hash}")
     * @Rest\View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getProjectJobAction($hash): View
    {
        $job = $this->tryToGetJobAndCheckRights($hash);
        return $this->view($job, Response::HTTP_OK);
    }

    /**
     *
     * @SWG\Parameter(
     *     name="Job",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the job to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the job"
     *         ),
     *         @SWG\Property(
     *             property="contact",
     *             type="string",
     *             description="Username of the contact for the job"
     *         ),
     *         @SWG\Property(
     *             property="holder",
     *             type="string",
     *             description="Username of the user who has the job"
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         )
     *     ),
     *     required=true,
     *     description="Job that should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to which the job should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created job",
     *     @Model(type=ProjectJob::class, groups={"Default", "job"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to create the job",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project was not found",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="The submitted data is incorrect",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/projects/{hash}/jobs")
     * @Rest\View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postProjectJobAction(Request $request, $hash): View
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $project = $this
            ->projetService
            ->tryToFetchProject($hash);

        $this
            ->projetMembershipService
            ->checkUserRights($currentUser, $project, ProjectPermissions::MANAGE_JOBS);

        $job = new ProjectJob();
        $form = $this->createForm(ProjectJobType::class, $job);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $job->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:ProjectJob')
            )
        );
        $job->setProject($project);
        $em->persist($job);
        $em->flush();

        return $this->view($job, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing job of a project.
     *
     * @SWG\Parameter(
     *     name="Job",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the job to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the job"
     *         ),
     *         @SWG\Property(
     *             property="contact",
     *             type="string",
     *             description="Username of the contact for the job"
     *         ),
     *         @SWG\Property(
     *             property="holder",
     *             type="string",
     *             description="Username of the user who has the job"
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         )
     *     ),
     *     required=true,
     *     description="Job that should be updated"
     * )
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the job to update"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The job was successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The job could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to update the job",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/projects/jobs/{hash}")
     *
     * @Rest\View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putProjectJobAction(Request $request, $hash): View
    {
        $job = $this->tryToGetJobAndCheckRights($hash, true);
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ProjectJobType::class, $job);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /*
         * The job may have various candidacies.
         * If the job has been attributed, remove all of them.
         */
        if ($job->getHolder() != null) {
            foreach ($job->getApplications() as $application) {
                if ($application !== null) {
                    $job->removeApplication($application);
                    $em->remove($application);
                }
            }
        }

        $em->flush();
        return $this->view($job, Response::HTTP_OK);
    }

    /**
     * Removes a job.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the job to delete"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the job is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The job could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to delete the job",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Delete("/projects/jobs/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteProjectJobAction($hash): View
    {
        $job = $this
            ->tryToGetJobAndCheckRights($hash, true);

        $em = $this->getDoctrine()->getManager();
        $em->remove($job);
        $em->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Quits a job
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the job to quit"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the user has successfully quit the job",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The job could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to quit the job",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/projects/jobs/{hash}/quit")
     *
     * @param $hash
     * @return View
     */
    public function quitJobAction($hash)
    {
        $job = $this->tryToGetJobAndCheckRights($hash, false);

        if ($this->getUser() !== $job->getHolder()) {
            throw new AccessDeniedHttpException('You cannot quit a job you do not hold');
        }

        $job->setHolder(null);
        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetJobAndCheckRights(string $hash, bool $write = false): ProjectJob
    {
        /** @var ProjectJob $job */
        $job = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('App:ProjectJob')
            ->findOneBy(array(
                'hash' => $hash
            ));

        if (null === $job) {
            throw new JobNotFoundException($hash);
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if ($write) {
            if ($job->getContact() !== $currentUser) {
                $this
                    ->projetMembershipService
                    ->checkUserRights(
                        $currentUser,
                        $job->getProject(),
                        ProjectPermissions::MANAGE_JOBS
                    );
            }
        } else {
            if ($job->getHolder() !== $currentUser && $job->getContact() !== $currentUser) {
                $this->projetService
                    ->checkProjectVisibleForUser($job->getProject(), $currentUser);
            }
        }

        return $job;
    }

}
