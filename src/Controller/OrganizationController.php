<?php

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\User;
use App\Exception\OrganizationAlreadyExistsException;
use App\Exception\OrganizationNotFoundException;
use App\Form\OrganizationType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Organization controller.
 *
 * @Rest\Route("/v1")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class OrganizationController extends AbstractFOSRestController
{
    /**
     * Lists all organization entities.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user successfully fetches all the organizations",
     * )
     *
     * @SWG\Tag(name="organizations")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Get("/organizations")
     * @Rest\View(serializerGroups={"Default", "organizations"},serializerEnableMaxDepthChecks=true)
     */
    public function getOrganizationsAction(): View
    {
        $organizations = $this
            ->getDoctrine()
            ->getRepository('App:Organization')
            ->findAll();

        return $this->view($organizations, Response::HTTP_OK);
    }

    /**
     * Creates a new organization entity.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when the organization is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Returned when the users does not have the rights to create the organization",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Returned when the submitted form is incorrect",
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Returned when the organization that was to be created already exists",
     * )
     *
     * @SWG\Tag(name="organizations")
     *
     * @Rest\Post("/organizations")
     *
     * @param Request $request
     * @return View
     */
    public function postOrganizationAction(Request $request): View
    {
        $organization = new Organization();
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $existingOrganization = $em
            ->getRepository('App:Organization')
            ->findOneBy(array(
                'name' => $organization->getName()
            ));

        if (null !== $existingOrganization) {
            throw new OrganizationAlreadyExistsException();
        }

        $em->persist($organization);
        $em->flush();

        return $this->view($organization, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing organization
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when the organization is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Returned when the users does not have the rights to update the organization",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Returned when the submitted form is incorrect",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Returned when the organization to update does not exist",
     * )
     *
     * @Rest\Put("/organizations/{name}")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param $name
     * @return View
     */
    public function putOrganizationAction(Request $request, $name): View
    {
        $organization = $this->tryToGetOrganization($name);

        $form = $this->createForm(OrganizationType::class, $organization);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($organization);
        $em->flush();

        return $this->view($organization, Response::HTTP_OK);
    }

    /**
     * Deletes a organization entity.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the organization is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Returned when the users does not have the rights to delete the organization",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Returned when the organization to delete does not exist",
     * )
     *
     * @Rest\Delete("/organizations/{name}")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param $name
     * @return View
     */
    public function deleteOrganizationAction($name): View
    {
        $organization = $this->tryToGetOrganization($name);

        /** @var User $user */
        foreach ($organization->getUsers() as $user) {
            $user->setOrganization(null);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($organization);
        $em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetOrganization(string $name): Organization
    {
        /** @var Organization $organization */
        $organization = $this
            ->getDoctrine()
            ->getRepository('App:Organization')
            ->findOneBy(array('name' => $name));

        if (null === $organization) {
            throw new OrganizationNotFoundException($name);
        }

        return $organization;
    }

}
