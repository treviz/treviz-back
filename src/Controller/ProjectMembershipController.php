<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 11:30
 */

namespace App\Controller;

use App\Entity\Enums\ProjectPermissions;
use App\Entity\ProjectMembership;
use App\Entity\User;
use App\Exception\ProjectMembershipNotFoundException;
use App\Form\ProjectMembershipType;
use App\Service\ProjectMembershipService;
use App\Service\ProjectService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;


/**
 * Class ProjectMembershipController
 * @package App\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 */
class ProjectMembershipController extends AbstractFOSRestController
{

    /** @var ProjectService $projetService */
    private $projetService;

    /** @var ProjectMembershipService $projetMembershipService */
    private $projetMembershipService;

    public function __construct(ProjectService $projetService, ProjectMembershipService $projetMembershipService)
    {
        $this->projetService = $projetService;
        $this->projetMembershipService = $projetMembershipService;
    }

    /**
     * Fetches the memberships of a project.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships of a specified project",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/{hash}/memberships")
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getProjectMembershipsAction($hash): View
    {
        $project = $this
            ->projetService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();
        $this
            ->projetService
            ->checkProjectVisibleForUser($project, $user);

        return $this->view($project->getMemberships(), Response::HTTP_OK);
    }

    /**
     * Fetches the projects of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships specified by the query",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/memberships")
     * @Rest\QueryParam(name="user", description="username of the user from whom fetch memberships", nullable=false)
     * @Rest\View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getProjectsMembershipsAction(ParamFetcher $paramFetcher): View
    {
        $entityManager = $this->getDoctrine()->getManager();

        $user = $entityManager
            ->getRepository('App:User')
            ->findOneBy(array('username' => $paramFetcher->get('user')));

        if (null === $this->getUser()) {
            return $this->view(null, Response::HTTP_OK);
        }

        $currentUser = $this->getUser();

        if ($user === $currentUser) {
            return $this->view($user->getProjectsMemberships(), Response::HTTP_OK);
        }

        $memberships = $entityManager
            ->getRepository('App:ProjectMembership')
            ->searchMembershipsByUser($user, $currentUser);

        return $this->view($memberships, Response::HTTP_OK);
    }

    /**
     * Updates an existing membership of a project.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned the memberships was successfully updated",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/projects/memberships/{hash}")
     *
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putProjectMembershipAction(Request $request, $hash): View
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $currentMembership = $this
            ->tryToGetMembership($hash);

        $project = $currentMembership->getProject();

        $this
            ->projetMembershipService
            ->checkUserRights($currentUser, $project, ProjectPermissions::MANAGE_MEMBERSHIP);

        $form = $this->createForm(ProjectMembershipType::class, $currentMembership);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this
            ->getDoctrine()
            ->getManager()
            ->flush();

        return $this->view($currentMembership, Response::HTTP_OK);
    }

    /**
     * Removes a user from a project team.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the membership is successfully deleted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Delete("/projects/memberships/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteProjectMembershipAction($hash): View
    {
        $membership = $this->tryToGetMembership($hash);

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if ($membership->getUser() !== $currentUser) {
            $this
                ->projetMembershipService
                ->checkUserRights($currentUser, $membership->getProject(), ProjectPermissions::MANAGE_MEMBERSHIP);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($membership);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetMembership($hash): ProjectMembership
    {
        /** @var ProjectMembership $membership */
        $membership = $this
            ->getDoctrine()
            ->getRepository('App:ProjectMembership')
            ->findOneBy(array('hash' => $hash));

        if (null === $membership) {
            throw new ProjectMembershipNotFoundException($hash);
        }

        return $membership;
    }

}
