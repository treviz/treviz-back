<?php

namespace App\Controller;

use App\Entity\CommunityMembership;
use App\Entity\Document;
use App\Entity\ProjectMembership;
use App\Entity\User;
use App\EventDispatcher\Dispatcher;
use App\Events\DocumentCreatedEvent;
use App\Exception\DocumentNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Form\DocumentType;
use App\Message\DocumentCreatedMessage;
use App\Repository\DocumentRepository;
use App\Utils\HashGeneratorService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class DocumentController
 * @package Treviz\DocumentBundle\Controller
 *
 * @Rest\Route("/v1/documents")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class DocumentController extends AbstractFOSRestController
{
    private $kernelProjectDir;
    private $uploadFiles;
    private $backendUrl;
    /** @var MessageBusInterface */
    private $messageBus;

    /**
     * DocumentController constructor.
     * @param string $kernelProjectDir
     * @param string $uploadFiles
     * @param string $backendUrl
     * @param MessageBusInterface $messageBus
     */
    public function __construct(string $kernelProjectDir, string $uploadFiles, string $backendUrl, MessageBusInterface $messageBus)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        $this->uploadFiles = $uploadFiles;
        $this->backendUrl = $backendUrl;
        $this->messageBus = $messageBus;
    }


    /**
     * Fetches a document according to its hash.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the document identified by its hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Returned when the user does not have the rights to fetch the document",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @Rest\Get("/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getDocumentAction($hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Document $doc */
        $doc = $entityManager
            ->getRepository('App:Document')
            ->findOneBy(array('hash' => $hash));

        if (null === $doc) {
            throw new DocumentNotFoundException($hash);
        }

        if (!$this->canUserAccessDocument($doc)) {
            throw new ForbiddenActionException();
        }

        return $this->view($doc, Response::HTTP_OK);
    }

    /**
     * Fetches documents according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of documents matching the query parameter",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @Rest\Get("")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @Rest\QueryParam(name="project", description="hash of the project to which the documents belong")
     * @Rest\QueryParam(name="community", description="hash of the community to which the documents belong")
     * @Rest\QueryParam(name="idea", description="hash of the idea to which the documents belong")
     * @Rest\QueryParam(name="room", description="hash of the room to which the documents belong")
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getDocumentsAction(ParamFetcher $paramFetcher): View
    {
        /** @var DocumentRepository $repository */
        $repository = $this
            ->getDoctrine()
            ->getRepository('App:Document');

        /** @var User $user */
        $user = $this->getUser();
        $documents = $repository
            ->searchDocument(
                $paramFetcher->get('project'),
                $paramFetcher->get('community'),
                $paramFetcher->get('idea'),
                $paramFetcher->get('room'),
                $user
            );

        return $this->view($documents, Response::HTTP_OK);

    }

    /**
     * Creates a new document.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created document",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Returned when the user does not have the rights to create the document",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returned when no community, project, idea or room was specified",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Returned when the form is not valid",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @Rest\Post("")
     *
     * @param Request $request
     * @return View
     */
    public function postDocumentAction(Request $request): View
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /** @var File $file */
        $file = $request->files->get('file');

        $fileExtension = $file->guessExtension();
        $fileHash = HashGeneratorService::generateFileHashName($file);
        $fileName = $fileHash . '.' . $fileExtension;

        $file->move(
            $this->kernelProjectDir .
            '/public' .
            $this->uploadFiles,
            $fileName
        );

        $document->setFileUrl(
            $this->backendUrl .
            $this->uploadFiles .
            $fileName
        );
        $document->setExtension($fileExtension);
        $document->setHash($fileHash);

        /** @var User $user */
        $user = $this->getUser();

        if (null === $document->getProject() &&
            null === $document->getCommunity() &&
            null === $document->getIdea() &&
            null === $document->getRoom()) {
            throw new BadRequestHttpException('You must specify a project, community, idea or room number');
        }

        /*
         * Checks if the user is part of the project or community.
         */
        if (!$this->canUserAccessDocument($document, true)) {
            throw new ForbiddenActionException();
        }

        $document->setOwner($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($document);
        $entityManager->flush();

        $this->messageBus->dispatch(new DocumentCreatedMessage($document->getId()));
        return $this->view($document, Response::HTTP_CREATED);
    }

    /**
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the document was successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="Returned when the user does not have the rights to delete the document",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Returned when the document does not exist",
     * )
     *
     * @Rest\Delete("/{hash}")
     *
     * @SWG\Tag(name="documents")
     *
     * @param $hash
     * @return View
     */
    public function deleteDocumentAction($hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Document $document */
        $document = $entityManager
            ->getRepository('App:Document')
            ->findOneBy(array('hash' => $hash));

        if (null === $document) {
            throw new DocumentNotFoundException($hash);
        }

        if (!$this->canUserAccessDocument($document, true, true)) {
            throw new ForbiddenActionException();
        }

        $entityManager->remove($document);
        $entityManager->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function canUserAccessDocument(Document $doc, bool $write = false, bool $checkAdminRights = false): bool {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (!$doc->getOwner() !== $currentUser) {
            if ($project = $doc->getProject()) {
                if ($write || !$project->isVisible()) {
                    /** @var ProjectMembership $membership */
                    $membership = $entityManager
                        ->getRepository('App:ProjectMembership')
                        ->findOneBy(array(
                            'project' => $project,
                            'user' => $currentUser
                        ));

                    if (null === $membership || ($checkAdminRights && !$membership->canManageDocuments())) {
                       return false;
                    }
                }
            } elseif ($community = $doc->getRelatedCommunity()) {
                if ($write || !$community->isVisible()) {
                    /** @var CommunityMembership $membership */
                    $membership = $entityManager
                        ->getRepository('App:CommunityMembership')
                        ->findOneBy(array(
                            'community' => $community,
                            'user' => $currentUser
                        ));
                    if (null === $membership || ($checkAdminRights && !$membership->canManageDocuments())){
                        return false;
                    }
                }
            } elseif ($room = $doc->getRoom()) {
                if (!$currentUser->isRoomMember($room)) {
                    return false;
                }
            }
        }

        return true;
    }

}
