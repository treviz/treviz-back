<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Exception\TagNotFoundException;
use App\Form\TagType;
use App\Repository\TagRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class TagController
 * @package App\Controller
 *
 * @Rest\Route("/v1/tags")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class TagController extends AbstractFOSRestController
{
    /**
     * Fetches all existing tags, or the ones matching the query string.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of tags, matching the query string",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Tag::class)
     *     )
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Rest\Get("")
     * @Rest\QueryParam(name="name", description="Name, or part of the name of the tags to fetch", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getTagsAction(ParamFetcher $paramFetcher): View
    {
        /** @var TagRepository $repository */
        $repository = $this
            ->getDoctrine()
            ->getRepository('App:Tag');

        return $this->view(
            $repository->searchTagByName($paramFetcher->get('name')),
            Response::HTTP_OK
        );
    }

    /**
     * Creates a new tag
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created tag",
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Tag already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Tag to create",
     *     required=true,
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Rest\Post("")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @return View
     */
    public function postTagsAction(Request $request): View
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->throwErrorIfConflict($tag->getName());
        $em = $this->getDoctrine()->getManager();
        $em->persist($tag);
        $em->flush();

        return $this->view($tag, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing tag
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated tag",
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Tag does not exist"
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Tag already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Updated Tag",
     *     required=true,
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the tag to update"
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Rest\Put("/{name}")
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param $name
     * @return View
     */
    public function putTagAction(Request $request, $name): View
    {
        $tag = $this->tryToGetTag($name);
        $form = $this->createForm(TagType::class, $tag);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->throwErrorIfConflict($tag->getName());
        $this->getDoctrine()->getManager()->flush();
        return $this->view($tag, Response::HTTP_OK);
    }

    /**
     * Deletes a tag
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the tag was correctly deleted"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Tag does not exist"
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the tag to delete"
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Rest\Delete("/{name}")
     *
     * @param $name
     * @return View
     */
    public function deleteTagAction($name): View
    {
        $tag = $this->tryToGetTag($name);
        $em = $this->getDoctrine()->getManager();
        $em->remove($tag);
        $em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetTag($name): Tag
    {
        /** @var Tag $tag */
        $tag = $this
            ->getDoctrine()
            ->getRepository('App:Tag')
            ->findOneBy(array(
                'name' => $name
            ));

        if (null === $tag) {
            throw new TagNotFoundException($name);
        }

        return $tag;
    }

    /**
     * Checks if a skill already exists with this name and throws an error if it is the case
     * @param $name
     */
    private function throwErrorIfConflict($name): void {
        if (null !== $this
                ->getDoctrine()
                ->getRepository('App:Tag')
                ->findOneBy(array('name' => $name))) {
            throw new ConflictHttpException();
        }
    }

}
