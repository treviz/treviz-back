<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 13/12/2017
 * Time: 18:03
 */

namespace App\Controller;


use App\Entity\Feedback;
use App\Entity\Task;
use App\Exception\TaskNotFoundException;
use App\Form\FeedbackType;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class FeedbackController
 * @package App\Controller
 *
 * @Rest\Route("/v1")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class KanbanFeedbackController extends AbstractFOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return an array of feedback.",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\QueryParam(name="given", description="If true, fetch the feedback that was given; otherwise fetch the received feedback", nullable=true)
     * @Rest\QueryParam(name="task", description="Hash of the task from which the feedback should be fetched", nullable=true)
     *
     * @Rest\Get("/boards/columns/tasks/feedback")
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getFeedbackAction(ParamFetcher $paramFetcher): View
    {
        $feedback = $this
            ->getDoctrine()
            ->getRepository('App:Feedback')
            ->searchFeedback(
                $this->getUser(),
                $paramFetcher->get('given') === 'true',
                $paramFetcher->get('task')
            );

        return $this->view($feedback, Response::HTTP_OK);
    }

    /**
     * Post a feedback for a task.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Feedback was successfully given.",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The current user cannot submit feedback for this task.",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Post("/boards/columns/tasks/{hash}/feedback")
     *
     * @param Request $request
     * @param string  $hash Hash of the task to which add feedback.
     *
     * @return View
     * @throws Exception
     */
    public function postFeedbackAction(Request $request, $hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Task $task */
        $task = $entityManager
            ->getRepository('App:Task')
            ->findOneBy(array('hash' => $hash));

        if (null === $task) {
            throw new TaskNotFoundException($hash);
        }

        $currentUser = $this->getUser();

        if ($currentUser !== $task->getSupervisor()) {
            throw new AccessDeniedHttpException('You cannot post feedback for a task you do not supervise');
        }

        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $feedback->setTask($task);
        $feedback->setGiver($currentUser);
        $feedback->setReceiver($task->getAssignee());

        $feedback->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:Feedback')
            )
        );
        $entityManager->persist($feedback);
        $entityManager->flush();

        return $this->view($feedback, Response::HTTP_CREATED);
    }

}
