<?php

namespace App\Controller;

use App\Entity\Enums\ProjectPermissions;
use App\Entity\Task;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\TaskNotFoundException;
use App\Form\TaskType;
use App\Message\TaskApprovedMessage;
use App\Message\TaskAssignedMessage;
use App\Message\TaskCreatedMessage;
use App\Message\TaskRejectedMessage;
use App\Message\TaskSubmittedMessage;
use App\Repository\TaskRepository;
use App\Service\ColumnService;
use App\Service\ProjectMembershipService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Task controller.
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 */
class KanbanTaskController extends AbstractFOSRestController
{
    /** @var MessageBusInterface $messageBus */
    private $messageBus;
    /** @var ColumnService $columnService */
    private $columnService;
    /** @var ProjectMembershipService $projectMembershipService */
    private $projectMembershipService;

    /**
     * TaskController constructor.
     *
     * @param MessageBusInterface $messageBus
     * @param ColumnService $columnService
     * @param ProjectMembershipService $projectMembershipService
     */
    public function __construct(
        MessageBusInterface $messageBus,
        ColumnService $columnService,
        ProjectMembershipService $projectMembershipService
    )
    {
        $this->messageBus = $messageBus;
        $this->columnService = $columnService;
        $this->projectMembershipService = $projectMembershipService;
    }


    /**
     * Lists all tasks of a column.
     * By default, the archived tasks are not sent back, but it can be changed with the query parameter ?show_archived.
     *
     * @QueryParam(name="show_archived", description="List archived tasks alongside pending ones", nullable=true)
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of tasks matching the query",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No column is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the column's tasks",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards/columns/{hash}/tasks")
     *
     * @param string $hash Hash of the column from which fetch the task.
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getColumnTasksAction(string $hash, ParamFetcher $paramFetcher): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $column = $this
            ->columnService
            ->tryToGetColumnAndCheckAccess($hash, $user, false);

        /** @var TaskRepository $repository */
        $repository = $this
            ->getDoctrine()
            ->getRepository('App:Task');

        $tasks = $paramFetcher->get('show_archived') ?
            $repository->findTasksByColumn($column) :
            $repository->findNotArchivedTasksByColumn($column);

        return $this->view($tasks, Response::HTTP_OK);
    }

    /**
     * Fetches tasks according to a query.
     *
     * @Rest\Get("/boards/columns/tasks")
     *
     * @Rest\QueryParam(name="assignee", description="Username of the user to whom the task must be assigned", nullable=true)
     * @Rest\QueryParam(name="supervisor", description="Username of the user who supervises the task", nullable=true)
     * @Rest\QueryParam(name="to_approve", description="True if the task is waiting approval by the current user", nullable=true)
     *
     * @Rest\View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of tasks matching the query",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the column's tasks",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getTasksAction(ParamFetcher $paramFetcher): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $tasks = $this
            ->getDoctrine()
            ->getRepository('App:Task')
            ->findTasks(
                $paramFetcher->get('assignee'),
                $paramFetcher->get('supervisor'),
                $paramFetcher->get('to_approve'),
                $user
            );

        return $this->view($tasks, Response::HTTP_OK);
    }

    /**
     * Finds and returns a task.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Task is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the task",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards/columns/tasks/{hash}")
     *
     * @Rest\View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getTaskAction($hash): View
    {
        return $this->view(
            $this->tryToGetTaskAndCheckAccess($hash),
            Response::HTTP_OK
        );
    }


    /**
     * Creates a new task.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Task is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the task",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Post("/boards/columns/{hash}/tasks")
     *
     * @Rest\View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     * @throws Exception
     */
    public function postTaskAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $column = $this
            ->columnService
            ->tryToGetColumnAndCheckAccess(
                $hash,
                $user,
                false
            );

        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $task->setColumn($column);
        $task->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:Task')
            )
        );

        /*
         * Checks if the assignee and supervisor are member of the project before persisting it;
         */
        $this->checkAssignee($task);
        $this->checkSupervisor($task);

        $em->persist($task);
        $em->flush();

        /*
         * Send notification to the members of the project
         */
        $this->messageBus->dispatch(new TaskCreatedMessage($task->getId()));

        return $this->view($task, Response::HTTP_CREATED);
    }


    /**
     * Edit an existing task.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Task is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to update the task",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Put("/boards/columns/tasks/{hash}")
     *
     * @Rest\View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putTaskAction(Request $request, $hash): View
    {
        $task = $this
            ->tryToGetTaskAndCheckAccess($hash);

        $em = $this
            ->getDoctrine()
            ->getManager();

        $initialSupervisor = $task->getSupervisor();
        $initialAssignee = $task->getAssignee();
        $isTaskToBeApproved = $task->isPendingApproval();
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $form = $this->createForm(TaskType::class, $task);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->checkUserBelongsToProject($task, $currentUser);
        $this->checkAssignee($task);
        $this->checkSupervisor($task);

        /*
         * Send notifications if necessary
         */
        if ($task->getAssignee() !== $initialAssignee) {
            $this->messageBus->dispatch(new TaskAssignedMessage($task->getId()));
        }

        if ($isTaskToBeApproved !== $task->isPendingApproval()) {
            if ($currentUser !== $initialSupervisor) {
                throw new AccessDeniedHttpException('Only the supervisor can approve or reject a task');
            }

            if (!$task->isPendingApproval()) {
                if ($task->isArchived()) {
                    $this->messageBus->dispatch(new TaskApprovedMessage($task->getId()));
                } else {
                    $this->messageBus->dispatch(new TaskRejectedMessage($task->getId()));
                }
            }
        }

        $em->flush();
        return $this->view($task, Response::HTTP_OK);
    }

    /**
     * Deletes a task entity.
     * Only a project administrator or the task's supervisor can delete it.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Task is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the task",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Delete("/boards/columns/tasks/{hash}")
     *
     * @Rest\View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function deleteColumnTaskAction(String $hash): View
    {
        $em = $this
            ->getDoctrine()
            ->getManager();

        $task = $this->tryToGetTaskAndCheckAccess($hash);
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($task->getSupervisor() !== $currentUser) {
            $this
                ->projectMembershipService
                ->checkUserRights(
                    $currentUser,
                    $task->getColumn()->getBoard()->getProject(),
                    ProjectPermissions::MANAGE_KANBAN
                );
        }

        $em->remove($task);
        $em->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Submits a task for approval.
     * Only the task's assignee can submit it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Task was successfully submitted for approval",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to submit the task for approval",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Post("/boards/columns/tasks/{hash}/submit")
     *
     * @Rest\View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function submitForApprovalAction($hash): View
    {
        $em = $this
            ->getDoctrine()
            ->getManager();

        $task = $this->tryToGetTaskAndCheckAccess($hash);
        if ($task->getAssignee() !== $this->getUser()) {
            throw new AccessDeniedHttpException(
                'You are not authorized to submit this task for approval'
            );
        }

        $task->setPendingApproval(true);
        $em->flush();

        /*
         * Send notifications
         */
        $this->messageBus->dispatch(new TaskSubmittedMessage($task->getId()));

        return $this->view($task, Response::HTTP_OK);
    }

    private function tryToGetTaskAndCheckAccess(string $hash): Task {
        /** @var Task $task */
        $task = $this
            ->getDoctrine()
            ->getRepository('App:Task')
            ->findOneBy(array('hash' => $hash));

        if (null === $task) {
            throw new TaskNotFoundException($hash);
        }

        /** @var User $user */
        $user = $this->getUser();
        $this->columnService
            ->checkUserCanAccessColumn(
                $task->getColumn(),
                $user,
                false
            );

        return $task;
    }

    private function checkAssignee(Task $task) {
        $assignee = $task->getAssignee();
        return null === $assignee ?: $this->checkUserBelongsToProject($task, $assignee);
    }

    private function checkSupervisor(Task $task) {
        if (null === $task->getSupervisor()) {
            /** @var User $user */
            $user = $this->getUser();
            $task->setSupervisor($user);
        }
        return $this->checkUserBelongsToProject($task, $task->getSupervisor());
    }

    private function checkUserBelongsToProject(Task $task, User $user) {
        $membership = $this
            ->projectMembershipService
            ->getUserMembership(
                $user,
                $task->getColumn()->getBoard()->getProject()
            );

        if (null === $membership) {
            throw new ForbiddenActionException();
        }
    }

}
