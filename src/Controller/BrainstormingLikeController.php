<?php

namespace App\Controller;

use App\Entity\Idea;
use App\Entity\User;
use App\Service\IdeaService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class LikeController
 * @package App\Controller
 */
class BrainstormingLikeController extends AbstractFOSRestController
{
    /** @var IdeaService */
    private $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    /**
     * Like an idea as a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when an idea was successfully liked by the user authenticated by the auth. token",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/ideas/{hash}/like")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function likeIdeaAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this->tryToGetLikableIdea($hash, $user, true);

        $idea->addLiked($user);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->view($idea, Response::HTTP_OK);
    }

    /**
     *
     * Stop liking an idea as a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user stopped liking an idea",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/ideas/{hash}/unlike")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function unlikeIdeaAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this->tryToGetLikableIdea($hash, $user, false);

        $idea->removeLiked($user);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->view($idea, Response::HTTP_OK);
    }

    private function tryToGetLikableIdea(string $hash, User $user, $like): Idea
    {
        $idea = $this
            ->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user, false, true);

        $userHasLikedIdea = $idea->getLiked()->contains($user);
        if (($like && $userHasLikedIdea) || (!$like && !$userHasLikedIdea)) {
            throw new ConflictHttpException();
        }

        return $idea;
    }

}
