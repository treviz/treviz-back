<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 11:36
 */

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProjectIllustrationController
 * @package App\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 */
class ProjectIllustrationController extends AbstractFOSRestController
{
    private $kernelProjectDir;
    private $baseProjects;
    private $backendUrl;

    /**
     * ProjectIllustrationController constructor.
     * @param $kernelProjectDir
     * @param $baseProjects
     * @param $backendUrl
     */
    public function __construct($kernelProjectDir, $baseProjects, $backendUrl)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        $this->baseProjects = $baseProjects;
        $this->backendUrl = $backendUrl;
    }


    /**
     * Return an array containing all the base illustration urls for projects.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned an array of the different pictures urls used to illustrate the projects",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(
     *             type="string"
     *         ),
     *         description="array of urls"
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/base-illustrations")
     */
    public function getProjectBaseIllustrationsAction(): View
    {
        $file_urls = [];
        $dir = $this->kernelProjectDir .
            '/public' .
            $this->baseProjects;
        $baseUrl = $this->backendUrl . $this->baseProjects;

        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != "." && $file != "..") {
                        $file_urls[] = $baseUrl . '/' .$file;
                    }
                }
            }
        }

        return $this->view($file_urls, Response::HTTP_OK);
    }
}
