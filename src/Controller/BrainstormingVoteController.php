<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 24/08/2018
 * Time: 21:44
 */

namespace App\Controller;


use App\Entity\Idea;
use App\Entity\User;
use App\Entity\Vote;
use App\Form\VoteType;
use App\Service\IdeaService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class VoteController
 * @package App\Controller
 */
class BrainstormingVoteController extends AbstractFOSRestController
{
    /** @var IdeaService */
    private $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    /**
     * Votes for an idea
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a vote was successfully casted",
     *     @Model(type=Idea::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Idea does not exist"
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="You do not have the rights to do this"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="score",
     *     in="body",
     *     description="Score given to the idea, between -1 and +1",
     *     required=true
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\View(serializerGroups={"Default", "idea"}, serializerEnableMaxDepthChecks=true)
     *
     * @Rest\Post("/brainstorming-sessions/ideas/{hash}/votes")
     * @param Request $request
     * @param $hash
     * @return View
     * @throws Exception
     */
    public function postVoteAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this
            ->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user, false, true);

        $em = $this->getDoctrine()->getManager();
        $vote = $em
            ->getRepository('App:Vote')
            ->findOneBy(array(
                'idea' => $idea,
                'user' => $user
            )) ?: new Vote();

        $form = $this->createForm(VoteType::class, $vote);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        if (null == $vote->getHash()) {
            $vote->setIdea($idea);
            $vote->setHash(
                HashGeneratorService::generateHash(
                    $em->getRepository('App:Vote')
                )
            );
        }
        $vote->setUser($user);

        $em->persist($vote);
        $em->flush();

        return $this->view($idea, Response::HTTP_CREATED);
    }

}
