<?php

namespace App\Controller;

use App\Entity\Skill;
use App\Exception\SkillNotFoundException;
use App\Form\SkillType;
use App\Repository\SkillRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class SkillController
 * @package Treviz\SkillBundle\Controller
 *
 * @Rest\Route("/v1/skills")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class SkillController extends AbstractFOSRestController
{
    /**
     * Fetches all existing skills, or the ones matching the query string.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of skills, matching the query string",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Skill::class)
     *     )
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Rest\Get("")
     * @Rest\QueryParam(name="name", description="Name, or part of the name of the skills to fetch", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getSkillsAction(ParamFetcher $paramFetcher): View
    {
        /** @var SkillRepository $repository */
        $repository = $this
            ->getDoctrine()
            ->getRepository('App:Skill');

        return $this->view(
            $repository->searchSkillsByName($paramFetcher->get('name')),
            Response::HTTP_OK
        );
    }

    /**
     * Creates a new skill
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created skill",
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Skill already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Skill to create",
     *     required=true,
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("")
     *
     * @param Request $request
     * @return View
     */
    public function postSkillsAction(Request $request): View
    {
        $skill = new Skill();
        $form = $this->createForm(SkillType::class, $skill);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->throwErrorIfConflict($skill->getName());

        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $entityManager->persist($skill);
        $entityManager->flush();

        return $this->view($skill, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing skill
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated skill",
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Skill does not exist"
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Skill already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Updated Skill",
     *     required=true,
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the skill to update"
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Rest\Put("/{name}")
     *
     * @param Request $request
     * @param $name
     * @return View
     */
    public function putSkillAction(Request $request, $name): View
    {
        $skill = $this->tryToGetSkill($name);
        $form = $this->createForm(SkillType::class, $skill);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->throwErrorIfConflict($skill->getName());
        $this->getDoctrine()->getManager()->flush();
        return $this->view($skill, Response::HTTP_OK);
    }

    /**
     * Deletes a skill.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the skill was correctly deleted"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Skill does not exist"
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the skill to delete"
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Rest\Delete("/{name}")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param $name
     * @return View
     */
    public function deleteSkillAction($name): View
    {
        $skill = $this->tryToGetSkill($name);
        $em = $this->getDoctrine()->getManager();
        $em->remove($skill);
        $em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Try to get a skill by its name, and throw an error if none could be found
     *
     * @param $name
     * @return Skill
     */
    private function tryToGetSkill($name): Skill {
        /** @var Skill $skill */
        $skill = $this
            ->getDoctrine()
            ->getRepository('App:Skill')
            ->findOneBy(array(
                'name' => $name
            ));

        if (null === $skill) {
            throw new SkillNotFoundException($name);
        }

        return $skill;
    }

    /**
     * Checks if a skill already exists with this name and throws an error if it is the case
     * @param $name
     */
    private function throwErrorIfConflict($name): void {
        if (null !== $this
                ->getDoctrine()
                ->getRepository('App:Skill')
                ->findOneBy(array('name' => $name))) {
            throw new ConflictHttpException();
        }
    }

}
