<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 01/11/2017
 * Time: 20:08
 */

namespace App\Controller;

use App\Entity\Session;
use App\Entity\User;
use App\Form\SessionType;
use App\Message\BrainstormingCreatedMessage;
use App\Repository\SessionRepository;
use App\Service\SessionService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class SessionController
 * @package App\Controller
 */
class BrainstormingSessionController extends AbstractFOSRestController
{
    /** @var SessionService */
    private $sessionService;
    /** @var MessageBusInterface */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus,
                                SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
        $this->messageBus = $messageBus;
    }

    /**
     * Fetches the brainstorming sessions of a project or community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of brainstorming sessiongs for the selected community",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions")
     *
     * @Rest\QueryParam(name="community", description="hash of the community from which fetch the brainstorming sessions")
     * @Rest\QueryParam(name="project", description="hash of the project from which fetch the brainstorming sessions")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $paramFetcher
     * @return View
     */
    public function getSessionsAction(ParamFetcher $paramFetcher): View
    {
        /** @var SessionRepository $repository */
        $repository = $this
            ->getDoctrine()
            ->getRepository('App:Session');

        /** @var User $user */
        $user = $this->getUser();
        return $this->view(
            $repository->searchSession(
                $user,
                $paramFetcher->get('community'),
                $paramFetcher->get('project')
            ),
            Response::HTTP_OK
        );
    }

    /**
     * Fetches a specific brainstorming session.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a session was successfully fetched",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getSessionAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $session = $this
            ->sessionService
            ->tryToGetSessionAndCheckRights($hash, $user);
        return $this->view($session, Response::HTTP_OK);
    }

    /**
     * Creates a new session, for a community or project.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a session was successfully created",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Post("/brainstorming-sessions")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function postSessionAction(Request $request): View
    {
        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /** @var User $user */
        $user = $this->getUser();
        $this
            ->sessionService
            ->checkUserCanWriteSession($session, $user);

        $em = $this->getDoctrine()->getManager();
        $session->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:Session')
            )
        );
        $em->persist($session);
        $em->flush();

        $event = new BrainstormingCreatedMessage($session->getId());
        $this
            ->messageBus
            ->dispatch($event);

        return $this->view($session, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing session, for instance to close it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a session was successfully updated",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Put("/brainstorming-sessions/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putSessionAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $session = $this
            ->sessionService
            ->tryToGetSessionAndCheckRights($hash, $user, true);

        $form = $this->createForm(SessionType::class, $session);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->view($session, Response::HTTP_OK);
    }

    /**
     * Removes an existing session.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a session was successfully deleted",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Delete("/brainstorming-sessions/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function deleteSessionAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $session = $this
            ->sessionService
            ->tryToGetSessionAndCheckRights($hash, $user, true);
        $em = $this->getDoctrine()->getManager();

        /*
         * Unlink all ideas that may have generated projects
         */
        foreach ($session->getIdeas() as $idea) {
            if ($idea->getForked()->count()) {
                $session->removeIdea($idea);
            } else {
                $em->remove($idea);
            }
        }
        $em->flush();

        $em->remove($session);
        $em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
