<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 11/02/2017
 * Time: 17:32
 */

namespace App\Controller;

use App\Entity\User;
use App\Exception\ExpiredTokenException;
use App\Exception\InvalidTokenException;
use App\Exception\UserAlreadyEnabledException;
use App\Exception\UserNotFoundException;
use App\Message\UserResetPasswordMessage;
use App\Service\AccountService;
use DateInterval;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class AccountController
 *
 * @Rest\Route("/v1")
 *
 * @package App\Controller
 */
class AccountController extends AbstractFOSRestController
{

    private $frontendUrl;
    private $tokenGenerator;
    private $accountService;
    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    public function __construct(
        String $frontendUrl,
        TokenGeneratorInterface $tokenGenerator,
        AccountService $accountService,
        MessageBusInterface $messageBus
    )
    {
        $this->frontendUrl = $frontendUrl;
        $this->tokenGenerator = $tokenGenerator;
        $this->accountService = $accountService;
        $this->messageBus = $messageBus;
    }

    /**
     * Confirms the registration of a user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user was successfully enabled",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Post("/users/{username}/confirm")
     * @Rest\RequestParam(name="token", nullable=false)
     *
     * @param ParamFetcher $paramFetcher
     * @param              $username
     *
     * @return View
     * @throws Exception
     */
    public function postConfirmationAction(ParamFetcher $paramFetcher, $username): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $token = $paramFetcher->get('token');

        /** @var User $user */
        $user = $entityManager
            ->getRepository('App:User')
            ->findOneBy(array('username' => $username));

        if (null === $user) {
            throw new UserNotFoundException($username);
        }

        if ($user->isEnabled()) {
            throw new UserAlreadyEnabledException();
        }

        if ($token !== $user->getConfirmationToken()) {
            throw new InvalidTokenException();
        }

        $now = new DateTime();

        /*
         * If account was generated more than a day ago, ask the user to change his/her password.
         */
        $passwordRequestTime = $user->getPasswordRequestedAt();
        if (null === $passwordRequestTime || $passwordRequestTime->add(new DateInterval('P1D')) < $now) {
            throw new ExpiredTokenException();
        }

        $user->setEnabled(true);
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $entityManager->flush();

        return $this->view('User successfully enabled', Response::HTTP_OK);
    }

    /**
     * Sends an email to retrieve a password, resets the confirmation token.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user successfully performed a reset password action",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Get("/reset-password")
     * @Rest\QueryParam(name="email", nullable=false)
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws Exception
     */
    public function getResetPasswordAction(ParamFetcher $paramFetcher): Response
    {
        $email = $paramFetcher->get('email');

        $user = $this
            ->getDoctrine()
            ->getRepository('App:User')
            ->findOneBy(['email' => $email]);

        if (null === $user) {
            throw new UserNotFoundException($email);
        }

        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $user->setPasswordRequestedAt(new DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        $this
            ->messageBus
            ->dispatch(new UserResetPasswordMessage($user->getId()));

        $view = $this->view(json_encode(['result' => 'Email sent']), Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Resets the password of a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user successfully reset his or her password",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Post("/users/{username}/reset")
     * @Rest\RequestParam(name="token", nullable=false)
     * @Rest\RequestParam(name="password", nullable=false)
     *
     * @param ParamFetcher $paramFetcher
     * @param              $username
     *
     * @return View
     * @throws Exception
     */
    public function postResetPasswordAction(ParamFetcher $paramFetcher, $username): View
    {
        $token = $paramFetcher->get('token');
        $password = $paramFetcher->get('password');

        $user = $this
            ->getDoctrine()
            ->getRepository('App:User')
            ->findOneBy(['username' => $username]);

        if (null === $user) {
            throw new UserNotFoundException($username);
        }

        /** @var DateTime $requestExpireAt */
        $requestExpireAt = $user->getPasswordRequestedAt();

        if (null === $requestExpireAt ||
            $requestExpireAt->add(new DateInterval('PT3H')) <= new DateTime()) {
            throw new ExpiredTokenException();
        }

        if ($token !== $user->getConfirmationToken()) {
            throw new InvalidTokenException();
        }

        $user->setPlainPassword($password);
        $user->setEnabled(true);

        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->view('Password successfully changed', Response::HTTP_OK);
    }


    /**
     * Returns all the personal data of a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user successfully fetches his or her information",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Get("/users/{username}/archive")
     * @Rest\View(serializerEnableMaxDepthChecks=true, serializerGroups={"Default", "user"})
     *
     * @param              $username
     *
     * @return View
     * @throws Exception
     */
    public function getAccountArchiveAction($username): ?View
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if ($currentUser->getUsername() !== $username) {
            throw new AccessDeniedHttpException(
                'You cannot request an archive for an other account than yours'
            );
        }
        $userInfo = $this
            ->accountService
            ->getUserInfo($currentUser);
        return $this->view($userInfo, Response::HTTP_OK);
    }

}
