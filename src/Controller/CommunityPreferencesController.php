<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:33
 */

namespace App\Controller;


use App\Entity\CommunityMembership;
use App\Entity\User;
use App\Exception\CommunityMembershipNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Form\CommunityNotificationPreferencesType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class CommunityPreferencesController
 * @package App\Controller
 *
 * @Rest\Route("/v1")
 */
class CommunityPreferencesController extends AbstractFOSRestController
{

    /**
     * @Rest\Get("/communities/memberships/{hash}/preferences")
     *
     * @SWG\Tag(name="community")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the notification preferences of a user regarding a community",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has not the rights to perform this action",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No community membership could be found for this membership",
     * )
     *
     * @param string $hash
     * @return View
     */
    public function getCommunityPreferencesAction(string $hash): View
    {
        $membership = $this->tryToGetCommunityMembership($hash);

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($membership->getUser() !== $currentUser) {
            throw new ForbiddenActionException();
        }

        return $this->view($membership->getPreferences(), Response::HTTP_OK);
    }

    /**
     * Updates the preferences of a community membership.
     *
     * @Rest\Put("/communities/memberships/{hash}/preferences")
     *
     * @SWG\Tag(name="community")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the notification preferences of a user regarding a community",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has not the rights to perform this action",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No community membership could be found for this membership",
     * )
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     */
    public function updateCommunityMembershipPreferencesAction(Request $request, string $hash): View
    {
        $membership = $this->tryToGetCommunityMembership($hash);

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($membership->getUser() !== $currentUser) {
            throw new ForbiddenActionException();
        }

        $preferences = $membership->getPreferences();
        $form = $this->createForm(CommunityNotificationPreferencesType::class, $preferences);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return $this->view($preferences, Response::HTTP_OK);

    }

    private function tryToGetCommunityMembership(string $hash): CommunityMembership
    {
        /** @var CommunityMembership $membership */
        $membership = $this
            ->getDoctrine()
            ->getRepository('App:CommunityMembership')
            ->findOneBy(
                array('hash' => $hash)
            );

        if (null === $membership) {
            throw new CommunityMembershipNotFoundException($hash);
        }
        return $membership;
    }

}
