<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace App\Controller;

use App\Entity\CommunityMembership;
use App\Entity\CommunityRole;
use App\Entity\User;
use App\Exception\CommunityMembershipNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Form\CommunityMembershipType;
use App\Service\CommunityRoleService;
use App\Service\CommunityService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1/communities")
 *
 * Class CommunityMembershipController
 * @package App\Controller
 */
class CommunityMembershipController extends AbstractFOSRestController
{
    /** @var CommunityRoleService */
    private $communityRoleService;

    /** @var CommunityService */
    private $communityService;

    /**
     * CommunityMembershipController constructor.
     *
     * @param CommunityRoleService $communityRoleService
     * @param CommunityService $communityService
     */
    public function __construct(CommunityRoleService $communityRoleService, CommunityService $communityService)
    {
        $this->communityRoleService = $communityRoleService;
        $this->communityService = $communityService;
    }


    /**
     * Fetches the memberships of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("/{hash}/memberships")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getCommunityMembershipsAction($hash): View
    {
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        /** @var User $user */
        $user = $this->getUser();
        $this->communityService
            ->checkCommunityVisibleForUser($community, $user);

        return $this->view($community->getMemberships(), Response::HTTP_OK);
    }

    /**
     * Fetches the communities of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships specified by the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("/memberships")
     * @Rest\QueryParam(name="user", description="username of the user from whom fetch memberships", nullable=false)
     * @Rest\View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getCommunitiesMembershipsAction(ParamFetcher $paramFetcher): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $entityManager
            ->getRepository('App:User')
            ->findOneBy(array('username' => $paramFetcher->get('user')));

        if (null === $user) {
            return $this->view([], Response::HTTP_OK);
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($user === $currentUser) {
            return $this->view($user->getCommunitiesMemberships(), Response::HTTP_OK);
        }

        $memberships = $entityManager
            ->getRepository('App:CommunityMembership')
            ->searchUserMemberships($user, $currentUser);

        return $this->view($memberships, Response::HTTP_OK);
    }

    /**
     * Add a new member to a community.
     * If the community is public, one user can create its own membership. His role will therefore be the default one.
     * If the user has been invited, he or she can also create its own membership, with default role.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the created membership",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/{hash}/memberships")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postCommunityMembershipAction(Request $request, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        $currentUserMembership = $entityManager
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array(
                'community' => $community,
                'user' => $currentUser
            ));

        /*
         * If the user who wants to create the membership is not a member of the project,
         * it must either follow an invitation he or she accepts, or the community must be open.
         */
        if ($currentUserMembership === null) {

            $invitation = $entityManager
                ->getRepository('App:CommunityInvitation')
                ->findOneBy(array(
                    'hash' => $request->request->get(
                        'invitation'
                    )));

            if ($community->isOpen() || ($invitation !== null
                    && $invitation->getUser() === $currentUser
                    && $invitation->getCommunity() === $community)) {

                $membership = new CommunityMembership();
                $membership->setHash(
                    HashGeneratorService::generateHash(
                        $entityManager->getRepository('App:CommunityMembership')
                    )
                );

                /*
                 * Fetches the default member role.
                 * If there is not, create some.
                 */
                $defaultMemberRole = $this
                    ->communityRoleService
                    ->getOrCreateDefaultMemberRole($community);

                $membership->setCommunity($community);
                $membership->setRole($defaultMemberRole);
                $membership->setUser($currentUser);

                if ($invitation) {
                    $entityManager->remove($invitation);
                }
                $entityManager->persist($membership);
                $entityManager->flush();

                return $this->view($membership, Response::HTTP_CREATED);

            }

            throw new AccessDeniedHttpException('You cannot do this');

        }

        $candidacy = $entityManager
            ->getRepository('App:CommunityCandidacy')
            ->findOneBy(array('hash' => $request->request->get('candidacy')));

        if ($candidacy !== null && $currentUserMembership->canManageCandidacies()) {

            /*
             * Checks if there is no conflict with an existing member.
             */
            $userCurrentMembership = $entityManager
                ->getRepository('App:CommunityMembership')
                ->findOneBy(array(
                    'community' => $community,
                    'user' => $candidacy->getUser()
                ));

            if ($userCurrentMembership === null) {

                $membership = new CommunityMembership();
                $membership->setHash(
                    HashGeneratorService::generateHash(
                        $entityManager->getRepository('App:CommunityMembership')
                    )
                );
                $membership->setCommunity($community);
                $membership->setUser($candidacy->getUser());

                /** @var CommunityRole $role */
                $role = $entityManager
                    ->getRepository('App:CommunityRole')
                    ->findOneBy(array(
                        'hash' => $request->request->get(
                            'role'
                        )));

                if ($role->isGlobal() || $community->getRoles()->contains($role)) {
                    $membership->setRole($role);

                    $entityManager->persist($membership);
                    $entityManager->remove($candidacy);
                    $entityManager->flush();

                    return $this->view($membership, Response::HTTP_OK);
                }

                throw new UnprocessableEntityHttpException('Invalid Role');

            }

            throw new ConflictHttpException('An existing membership already exist between this user and this community');

        }

        throw new AccessDeniedHttpException('You are not authorized to do this');
    }

    /**
     * Updates an existing membership of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated membership",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/memberships/{hash}")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putCommunityMembershipAction(Request $request, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $currentMembership = $this
            ->tryToGetCommunityMembership($hash);

        $community = $currentMembership->getCommunity();

        /** @var CommunityMembership $currentUserMembership */
        $currentUserMembership = $entityManager
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array(
                'community' => $community,
                'user' => $currentUser
            ));

        if (!$currentUser->isAdmin() && (null === $currentMembership || !$currentUserMembership->canManageMemberships())) {
            throw new ForbiddenActionException();
        }

        $form = $this->createForm(CommunityMembershipType::class, $currentMembership);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager->flush();
        return $this->view($currentMembership, Response::HTTP_OK);
    }

    /**
     * Removes a user from a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a membership was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Delete("/memberships/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteCommunityMembershipAction($hash): View
    {
        $currentMembership = $this
            ->tryToGetCommunityMembership($hash);

        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $community = $currentMembership->getCommunity();

        $currentUserMembership = $entityManager
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array(
                'community' => $community,
                'user' => $currentUser
            ));

        if (!$currentUser->isAdmin() &&
            $currentMembership->getUser() !== $currentUser
            && (null === $currentUserMembership || !$currentUserMembership->canManageMemberships())) {
            throw new ForbiddenActionException();
        }

        $entityManager->remove($currentMembership);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetCommunityMembership(string $hash): CommunityMembership
    {
        /** @var CommunityMembership $currentMembership */
        $currentMembership = $this
            ->getDoctrine()
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array('hash' => $hash));

        if (null === $currentMembership) {
            throw new CommunityMembershipNotFoundException($hash);
        }

        return $currentMembership;
    }

}
