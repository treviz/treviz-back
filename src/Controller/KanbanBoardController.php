<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\User;
use App\Form\BoardType;
use App\Service\BoardService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Board controller.
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 */
class KanbanBoardController extends AbstractFOSRestController
{
    /** @var BoardService */
    private $boardService;

    /**
     * ColumnController constructor.
     *
     * @param BoardService $boardService
     */
    public function __construct(BoardService $boardService)
    {
        $this->boardService = $boardService;
    }

    /**
     * Lists all boards according to the query.
     * A user can fetch the boards of his/her project, unless he or she is admin.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of boards matching the query",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards")
     * @Rest\QueryParam(name="user", description="Username of the user from which the boards should be fetched", nullable=true)
     * @Rest\QueryParam(name="project", description="hash of the project from which the boards should be fetched", nullable=true)
     * @Rest\QueryParam(name="archived", description="set to true to fetch closed boards. False by default.", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getBoardsAction(ParamFetcher $paramFetcher): View
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $project = $paramFetcher->get('project');
        $user = $paramFetcher->get('user');

        $boards = $this
            ->getDoctrine()
            ->getRepository('App:Board')
            ->searchBoards(
                $currentUser,
                $project,
                $user,
                $paramFetcher->get('archived') === 'true'
            );
        $context = (new Context())
            ->enableMaxDepth()
            ->setGroups(['Default']);

        if (!$project) {
            $context->addGroup('user');
        }

        return $this
            ->view($boards, Response::HTTP_OK)
            ->setContext($context);
    }

    /**
     * Finds and returns a board.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Board is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the board",
     * )
     *
     * @Rest\Get("/boards/{hash}")
     * @SWG\Tag(name="kanban")
     *
     * @param $hash
     * @return View
     */
    public function getBoardAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        return $this->view(
            $this->boardService->tryToGetBoardAndCheckAccess($hash, $user, false),
            Response::HTTP_OK
        );
    }

    /**
     * Creates a new board entity.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Board is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the board",
     * )
     *
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid given object; for instance the project hash does not exist.",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Post("/boards")
     *
     * @param Request $request
     *
     * @return View
     * @throws Exception
     */
    public function postBoardAction(Request $request): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $board = new Board();
        $form = $this->createForm(BoardType::class, $board);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this
            ->boardService
            ->checkUserCanAccessBoard($board, $user, true);

        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        $board->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:Board')
            )
        );
        $entityManager->persist($board);
        $entityManager->flush();

        return $this->view($board, Response::HTTP_CREATED);
    }

    /**
     * Update an existing board
     *
     * @SWG\Response(
     *     response=200,
     *     description="Board is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to update the board",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Given object is invalid; for instance the project hash does not exist.",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Put("/boards/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putBoardAction(Request $request, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $board = $this
            ->boardService
            ->tryToGetBoardAndCheckAccess($hash, $user, true);

        $form = $this->createForm(BoardType::class, $board);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager->flush();
        return $this->view($board, Response::HTTP_OK);
    }

    /**
     * Deletes a board.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Board is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the board",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Delete("/boards/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteBoardAction($hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var User $user */
        $user = $this->getUser();
        $board = $this
            ->boardService
            ->tryToGetBoardAndCheckAccess($hash, $user, true);

        $entityManager->remove($board);
        $entityManager->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

}
