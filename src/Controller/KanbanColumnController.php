<?php

namespace App\Controller;

use App\Entity\Column;
use App\Entity\User;
use App\Form\ColumnType;
use App\Service\BoardService;
use App\Service\ColumnService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Column controller.
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 */
class KanbanColumnController extends AbstractFOSRestController
{
    /** @var BoardService */
    private $boardService;

    /** @var ColumnService */
    private $columnService;

    /**
     * ColumnController constructor.
     *
     * @param BoardService $boardService
     * @param ColumnService $columnService
     */
    public function __construct(BoardService $boardService, ColumnService $columnService)
    {
        $this->boardService = $boardService;
        $this->columnService = $columnService;
    }


    /**
     * Lists all columns of a board.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of columns matching the query",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the board's columns",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards/{hash}/columns")
     * @Rest\View(serializerGroups={"Default", "columns"}, serializerEnableMaxDepthChecks=true)
     *
     * @param string $hash Hash of the board from which fetch the column.
     * @return View
     */
    public function getBoardColumnsAction(string $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $board = $this
            ->boardService
            ->tryToGetBoardAndCheckAccess($hash, $user, false);

        return $this->view($board->getColumns(), Response::HTTP_OK);
    }

    /**
     * Finds and returns a column.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Column is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No column is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the column",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards/columns/{hash}", requirements={"hash"="^(?!tasks).\w+"})
     * @Rest\View(serializerGroups={"Default", "columns"},serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getBoardColumnAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $column = $this->columnService
            ->tryToGetColumnAndCheckAccess(
                $hash,
                $user,
                false
            );

        return $this->view($column, Response::HTTP_OK);

    }


    /**
     * Creates a new column.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Column is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the column",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Post("/boards/{hash}/columns")
     *
     * @param Request $request
     * @param $hash
     * @return View
     * @throws Exception
     */
    public function postBoardColumnAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $board = $this
            ->boardService
            ->tryToGetBoardAndCheckAccess(
                $hash,
                $user,
                true
            );

        $column = new Column();
        $form = $this->createForm(ColumnType::class, $column);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $column->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:Column')
            )
        );
        $column->setBoard($board);
        $entityManager->persist($column);
        $entityManager->flush();

        return $this->view($column, Response::HTTP_CREATED);
    }

    /**
     * Edit an existing column.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Column is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to update the column",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Put("/boards/columns/{hash}")
     * @Rest\View(serializerGroups={"Default", "columns"},serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putBoardColumnAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $column = $this
            ->columnService
            ->tryToGetColumnAndCheckAccess(
                $hash,
                $user,
                true
            );

        $form = $this->createForm(ColumnType::class, $column);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->getDoctrine()
            ->getManager()
            ->flush();
        return $this->view($column, Response::HTTP_OK);
    }

    /**
     * Deletes a column entity.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Column is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the column",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No column is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Delete("/boards/columns/{hash}")
     * @param $hash
     * @return View
     */
    public function deleteBoardColumnAction($hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $column = $this
            ->columnService
            ->tryToGetColumnAndCheckAccess(
                $hash,
                $user,
                true
            );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($column);
        $entityManager->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

}
