<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 30/10/2017
 * Time: 15:25
 */

namespace App\Controller;

use App\Entity\ProjectCandidacy;
use App\Entity\ProjectMembership;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\ProjectCandidacyNotFoundException;
use App\Form\ProjectCandidacyType;
use App\Message\ProjectCandidacyAcceptedMessage;
use App\Message\ProjectCandidacyCreatedMessage;
use App\Message\ProjectCandidacyRejectedMessage;
use App\Service\ProjectMembershipService;
use App\Service\ProjectRoleService;
use App\Service\ProjectService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 * Class ProjectCandidacyController
 * @package App\Controller
 */
class ProjectCandidacyController extends AbstractFOSRestController
{
    /** @var MessageBusInterface */
    private $messageBus;
    /** @var ProjectRoleService */
    private $projectRoleService;
    /** @var ProjectMembershipService */
    private $projectMembershipService;
    /** @var ProjectService */
    private $projectService;

    /**
     * ProjectCandidacyController constructor.
     *
     * @param MessageBusInterface $mesageBus
     * @param ProjectRoleService $projectRoleService
     * @param ProjectMembershipService $projectMembershipService
     * @param ProjectService $projectService
     */
    public function __construct(MessageBusInterface $mesageBus,
                                ProjectService $projectService,
                                ProjectRoleService $projectRoleService,
                                ProjectMembershipService $projectMembershipService)
    {
        $this->projectRoleService = $projectRoleService;
        $this->messageBus = $mesageBus;
        $this->projectMembershipService = $projectMembershipService;
        $this->projectService = $projectService;
    }

    /**
     * Fetches the candidacies of a project
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the candidacies should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectCandidacy::class, groups={"Default", "project"})
     *     )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to fetch candidacies for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/{hash}/candidacies")
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getProjectCandidaciesAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();

        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        if ($user->isAdmin() || (null !== $membership && $membership->canManageCandidacies())) {
            return $this->view($project->getCandidacies(), Response::HTTP_OK);
        }

        $candidacy = $entityManager
            ->getRepository('App:ProjectCandidacy')
            ->findOneBy(array(
                'project' => $project,
                'user'    => $user
            ));

        if (null !== $candidacy) {
            return $this->view([$candidacy], Response::HTTP_OK);
        }

        throw new AccessDeniedHttpException(
            'You are not authorized to do this'
        );
    }

    /**
     * Fetches the candidacies according to the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies specified by the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectCandidacy::class, groups={"Default", "user"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/candidacies")
     * @Rest\QueryParam(name="user", description="username of the user from whom fetch candidacies", nullable=true)
     * @Rest\QueryParam(name="job", description="Hash of the job from which fetch the candidacies", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getProjectsCandidaciesAction(ParamFetcher $paramFetcher): View
    {
        $context = new Context();
        $context->setGroups(array('Default'));
        $context->enableMaxDepth();

        $username = $paramFetcher->get('user');
        $job = $paramFetcher->get('job');
        if ($username !== null) {
            $context->addGroup('user');
        }
        if ($job !== null) {
            $context->addGroup('project');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $candidacies = $this
            ->getDoctrine()
            ->getRepository('App:ProjectCandidacy')
            ->getUserCandidacies($currentUser, $username, $job);

        return $this->view($candidacies, Response::HTTP_OK)
            ->setContext($context);
    }

    /**
     * Creates a new candidacy for a project or job.
     * A candidacy is only acceptable for a job if it is not currently attributed.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project for which the candidacy should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="candidacy",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="job",
     *             type="string",
     *             description="Hash of the job to which candidate"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             description="Message to justify the candidacy"
     *         )
     *     ),
     *     required=true,
     *     description="Candidacy that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created candidacy",
     *     @Model(type=ProjectCandidacy::class, groups={"Default"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to create candidacies for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data was submitted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Post("/projects/{hash}/candidacies")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postProjectCandidacyAction(Request $request, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();

        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        $searchParams = array('project' => $project, 'user' => $user);

        $invitation = $entityManager
            ->getRepository('App:ProjectInvitation')
            ->findOneBy($searchParams);

        $candidacy = $entityManager
            ->getRepository('App:ProjectCandidacy')
            ->findOneBy($searchParams);

        if (null !== $candidacy || null !== $invitation) {
            throw new ConflictHttpException('You are already candidate or invited to this project');
        }

        /*
         * A user can only candidate to projects if he has not yet been invited, candidate, or if he candidates
         * for a job.
         */
        if (!$project->isOpen() || ($membership !== null && !$request->get('job'))) {
            throw new AccessDeniedHttpException('You cannot candidate to this project');
        }

        $candidacy = new ProjectCandidacy();
        $form = $this->createForm(ProjectCandidacyType::class, $candidacy);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        if (($job = $candidacy->getJob()) && $job->getHolder() !== null) {
            throw new ConflictHttpException('The job is already attributed');
        }

        $candidacy->setProject($project);
        $candidacy->setUser($user);
        $candidacy->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:ProjectCandidacy')
            )
        );

        $entityManager->persist($candidacy);
        $entityManager->flush();

        $this->messageBus->dispatch(new ProjectCandidacyCreatedMessage($candidacy->getId()));

        return $this->view($candidacy, Response::HTTP_CREATED);

    }

    /**
     * Removes a candidacy from a project.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the candidacy is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to delete candidacies for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Candidacy does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Delete("/projects/candidacies/{hash}")
     * @param $hash
     * @return View
     */
    public function deleteProjectCandidacyAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $candidacy = $this->tryToGetCandidacy($hash);

        /** @var User $user */
        $user = $this->getUser();

        /** @var ProjectMembership $membership */
        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $candidacy->getProject());

        if ($candidacy->getUser() !== $user && (null === $membership || !$membership->canManageCandidacies())) {
            throw new ForbiddenActionException();
        }

        if ($user !== $candidacy->getUser()) {
            $this->messageBus->dispatch(new ProjectCandidacyRejectedMessage(
                $candidacy->getProject()->getId(),
                $candidacy->getUser()->getId()
            ));
        }

        $entityManager->remove($candidacy);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Accept a candidacy for a project and/or job.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the candidacy to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The candidacy is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="The submitted role is invalid",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the candidacy",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The candidacy could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="The job the user wants is already taken.",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Post("/projects/candidacies/{hash}/accept")
     * @Rest\RequestParam(name="role", description="hash of the role to give to the user if he/she is not yet member of the project.")
     *
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function acceptProjectCandidacyAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $candidacy = $this->tryToGetCandidacy($hash);
        $project = $candidacy->getProject();

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $user = $candidacy->getUser();

        /** @var ProjectMembership $currentUserMembership */
        $currentUserMembership = $this
            ->projectMembershipService
            ->getUserMembership($currentUser, $project);

        if (!$currentUser->isAdmin() &&
            (null === $currentUserMembership || !$currentUserMembership->canManageCandidacies())) {
            throw new ForbiddenActionException();
        }

        $userCurrentMembership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        $job = $candidacy->getJob();

        /*
         * A candidacy can only be accepted for a job if it has no current holder.
         * If such is the case, all other candidacies are deleted.
         * The candidate becomes a member of the project (if he/she was not already).
         */
        if ($job !== null) {
            if ($job->getHolder() === null) {
                $job->setHolder($user);
                foreach ($job->getApplications() as $application) {
                    $job->removeApplication($application);
                }
            } else {
                throw new ConflictHttpException(
                    'This job is already taken'
                );
            }
        }

        if ($userCurrentMembership === null) {
            $membership = new ProjectMembership();
            $membership->setProject($project);
            $membership->setUser($user);
            $membership->setHash(
                HashGeneratorService::generateHash(
                    $entityManager->getRepository('App:ProjectMembership')
                )
            );

            $role = $this
                ->projectRoleService
                ->getOrCreateDefaultRole($project);

            $membership->setRole($role);
            $entityManager->persist($membership);

            /** Add the user to the default chat room */
            $user->addRoom($project->getRooms()->first());
        }

        if ($userCurrentMembership !== null && $job === null) {
            throw new ConflictHttpException('This user is already member of this project');
        }

        if (!isset($membership)) {
            $membership = $currentUserMembership;
        }

        $this->messageBus->dispatch(new ProjectCandidacyAcceptedMessage(
            $candidacy->getProject()->getId(),
            $candidacy->getUser()->getId()
        ));

        $entityManager->remove($candidacy);
        $entityManager->flush();

        return $this->view($membership, Response::HTTP_OK);
    }

    public function tryToGetCandidacy(string $hash): ProjectCandidacy
    {
        /** @var ProjectCandidacy $candidacy */
        $candidacy = $this
            ->getDoctrine()
            ->getRepository('App:ProjectCandidacy')
            ->findOneBy(array(
                'hash' => $hash
            ));
        if (null === $candidacy) {
            throw new ProjectCandidacyNotFoundException($hash);
        }

        return $candidacy;
    }

}
