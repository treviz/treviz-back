<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 11/07/2017
 * Time: 22:47
 */

namespace App\Controller;

use App\Entity\Room;
use App\Entity\User;
use App\EventDispatcher\Dispatcher;
use App\Events\RoomEvent;
use App\Exception\ForbiddenActionException;
use App\Exception\RoomNotFoundException;
use App\Form\RoomType;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1/rooms")
 *
 * Class RoomController
 * @package App\Controller
 */
class ChatRoomController extends AbstractFOSRestController
{

    /**
     * @var Dispatcher $dispatcher
     */
    private $dispatcher;

    /**
     * MessageController constructor.
     *
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Fetches the rooms according to the query parameters.
     * A user can see all the rooms of a project or community he is member of; but he can only
     * post messages in rooms he is member of.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of rooms matching the query",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Get("")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @QueryParam(name="project", description="hash of the project from which the rooms should be fetched", nullable=true)
     * @QueryParam(name="community", description="hash of the community from which the rooms should be fetched", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getRoomsAction(ParamFetcher $paramFetcher): View
    {
        $s_project = $paramFetcher->get('project');
        $s_community = $paramFetcher->get('community');
        $user = $this->getUser();

        $rooms = $this
            ->getDoctrine()
            ->getRepository('App:Room')
            ->searchRooms($user, $s_project, $s_community);

        return $this->view($rooms, Response::HTTP_OK);
    }

    /**
     * Creates a new room for a project or community.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a chat room was successfully created",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return View
     * @throws Exception
     * @internal param ParamFetcher $paramFetcher
     */
    public function postRoomsAction(Request $request): View
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            /** @var User $user */
            $user = $this->getUser();
            $room->addUser($user);
            $room->setHash(
                HashGeneratorService::generateHash(
                    $this->getDoctrine()->getRepository('App:Room')
                )
            );

            if (($room->getCommunity() == null) && $project = $room->getProject()) {

                $membership = $this->getDoctrine()->getRepository(
                    'App:ProjectMembership'
                )
                    ->findOneBy(array(
                        'project' => $project,
                        'user'    => $this->getUser()
                    ));

                if ($membership !== null) {
                    return $this->persistRoomAndSendEvent($room);
                }

                throw new ForbiddenActionException();

            }

            if (($room->getProject() == null) && $community = $room->getCommunity()) {

                $membership = $this->getDoctrine()->getRepository(
                    'App:CommunityMembership'
                )
                    ->findOneBy(array(
                        'community' => $community,
                        'user'      => $this->getUser()
                    ));

                if($membership !== null) {
                    return $this->persistRoomAndSendEvent($room);
                }

                throw new ForbiddenActionException();

            }

            return $this->persistRoomAndSendEvent($room);

        }

        throw new UnprocessableEntityHttpException(json_encode($form));

    }

    /**
     * Updates an existing chat room.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a chat room was successfully updated",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Put("/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param string $hash Hash of the room to update
     * @param Request $request JSON Body containing the updated room
     * @return View
     */
    public function putRoomAction($hash, Request $request): View
    {
        $room = $this->findRoomToUpdate($hash);
        $form = $this->createForm(RoomType::class, $room);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        return $this->updateRoomAndSendEvent($room);
    }

    /**
     * Add users to a chat room.
     *
     * We expect a JSON array of usernames : ['johndoe', 'janesmith']
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a user was successfully added to a room",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/{hash}/invite")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @param Request $request
     * @return View
     */
    public function addUserToRoomAction($hash, Request $request): View
    {
        $em = $this->getDoctrine()->getManager();
        $room = $this->findRoomToUpdate($hash);
        $usernames = json_decode($request->getContent());

        foreach ($usernames as $username) {
            /** @var User $user */
            $user = $em
                ->getRepository('App:User')
                ->findOneBy(array('username' => $username));

            if ($user && !$room->getUsers()->contains($user)) {
                $room->addUser($user);
            }
        }

        return $this->updateRoomAndSendEvent($room);
    }

    /**
     * Removes a user from a chat room.
     * If the chat rooms has no more users, deletes it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user was successfully removed from a room",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/{hash}/remove")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @param Request $request
     * @return View
     */
    public function removeUserFromRoomAction($hash, Request $request): View
    {
        $em = $this->getDoctrine()->getManager();
        $room = $this->findRoomToUpdate($hash);
        $usernames = json_decode($request->getContent());

        foreach ($usernames as $username) {
            /** @var User $user */
            $user = $em
                ->getRepository('App:User')
                ->findOneBy(array('username' => $username));

            if ($user && $room->getUsers()->contains($user)) {
                $room->removeUser($user);
            }

        }

        if ($room->getUsers()->count() === 0) {
            $em->remove($room);
        }

        return $this->updateRoomAndSendEvent($room);
    }

    /**
     * Deletes a chat room and all its messages
     *
     * @SWG\Response(
     *     response=204,
     *     description="The chat room was successfully deleted",
     * )
     *
     * @Rest\Delete("/{hash}")
     *
     * @SWG\Tag(name="chat")
     *
     * @param $hash
     * @return View
     */
    public function deleteRoomAction($hash)
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Room $room */
        $room = $entityManager
            ->getRepository('App:Room')
            ->findOneBy(array('hash' => $hash));

        if (null === $room) {
            throw new RoomNotFoundException($hash);
        }

        if (!$room->getUsers()->contains($this->getUser())) {
            throw new AccessDeniedHttpException('Only a room member can delete it');
        }

        $entityManager->remove($room);
        $entityManager->flush();

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $event = new RoomEvent($room, $currentUser);
        $this->dispatcher->dispatch(RoomEvent::DELETED, $event);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function findRoomToUpdate($hash): Room {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Room $room
         */
        $room = $em
            ->getRepository('App:Room')
            ->findOneBy(array('hash' => $hash));

        if (null === $room) {
            throw new RoomNotFoundException($hash);
        }

        if(!$room->getUsers()->contains($this->getUser())){
            throw new ForbiddenActionException();
        }

        return $room;
    }

    private function persistRoomAndSendEvent($room): View
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($room);
        $em->flush();

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $event = new RoomEvent($room, $currentUser);
        $this->dispatcher->dispatch(RoomEvent::CREATED, $event);
        return $this->view($room, Response::HTTP_CREATED);
    }

    private function updateRoomAndSendEvent($room): View
    {
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $event = new RoomEvent($room, $currentUser);
        $this->dispatcher->dispatch(RoomEvent::UPDATED, $event);
        return $this->view($room, Response::HTTP_OK);
    }

}
