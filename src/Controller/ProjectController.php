<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 10/02/Response::HTTP_CREATED7
 * Time: 12:42
 */

namespace App\Controller;

use App\Entity\Project;
use App\Entity\ProjectMembership;
use App\Entity\ProjectRole;
use App\Entity\User;
use App\Exception\ProjectNotFoundException;
use App\Form\ProjectType;
use App\Message\ProjectCreatedMessage;
use App\Message\ProjectForkedMessage;
use App\Service\ProjectMembershipService;
use App\Service\ProjectService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class ProjectController
 * @package App\Controller
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 */
class ProjectController extends AbstractFOSRestController
{
    private $kernelProjectDir;
    private $backendUrl;
    private $projectLogoPath;
    /** @var ProjectMembershipService */
    private $projectMembershipService;
    /** @var ProjectService */
    private $projectService;
    /** @var MessageBusInterface $messageBus */
    private $messageBus;

    /**
     * ProjectController constructor.
     *
     * @param String $kernelProjectDir
     * @param String $backendUrl
     * @param String $projectLogoPath
     * @param ProjectService $projectService
     * @param ProjectMembershipService $projectMembershipService
     * @param MessageBusInterface $messageBus
     */
    public function __construct(
        String $kernelProjectDir,
        String $backendUrl,
        String $projectLogoPath,
        ProjectService $projectService,
        ProjectMembershipService $projectMembershipService,
        MessageBusInterface $messageBus)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        $this->backendUrl = $backendUrl;
        $this->projectLogoPath = $projectLogoPath;
        $this->projectService = $projectService;
        $this->projectMembershipService = $projectMembershipService;
        $this->messageBus = $messageBus;
    }


    /**
     * Fetches a specific project, if the user has access to it.
     * A user can access a project if:
     *   * he or she is member of the project
     *   * the project is public
     *   * the project is private and the user belongs to a community it is linked to
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to fetch"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns a project identified by its hash",
     *     @Model(type=Project::class, groups={"Default", "project"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to see this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/{hash}", requirements={"hash"="^(?!(memberships|candidacies|invitations|jobs)).\w+"})
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     * @throws ProjectNotFoundException
     */
    public function getProjectAction($hash): View
    {
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();
        $this->projectService
            ->checkProjectVisibleForUser($project, $user);

        return $this->view($project, Response::HTTP_OK);
    }

    /**
     * Fetches specific projects according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of projects matching the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Project::class, groups={"Default"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects")
     * @Rest\View(serializerGroups={"Default"},serializerEnableMaxDepthChecks=true)
     *
     * @Rest\QueryParam(name="name", requirements="[\p{L}\p{N}_\s]+", description="search  by name", nullable=true)
     * @Rest\QueryParam(name="tags", description="search by tag", nullable=true)
     * @Rest\QueryParam(name="skills", description="search by skills", nullable=true)
     * @Rest\QueryParam(name="offset", description="\d+", nullable=true)
     * @Rest\QueryParam(name="nb", description="\d+", nullable=true)
     * @Rest\QueryParam(name="user", description="username of the user who must take part in the project", nullable=true)
     * @Rest\QueryParam(name="community", description="hash of the community the project must be linked to", nullable=true)
     * @Rest\QueryParam(name="role", description="name of role of the current user in those projects", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getProjectsAction(ParamFetcher $paramFetcher): View
    {

        $offset = $paramFetcher->get('offset');
        $nbResult = $paramFetcher->get('nb');

        /** @var User $user */
        $user = $this->getUser();
        $projects = $this
            ->getDoctrine()
            ->getRepository('App:Project')
            ->searchProject(
                $user,
                $paramFetcher->get('name'),
                $paramFetcher->get('community'),
                $paramFetcher->get('tags'),
                $paramFetcher->get('skills'),
                $paramFetcher->get('user'),
                $paramFetcher->get('role'),
                $offset ?? 0,
                $nbResult ?? 20
            );

        return $this->view($projects, Response::HTTP_OK);
    }

    /**
     * Creates a new project in database, and returns it
     *
     * @SWG\Parameter(
     *     name="Project",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the project to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the project"
     *         ),
     *         @SWG\Property(
     *             property="shortDescription",
     *             type="string",
     *             description="Short description of the project",
     *             maxLength=140
     *         ),
     *         @SWG\Property(
     *             property="isVisible",
     *             type="string",
     *             description="If true, the project will be displayed to all users, logged in or not"
     *         ),
     *         @SWG\Property(
     *             property="logo",
     *             type="file",
     *             description="Logo of the project to upload"
     *         ),
     *         @SWG\Property(
     *             property="logoUrl",
     *             type="string",
     *             description="URL of the logo to upload, of a file cannot be sent."
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         ),
     *         @SWG\Property(
     *             property="communities",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of community names to which link the project"
     *         ),
     *         @SWG\Property(
     *             property="idea",
     *             type="file",
     *             description="Hash of the idea the project comes from"
     *         )
     *     ),
     *     required=true,
     *     description="Project that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created project",
     *     @Model(type=Project::class, groups={"Default", "project"})
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/projects")
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return View
     * @throws Exception
     */
    public function postProjectAction(Request $request): View
    {
        /** @var User $user */
        $user = $this->getUser();

        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager = $this->getDoctrine()->getManager();

        $this->handleLogo($request->files->get('logo'), $project);

        $membership = new ProjectMembership();
        $membership->setProject($project);
        $membership->setUser($user);

        $defaultCreatorRole = $entityManager
            ->getRepository('App:ProjectRole')
            ->findOneBy(array('defaultCreator' => true));

        /*
         * If no default role has been defined for project creators, we generate one.
         */
        if ($defaultCreatorRole === null) {
            $defaultCreatorRole = ProjectRole::generateDefaultCreatorRole();
        }

        $membership->setRole($defaultCreatorRole);

        $project->setHash(
            HashGeneratorService::generateHash($entityManager->getRepository('App:Project'))
        );
        $membership->setHash(
            HashGeneratorService::generateHash($entityManager->getRepository('App:ProjectMembership'))
        );

        /*
         * Add user to the default chatroom
         */
        $user->addRoom($project->getRooms()->first());

        /*
         * Persist data
         */
        $entityManager->persist($project);
        $entityManager->persist($membership);
        $entityManager->flush();

        /*
         * Sends a mail to the members of the project's communities
         */
        $this->messageBus->dispatch(new ProjectCreatedMessage($project->getId()));

        /*
         * If the project is a fork, notify the parent's projects members
         */
        if ($project->getParent() !== null) {
            $this->messageBus->dispatch(new ProjectForkedMessage($project->getParent()->getId(), $project->getId()));
        }

        return $this->view($project, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing project
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to update"
     * )
     *
     * @SWG\Parameter(
     *     name="Project",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the project to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the project"
     *         ),
     *         @SWG\Property(
     *             property="shortDescription",
     *             type="string",
     *             description="Short description of the project",
     *             maxLength=140
     *         ),
     *         @SWG\Property(
     *             property="isVisible",
     *             type="string",
     *             description="If true, the project will be displayed to all users, logged in or not"
     *         ),
     *         @SWG\Property(
     *             property="logo",
     *             type="file",
     *             description="Logo of the project to upload"
     *         ),
     *         @SWG\Property(
     *             property="logoUrl",
     *             type="string",
     *             description="URL of the logo to upload, of a file cannot be sent."
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         ),
     *         @SWG\Property(
     *             property="communities",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of community names to which link the project"
     *         ),
     *         @SWG\Property(
     *             property="idea",
     *             type="file",
     *             description="Hash of the idea the project comes from"
     *         )
     *     ),
     *     required=true,
     *     description="Project that should be updated"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated project"
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to update the project"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project does not exist"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/projects/{hash}", requirements={"hash"="^(?!(memberships|candidacies|invitations|jobs)).\w+"})
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putProjectAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        $project = $this->projectService->tryToFetchProject($hash);

        /*
         * Then, we check if the user has rights to update the project.
         * Otherwise, we send a Response::HTTP_FORBIDDEN error code.
         */
        /** @var ProjectMembership $membership */
        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        if ($membership === null || !$membership->canUpdateProject()) {
            throw new AccessDeniedHttpException('You are not allowed to update this project');
        }

        /*
         * Sets the project logo with the actual file so that the form does not crash.
         */
        if ($projectLogoName = $project->getLogo()) {
            $project->setLogo(new File($this->kernelProjectDir . '/public' . $this->projectLogoPath . $projectLogoName));
        }

        $form = $this->createForm(ProjectType::class, $project);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $this->handleLogo($request->files->get('logo'), $project);

        $entityManager->flush();
        return $this->view($project, Response::HTTP_OK);
    }

    /**
     * Deletes an existing project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to delete"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the project was successfully deleted"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Delete("/projects/{hash}", requirements={"hash"="^(?!(memberships|candidacies|invitations|jobs)).\w+"})
     *
     * @param $hash
     * @return View
     */
    public function deleteProjectAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /*
         * Then, we check if the user has rights to delete project.
         * Otherwise, we send a Response::HTTP_FORBIDDEN error code.
         */
        /** @var ProjectMembership $membership */
        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        if ((null === $membership || !$membership->canDeleteProject()) && !$user->isAdmin()) {
            throw new AccessDeniedHttpException('You cannot delete this project');
        }

        /**
         * To preserve database integrity, we need first to delete all project memberships. Then, we
         * can delete the projects, and the associated roles via cascade.
         */
        /** @var ProjectMembership $membership*/
        foreach ($project->getMemberships() as $membership) {
            $entityManager->remove($membership);
        }

        $entityManager->remove($project);
        $entityManager->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Uploads a new logo for a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to delete"
     * )
     *
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     @SWG\Schema(
     *         type="file"
     *     ),
     *     required=true,
     *     description="New logo to upload"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated project with a new logo"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Post("/projects/{hash}/logo")
     * @Rest\FileParam(name="image", image=true, nullable=false)
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @param $hash
     * @return View
     */
    public function postProjectLogoAction(ParamFetcher $paramFetcher, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /** @var ProjectMembership $membership */
        $membership = $this
            ->projectMembershipService
            ->getUserMembership($currentUser, $project);

        if (null === $membership || !$membership->canUpdateProject()) {
            throw new AccessDeniedHttpException('You cannot delete this project');
        }

        /** @var File $logo */
        $logo = $paramFetcher->get('image');
        $this->handleLogo($logo, $project);
        $entityManager->flush();

        return $this->view($project, Response::HTTP_OK);
    }

    private function handleLogo(?File $logo, Project $project): void
    {
        if (null === $logo) {
            return;
        }

        $name = HashGeneratorService::generateFileHashName($logo);
        $logo->move($this->kernelProjectDir . '/public' . $this->projectLogoPath, $name);
        $project->setLogo($name);
        $project->setLogoUrl($this->backendUrl . $this->projectLogoPath . $name);
    }

}
