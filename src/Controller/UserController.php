<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 10/02/2017
 * Time: 12:42
 */

namespace App\Controller;

use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\UserAlreadyExistsException;
use App\Exception\UserCannotBeViewedException;
use App\Exception\UserNotFoundException;
use App\Form\UserType;
use App\Message\UserCreatedMessage;
use App\Repository\UserRepository;
use App\Service\UserService;
use App\Utils\HashGeneratorService;
use DateTime;
use Doctrine\ORM\ORMException;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Rest\Route("/v1")
 */
class UserController extends AbstractFOSRestController
{
    private $frontendUrl;
    private $isOrganizationOpen;
    private $kernelProjectDir;
    private $uploadAvatarPath;
    private $uploadUserBackgroundPath;
    private $backendUrl;

    /** @var TokenGeneratorInterface $tokenGenerator */
    private $tokenGenerator;
    /** @var UserService */
    private $userService;
    /** @var UserManagerInterface */
    private $userManager;
    /** @var MessageBusInterface */
    private $messageBus;

    /**
     * UserController constructor.
     *
     * @param String $frontendUrl
     * @param String $isOrganizationOpen
     * @param String $kernelProjectDir
     * @param String $uploadAvatarPath
     * @param String $uploadUserBackgroundPath
     * @param String $backendUrl
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UserManagerInterface $userManager
     * @param UserService $userService
     * @param MessageBusInterface $messageBus
     */
    public function __construct(
        String $frontendUrl,
        String $isOrganizationOpen,
        String $kernelProjectDir,
        String $uploadAvatarPath,
        String $uploadUserBackgroundPath,
        String $backendUrl,
        TokenGeneratorInterface $tokenGenerator,
        UserManagerInterface $userManager,
        UserService $userService,
        MessageBusInterface $messageBus
    )
    {
        $this->frontendUrl = $frontendUrl;
        $this->isOrganizationOpen = $isOrganizationOpen;
        $this->kernelProjectDir = $kernelProjectDir;
        $this->uploadAvatarPath = $uploadAvatarPath;
        $this->uploadUserBackgroundPath = $uploadUserBackgroundPath;
        $this->backendUrl = $backendUrl;
        $this->tokenGenerator = $tokenGenerator;
        $this->userService = $userService;
        $this->userManager = $userManager;
        $this->messageBus = $messageBus;
    }

    /**
     * Get the details of a specified user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when the information of a user was successfully fetched",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Get("/users/{username}")
     * @Rest\View(serializerEnableMaxDepthChecks=true, serializerGroups={"Default", "user"})
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param string $username
     * @return View
     */
    public function getUserAction(string $username): View
    {
        $user = $this
            ->getDoctrine()
            ->getRepository('App:User')
            ->findOneBy(array('username' => $username));

        if (null === $user) {
            throw new UserNotFoundException($username);
        }

        if (!$user->isEnabled() && !$this->getUser()->isAdmin()) {
            throw new UserCannotBeViewedException();
        }

        return $this->view($user, Response::HTTP_OK);
    }

    /**
     * Get the detail of all the users (or just the ones that match a query).
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned an array of users matching the query",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Get("/users")
     *
     * [\p{L}\p{N}_\s]+ matches all words ands letters, using extended latin alphabet (ie characters like é,è,ê,ë... are accepted)
     * @Rest\QueryParam(name="name", requirements="[\p{L}\p{N}_\s]{1,16}", description="search by name", nullable=true)
     * @Rest\QueryParam(name="organization", requirements="[\p{L}\p{N}_\s]{1,16}", description="search by organization", nullable=true)
     * @Rest\QueryParam(name="tags", description="search by interests", nullable=true)
     * @Rest\QueryParam(name="skills", description="search by skills", nullable=true)
     * @Rest\QueryParam(name="offset", description="\d+", nullable=true)
     * @Rest\QueryParam(name="nb", description="\d+", nullable=true)
     * @Rest\QueryParam(name="show_disabled", nullable=true)
     *
     * @Rest\View(serializerGroups={"Default"},serializerEnableMaxDepthChecks=true)
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getUsersAction(ParamFetcher $paramFetcher): View
    {
        $users = $this
            ->getDoctrine()
            ->getRepository('App:User')
            ->searchUser(array(
                UserRepository::$SEARCH_KEY_NAME            => $paramFetcher->get('name'),
                UserRepository::$SEARCH_KEY_ORGANIZATION    => $paramFetcher->get('organization'),
                UserRepository::$SEARCH_KEY_INTERESTS       => $paramFetcher->get('tags'),
                UserRepository::$SEARCH_KEY_SKILLS          => $paramFetcher->get('skills'),
                UserRepository::$SEARCH_KEY_NB              => $paramFetcher->get('nb'),
                UserRepository::$SEARCH_KEY_OFFSET          => $paramFetcher->get('offset'),
                UserRepository::$SEARCH_KEY_SHOW_DISABLED   => $paramFetcher->get('show_disabled')
            ));

        return $this->view($users, Response::HTTP_OK);
    }

    /**
     * Creates a new user in database and sends a confirmation email.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned a user was successfully created",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Post("/users")
     *
     * @param Request $request
     *
     * @return View
     * @throws Exception
     */
    public function postUserAction(Request $request): View
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User|null $currentUser */
        $currentUser = $this->getUser();
        $isCurrentUserAdmin = ($currentUser !== null
            && $currentUser->hasRole(
                'ROLE_ADMIN'
            ));

        if ($currentUser !== null && !$currentUser->isAdmin()) {
            throw new AccessDeniedHttpException(
                'An authenticated user cannot create an additional account'
            );
        }

        $password = $request->get('password');
        $request->request->remove('password');

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->submit($request->request->all());

        if (null === $password || !$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $email = $user->getEmail();

        /*
         * We extract the domain name from the mail thanks to a regex.
         * Then, we check if it matches any listed domain.
         *
         * If the organization is public and a match is found, the user will be rejected.
         * If the organization is private and a match is found, the user will be accepted.
         */
        $pattern = '/^[a-zA-Z0-9.]+@/';
        preg_match($pattern, $email, $matches);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new BadRequestHttpException('Incorrect email');
        }

        $domain = substr($email, strlen($matches[0]));
        $match = $entityManager
            ->getRepository('App:MailDomain')
            ->findOneBy(array('domain' => $domain));

        if (!$isCurrentUserAdmin) {
            if ((null !== $match && $this->isOrganizationOpen) ||
                (null === $match && !$this->isOrganizationOpen)) {
                throw new AccessDeniedHttpException();
            }
        }

        if (!$isCurrentUserAdmin && $user->isAdmin()) {
            throw new AccessDeniedHttpException(
                'You cannot create an admin user without being one yourself'
            );
        }

        if ($this->userManager->findUserByEmail($email) ||
            $this->userManager->findUserByUsername($user->getUsername())) {
            throw new UserAlreadyExistsException();
        }

        /*
         * Generate a confirmation token for the current user
         */
        $user->setPlainPassword($password)
            ->setEnabled(false)
            ->setWelcome(true)
            ->setConfirmationToken($this->tokenGenerator->generateToken())
            ->setPasswordRequestedAt(new DateTime());

        $this->userManager->updateUser($user);
        $entityManager->flush();

        $this
            ->messageBus
            ->dispatch(new UserCreatedMessage($user->getId()));

        return $this->view($user,Response::HTTP_CREATED);
    }

    /**
     * Updates an existing user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when the information of a user was successfully updated",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Put("/users/{username}")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param $username
     * @return View
     * @throws Exception
     */
    public function putUserAction(Request $request, $username): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userRepository = $entityManager->getRepository('App:User');
        /** @var User $user */
        $currentUser = $this->getUser();
        $user = $userRepository
            ->findOneBy(array('username' => $username));
        $roles = $user->getRoles();

        if ($currentUser !== $user && !$currentUser->isAdmin()) {
            throw new ForbiddenActionException();
        }

        $newUsername = $request->get('username');
        if ($newUsername !== null &&
            $newUsername !== $username &&
            $userRepository->findOneBy(array('username' => $newUsername))) {
            throw new UserAlreadyExistsException();
        }

        /*
         * We save email as it is constant, and cannot be modified for now.
         */
        $email = $user->getEmail();

        /*
         * Set the user avatar an background with the actual files so that the form builder does not crash.
         */
        if ($avatarFileName = $user->getAvatar()) {
            $user->setAvatar(new File($this->kernelProjectDir . '/public' . $this->uploadAvatarPath . $avatarFileName));
        }

        if ($backgroundLogoName = $user->getBackgroundImage()) {
            $user->setBackgroundImage(new File($this->kernelProjectDir . '/public' . $this->uploadUserBackgroundPath . $backgroundLogoName));
        }

        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /**
         * A non-admin should not be able to modify the roles of any user.
         */
        if (!$currentUser->isAdmin()) {
            if ($user->getRoles() !== $roles) {
                throw new AccessDeniedHttpException(
                    'You cannot modify the roles of this user'
                );
            }
        }

        /** @var File $avatar */
        $avatar = $request->files->get('avatar');

        if ($avatar){
            $avatarName = HashGeneratorService::generateFileHashName($avatar);

            $avatar->move(
                $this->kernelProjectDir .
                '/public' .
                $this->uploadAvatarPath,
                $avatarName
            );

            $user->setAvatar($avatarName);
            $user->setAvatarUrl(
                $this->backendUrl .
                $this->uploadAvatarPath .
                $avatarName
            );

        }

        /** @var File $logo */
        $background = $request->files->get('background');

        if ($background){
            $backgroundName = HashGeneratorService::generateFileHashName($background);

            $background->move(
                $this->kernelProjectDir .
                '/public' .
                $this->uploadUserBackgroundPath,
                $backgroundName
            );

            $user->setBackgroundImage($backgroundName);
            $user->setBackgroundImageUrl(
                $this->backendUrl .
                $this->uploadUserBackgroundPath .
                $backgroundName
            );
        }

        $user->setEmail($email);
        $entityManager->flush();
        return $this->view($user, Response::HTTP_OK);
    }

    /**
     * Deletes a user
     *
     * @SWG\Response(
     *      response=204,
     *      description="Deletes a user from the platform, and all his or her personal data"
     * )
     * @SWG\Tag(name="users")
     *
     * @Rest\Delete("/users/{username}")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param String $username
     *
     * @return View|Response
     * @throws ORMException
     */
    public function deleteUserAction(String $username)
    {
        $user = $this
            ->userService
            ->tryToGetUpdatableUser($username, $this->getUser());

        $this->userService
            ->deleteUser($user);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Updates the avatar of a user.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when the avatar of a user was successfully updated",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Post("/users/{username}/avatar")
     * @Rest\FileParam(name="avatar", image=true, nullable=false)
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @param $username
     *
     * @return View
     * @throws Exception
     */
    public function postUserAvatarAction(ParamFetcher $paramFetcher, $username): View
    {
        $user = $this
            ->userService
            ->tryToGetUpdatableUser($username, $this->getUser());

        /** @var File $avatar */
        $avatar = $paramFetcher->get('avatar');
        $avatarName = HashGeneratorService::generateFileHashName($avatar);

        $avatar->move(
            $this->kernelProjectDir .
            '/public' .
            $this->uploadAvatarPath,
            $avatarName
        );

        $user->setAvatar($avatarName);
        $user->setAvatarUrl(
            $this->backendUrl .
            $this->uploadAvatarPath .
            $avatarName
        );

        $this->getDoctrine()->getManager()->flush();

        return $this->view($user, Response::HTTP_OK);
    }

    /**
     * Creates a new background picture for a user
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when the background picture of a user was successfully updated",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Rest\Post("/users/{username}/background")
     * @Rest\FileParam(name="background", image=true, nullable=true)
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @param $username
     *
     * @return View|Response
     * @throws Exception
     */
    public function postUserBackgroundAction(ParamFetcher $paramFetcher, $username)
    {
        $user = $this
            ->userService
            ->tryToGetUpdatableUser($username, $this->getUser());

        /** @var File $background */
        $background = $paramFetcher->get('background');
        $backgroundName = HashGeneratorService::generateFileHashName($background);

        $background->move(
            $this->kernelProjectDir .
            '/public' .
            $this->uploadUserBackgroundPath,
            $backgroundName
        );

        $user->setBackgroundImage($backgroundName);
        $user->setBackgroundImageUrl(
            $this->backendUrl .
            $this->uploadUserBackgroundPath .
            $backgroundName
        );

        $this->getDoctrine()->getManager()->flush();
        return $this->view($user, Response::HTTP_OK);
    }

}
