<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 11/07/2017
 * Time: 22:47
 */

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Room;
use App\Entity\User;
use App\EventDispatcher\Dispatcher;
use App\Events\MessageEvent;
use App\Exception\MessageNotFoundException;
use App\Exception\RoomNotFoundException;
use App\Form\MessageType;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1/rooms")
 *
 * Class MessageController
 * @package App\Controller
 */
class ChatMessageController extends AbstractFOSRestController
{

    /**
     * @var Dispatcher $dispatcher
     */
    private $dispatcher;

    /**
     * MessageController constructor.
     *
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Fetches the messages of a chatroom, according to some query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of messages matching the query",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Get("/{hash}/messages")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @Rest\QueryParam(name="limit", description="Number of messages to fetch. Defaults to 20", nullable=true)
     * @Rest\QueryParam(name="offset", description="Offset of the query. Defaults to 0", nullable=true)
     * @Rest\QueryParam(name="since", description="Timestamp representing the date from which retrieve messages", nullable=true)
     *
     * @param $hash
     * @param ParamFetcher $paramFetcher
     * @return mixed
     */
    public function getMessageAction($hash, ParamFetcher $paramFetcher): View
    {
        $limit = $paramFetcher->get('limit') ?: 20;
        $offset = $paramFetcher->get('offset') ?: 0;
        $since = $paramFetcher->get('since') ?: 0;

        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Room $room */
        $room = $entityManager
            ->getRepository('App:Room')
            ->findOneBy(array('hash' => $hash));

        if ($room === null) {
            throw new RoomNotFoundException($hash);
        }

        if (!$room->getUsers()->contains($this->getUser())) {
            throw new AccessDeniedHttpException('You are not authorized to see this room\'s messages');
        }

        $messages = $entityManager
            ->getRepository('App:Message')
            ->searchMessages($room, $since, $limit, $offset);

        return $this->view($messages, Response::HTTP_OK);
    }

    /**
     * Adds a new message in a room.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a message was successfully posted",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/{hash}/messages")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $hash
     *
     * @return mixed
     * @throws Exception
     */
    public function postMessageAction(Request $request, $hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Room $room */
        $room = $entityManager
            ->getRepository('App:Room')
            ->findOneBy(array('hash' => $hash));

        if ($room === null) {
            throw new RoomNotFoundException($hash);
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$room->getUsers()->contains($user)) {
            throw new AccessDeniedHttpException('You are not authorized to post a message in this room');
        }

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $message->setHash(
            HashGeneratorService::generateHash(
                $this->getDoctrine()->getRepository('App:Message'),
                $message->getText() . $message->getPosted()->getTimestamp()
            )
        );

        $message->setOwner($user);

        $message->setRoom($room);
        $entityManager->persist($message);
        $entityManager->flush();

        # Send event
        $event = new MessageEvent($message);
        $this->dispatcher->dispatch(MessageEvent::CREATED, $event);

        return $this->view($message, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing message in a room.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a message was successfully updated",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Put("/messages/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return mixed
     */
    public function putMessageAction(Request $request, $hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Message $message */
        $message = $entityManager
            ->getRepository('App:Message')
            ->findOneBy(array('hash' => $hash));

        if ($message === null) {
            throw new MessageNotFoundException($hash);
        }

        if ($message->getOwner() !== $this->getUser()) {
            throw new AccessDeniedHttpException('You are not authorized to update this message');
        }

        $form = $this->createForm(MessageType::class, $message);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $entityManager->flush();

        # Send event
        $event = new MessageEvent($message);
        $this->dispatcher->dispatch(MessageEvent::UPDATED, $event);

        return $this->view($message, Response::HTTP_OK);
    }

    /**
     * Deletes an existing message in a room.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a message was successfully deleted",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Delete("/messages/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return mixed
     */
    public function deleteMessageAction($hash): View
    {
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

        /** @var Message $message */
        $message = $entityManager
            ->getRepository('App:Message')
            ->findOneBy(array('hash' => $hash));

        if (null === $message) {
            throw new MessageNotFoundException($hash);
        }

        if ($message->getOwner() !== $this->getUser()) {
            throw new AccessDeniedHttpException('You are not authorized to update this message');
        }

        $entityManager->remove($message);
        $entityManager->flush();

        # Send event
        $event = new MessageEvent($message);
        $this->dispatcher->dispatch(MessageEvent::DELETED, $event);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

}
