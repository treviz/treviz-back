<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 12:11
 */

namespace App\Controller;

use App\Entity\Enums\ProjectPermissions;
use App\Entity\Project;
use App\Entity\ProjectRole;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\ProjectRoleNotFoundException;
use App\Form\ProjectRoleType;
use App\Service\ProjectMembershipService;
use App\Service\ProjectService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Manages the various roles of a project.
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 * Class ProjectRoleController
 * @package App\Controller
 */
class ProjectRoleController extends AbstractFOSRestController
{
    /** @var ProjectMembershipService */
    private $projectMembershipService;

    /** @var ProjectService $projectService */
    private $projectService;

    /**
     * ProjectRoleController constructor.
     *
     * @param ProjectService $projectService
     * @param ProjectMembershipService $projectMembershipService
     */
    public function __construct(ProjectService $projectService, ProjectMembershipService $projectMembershipService)
    {
        $this->projectMembershipService = $projectMembershipService;
        $this->projectService = $projectService;
    }


    /**
     * Fetches the roles of a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the roles should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the roles of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectRole::class)
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/{hash}/roles")
     *
     * @param $hash
     *
     * @return View
     */
    public function getProjectRolesAction($hash): View
    {
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        $roles = $project->getRoles();
        $globalRoles = $this
            ->getDoctrine()
            ->getRepository('App:ProjectRole')
            ->findBy(array('global' => true));

        foreach ($globalRoles as $globalRole) {
            $roles->add($globalRole);
        }

        return $this->view($roles, Response::HTTP_OK);
    }

    /**
     * Adds a new role to a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to which the role should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="Role",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the role to create"
     *         ),
     *         @SWG\Property(
     *             property="permissions",
     *             type="array",
     *             @SWG\Items(
     *                 type="string"
     *             ),
     *             description="Array of the permissions the role should have",
     *             enum={"UPDATE_PROJECT",
     *                   "DELETE_PROJECT",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_POST",
     *                   "MANAGE_CANDIDACIES",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_CROWD_FUND",
     *                   "MANAGE_JOB"}
     *         ),
     *         @SWG\Property(
     *             property="defaultMember",
     *             type="boolean",
     *             description="True if this role is the role by default of new members of the project."
     *         )
     *     ),
     *     required=true,
     *     description="Role that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created role",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectRole::class)
     *     )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to create this role"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project does not exist"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Submitted data is invalid"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Post("/projects/{hash}/roles")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postProjectRoleAction(Request $request, $hash): View
    {
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();

        $this->projectMembershipService
            ->checkUserRights($user, $project, ProjectPermissions::MANAGE_ROLE);

        $role = new ProjectRole();
        $form = $this->createForm(ProjectRoleType::class, $role);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $role->setProject($project);
        $role->setGlobal(false);

        /*
         * If this is the default member role, unset the previous default member role.
         */
        if ($role->isDefaultMember()) {
            $this->removePreviousDefaultRole($project);
        }

        $role->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:ProjectRole')
            )
        );
        $em->persist($role);
        $em->flush();
        return $this->view($role, Response::HTTP_CREATED);
    }

    /**
     * Adds a new role that can be used in all projects.
     *
     * @SWG\Parameter(
     *     name="Role",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the role to create"
     *         ),
     *         @SWG\Property(
     *             property="permissions",
     *             type="array",
     *             @SWG\Items(
     *                 type="string"
     *             ),
     *             description="Array of the permissions the role should have",
     *             enum={"UPDATE_PROJECT",
     *                   "DELETE_PROJECT",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_POST",
     *                   "MANAGE_CANDIDACIES",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_CROWD_FUND",
     *                   "MANAGE_JOB"}
     *         ),
     *         @SWG\Property(
     *             property="defaultMember",
     *             type="boolean",
     *             description="True if this role is the role by default of new members of the project."
     *         ),
     *         @SWG\Property(
     *             property="defaultCreator",
     *             type="boolean",
     *             description="True if this role is the role by default of new project creators."
     *         )
     *     ),
     *     required=true,
     *     description="Role that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created global role",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectRole::class)
     *     )
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Submitted data is invalid"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Rest\Post("/projects/roles")
     *
     * @param Request $request
     *
     * @return View
     * @throws Exception
     */
    public function postProjectsRoleAction(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();
        $role = new ProjectRole();
        $form = $this->createForm(ProjectRoleType::class, $role);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        /*
         * If this is the default creator role, unset the previous default creator role.
         */
        if ($role->isDefaultCreator()) {
            $previousDefaultRole = $em->getRepository('App:ProjectRole')
                ->findOneBy(array(
                    'defaultCreator' => true,
                    'global' => true
                ));
            if ($previousDefaultRole !== null) {
                $previousDefaultRole->setDefaultMember(false);
            }
        }

        /*
         * If this is the default member role, unset the previous default member role.
         */
        if ($role->isDefaultMember()) {
            $previousDefaultRole = $em->getRepository('App:ProjectRole')
                ->findOneBy(array(
                    'defaultMember' => true,
                    'global' => true
                ));
            if ($previousDefaultRole !== null) {
                $previousDefaultRole->setDefaultMember(false);
            }
        }

        $role->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:ProjectRole')
            )
        );
        $role->setGlobal(true);
        $em->persist($role);
        $em->flush();

        return $this->view($role, Response::HTTP_OK);
    }

    /**
     * Updates an existing role.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the role that should be updated"
     * )
     *
     * @SWG\Parameter(
     *     name="Role",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the role to create"
     *         ),
     *         @SWG\Property(
     *             property="permissions",
     *             type="array",
     *             @SWG\Items(
     *                 type="string"
     *             ),
     *             description="Array of the permissions the role should have",
     *             enum={"MANAGE_PROJECT",
     *                   "MANAGE_PROJECT",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_POST",
     *                   "MANAGE_POST",
     *                   "MANAGE_POST",
     *                   "READ_CANDIDACIES",
     *                   "READ_INVITATIONS",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_DOCUMENTS",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_CROWD_FUND",
     *                   "MANAGE_JOB",
     *                   "MANAGE_JOB",
     *                   "MANAGE_JOB",
     *                   "MANAGE_APPLICATION"}
     *         ),
     *         @SWG\Property(
     *             property="defaultMember",
     *             type="boolean",
     *             description="True if this role is the role by default of new members of the project."
     *         ),
     *         @SWG\Property(
     *             property="defaultCreator",
     *             type="boolean",
     *             description="True if this role is the role by default of new project creators."
     *         )
     *     ),
     *     required=true,
     *     description="Role that should be updated"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated role",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Put("/projects/roles/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function updateProjectRoleAction(Request $request, $hash): View
    {
        $em = $this->getDoctrine()->getManager();
        $role = $this->tryToGetRoleAndCheckRights($hash);

        $form = $this->createForm(ProjectRoleType::class, $role);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        if (!$role->isGlobal()) {
            $role->setDefaultCreator(false);
        }

        if ($role->isDefaultMember()) {
            $this->removePreviousDefaultRole($role->getProject());
        }

        $em->flush();
        return $this->view($role, Response::HTTP_OK);
    }

    /**
     * Deletes an existing role of a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the role that should be deleted"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the role was successfully deleted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Delete("/projects/roles/{hash}")
     *
     * @param $hash
     *
     * @return View
     */
    public function deleteProjectRoleAction($hash): View
    {
        $role = $this->tryToGetRoleAndCheckRights($hash);

        $em = $this->getDoctrine()->getManager();
        $em->remove($role);
        $em->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetRoleAndCheckRights($hash): ProjectRole
    {
        /** @var ProjectRole $role */
        $role = $this
            ->getDoctrine()
            ->getRepository('App:ProjectRole')
            ->findOneBy(array('hash' => $hash));

        if ($role === null) {
            throw new ProjectRoleNotFoundException($hash);
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if ($role->isGlobal() && !$currentUser->isAdmin()) {
            throw new ForbiddenActionException();
        }

        $this->projectMembershipService
            ->checkUserRights($currentUser, $role->getProject(), ProjectPermissions::MANAGE_ROLE);

        return $role;
    }

    private function removePreviousDefaultRole(Project $project) {
        $previousDefaultRole = $this
            ->getDoctrine()
                ->getRepository('App:ProjectRole')
                ->findOneBy(array(
                    'defaultMember' => true,
                    'project' => $project
                ));
            if ($previousDefaultRole !== null) {
                $previousDefaultRole->setDefaultMember(false);
            }
    }
}
