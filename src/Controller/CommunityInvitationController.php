<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace App\Controller;

use App\Entity\CommunityInvitation;
use App\Entity\CommunityMembership;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\User;
use App\Exception\CommunityInvitationNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Form\CommunityInvitationType;
use App\Message\CommunityInvitationAcceptedMessage;
use App\Message\CommunityInvitationCreatedMessage;
use App\Message\CommunityInvitationRejectedMessage;
use App\Service\CommunityRoleService;
use App\Service\CommunityService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class CommunityInvitationController
 * @package App\Controller
 */
class CommunityInvitationController extends AbstractFOSRestController
{
    /** @var CommunityRoleService */
    private $communityRoleService;
    /** @var CommunityService */
    private $communityService;
    /** @var MessageBusInterface */
    private $messageBus;

    /**
     * CommunityInvitationController constructor.
     *
     * @param CommunityRoleService $communityRoleService
     * @param CommunityService $communityService
     * @param MessageBusInterface $messageBus
     */
    public function __construct(CommunityRoleService $communityRoleService,
                                CommunityService $communityService,
                                MessageBusInterface $messageBus)
    {
        $this->communityRoleService = $communityRoleService;
        $this->communityService = $communityService;
        $this->messageBus = $messageBus;
    }


    /**
     * Fetches the invitations of a community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("/communities/{hash}/invitations")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getCommunityInvitationsAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        $user = $this->getUser();

        /** @var CommunityMembership $membership */
        $membership = $entityManager
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array(
                'community' => $community,
                'user' => $user
            ));

        if ($membership !== null && $membership->canManageInvitations()) {
            return $this->view($community->getInvitations(), Response::HTTP_OK);
        }

        $invitation = $entityManager
            ->getRepository('App:CommunityInvitation')
            ->findOneBy(array(
                'community' => $community,
                'user' => $user
            ));

        if (null !== $invitation) {
            return $this->view([$invitation], Response::HTTP_OK);
        }

        throw new ForbiddenActionException();
    }

    /**
     * Fetches the communities of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations specified by the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("/communities/invitations")
     * @Rest\QueryParam(name="user", description="username of the user from whom fetch invitations", nullable=false)
     * @Rest\View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getCommunitiesInvitationsAction(ParamFetcher $paramFetcher): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $entityManager
            ->getRepository('App:User')
            ->findOneBy(array('username' => $paramFetcher->get('user')));

        if ($user !== null) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if ($user === $currentUser) {
                $invitations = $user->getCommunitiesInvitations();
            } else {
                $invitations = $entityManager
                    ->getRepository('App:CommunityInvitation')
                    ->getUserInvitations($user, $currentUser);

            }

            return $this->view($invitations, Response::HTTP_OK);
        }

        return $this->view(null, Response::HTTP_OK);

    }

    /**
     * Creates a new invitation for a community.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created invitation",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Post("/communities/{hash}/invitations")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postCommunityInvitationAction(Request $request, $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $community = $this
            ->communityService
            ->tryToGetCommunityAndCheckRights($hash, $user, CommunityPermissions::MANAGE_INVITATIONS);

        $entityManager = $this->getDoctrine()->getManager();

        $invitation = new CommunityInvitation();
        $form = $this->createForm(CommunityInvitationType::class, $invitation);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException($form);
        }

        $existingInvitation = $entityManager
            ->getRepository('App:CommunityInvitation')
            ->findOneBy(array(
                'community' => $community,
                'user' => $invitation->getUser()
            ));

        if (null !== $existingInvitation) {
            throw new ConflictHttpException('An invitation already exist for this user');
        }

        $invitation->setCommunity($community);
        $invitation->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:CommunityInvitation')
            )
        );

        $entityManager->persist($invitation);
        $entityManager->flush();

        $this->messageBus->dispatch(new CommunityInvitationCreatedMessage($invitation->getId()));

        return $this->view($invitation, Response::HTTP_CREATED);
    }

    /**
     * Removes an invitation from a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the invitation was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Delete("/communities/invitations/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteCommunityInvitationAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invitation = $this->tryToGetInvitation($hash);
        $user = $this->getUser();

        /** @var CommunityMembership $membership */
        $membership = $entityManager
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array(
                'community' => $invitation->getCommunity(),
                'user' => $user
            ));

        if ($invitation->getUser() !== $user &&
            ($membership === null || !$membership->canManageInvitations())) {
            throw new ForbiddenActionException();
        }

        if ($user === $invitation->getUser()) {
            $this->messageBus->dispatch(new CommunityInvitationRejectedMessage(
                $invitation->getCommunity()->getId(),
                $invitation->getUser()->getId()
            ));
        }

        $entityManager->remove($invitation);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Accept an invitation to a community
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the invitation to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The invitation is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the invitation",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The invitation could not be fetched",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Post("/communities/invitations/{hash}/accept")
     *
     * @param $hash
     *
     * @return View
     * @throws Exception
     */
    public function acceptCommunityInvitationAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var CommunityInvitation $invitation */
        $invitation = $this->getDoctrine()
            ->getRepository('App:CommunityInvitation')
            ->findOneBy(array('hash' => $hash));

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $community = $invitation->getCommunity();

        if ($invitation !== null && $invitation->getUser() === $currentUser) {

            $currentMembership = $entityManager
                ->getRepository('App:CommunityMembership')
                ->findOneBy(array(
                    'user' => $currentUser,
                    'community' => $community
                ));

            if ($currentMembership === null) {

                $membership = new CommunityMembership();
                $membership->setHash(
                    HashGeneratorService::generateHash(
                        $entityManager->getRepository('App:CommunityMembership')
                    )
                );

                $defaultMemberRole = $this
                    ->communityRoleService
                    ->getOrCreateDefaultMemberRole($community);

                $membership->setCommunity($community);
                $membership->setRole($defaultMemberRole);
                $membership->setUser($invitation->getUser());

                $this->messageBus->dispatch(new CommunityInvitationAcceptedMessage(
                    $invitation->getCommunity()->getId(),
                    $invitation->getUser()->getId()
                ));

                $entityManager->remove($invitation);
                $entityManager->persist($membership);
                $entityManager->flush();

                return $this->view($membership, Response::HTTP_OK);

            }

            throw new ConflictHttpException('You are already member of the community');

        }

        throw new AccessDeniedHttpException('You cannot do this');

    }

    private function tryToGetInvitation($hash): CommunityInvitation
    {
        /** @var CommunityInvitation $invitation */
        $invitation = $this
            ->getDoctrine()
            ->getRepository('App:CommunityInvitation')
            ->findOneBy(array('hash' => $hash));

        if (null === $invitation) {
            throw new CommunityInvitationNotFoundException($hash);
        }

        return $invitation;
    }

}
