<?php

namespace App\Controller;

use App\Entity\CommunityMembership;
use App\Entity\IdeaEnhancement;
use App\Entity\User;
use App\Exception\EnhancementNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Form\IdeaEnhancementType;
use App\Service\IdeaService;
use App\Utils\HashGeneratorService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class IdeaEnhancementController
 * @package App\Controller
 */
class BrainstormingIdeaEnhancementController extends AbstractFOSRestController
{
    /** @var IdeaService */
    private $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    /**
     * Fetches the enhancements of an existing idea.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array containing the enhancements of an idea",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/ideas/{hash}/enhancements")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getIdeaEnhancementsAction(string $hash): View
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user);
        return $this->view($idea->getEnhancements(), Response::HTTP_OK);
    }

    /**
     * Fetch a specific idea enhancement.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a enhancement was successfully fetched",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Get("/brainstorming-sessions/ideas/enhancements/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getIdeaEnhancementAction($hash)
    {
        /** @var User $user */
        $user = $this->getUser();
        $enhancement = $this->tryToGetEnhancementAndCheckRights($hash);
        $this->ideaService->checkRights($enhancement->getIdea(), $user);

        return $this->view($enhancement, Response::HTTP_OK);
    }

    /**
     * Adds a enhancement to an existing idea.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a enhancement was successfully created",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Post("/brainstorming-sessions/ideas/{hash}/enhancements")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function postIdeaEnhancementAction(Request $request, $hash)
    {
        /** @var User $user */
        $user = $this->getUser();
        $idea = $this->ideaService
            ->tryToGetIdeaAndCheckRights($hash, $user, false, true);

        $enhancement = new IdeaEnhancement();
        $form = $this->createForm(IdeaEnhancementType::class, $enhancement);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();

        $enhancement->setIdea($idea);
        $enhancement->setAuthor($user);
        $enhancement->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:IdeaEnhancement')
            )
        );

        $em->persist($enhancement);
        $em->flush();

        return $this->view($enhancement, Response::HTTP_CREATED);
    }

    /**
     * Updates an existing idea enhancement.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a enhancement was successfully updated",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Put("/brainstorming-sessions/ideas/enhancements/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putIdeaEnhancementAction(Request $request, $hash)
    {
        $enhancement = $this->tryToGetEnhancementAndCheckRights($hash, true);

        $form = $this->createForm(IdeaEnhancementType::class, $enhancement);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->view($enhancement, Response::HTTP_OK);
    }

    /**
     * Deletes an existing idea enhancement.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a enhancement was successfully deleted",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Rest\Delete("/brainstorming-sessions/ideas/enhancements/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function deleteIdeaEnhancementAction($hash)
    {
        $enhancement = $this->tryToGetEnhancementAndCheckRights($hash, true);
        $em = $this->getDoctrine()->getManager();
        $em->remove($enhancement);
        $em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetEnhancementAndCheckRights(string $hash, bool $write = false)
    {
        /** @var IdeaEnhancement $enhancement */
        $enhancement = $this
            ->getDoctrine()
            ->getRepository('App:IdeaEnhancement')
            ->findOneBy(array('hash' => $hash));

        if (null === $enhancement) {
            throw new EnhancementNotFoundException($hash);
        }

        if ($write) {
            /** @var User $user */
            $user = $this->getUser();
            if ($enhancement->getAuthor() !== $this && !$user->isAdmin()) {
                /** @var CommunityMembership $membership */
                $membership = $this
                    ->getDoctrine()
                    ->getRepository('App:CommunityMembership')
                    ->findOneBy(array(
                        'user' => $this->getUser(),
                        'community' => $enhancement->getIdea()->getSession()->getCommunity()
                    ));

                if (null === $membership || !$membership->canManageBrainstormingIdeas()) {
                    throw new ForbiddenActionException();
                }
            }
        }

        return $enhancement;
    }
}
