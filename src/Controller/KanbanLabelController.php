<?php

namespace App\Controller;

use App\Entity\Label;
use App\Exception\LabelNotFoundException;
use App\Form\LabelType;
use App\Service\BoardService;
use App\Utils\HashGeneratorService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Label controller.
 *
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 */
class KanbanLabelController extends AbstractFOSRestController
{
    /** @var BoardService */
    private $boardService;

    /**
     * ColumnController constructor.
     *
     * @param BoardService $boardService
     */
    public function __construct(BoardService $boardService)
    {
        $this->boardService = $boardService;
    }

    /**
     * Lists all labels of a board.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of labels matching the query",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the board's labels",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards/{hash}/labels")
     *
     * @param string $hash Hash of the board from which fetch the label.
     * @return View
     */
    public function getBoardLabelsAction(string $hash): View
    {
        $board = $this
            ->boardService
            ->tryToGetBoardAndCheckAccess(
                $hash,
                $this->getUser()
            );

        return $this->view($board->getLabels(), Response::HTTP_OK);
    }

    /**
     * Finds and returns a label.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Label is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No label is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the label",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Get("/boards/labels/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function getBoardLabelAction($hash): View
    {
        $label = $this
            ->tryToGetLabelAndCheckRights($hash, false);
        return $this->view($label, Response::HTTP_OK);
    }


    /**
     * Creates a new label.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Label is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the label",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Post("/boards/{hash}/labels")
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function postBoardLabelAction(Request $request, $hash)
    {
        $board = $this
            ->boardService
            ->tryToGetBoardAndCheckAccess(
                $hash,
                $this->getUser(),
                true
            );

        $label = new Label();
        $form = $this->createForm(LabelType::class, $label);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();

        $board->addLabel($label);
        $label->setHash(
            HashGeneratorService::generateHash(
                $em->getRepository('App:Label')
            )
        );
        $em->persist($label);
        $em->flush();

        return $this->view($label, Response::HTTP_CREATED);
    }


    /**
     * Edit an existing label.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Label is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to update the label",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Put("/boards/labels/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return View
     */
    public function putLabelAction(Request $request, $hash): View
    {
        $label = $this
            ->tryToGetLabelAndCheckRights(
                $hash,
                true
            );

        $form = $this->createForm(LabelType::class, $label);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->view($label, Response::HTTP_OK);
    }

    /**
     * Deletes a label entity.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Label is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the label",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No label is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Rest\Delete("/boards/labels/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteBoardLabelAction($hash): View
    {
        $label = $this
            ->tryToGetLabelAndCheckRights(
                $hash,
                true
            );
        $em = $this->getDoctrine()->getManager();
        $em->remove($label);
        $em->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    private function tryToGetLabelAndCheckRights(string $hash, bool $write = false) {
        /** @var Label $label */
        $label = $this
            ->getDoctrine()
            ->getRepository('App:Label')
            ->findOneBy(array('hash' => $hash));

        if (null === $label) {
            throw new LabelNotFoundException($hash);
        }

        $this
            ->boardService
            ->checkUserCanAccessBoard(
                $label->getBoard(),
                $this->getUser(),
                $write
            );

        return $label;
    }

}
