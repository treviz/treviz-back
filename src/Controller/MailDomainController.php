<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 21/01/2019
 * Time: 22:33
 */

namespace App\Controller;


use App\Entity\MailDomain;
use App\Exception\MailDomainAlreadyExistsException;
use App\Exception\MailDomainNotFoundException;
use App\Form\MailDomainType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class MailDomainController
 *
 * @Rest\Route("/v1")
 *
 * @package App\Controller
 * @Security("is_granted('ROLE_ADMIN')")
 */
class MailDomainController extends AbstractFOSRestController
{

    /**
     * @Rest\Get("/platform/mail-domains")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of mail domains",
     * )
     *
     * @SWG\Tag(name="Settings")
     *
     * @return View
     */
    public function getMailDomainsAction(): View
    {
        $mailDomains = $this
            ->getDoctrine()
            ->getRepository('App:MailDomain')
            ->findAll();

        return $this->view($mailDomains, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/platform/mail-domains")
     *
     * @SWG\Response(
     *     response=201,
     *     description="The domain was successfully created",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="An error in the request prevented the domain from being created",
     * )
     *
     * @SWG\Tag(name="settings")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postMailDomainAction(Request $request): View
    {
        $domain = new MailDomain();
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(MailDomainType::class, $domain);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $existingDomain = $entityManager
            ->getRepository('App:MailDomain')
            ->findOneBy(array(
                'domain' => $domain->getDomain()
            ));

        if (null !== $existingDomain) {
            throw new MailDomainAlreadyExistsException();
        }

        $entityManager->persist($domain);
        $entityManager->flush();
        return $this->view($domain, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Put("/platform/mail-domains/{domain}.{ext}")
     *
     * @SWG\Response(
     *     response=200,
     *     description="The domain was successfully upadted",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="The domain could not be found",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="An error in the request prevented the domain from being updated",
     * )
     *
     * @SWG\Tag(name="settings")
     *
     * @param Request $request
     *
     * @param         $domain
     *
     * @param         $ext
     *
     * @return View
     */
    public function putMailDomainAction(Request $request, $domain, $ext): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $domainRepository = $entityManager
            ->getRepository('App:MailDomain');
        $domain = $domainRepository
            ->findOneBy(array(
                'domain' => "$domain.$ext"
            ));

        if (null === $domain) {
            throw new MailDomainNotFoundException("$domain.$ext");
        }

        $form = $this->createForm(MailDomainType::class, $domain);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $existingDomain = $domainRepository
            ->findOneBy(array(
                'domain' => $domain->getDomain()
            ));

        if (null !== $existingDomain) {
            throw new MailDomainAlreadyExistsException();
        }

        $entityManager->flush();
        return $this->view($domain, Response::HTTP_OK);

    }

    /**
     * @Rest\Delete("/platform/mail-domains/{domain}.{ext}")
     *
     * @SWG\Response(
     *     response=204,
     *     description="The domain was deleted",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="The domain could not be found",
     * )
     *
     * @SWG\Tag(name="settings")
     *
     * @param         $domain
     *
     * @param         $ext
     *
     * @return View
     */
    public function deleteMailDomainAction($domain, $ext): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $domain = $entityManager
            ->getRepository('App:MailDomain')
            ->findOneBy(array(
                'domain' => "$domain.$ext"
            ));

        if (null === $domain) {
            throw new MailDomainNotFoundException("$domain.$ext");
        }

        $entityManager->remove($domain);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

}
