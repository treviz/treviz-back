<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 30/10/2017
 * Time: 15:25
 */

namespace App\Controller;

use App\Entity\ProjectInvitation;
use App\Entity\ProjectMembership;
use App\Entity\User;
use App\Exception\ForbiddenActionException;
use App\Exception\ProjectInvitationNotFoundException;
use App\Form\ProjectInvitationType;
use App\Message\ProjectInvitationAcceptedMessage;
use App\Message\ProjectInvitationCreatedMessage;
use App\Message\ProjectInvitationRejectedMessage;
use App\Service\ProjectMembershipService;
use App\Service\ProjectRoleService;
use App\Service\ProjectService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;


/**
 * @Security("is_granted('ROLE_USER')")
 * @Rest\Route("/v1")
 *
 * Class ProjectInvitationController
 * @package App\Controller
 */
class ProjectInvitationController extends AbstractFOSRestController
{
    /** @var ProjectRoleService */
    private $projectRoleService;
    /** @var MessageBusInterface */
    private $messageBus;
    /** @var ProjectMembershipService */
    private $projectMembershipService;
    /** @var ProjectService */
    private $projectService;

    /**
     * ProjectInvitationController constructor.
     *
     * @param ProjectRoleService $projectRoleService
     * @param MessageBusInterface $messageBus
     * @param ProjectMembershipService $projectMembershipService
     * @param ProjectService $projectService
     */
    public function __construct(
        ProjectRoleService $projectRoleService,
        MessageBusInterface $messageBus,
        ProjectMembershipService $projectMembershipService,
        ProjectService $projectService
    )
    {
        $this->projectRoleService = $projectRoleService;
        $this->messageBus = $messageBus;
        $this->projectMembershipService = $projectMembershipService;
        $this->projectService = $projectService;
    }

    /**
     * Fetches the invitations of a project.
     * Only a user with the permission can see all the invitation of the project.
     * An invited user can only see his/her own invitation.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the invitations should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectInvitation::class, groups={"Default", "project"})
     *     )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to fetch invitations for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/{hash}/invitations")
     * @Rest\View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getProjectInvitationsAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $user */
        $user = $this->getUser();

        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $project);

        if ($user->isAdmin() || ($membership !== null && $membership->canManageInvitations())) {
            return $this->view($project->getInvitations(), Response::HTTP_OK);
        }

        $invitation = $entityManager
            ->getRepository('App:ProjectInvitation')
            ->findOneBy(array(
                'project' => $project,
                'user'    => $user
            ));

        if (null === $invitation) {
            throw new ForbiddenActionException();
        }

        return $this->view([$invitation], Response::HTTP_OK);
    }

    /**
     * Fetches the projects of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations specified by the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectInvitation::class, groups={"Default", "user"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Get("/projects/invitations")
     * @Rest\QueryParam(name="user", description="username of the user from whom fetch invitations", nullable=false)
     *
     * @Rest\View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getProjectsInvitationsAction(ParamFetcher $paramFetcher): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $entityManager
            ->getRepository('App:User')
            ->findOneBy(array('username' => $paramFetcher->get('user')));

        if ($user !== null) {
            $currentUser = $this->getUser();

            if ($user === $currentUser) {
                $invitations = $user->getProjectsInvitations();
            } else {
                $invitations = $entityManager
                    ->getRepository('App:ProjectInvitation')
                    ->getUserInvitations($user, $currentUser);
            }

            return $this->view($invitations, Response::HTTP_OK);
        }

        return $this->view([], Response::HTTP_OK);

    }

    /**
     * Creates a new invitation for a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project for which the invitation should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="invitation",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="user",
     *             type="string",
     *             description="Name of the user to invite"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             description="Message to justify the invitation"
     *         )
     *     ),
     *     required=true,
     *     description="Invitation that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created invitation",
     *     @Model(type=ProjectInvitation::class, groups={"Default"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to create invitations for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data was submitted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Post("/projects/{hash}/invitations")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postProjectInvitationAction(Request $request, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $this
            ->projectService
            ->tryToFetchProject($hash);

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /** @var  $membership */
        $membership = $this
            ->projectMembershipService
            ->getUserMembership($currentUser, $project);

        if (!$currentUser->isAdmin() && (null === $membership || !$membership->canManageInvitations())) {
            throw new AccessDeniedHttpException(
                'You cannot candidate to a project you are part of, candidate or invited to.'
            );
        }

        $invitation = new ProjectInvitation();
        $form = $this->createForm(ProjectInvitationType::class, $invitation);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $user = $invitation->getUser();
        $existingInvitation = $entityManager
            ->getRepository('App:ProjectInvitation')
            ->findOneBy(array(
                'user' => $user,
                'project' => $project
            ));

        if (null !== $existingInvitation) {
            throw new ConflictHttpException('This user is already invited to this project');
        }

        $invitation->setProject($project);
        $invitation->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:ProjectInvitation')
            )
        );

        $entityManager->persist($invitation);
        $entityManager->flush();

        $this->messageBus->dispatch(new ProjectInvitationCreatedMessage($invitation->getId()));

        return $this->view($invitation, Response::HTTP_CREATED);
    }

    /**
     * Removes an invitation from a project.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the invitation is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to delete invitations for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Invitation does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Rest\Delete("/projects/invitations/{hash}")
     * @param $hash
     * @return View
     */
    public function deleteProjectInvitationAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invitation = $this->tryToGetInvitation($hash);

        $user = $this->getUser();
        $membership = $this
            ->projectMembershipService
            ->getUserMembership($user, $invitation->getProject());

        if ($invitation->getUser() !== $user
            && ($membership === null || !$membership->canManageInvitations())
        ) {
            throw new ForbiddenActionException();
        }

        if ($invitation->getUser() === $user) {
            $this->messageBus->dispatch(new ProjectInvitationRejectedMessage(
                $invitation->getProject()->getId(),
                $invitation->getUser()->getId()
            ));
        }

        $entityManager->remove($invitation);
        $entityManager->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Accept an invitation to a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the invitation to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The invitation is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the invitation",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The invitation could not be fetched",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Post("/projects/invitations/{hash}/accept")
     *
     * @param $hash
     *
     * @return View
     * @throws Exception
     */
    public function acceptProjectInvitationAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProjectInvitation $invitation */
        $invitation = $entityManager
            ->getRepository('App:ProjectInvitation')
            ->findOneBy(array('hash' => $hash));

        $currentUser = $this->getUser();
        if ($invitation->getUser() !== $currentUser) {
            throw new ForbiddenActionException();
        }

        $project = $invitation->getProject();
        if (null === $project) {
            throw new BadRequestHttpException(
                'This invitation is not linked to any project'
            );
        }

        $currentUserMembership = $this
            ->projectMembershipService
            ->getUserMembership($currentUser, $project);

        if (null !== $currentUserMembership) {
            throw new ConflictHttpException('You are already member of this project');
        }

        $membership = new ProjectMembership();
        $membership->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:ProjectMembership')
            )
        );

        /*
         * Fetches the default member role.
         * If there is none, create some.
         */
        $defaultMemberRole = $this
            ->projectRoleService
            ->getOrCreateDefaultRole($project);

        $membership->setProject($project);
        $membership->setRole($defaultMemberRole);
        $membership->setUser($invitation->getUser());
        $entityManager->remove($invitation);
        $entityManager->persist($membership);
        $entityManager->flush();

        $this->messageBus->dispatch(new ProjectInvitationAcceptedMessage(
            $invitation->getProject()->getId(),
            $invitation->getUser()->getId()
        ));

        return $this->view($membership, Response::HTTP_OK);
    }

    private function tryToGetInvitation($hash): ProjectInvitation
    {
        /** @var ProjectInvitation $invitation */
        $invitation = $this
            ->getDoctrine()
            ->getRepository('App:ProjectInvitation')
            ->findOneBy(array('hash' => $hash));
        if (null === $invitation) {
            throw new ProjectInvitationNotFoundException($hash);
        }

        return $invitation;
    }

}
