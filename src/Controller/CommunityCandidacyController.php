<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace App\Controller;

use App\Entity\CommunityCandidacy;
use App\Entity\CommunityMembership;
use App\Entity\Enums\CommunityPermissions;
use App\Entity\User;
use App\Exception\CommunityCandidacyNotFoundException;
use App\Exception\ForbiddenActionException;
use App\Exception\UserNotFoundException;
use App\Form\CommunityCandidacyType;
use App\Message\CommunityCandidacyAcceptedMessage;
use App\Message\CommunityCandidacyCreatedMessage;
use App\Message\CommunityCandidacyRejectedMessage;
use App\Service\CommunityMembershipService;
use App\Service\CommunityRoleService;
use App\Service\CommunityService;
use App\Utils\HashGeneratorService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @Rest\Route("/v1")
 *
 * Class CommunityCandidacyController
 * @package App\Controller
 */
class CommunityCandidacyController extends AbstractFOSRestController
{

    /** @var CommunityMembershipService */
    private $communityMembershipService;
    /** @var CommunityRoleService */
    private $communityRoleService;
    /** @var CommunityService */
    private $communityService;
    /** @var MessageBusInterface */
    private $messageBus;

    /**
     * CommunityCandidacyController constructor.
     *
     * @param CommunityMembershipService $communityMembershipService
     * @param CommunityRoleService $communityRoleService
     * @param CommunityService $communityService
     * @param MessageBusInterface $messageBus
     */
    public function __construct(CommunityMembershipService $communityMembershipService,
                                CommunityRoleService $communityRoleService,
                                CommunityService $communityService,
                                MessageBusInterface $messageBus)
    {
        $this->communityMembershipService = $communityMembershipService;
        $this->communityRoleService = $communityRoleService;
        $this->communityService = $communityService;
        $this->messageBus = $messageBus;
    }

    /**
     * Fetches the candidacies of a community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("/communities/{hash}/candidacies")
     * @Rest\View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return View
     */
    public function getCommunityCandidaciesAction($hash): View
    {
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        /** @var User $user */
        $user = $this->getUser();

        /** @var CommunityMembership $membership */
        $membership = $this
            ->communityMembershipService
            ->getUserMembership($user, $community);

        if (null === $membership || !$membership->canManageCandidacies()) {
            $candidacy = $this
                ->getDoctrine()
                ->getRepository('App:CommunityCandidacy')
                ->findOneBy(array(
                    'community' => $community,
                    'user' => $user
                ));

            if (null === $candidacy) {
                throw new ForbiddenActionException();
            } else {
                return $this->view([$candidacy], Response::HTTP_OK);
            }
        }

        return $this->view($community->getCandidacies(), Response::HTTP_OK);

    }

    /**
     * Fetches the communities of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies specified by the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Get("/communities/candidacies")
     * @Rest\QueryParam(name="user", description="username of the user from whom fetch candidacies", nullable=false)
     * @Rest\View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getCommunitiesCandidaciesAction(ParamFetcher $paramFetcher): View
    {
        $username = $paramFetcher->get('user');
        /** @var User $user */
        $user = $this
            ->getDoctrine()
            ->getRepository('App:User')
            ->findOneBy(array('username' => $username));

        if (null === $user) {
            throw new UserNotFoundException($username);
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $candidacies = $user === $currentUser ?
            $user->getCommunitiesCandidacies() :
            $this
                ->getDoctrine()
                ->getRepository('App:CommunityCandidacy')
                ->getUserCandidacies($user, $currentUser);

        return $this->view($candidacies, Response::HTTP_OK);

    }

    /**
     * Creates a new candidacy for a community.
     * If the community is open, this creates and returns a new membership for the current user.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created candidacy",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Post ("/communities/{hash}/candidacies")
     *
     * @param Request $request
     * @param         $hash
     *
     * @return View
     * @throws Exception
     */
    public function postCommunityCandidacyAction(Request $request, $hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $community = $this
            ->communityService
            ->tryToGetCommunity($hash);

        /** @var User $user */
        $user = $this->getUser();

        $membership = $this
            ->communityMembershipService
            ->getUserMembership($user, $community);

        $invitation = $entityManager
            ->getRepository('App:CommunityInvitation')
            ->findOneBy(array(
                'community' => $community,
                'user' => $user
            ));

        $candidacy = $entityManager
            ->getRepository('App:CommunityCandidacy')
            ->findOneBy(array(
                'community' => $community,
                'user' => $user
            ));

        if (null !== $membership || null !== $invitation || null !== $candidacy) {
            throw new ConflictHttpException(
                'You cannot candidate to a community you are part of, candidate or invited to.'
            );
        }

        $candidacy = new CommunityCandidacy();
        $form = $this->createForm(CommunityCandidacyType::class, $candidacy);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(json_encode($form));
        }

        $candidacy->setCommunity($community);
        $candidacy->setUser($user);
        $candidacy->setHash(
            HashGeneratorService::generateHash(
                $entityManager->getRepository('App:CommunityCandidacy')
            )
        );

        $entityManager->persist($candidacy);
        $entityManager->flush();

        $this
            ->messageBus
            ->dispatch(new CommunityCandidacyCreatedMessage($candidacy->getId()));

        return $this->view($candidacy, Response::HTTP_CREATED);
    }

    /**
     * Removes a candidacy from a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the candidacy was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Delete("/communities/candidacies/{hash}")
     *
     * @param $hash
     * @return View
     */
    public function deleteCommunityCandidacyAction($hash): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var CommunityCandidacy $candidacy */
        $candidacy = $entityManager
            ->getRepository('App:CommunityCandidacy')
            ->findOneBy(array('hash' => $hash));

        if (null === $candidacy) {
            throw new CommunityCandidacyNotFoundException($hash);
        }

        $user = $this->getUser();

        /** @var CommunityMembership $membership */
        $membership = $entityManager
            ->getRepository('App:CommunityMembership')
            ->findOneBy(array(
                'community' => $candidacy->getCommunity(),
                'user' => $user
            ));

        if ($candidacy->getUser() !== $user &&
            (null === $membership || !$membership->canManageCandidacies())) {
            throw new ForbiddenActionException();
        }

        if ($candidacy->getUser() !== $user) {
            $this
                ->messageBus
                ->dispatch(new CommunityCandidacyRejectedMessage(
                    $candidacy->getCommunity()->getId(),
                    $candidacy->getUser()->getId()
                ));
        }

        $entityManager->remove($candidacy);
        $entityManager->flush();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Accepts a candidacy from a community.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the candidacy to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The candidacy is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="The submitted role is invalid",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the candidacy",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The candidacy could not be fetched",
     * )
     *
     *
     * @SWG\Tag(name="communities")
     *
     * @Rest\Post("/communities/candidacies/{hash}/accept")
     * @param $hash
     *
     * @return View
     * @throws Exception
     */
    public function acceptCommunityCandidacyAction($hash): View
    {
        $candidacy = $this->tryToGetCandidacy($hash);

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $community = $candidacy->getCommunity();
        $user = $candidacy->getUser();

        $this
            ->communityMembershipService
            ->checkUserRights($currentUser, $community, CommunityPermissions::MANAGE_CANDIDACIES);

        $entityManager = $this->getDoctrine()->getManager();
        $newMembership = new CommunityMembership();
        $newMembership->setCommunity($community);
        $newMembership->setUser($user);
        $newMembership->setHash(
            HashGeneratorService::generateHash(
                $entityManager
                    ->getRepository('App:CommunityMembership')
            )
        );

        $role = $this
            ->communityRoleService
            ->getOrCreateDefaultMemberRole($community);

        $newMembership->setRole($role);

        $this
            ->messageBus
            ->dispatch(new CommunityCandidacyAcceptedMessage(
                $candidacy->getCommunity()->getId(),
                $candidacy->getUser()->getId()
            ));

        $entityManager->persist($newMembership);
        $entityManager->remove($candidacy);
        $entityManager->flush();

        return $this->view($newMembership, Response::HTTP_OK);
    }

    private function tryToGetCandidacy(string $hash): CommunityCandidacy
    {
        /** @var CommunityCandidacy $candidacy */
        $candidacy = $this
            ->getDoctrine()
            ->getRepository('App:CommunityCandidacy')
            ->findOneBy(array('hash' => $hash));

        if (null === $candidacy) {
            throw new CommunityCandidacyNotFoundException($hash);
        }

        return $candidacy;
    }

}
