<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 15/10/2018
 * Time: 22:39
 */

namespace App\Events;


use App\Entity\Message;
use Symfony\Contracts\EventDispatcher\Event;

class MessageEvent extends Event
{

    const CREATED = 'chat.message.created';
    const UPDATED = 'chat.message.updated';
    const DELETED = 'chat.message.deleted';

    protected $message;

    public function __construct(Message $message)
    {
        $this->message= $message;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }
}
