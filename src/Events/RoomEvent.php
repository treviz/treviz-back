<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 15/10/2018
 * Time: 22:39
 */

namespace App\Events;


use App\Entity\Room;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class RoomEvent extends Event
{

    const CREATED = 'chat.room.created';
    const UPDATED = 'chat.room.updated';
    const DELETED = 'chat.room.deleted';

    protected $room;
    protected $author;

    public function __construct(Room $room, User $author)
    {
        $this->room= $room;
        $this->author = $author;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }
}
