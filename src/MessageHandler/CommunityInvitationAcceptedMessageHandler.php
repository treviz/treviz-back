<?php


namespace App\MessageHandler;


use App\Entity\Community;
use App\Entity\CommunityMembership;
use App\Message\CommunityInvitationAcceptedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommunityInvitationAcceptedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(CommunityInvitationAcceptedMessage $message)
    {
        /** @var Community $community */
        $community = $this->entityManager
            ->getRepository('App:Community')
            ->find($message->getCommunityId());
        $user = $this
            ->entityManager
            ->getRepository('App:User')
            ->find($message->getNewMemberId());

        $url = $this->frontendUrl . '/communities/' . $community->getHash();
        /** @var CommunityMembership $membership */
        foreach ($community->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnInvitationAccepted() && $membership->canManageInvitations()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $this->translator->trans('updates.community.invitations.accepted.subject'),
                    $this->translator->trans(
                        'updates.community.invitations.accepted.message',
                        array(
                            'name' => $user->getUsername(),
                            'community' => $community->getName()
                        )
                    ),
                    $this->translator->trans('updates.actions.community'),
                    $url
                );
            }
        }
    }
}