<?php


namespace App\MessageHandler;


use App\Entity\CommunityMembership;
use App\Entity\Post;
use App\Message\PostCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PostCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(PostCreatedMessage $message)
    {
        /** @var Post $post */
        $post = $this->entityManager
            ->getRepository('App:Post')
            ->find($message->getEntityId());

        $community = $post->getCommunity();
        $project = $post->getProject();
        /** @var CommunityMembership $membership */
        if ($community !== null) {
            $url = $this->frontendUrl . "/communities/" . $community->getHash();
            foreach ($community->getMemberships() as $membership) {
                if ($membership->getPreferences()->isOnPost() && $membership->getUser() !== $post->getAuthor()) {
                    $this->mailerService->sendUpdateMail(
                        $membership->getUser(),
                        $this->translator->trans('updates.community.post.created.subject'),
                        $this->translator->trans(
                            'updates.community.post.created.message',
                            array(
                                'name' => $post->getAuthor()->getUsername(),
                                'community' => $community->getName()
                            )
                        ),
                        $this->translator->trans('updates.actions.community'),
                        $url
                    );
                }
            }
        }
        if ($project !== null) {
            $url = $this->frontendUrl . "/projects/" . $project->getHash();
            foreach ($project->getMemberships() as $membership) {
                if ($membership->getPreferences()->isOnPost() && $membership->getUser() !== $post->getAuthor()) {
                    $this->mailerService->sendUpdateMail(
                        $membership->getUser(),
                        $this->translator->trans('updates.project.post.created.subject'),
                        $this->translator->trans(
                            'updates.project.post.created.message',
                            array(
                                'name' => $post->getAuthor()->getUsername(),
                                'project' => $project->getName()
                            )
                        ),
                        $this->translator->trans('updates.actions.project'),
                        $url
                    );
                }
            }
        }
    }
}