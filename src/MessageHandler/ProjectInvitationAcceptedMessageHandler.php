<?php


namespace App\MessageHandler;


use App\Entity\Project;
use App\Entity\ProjectMembership;
use App\Entity\User;
use App\Message\ProjectInvitationAcceptedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProjectInvitationAcceptedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ProjectInvitationAcceptedMessage $message)
    {
        /** @var Project $invitation */
        $project = $this->entityManager
            ->getRepository('App:Project')
            ->find($message->getProjectId());

        /** @var User $user */
        $user = $this->entityManager
            ->getRepository('App:User')
            ->find($message->getNewMemberId());

        $url = $this->frontendUrl . '/projects/' . $project->getHash();
        /** @var ProjectMembership $membership */
        foreach ($project->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnInvitationAccepted() && $membership->canManageInvitations()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $this->translator->trans('updates.project.invitations.accepted.subject'),
                    $this->translator->trans(
                        'updates.project.invitations.accepted.message',
                        array(
                            'project' => $project->getName(),
                            'name' => $user->getUsername()
                        )
                    ),
                    $this->translator->trans('updates.actions.project'),
                    $url
                );
            }
        }
    }
}