<?php


namespace App\MessageHandler;


use App\Entity\User;
use App\Message\UserCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserCreatedMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MailerService
     */
    private $service;
    /**
     * @var string
     */
    private $frontendUrl;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $entityManager,
                                MailerService $service,
                                LoggerInterface $logger,
                                string $frontendUrl)
    {
        $this->service = $service;
        $this->entityManager = $entityManager;
        $this->frontendUrl = $frontendUrl;
        $this->logger = $logger;
    }

    public function __invoke(UserCreatedMessage $message)
    {
        $this->logger->info("Dispatch UserCreatedMessage for user " . $message->getEntityId());
        /** @var User $user */
        $user = $this->entityManager->getRepository('App:User')->find($message->getEntityId());
        if ($user != null) {
            $this->logger->info("Sending registration confirmation mail to: " . $user->getEmail());
            $url = $this->frontendUrl . '/confirm-registration?user=' . $user->getUsername() . '&token=' . $user->getConfirmationToken();
            try {
                $this->service->sendRegistrationMail($user, $url);
            } catch (TransportExceptionInterface $e) {
                $this->logger->error("Mail could not be delivered :" . $e->getMessage());
            }
        } else {
            $this->logger->info("Could not send registration mail to unknown user");
        }
    }
}