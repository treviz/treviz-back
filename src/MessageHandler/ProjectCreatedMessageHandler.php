<?php


namespace App\MessageHandler;


use App\Entity\Community;
use App\Entity\CommunityMembership;
use App\Entity\Project;
use App\Entity\ProjectInvitation;
use App\Message\ProjectCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProjectCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ProjectCreatedMessage $message)
    {
        /** @var Project $project */
        $project = $this->entityManager
            ->getRepository('App:Project')
            ->find($message->getEntityId());
        $communities = $project->getCommunities();
        if(sizeof($communities->toArray()) > 0) {
            /** @var Community $community */
            foreach ($communities as $community) {
                $url = $this->frontendUrl . '/projects/' . $project->getHash();
                $prefsUrl = $this->frontendUrl . '/communities/' . $community->getHash() . '#preferences';
                /** @var CommunityMembership $membership */
                foreach ($community->getMemberships() as $membership) {
                    if ($membership->getPreferences()->isOnNewProject()) {
                        $this->mailerService->sendUpdateMail(
                            $membership->getUser(),
                            $this->translator->trans('updates.community.projects.new.subject'),
                            $this->translator->trans(
                                'updates.community.projects.new.message',
                                array(
                                    'community' => $community->getName(),
                                    'project' => $project->getName()
                                )
                            ),
                            $this->translator->trans('updates.actions.project'),
                            $url,
                            $prefsUrl
                        );
                    }
                }
            }
        }
    }
}