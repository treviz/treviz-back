<?php


namespace App\MessageHandler;


use App\Entity\ProjectCandidacy;
use App\Entity\ProjectMembership;
use App\Message\ProjectCandidacyCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProjectCandidacyCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ProjectCandidacyCreatedMessage $message)
    {
        /** @var ProjectCandidacy $candidacy */
        $candidacy = $this->entityManager
            ->getRepository('App:ProjectCandidacy')
            ->find($message->getEntityId());
        $project = $candidacy->getProject();

        $url = $this->frontendUrl . '/projects/' . $project->getHash();
        /** @var ProjectMembership $membership */
        foreach ($project->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnCandidacy() && $membership->canManageCandidacies()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $this->translator->trans('updates.project.candidacies.created.subject'),
                    $this->translator->trans(
                        'updates.project.candidacies.created.message',
                        array(
                            'project' => $project->getName(),
                            'name' => $candidacy->getUser()->getUsername()
                        )
                    ),
                    $this->translator->trans('updates.actions.project'),
                    $url
                );
            }
        }
    }
}