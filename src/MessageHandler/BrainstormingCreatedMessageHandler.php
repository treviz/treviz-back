<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:27
 */

namespace App\MessageHandler;


use App\Entity\CommunityMembership;
use App\Message\BrainstormingCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class BrainstormingEventSubscriber
 *
 * Listens to brainstorming creations and update, to trigger the appropriate
 * notifications to be sent.
 *
 * @package App\EventSubscriber
 */
class BrainstormingCreatedMessageHandler implements MessageHandlerInterface
{
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(BrainstormingCreatedMessage $message)
    {
        $brainstorming = $this
            ->entityManager
            ->getRepository('App:Session')
            ->find($message->getEntityId());

        $community = $brainstorming->getCommunity();
        $url = $this->frontendUrl . "/communities/" . $community->getHash();
        /** @var CommunityMembership $membership */
        if ($community !== null) {
            foreach ($community->getMemberships() as $membership) {
                if ($membership->getPreferences()->isOnNewBrainstorming()) {
                    $this->mailerService->sendUpdateMail(
                        $membership->getUser(),
                        $this->translator->trans('updates.brainstorming.created.subject'),
                        $this->translator->trans(
                            'updates.brainstorming.created.message',
                            array(
                                'brainstorming' => $brainstorming->getName(),
                                'community' => $community->getName()
                            )
                        ),
                        $this->translator->trans('updates.actions.community'),
                        $url
                    );
                }
            }
        }
    }
}
