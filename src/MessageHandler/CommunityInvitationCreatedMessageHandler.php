<?php


namespace App\MessageHandler;


use App\Entity\CommunityInvitation;
use App\Message\CommunityInvitationCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommunityInvitationCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(CommunityInvitationCreatedMessage $message)
    {
        /** @var CommunityInvitation $invitation */
        $invitation = $this->entityManager
            ->getRepository('App:CommunityInvitation')
            ->find($message->getEntityId());
        $community = $invitation->getCommunity();
        $user = $invitation->getUser();

        if ($user->getPreferences()->isOnInvitation()) {
            $url = $this->frontendUrl . '/communities/' . $community->getHash();
            $prefsUrl = $this->frontendUrl . '/settings/notifications';
            $this->mailerService->sendUpdateMail(
                $user,
                $this->translator->trans('updates.community.invitations.created.subject'),
                $this->translator->trans(
                    'updates.community.invitations.created.message',
                    array(
                        'community' => $community->getName()
                    )
                ),
                $this->translator->trans('updates.actions.community'),
                $url,
                $prefsUrl
            );
        }
    }
}