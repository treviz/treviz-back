<?php


namespace App\MessageHandler;


use App\Entity\CommunityCandidacy;
use App\Entity\CommunityMembership;
use App\Message\CommunityCandidacyCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommunityCandidacyCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(CommunityCandidacyCreatedMessage $message)
    {
        /** @var CommunityCandidacy $candidacy */
        $candidacy = $this->entityManager
            ->getRepository('App:CommunityCandidacy')
            ->find($message->getEntityId());
        $community = $candidacy->getCommunity();

        $url = $this->frontendUrl . '/communities/' . $community->getHash();
        /** @var CommunityMembership $membership */
        foreach ($community->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnCandidacy() && $membership->canManageCandidacies()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $this->translator->trans('updates.community.candidacies.created.subject'),
                    $this->translator->trans(
                        'updates.community.candidacies.created.message',
                        array(
                            'name' => $candidacy->getUser()->getUsername(),
                            'community' => $community->getName()
                        )
                    ),
                    $this->translator->trans('updates.actions.community'),
                    $url
                );
            }
        }
    }
}