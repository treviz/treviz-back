<?php


namespace App\MessageHandler;


use App\Entity\ProjectInvitation;
use App\Message\ProjectInvitationCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProjectInvitationCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ProjectInvitationCreatedMessage $message)
    {
        /** @var ProjectInvitation $invitation */
        $invitation = $this->entityManager
            ->getRepository('App:ProjectInvitation')
            ->find($message->getEntityId());
        $project = $invitation->getProject();
        $user = $invitation->getUser();

        if ($user->getPreferences()->isOnInvitation()) {
            $url = $this->frontendUrl . '/projects/' . $project->getHash();
            $prefsUrl = $this->frontendUrl . '/settings/notifications';
            $this->mailerService->sendUpdateMail(
                $user,
                $this->translator->trans('updates.project.invitations.created.subject'),
                $this->translator->trans(
                    'updates.project.invitations.created.message',
                    array(
                        'project' => $project->getName(),
                        'name' => $user->getUsername()
                    )
                ),
                $this->translator->trans('updates.actions.project'),
                $url,
                $prefsUrl
            );
        }
    }
}