<?php


namespace App\MessageHandler;


use App\Entity\ProjectMembership;
use App\Entity\Task;
use App\Message\TaskCreatedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TaskCreatedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(TaskCreatedMessage $message)
    {
        /** @var Task $task */
        $task = $this->entityManager
            ->getRepository('App:Task')
            ->find($message->getEntityId());

        $project = $task->getColumn()->getBoard()->getProject();
        $url = "{$this->frontendUrl}/projects/{$project->getHash()}";
        /** @var ProjectMembership $membership */
        foreach ($project->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnNewTask()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $this->translator->trans('updates.kanban.tasks.created.subject'),
                    $this->translator->trans(
                        'updates.kanban.tasks.created.message',
                        array(
                            'project' => $project->getName(),
                            'task' => $task->getName()
                        )
                    ),
                    $this->translator->trans('updates.actions.project'),
                    $url
                );
            }
        }
    }
}