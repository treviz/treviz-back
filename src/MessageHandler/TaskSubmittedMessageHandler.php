<?php


namespace App\MessageHandler;


use App\Entity\ProjectMembership;
use App\Entity\Task;
use App\Message\TaskSubmittedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TaskSubmittedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(TaskSubmittedMessage $message)
    {
        /** @var Task $task */
        $task = $this->entityManager
            ->getRepository('App:Task')
            ->find($message->getEntityId());

        $supervisor = $task->getSupervisor();
        $project = $task->getColumn()->getBoard()->getProject();
        $url = "{$this->frontendUrl}/projects/{$project->getHash()}";
        /** @var ProjectMembership $membership */
        foreach ($project->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnTaskSubmission() && $membership->getUser() === $supervisor) {
                $this->mailerService->sendUpdateMail(
                    $supervisor,
                    $this->translator->trans('updates.kanban.tasks.submitted.subject'),
                    $this->translator->trans(
                        'updates.kanban.tasks.submitted.message',
                        array(
                            'project' => $project->getName(),
                            'task' => $task->getName()
                        )
                    ),
                    $this->translator->trans('updates.actions.project'),
                    $url
                );
            }
        }
    }
}