<?php


namespace App\MessageHandler;


use App\Entity\Project;
use App\Entity\ProjectMembership;
use App\Message\ProjectForkedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProjectForkedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ProjectForkedMessage $message)
    {
        /** @var Project $forkedProject */
        $forkedProject = $this->entityManager
            ->getRepository('App:Project')
            ->find($message->getForkedProjectId());

        /** @var Project $fork */
        $fork = $this->entityManager
            ->getRepository('App:Project')
            ->find($message->getNewProjectId());

        $url = $this->frontendUrl . '/projects/' . $fork->getHash();
        $prefsUrl = $this->frontendUrl . '/projects' . $forkedProject->getHash() . '#preferences';
        /** @var ProjectMembership $membership */
        foreach ($forkedProject->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnFork()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $this->translator->trans('updates.project.forked.subject'),
                    $this->translator->trans(
                        'updates.project.forked.message',
                        array(
                            'project' => $forkedProject->getName(),
                            'fork' => $fork->getName()
                        )
                    ),
                    $this->translator->trans('updates.actions.project'),
                    $url,
                    $prefsUrl
                );
            }
        }
    }
}