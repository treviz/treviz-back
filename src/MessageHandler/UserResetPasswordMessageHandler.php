<?php


namespace App\MessageHandler;


use App\Entity\User;
use App\Message\UserResetPasswordMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserResetPasswordMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MailerService
     */
    private $mailerService;
    /**
     * @var string
     */
    private $frontendUrl;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                LoggerInterface $logger,
                                string $frontendUrl)
    {
        $this->mailerService = $mailerService;
        $this->entityManager = $entityManager;
        $this->frontendUrl = $frontendUrl;
        $this->logger = $logger;
    }

    public function __invoke(UserResetPasswordMessage $message)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository('App:User')->find($message->getEntityId());
        if ($user != null) {
            $url = $this->frontendUrl . '/password-reset?user=' . $user->getUsername() . '&token=' . $user->getConfirmationToken();
            $this->logger->info("Sending mail to: " . $user->getEmail() . " with url $url");
            $this->mailerService->sendResetPasswordMail($user, $url);
            $this->logger->info("UserResetPasswordMessageHandler successfully consumed and mail sent.");
        } else {
            throw new \Exception("Could not deliver UserResetPasswordMessageHandler for user " . $message->getEntityId());
        }
    }
}