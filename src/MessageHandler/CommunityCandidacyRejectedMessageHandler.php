<?php


namespace App\MessageHandler;


use App\Message\CommunityCandidacyRejectedMessage;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommunityCandidacyRejectedMessageHandler implements MessageHandlerInterface {
    private $mailerService;
    private $frontendUrl;
    private $translator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(string $frontendUrl,
                                EntityManagerInterface $entityManager,
                                MailerService $mailerService,
                                TranslatorInterface $translator)
    {
        $this->frontendUrl = $frontendUrl;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function __invoke(CommunityCandidacyRejectedMessage $message)
    {
        /** @var Community $community */
        $community = $this->entityManager
            ->getRepository('App:Community')
            ->find($message->getCommunityId());
        /** @var Community $community */
        $user = $this->entityManager
            ->getRepository('App:User')
            ->find($message->getUserId());

        if ($user->getPreferences()->isOnCandidacyChange()) {
            $url = $this->frontendUrl . '/communities/' . $community->getHash();
            $prefsUrl = $this->frontendUrl . '/settings/notifications';
            $this->mailerService->sendUpdateMail(
                $user,
                $this->translator->trans('updates.community.candidacies.rejected.subject'),
                $this->translator->trans(
                    'updates.community.candidacies.rejected.message',
                    array(
                        'community' => $community->getName()
                    )
                ),
                $this->translator->trans('updates.actions.community'),
                $url,
                $prefsUrl
            );
        }
    }
}