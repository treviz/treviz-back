<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\UsernameToUserTransformer;
use App\Form\DataTransformer\HashesToLabelArrayTransformer;
use App\Form\DataTransformer\HashToColumnTransformer;
use App\Form\DataTransformer\ISO8601ToDateTimeTransformer;

class TaskType extends AbstractType
{

    /** @var  EntityManagerInterface */
    private $em;

    /**
     * BoardType constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array("required" => true))
            ->add('deadline', TextType::class, array("required" => false))
            ->add('description', TextType::class, array("required" => false))
            ->add('column', TextType::class, array("required" => false))
            ->add('position', TextType::class, array("required" => true))
            ->add('archived')
            ->add('pendingApproval')
            ->add('labels', CollectionType::class, array(
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'entry_type' => TextType::class
            ))
            ->add('assignee', TextType::class, array(
                'required' => false
            ))
            ->add('supervisor', TextType::class, array(
                'required' => false
            ));

        $builder->get('deadline')->addModelTransformer(new ISO8601ToDateTimeTransformer(), true);
        $builder->get('labels')->addModelTransformer(new HashesToLabelArrayTransformer($this->em), true);
        $builder->get('assignee')->addModelTransformer(new UsernameToUserTransformer($this->em), true);
        $builder->get('supervisor')->addModelTransformer(new UsernameToUserTransformer($this->em), true);
        $builder->get('column')->addModelTransformer(new HashToColumnTransformer($this->em), true);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Task',
            'csrf_protection' => false
        ));
    }

}
