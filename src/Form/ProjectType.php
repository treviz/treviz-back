<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\HashToIdeaTransformer;
use App\Form\DataTransformer\HashesToCommunitiesTransformer;
use App\Entity\Project;
use App\Form\DataTransformer\HashToProjectTransformer;
use App\Form\DataTransformer\NamesToSkillsTransformer;
use App\Form\DataTransformer\NamesToTagsTransformer;

class ProjectType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, array('required' => true))
            ->add('isOpen', CheckboxType::class, array('required' => false, 'property_path' => 'open'))
            ->add('isVisible', CheckboxType::class, array('required' => true, 'property_path' => 'visible'))
            ->add('description', TextType::class, array('required' => true))
            ->add('shortDescription', TextType::class, array('required' => true))
            ->add('logo')
            ->add('logoUrl', TextType::class, array('required' => false))
            ->add('skills', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class
            ))
            ->add('tags', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class
            ))
            ->add('communities', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'allow_delete' => true,
                'entry_type' => TextType::class
            ))
            ->add('idea', TextType::class, array('required' => false))
            ->add('parent', TextType::class, array('required' => false));

        $builder->get('skills')
            ->addModelTransformer(new NamesToSkillsTransformer($this->em));
        $builder->get('tags')
            ->addModelTransformer(new NamesToTagsTransformer($this->em));
        $builder->get('communities')
            ->addModelTransformer(new HashesToCommunitiesTransformer($this->em));
        $builder->get('idea')
            ->addModelTransformer(new HashToIdeaTransformer($this->em));
        $builder->get('parent')
            ->addModelTransformer(new HashToProjectTransformer($this->em));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Project::class,
            'csrf_protection' => false
        ));
    }

}
