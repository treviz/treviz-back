<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\UsernameToUserTransformer;
use App\Form\DataTransformer\HashesToTaskArrayTransformer;
use App\Form\DataTransformer\NamesToSkillsTransformer;
use App\Form\DataTransformer\NamesToTagsTransformer;

class ProjectJobType extends AbstractType
{

    private $em;

    /**
     * JobType constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('description')
            ->add('contact', TextType::class, array("required" => true))
            ->add('holder', TextType::class, array("required" => false))
            ->add('tasks', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class
            ))
            ->add('skills', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class
            ))
            ->add('tags', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class
            ));

        $builder->get('contact')->addModelTransformer(new UsernameToUserTransformer($this->em));
        $builder->get('holder')->addModelTransformer(new UsernameToUserTransformer($this->em));
        $builder->get('skills')->addModelTransformer(new NamesToSkillsTransformer($this->em));
        $builder->get('tags')->addModelTransformer(new NamesToTagsTransformer($this->em));
        $builder->get('tasks')->addModelTransformer(new HashesToTaskArrayTransformer($this->em));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ProjectJob',
            'csrf_protection' => false
        ));
    }

}
