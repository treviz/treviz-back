<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 21:54
 */

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class ISO8601ToDateTimeTransformer implements DataTransformerInterface
{

    /**
     * @param \DateTime $datetime
     * @return string
     */
    public function transform($datetime)
    {
        if($datetime == null) {
            return '';
        }

        return $datetime->format(\DateTime::ISO8601);
    }

    /**
     * @param string $dateISO
     * @return mixed|DateTime|void
     */
    public function reverseTransform($dateISO)
    {
        if (!$dateISO) {
            return;
        }

        return new \DateTime($dateISO);

    }

}
