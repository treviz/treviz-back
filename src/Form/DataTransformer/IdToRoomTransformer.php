<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Room;

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 02/10/2017
 * Time: 16:06
 */
class IdToRoomTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Room $room
     * @return mixed|string
     */
    public function transform($room)
    {
        if($room == null) {
            return '';
        }

        return $room->getId();
    }

    /**
     * @param int $roomId
     * @return Room|null|object|void
     */
    public function reverseTransform($roomId)
    {
        if (!$roomId) {
            return;
        }

        $room = $this
            ->em
            ->getRepository('App:Room')
            ->find($roomId);

        if ($room == null) {
            throw new TransformationFailedException(sprintf('No idea with id ' . $roomId . ' exists.'));
        }

        return $room;
    }
}
