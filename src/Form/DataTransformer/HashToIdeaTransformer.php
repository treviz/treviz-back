<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Idea;

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 02/10/2017
 * Time: 16:02
 */
class HashToIdeaTransformer implements DataTransformerInterface
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Idea $idea
     * @return mixed|string
     */
    public function transform($idea)
    {
        if($idea == null) {
            return '';
        }

        return $idea->getId();
    }

    /**
     * @param string $hash
     * @return Idea|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $idea = $this->em->getRepository("App:Idea")->findOneBy(array("hash" => $hash));

        if ($idea == null) {
            throw new TransformationFailedException(sprintf('No idea with hash ' . $hash . ' exists.'));
        }

        return $idea;

    }

}
