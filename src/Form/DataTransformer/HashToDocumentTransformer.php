<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Document;

/**
 * Created by PhpStorm.
 * Document: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToDocumentTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * HashToDocumentTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Document $document
     * @return string
     */
    public function transform($document)
    {
        if(null === $document) {
            return '';
        }

        return $document->getHash();
    }

    /**
     * @param string $hash
     * @return Document|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $document = $this
            ->em
            ->getRepository('App:Document')
            ->findOneBy(array(
                'hash' => $hash
            ));

        if (null === $document) {
            throw new TransformationFailedException(sprintf('No document with hash ' . $hash . ' exists.'));
        }

        return $document;
    }
}
