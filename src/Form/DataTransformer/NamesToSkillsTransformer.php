<?php

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use App\Entity\Skill;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class NamesToSkillsTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ArrayCollection $skills
     * @return array
     */
    public function transform($skills)
    {
        $skillsNames = [];
        /** @var Skill $skill */
        foreach ($skills as $skill) {
            $skillsNames[] = $skill->getName();
        }
        return $skillsNames;
    }


    public function reverseTransform($skillsNames)
    {
        $skills = new ArrayCollection();
        $repository = $this
            ->em
            ->getRepository('App:Skill');

        foreach ($skillsNames as $skillName) {
            if ($skillName !== null) {
                $skill = $repository
                    ->findOneBy(array('name' => $skillName));

                if ($skill == null) {
                    $skill = new Skill();
                    $skill->setName($skillName);
                }

                $skills->add($skill);
            }
        }

        return $skills;
    }
}
