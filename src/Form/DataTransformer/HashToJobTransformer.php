<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\ProjectJob;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToJobTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ProjectJob $job
     * @return string
     */
    public function transform($job)
    {
        if($job == null) {
            return '';
        }

        return $job->getHash();
    }

    /**
     * @param string $hash
     * @return ProjectJob|null
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $job = $this->em->getRepository("App:ProjectJob")->findOneBy(array("hash" => $hash));

        if ($job == null) {
            throw new TransformationFailedException(sprintf('No job with hash ' . $hash . ' exists.'));
        }

        return $job;

    }
}
