<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 07/01/2019
 * Time: 20:32
 */

namespace App\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StringArrayToRolesTransformer implements DataTransformerInterface
{

    private $roles = array(
        "ROLE_USER",
        "ROLE_ADMIN"
    );

    /**
     * @param mixed $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     *
     * @throws TransformationFailedException when the transformation fails
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * @param mixed $value The value in the transformed representation
     *
     * @return mixed The value in the original representation
     *
     * @throws TransformationFailedException when the transformation fails
     */
    public function reverseTransform($value)
    {
        if ($value == null || $value == "" || $value == []) {
            $roles = [$this->roles[0]];
        } else {
            $roles =  array_map(function($role) {
                if (in_array($role, $this->roles)) {
                    return $role;
                } elseif ($role == "") {
                    return $this->roles[0];
                } else {
                    throw new TransformationFailedException("The desired role does not exist");
                }
            }, $value);
        }

        return $roles;
    }
}
