<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Enums\ProjectPermissions;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class ArrayToProjectPermissionsTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $permissions
     * @return string
     */
    public function transform($permissions)
    {
        return $permissions ?? '';
    }

    /**
     * Checks if all the permissions given are actual permissions.
     *
     * @param array $permissions
     * @return array|null
     */
    public function reverseTransform($permissions)
    {
        if (!$permissions) {
            return [];
        }

        return array_map(function ($permission) {
            if (in_array($permission, ProjectPermissions::getAvailablePermissions())) {
                return $permission;
            } else {
                throw new TransformationFailedException("$permission is not an actual permission");
            }
        }, $permissions);
    }
}
