<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Organization;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 07/01/2018
 * Time: 23:45
 */
class NameToOrganizationTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * UsernameToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Organization $organization
     * @return string
     */
    public function transform($organization)
    {
        if ($organization == null) {
            return '';
        }

        return $organization->getName();
    }

    /**
     * @param string $name
     * @return Organization|null|object|void
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return;
        }

        return $this
            ->em
            ->getRepository('App:Organization')
            ->getOrCreateOrganization($name);
    }
}
