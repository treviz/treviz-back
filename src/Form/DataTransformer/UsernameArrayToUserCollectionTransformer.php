<?php

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class UsernameArrayToUserCollectionTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * UsernameArrayToUserCollectionTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ArrayCollection $users
     * @return array
     */
    public function transform($users)
    {
        $userIds = [];
        foreach ($users as $user) {
            $userIds[] = $user->getUsername();
        }
        return $userIds;
    }

    /**
     * @param array $usernames
     * @return ArrayCollection
     */
    public function reverseTransform($usernames)
    {
        $users = new ArrayCollection();

        foreach ($usernames as $username) {
            if ($username !== null) {
                $user = $this->em->getRepository("App:User")->findOneBy(array("username" => $username));

                if ($user == null) {
                    throw new TransformationFailedException(sprintf('No user with username ' . $username . ' exists.'));
                }

                $users->add($user);
            }
        }

        return $users;

    }
}
