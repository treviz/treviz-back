<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\CommunityRole;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToCommunityRoleTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CommunityRole $role
     * @return string
     */
    public function transform($role)
    {
        if($role == null) {
            return '';
        }

        return $role->getHash();
    }

    /**
     * @param string $hash
     * @return CommunityRole|null
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $role = $this->em->getRepository("App:CommunityRole")->findOneBy(array("hash" => $hash));

        if ($role == null) {
            throw new TransformationFailedException(sprintf('No role with hash ' . $hash . ' exists.'));
        }

        return $role;
    }
}
