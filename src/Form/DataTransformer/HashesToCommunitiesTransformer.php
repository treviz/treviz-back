<?php

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Community;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashesToCommunitiesTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ArrayCollection $communities
     * @return array
     */
    public function transform($communities)
    {
        $communitiesHashes = [];
        /** @var Community $community */
        foreach ($communities as $community) {
            $communitiesHashes[] = $community->getHash();
        }
        return $communitiesHashes;
    }


    public function reverseTransform($communitiesHashes)
    {
        $communities = new ArrayCollection();

        foreach ($communitiesHashes as $communityHash) {
            $community = $this->em->getRepository("App:Community")
                ->findOneBy(array("hash" => $communityHash));

            if ($community == null) {
                throw new TransformationFailedException(sprintf('No community with hash ' . $communityHash . ' exists.'));
            }

            $communities->add($community);
        }

        return $communities;

    }
}
