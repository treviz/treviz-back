<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\User;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class UsernameToUserTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * UsernameToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @return string
     */
    public function transform($user)
    {
        if($user == null) {
            return '';
        }

        return $user->getUsername();
    }

    /**
     * @param string $name
     * @return User|null|object|void
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return;
        }

        $user = $this->em->getRepository("App:User")->findOneByUsername($name);

        if ($user == null) {
            throw new TransformationFailedException(sprintf('No user with username ' . $name . ' exists.'));
        }

        return $user;

    }
}
