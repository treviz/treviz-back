<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Project;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToProjectTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * HashToProjectTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Project $project
     * @return string
     */
    public function transform($project)
    {
        if (null === $project) {
            return '';
        }

        return $project->getHash();
    }

    /**
     * @param string $hash
     * @return mixed|Project|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $project = $this
            ->em
            ->getRepository('App:Project')
            ->findOneBy(
                array('hash' => $hash)
            );

        if ($project == null) {
            throw new TransformationFailedException(sprintf('No project with hash ' . $hash . ' exists.'));
        }

        return $project;
    }

}
