<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 13:38
 */

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Column;

class HashesToTaskArrayTransformer implements DataTransformerInterface
{

    /** @var  EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Takes an ArrayCollection of tasks, and returns an array of their hashes.
     *
     * @param ArrayCollection $tasks
     * @return string
     */
    public function transform($tasks)
    {
        $labelsHash = [];

        foreach ($tasks as $task) {
            $labelsHash[] = $task->getHash();
        }

        return $labelsHash;
    }

    /**
     * Takes an array of hashes, and return an ArrayCollection of the matching lables.
     *
     * @param array $hashes
     * @return Column|null|object|void
     */
    public function reverseTransform($hashes)
    {

        $labels = new ArrayCollection();

        foreach ($hashes as $hash) {
            $label = $this->em->getRepository("App:Task")->findOneByHash($hash);

            if ($label == null) {
                throw new TransformationFailedException(sprintf('No task with hash ' . $hash . ' exists.'));
            }

            $labels->add($label);
        }

        return $labels;

    }
}
