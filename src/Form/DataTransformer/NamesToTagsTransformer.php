<?php

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use App\Entity\Tag;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class NamesToTagsTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * NamesToTagsTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ArrayCollection $tags
     * @return array
     */
    public function transform($tags)
    {
        $tagsNames = [];
        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $tagsNames[] = $tag->getName();
        }
        return $tagsNames;
    }

    /**
     * @param array $tagsNames
     * @return ArrayCollection
     */
    public function reverseTransform($tagsNames)
    {
        $tags = new ArrayCollection();
        $repository = $this
            ->em
            ->getRepository('App:Tag');

        foreach ($tagsNames as $tagName) {
            if ($tagName !== null) {
                $tag = $repository
                    ->findOneBy(array('name' => $tagName));

                if (null === $tag) {
                    $tag = new Tag();
                    $tag->setName($tagName);
                }

                $tags->add($tag);
            }
        }

        return $tags;

    }
}
