<?php

namespace App\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Community;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToCommunityTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Community $community
     * @return string
     */
    public function transform($community): string
    {
        if ($community == null) {
            return '';
        }

        return $community->getHash();
    }

    /**
     * @param string $hash
     * @return Community|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $community = $this
            ->em
            ->getRepository('App:Community')
            ->findOneBy(array('hash' => $hash));

        if ($community == null) {
            throw new TransformationFailedException(sprintf('No community with hash ' . $hash . ' exists.'));
        }

        return $community;

    }
}
