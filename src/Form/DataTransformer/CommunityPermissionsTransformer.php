<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 07/09/2019
 * Time: 23:31
 */

namespace App\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\Enums\CommunityPermissions;

class CommunityPermissionsTransformer implements DataTransformerInterface
{

    /**
     * @param array $permissions
     * @return array|mixed
     */
    public function transform($permissions)
    {
        return $permissions ?? '';
    }

    public function reverseTransform($permissions)
    {
        if (!$permissions) {
            return [];
        }

        return array_map(function ($permission) {
            if (in_array($permission, CommunityPermissions::getAvailablePermissions(), true)) {
                return $permission;
            } else {
                throw new TransformationFailedException("The permission $permission is not available");
            }
        }, $permissions);
    }
}
