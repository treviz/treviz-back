<?php

namespace App\Form;

use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\HashToIdeaTransformer;
use App\Form\DataTransformer\IdToRoomTransformer;
use App\Form\DataTransformer\HashToCommunityTransformer;
use App\Form\DataTransformer\HashToProjectTransformer;

class DocumentType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('description')
            ->add('file')
            ->add('project', TextType::class, array("required" => false))
            ->add('community', TextType::class, array("required" => false))
            ->add('idea', TextType::class, array("required" => false))
            ->add('room', TextType::class, array("required" => false));

        $builder->get('project')
            ->addModelTransformer(new HashToProjectTransformer($this->em), true);
        $builder->get('community')
            ->addModelTransformer(new HashToCommunityTransformer($this->em), true);
        $builder->get('idea')
            ->addModelTransformer(new HashToIdeaTransformer($this->em), true);
        $builder->get('room')
            ->addModelTransformer(new IdToRoomTransformer($this->em), true);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Document::class,
            'csrf_protection' => false
        ));
    }
}
