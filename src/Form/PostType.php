<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\HashToCommunityTransformer;
use App\Form\DataTransformer\HashToDocumentTransformer;
use App\Form\DataTransformer\HashToTaskTransformer;
use App\Form\DataTransformer\HashToJobTransformer;
use App\Form\DataTransformer\HashToProjectTransformer;

class PostType extends AbstractType
{

    private $em;

    /**
     * PostType constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', TextType::class, array("required" => true))
            ->add('link', TextType::class, array("required" => false))
            ->add('project', TextType::class, array("required" => false))
            ->add('community', TextType::class, array("required" => false))
            ->add('task', TextType::class, array("required" => false))
            ->add('document', TextType::class, array("required" => false))
            ->add('job', TextType::class, array("required" => false));

        $builder->get('project')->addModelTransformer(new HashToProjectTransformer($this->em));
        $builder->get('community')->addModelTransformer(new HashToCommunityTransformer($this->em));
        $builder->get('task')->addModelTransformer(new HashToTaskTransformer($this->em));
        $builder->get('document')->addModelTransformer(new HashToDocumentTransformer($this->em));
        $builder->get('job')->addModelTransformer(new HashToJobTransformer($this->em));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Post',
            'csrf_protection' => false
        ));
    }

}
