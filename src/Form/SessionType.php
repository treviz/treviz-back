<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\HashToCommunityTransformer;
use App\Form\DataTransformer\HashToProjectTransformer;

class SessionType extends AbstractType
{

    private $em;

    /**
     * SessionType constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('isOpen', CheckboxType::class, array('required' => false, 'property_path' => 'open'))
            ->add('project', TextType::class, array('required' => false))
            ->add('community', TextType::class, array('required' => false));

        $builder->get('project')->addModelTransformer(new HashToProjectTransformer($this->em), true);
        $builder->get('community')->addModelTransformer(new HashToCommunityTransformer($this->em), true);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Session',
            'csrf_protection' => false
        ));
    }

}
