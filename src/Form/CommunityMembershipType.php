<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\HashToCommunityRoleTransformer;
use App\Form\DataTransformer\UsernameToUserTransformer;

class CommunityMembershipType extends AbstractType
{

    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('role', TextType::class, array("required" => true))
            ->add('user', TextType::class, array("required" => true));

        $builder->get('role')->addModelTransformer(new HashToCommunityRoleTransformer($this->em), true);
        $builder->get('user')->addModelTransformer(new UsernameToUserTransformer($this->em), true);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\CommunityMembership',
            'csrf_protection' => false
        ));
    }


}
