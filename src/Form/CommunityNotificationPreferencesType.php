<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 22:58
 */

namespace App\Form;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommunityNotificationPreferencesType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('onPost', CheckboxType::class)
            ->add('onCandidacy', CheckboxType::class)
            ->add('onInvitationAccepted', CheckboxType::class)
            ->add('onInvitationRejected', CheckboxType::class)
            ->add('onNewBrainstorming', CheckboxType::class)
            ->add('onNewProject', CheckboxType::class)
            ->add('onNewDocument', CheckboxType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\CommunityNotificationPreferences',
            'csrf_protection' => false
        ));
    }


}
