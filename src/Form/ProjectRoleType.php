<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\ArrayToProjectPermissionsTransformer;

class ProjectRoleType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('required' => true))
            ->add('permissions', CollectionType::class, array(
                'allow_add' => true,
                'required' => true,
                'allow_delete' => true,
                'entry_type' => TextType::class,
                'invalid_message' => 'One of the given permissions does not exist.'
            ))
            ->add('defaultCreator')
            ->add('defaultMember');

        $builder->get('permissions')->addModelTransformer(new ArrayToProjectPermissionsTransformer());

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ProjectRole',
            'csrf_protection' => false
        ));
    }
}
