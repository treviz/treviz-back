<?php

namespace App\Form;

use App\Entity\Room;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\HashToCommunityTransformer;
use App\Form\DataTransformer\UsernameArrayToUserCollectionTransformer;
use App\Form\DataTransformer\HashToProjectTransformer;

class RoomType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('users', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type'   => TextType::class
            ))
            ->add('project', TextType::class, array('required' => false))
            ->add('community', TextType::class, array('required' => false));

        $builder->get('project')
                ->addModelTransformer(new HashToProjectTransformer($this->em), true);
        $builder->get('community')
            ->addModelTransformer(new HashToCommunityTransformer($this->em), true);
        $builder->get("users")
            ->addModelTransformer(new UsernameArrayToUserCollectionTransformer($this->em), true);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Room::class,
            'csrf_protection' => false
        ));
    }

}
