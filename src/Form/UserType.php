<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;
use App\Form\DataTransformer\NameToOrganizationTransformer;
use App\Form\DataTransformer\StringArrayToRolesTransformer;
use App\Form\DataTransformer\NamesToSkillsTransformer;
use App\Form\DataTransformer\NamesToTagsTransformer;

class UserType extends AbstractType
{

    private $em;

    /**
     * UserType constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('lastName', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('username', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('email', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('description', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('avatar', FileType::class, array("required" => false, 'error_bubbling' => false))
            ->add('avatarUrl', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('backgroundImage', FileType::class, array("required" => false, 'error_bubbling' => false))
            ->add('backgroundImageUrl', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('welcome', CheckboxType::class, array("required" => false, 'error_bubbling' => false))
            ->add('agreedWithTerms', CheckboxType::class, array("required" => false, 'error_bubbling' => false))
            ->add('skills', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class,
                'error_bubbling' => false,
            ))
            ->add('interests', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class,
                'error_bubbling' => false,
            ))
            ->add('roles', CollectionType::class, array(
                'allow_add' => true,
                'entry_type' => TextType::class,
                'required' => false,
                'error_bubbling' => false))
            ->add('organization', TextType::class, array('required' => false));

        $builder->get('skills')
            ->addModelTransformer(new NamesToSkillsTransformer($this->em));
        $builder->get('interests')
            ->addModelTransformer(new NamesToTagsTransformer($this->em));
        $builder->get('roles')
            ->addModelTransformer(new StringArrayToRolesTransformer());
        $builder->get('organization')
            ->addModelTransformer(new NameToOrganizationTransformer($this->em));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'csrf_protection' => false
        ));
    }

}
