<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\UsernameToUserTransformer;

class CommunityInvitationType extends AbstractType
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CommunityInvitationType constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message')
            ->add('user', TextType::class, array("required" => true));

        $builder->get('user')->addModelTransformer(new UsernameToUserTransformer($this->em));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\CommunityInvitation',
            'csrf_protection' => false
        ));
    }

}
