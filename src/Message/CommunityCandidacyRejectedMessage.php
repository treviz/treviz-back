<?php


namespace App\Message;


class CommunityCandidacyRejectedMessage
{
    /**
     * @var Int
     */
    private $communityId;

    /**
     * @var Int
     */
    private $userId;

    /**
     * CommunityCandidacyRejectedMessage constructor.
     * @param Int $communityId
     * @param Int $newMemberId
     */
    public function __construct(int $communityId, int $newMemberId)
    {
        $this->communityId = $communityId;
        $this->userId = $newMemberId;
    }

    /**
     * @return Int
     */
    public function getCommunityId(): int
    {
        return $this->communityId;
    }

    /**
     * @return Int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}