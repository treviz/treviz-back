<?php


namespace App\Message;


class ProjectInvitationRejectedMessage {
    /**
     * @var Int
     */
    private $projectId;

    /**
     * @var Int
     */
    private $userId;

    /**
     * ProjectInvitationRejectedMessage constructor.
     * @param Int $projectId
     * @param Int $userId
     */
    public function __construct(int $projectId, int $userId)
    {
        $this->projectId = $projectId;
        $this->userId = $userId;
    }

    /**
     * @return Int
     */
    public function getProjectId(): int
    {
        return $this->projectId;
    }

    /**
     * @return Int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}