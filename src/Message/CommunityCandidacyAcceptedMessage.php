<?php


namespace App\Message;


class CommunityCandidacyAcceptedMessage
{
    /**
     * @var Int
     */
    private $communityId;

    /**
     * @var Int
     */
    private $newMemberId;

    /**
     * CommunityCandidacyAcceptedMessage constructor.
     * @param Int $communityId
     * @param Int $newMemberId
     */
    public function __construct(int $communityId, int $newMemberId)
    {
        $this->communityId = $communityId;
        $this->newMemberId = $newMemberId;
    }

    /**
     * @return Int
     */
    public function getCommunityId(): int
    {
        return $this->communityId;
    }

    /**
     * @return Int
     */
    public function getNewMemberId(): int
    {
        return $this->newMemberId;
    }
}