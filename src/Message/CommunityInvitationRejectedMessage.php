<?php


namespace App\Message;


class CommunityInvitationRejectedMessage
{
    /**
     * @var Int
     */
    private $communityId;

    /**
     * @var Int
     */
    private $userId;

    /**
     * CommunityInvitationRejectedMessage constructor.
     * @param Int $communityId
     * @param Int $userId
     */
    public function __construct(int $communityId, int $userId)
    {
        $this->communityId = $communityId;
        $this->userId = $userId;
    }

    /**
     * @return Int
     */
    public function getCommunityId(): int
    {
        return $this->communityId;
    }

    /**
     * @return Int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}