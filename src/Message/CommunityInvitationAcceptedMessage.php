<?php


namespace App\Message;


class CommunityInvitationAcceptedMessage
{

    /**
     * @var Int
     */
    private $communityId;

    /**
     * @var Int
     */
    private $newMemberId;

    /**
     * CommunityInvitationAcceptedMessage constructor.
     * @param Int $communityId
     * @param Int $newMemberId
     */
    public function __construct(int $communityId, int $newMemberId)
    {
        $this->communityId = $communityId;
        $this->newMemberId = $newMemberId;
    }

    /**
     * @return Int
     */
    public function getCommunityId(): int
    {
        return $this->communityId;
    }

    /**
     * @return Int
     */
    public function getNewMemberId(): int
    {
        return $this->newMemberId;
    }
}