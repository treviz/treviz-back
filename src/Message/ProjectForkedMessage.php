<?php


namespace App\Message;


class ProjectForkedMessage
{
    /**
     * @var Int
     */
    private $forkedProjectId;
    /**
     * @var Int
     */
    private $newProjectId;

    /**
     * ProjectForkedMessage constructor.
     * @param Int $forkedProjectId
     * @param Int $newProjectId
     */
    public function __construct(int $forkedProjectId, int $newProjectId)
    {
        $this->forkedProjectId = $forkedProjectId;
        $this->newProjectId = $newProjectId;
    }

    /**
     * @return Int
     */
    public function getForkedProjectId(): int
    {
        return $this->forkedProjectId;
    }

    /**
     * @return Int
     */
    public function getNewProjectId(): int
    {
        return $this->newProjectId;
    }
}