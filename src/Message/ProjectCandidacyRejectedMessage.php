<?php


namespace App\Message;


class ProjectCandidacyRejectedMessage
{
    /**
     * @var Int
     */
    private $projectId;

    /**
     * @var Int
     */
    private $userId;

    /**
     * ProjectCandidacyRejectedMessage constructor.
     * @param Int $projectId
     * @param Int $newMemberId
     */
    public function __construct(int $projectId, int $newMemberId)
    {
        $this->projectId = $projectId;
        $this->userId = $newMemberId;
    }

    /**
     * @return Int
     */
    public function getProjectId(): int
    {
        return $this->projectId;
    }

    /**
     * @return Int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}