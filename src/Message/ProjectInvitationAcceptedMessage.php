<?php


namespace App\Message;


class ProjectInvitationAcceptedMessage
{
    /**
     * @var Int
     */
    private $projectId;

    /**
     * @var Int
     */
    private $newMemberId;

    /**
     * ProjectCandidacyAcceptedMessage constructor.
     * @param Int $projectId
     * @param Int $newMemberId
     */
    public function __construct(int $projectId, int $newMemberId)
    {
        $this->projectId = $projectId;
        $this->newMemberId = $newMemberId;
    }

    /**
     * @return Int
     */
    public function getProjectId(): int
    {
        return $this->projectId;
    }

    /**
     * @return Int
     */
    public function getNewMemberId(): int
    {
        return $this->newMemberId;
    }
}