<?php


namespace App\Message;


abstract class AbstractIdMessage
{
    /**
     * @var Int
     */
    private $entityId;

    public function __construct(Int $entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return Int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }
}