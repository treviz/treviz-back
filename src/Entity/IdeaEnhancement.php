<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * IdeaEnhancement
 *
 * @ORM\Table(name="idea_enhancement")
 * @ORM\Entity(repositoryClass="App\Repository\IdeaEnhancementRepository")
 */
class IdeaEnhancement extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="publicationDate", type="datetime")
     */
    private $publicationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idea", inversedBy="enhancements")
     * @JMS\Exclude()
     */
    private $idea;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ideasEnhancements")
     * @JMS\MaxDepth(3)
     */
    private $author;

    public function __construct()
    {
        $this->publicationDate = new DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return IdeaEnhancement
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Set publicationDate
     *
     * @param DateTime $publicationDate
     *
     * @return IdeaEnhancement
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * Get publicationDate
     *
     * @return DateTime
     */
    public function getPublicationDate(): ?DateTime
    {
        return $this->publicationDate;
    }

    /**
     * @return Idea
     */
    public function getIdea(): ?Idea
    {
        return $this->idea;
    }

    /**
     * @param mixed $idea
     */
    public function setIdea($idea)
    {
        $this->idea = $idea;
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        if (!$author->getIdeasEnhancements()->contains($this)) {
            $author->addIdeaEnhancement($this);
        }
        $this->author = $author;
    }


}

