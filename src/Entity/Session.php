<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 28/10/2017
 * Time: 15:28
 */

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Brainstorming session
 *
 * @ORM\Table(name="brainstorming_session")
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 */
class Session extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var boolean $open
     *
     * @ORM\Column(name="open", type="boolean")
     * @JMS\SerializedName("isOpen")
     */
    protected $open = true;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Community", inversedBy="brainstormingSessions")
     * @JMS\Groups({"idea"})
     */
    protected $community;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="brainstormingSessions")
     * @JMS\Exclude()
     */
    protected $project;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Idea", mappedBy="session", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    protected $ideas;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    protected $startDate;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        $this->startDate = new DateTime();
        $this->ideas = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isOpen(): ?bool
    {
        return $this->open;
    }

    /**
     * @param boolean $open
     */
    public function setOpen(bool $open)
    {
        $this->open = $open;
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community)
    {
        $this->community = $community;
        if (!$community->getBrainstormingSessions()->contains($this)) {
            $community->addBrainstormingSession($this);
       }
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
        //if (!$project->getBrainstormingSessions()->contains($this)) {
        //    $project->addBrainstormingSession($this);
        //}
    }

    /**
     * @return Collection
     */
    public function getIdeas(): Collection
    {
        return $this->ideas;
    }

    /**
     * @param ArrayCollection $ideas
     */
    public function setIdeas(ArrayCollection $ideas)
    {
        $this->ideas = $ideas;
    }

    /**
     * @param Idea $idea
     */
    public function addIdea(Idea $idea)
    {
        $this->ideas->add($idea);
        if ($idea->getSession() == null) {
            $idea->setSession($this);
        }
    }

    /**
     * @param Idea $idea
     */
    public function removeIdea(Idea $idea)
    {
        $this->ideas->removeElement($idea);
        $idea->setSession(null);
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    /**
     * @JMS\VirtualProperty()
     */
    public function getNbIdeas()
    {
        return $this->getIdeas()->count();
    }


}
