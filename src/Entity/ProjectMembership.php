<?php

namespace App\Entity;

use App\Entity\Enums\ProjectPermissions;
use App\Entity\Superclass\Hashable;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ProjectMember
 *
 * @ORM\Table(name="project_membership")
 * @ORM\Entity(repositoryClass="App\Repository\ProjectMembershipRepository")
 */
class ProjectMembership extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var ProjectRole $role
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ProjectRole", cascade={"persist"})
     */
    private $role;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="memberships")
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"user"})
     */
    private $project;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projectsMemberships")
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"project"})
     */
    private $user;

    /**
     * @var ProjectNotificationPreferences
     *
     * @ORM\OneToOne(targetEntity="App\Entity\ProjectNotificationPreferences", inversedBy="projectMembership", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $preferences;

    public function __construct()
    {
        $this->preferences = new ProjectNotificationPreferences();
        $this->preferences->setProjectMembership($this);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ProjectRole
     */
    public function getRole(): ?ProjectRole
    {
        return $this->role;
    }

    /**
     * @param ProjectRole $role
     */
    public function setRole(ProjectRole $role)
    {
        $this->role = $role;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return ProjectNotificationPreferences
     */
    public function getPreferences(): ProjectNotificationPreferences
    {
        return $this->preferences;
    }

    /**
     * @param ProjectNotificationPreferences $preferences
     */
    public function setPreferences(ProjectNotificationPreferences $preferences
    ): void {
        $this->preferences = $preferences;
    }

    /**
     * Utilitarian functions
     */

    public function canUpdateProject(): bool
    {
        return $this->hasRole(ProjectPermissions::UPDATE_PROJECT);
    }

    public function canDeleteProject(): bool
    {
        return $this->hasRole(ProjectPermissions::DELETE_PROJECT);
    }

    public function canManageRole(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_ROLE);
    }

    public function canManageMemberships(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_MEMBERSHIP);
    }

    public function canManagePosts(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_POST);
    }

    public function canManageCandidacies(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_CANDIDACIES);
    }

    public function canManageInvitations(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_INVITATIONS);
    }

    public function canManageDocuments(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_DOCUMENT);
    }

    public function canManageKanban(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_KANBAN);
    }

    public function canManageJobs(): bool
    {
        return $this->hasRole(ProjectPermissions::MANAGE_JOBS);
    }

    public function hasRole(string $role): bool
    {
        $permissions = $this->role->getPermissions();
        return in_array($role, $permissions, true);
    }
}

