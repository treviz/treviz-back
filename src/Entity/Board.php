<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Board
 *
 * @ORM\Table(name="board")
 * @ORM\Entity(repositoryClass="App\Repository\BoardRepository")
 */
class Board extends Hashable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="archived", type="boolean")
     */
    private $archived = true;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Column", mappedBy="board", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $columns;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="boards")
     * @JMS\Groups({"job", "user"})
     */
    private $project;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Label", mappedBy="board", cascade={"persist", "remove"})
     */
    private $labels;

    /**
     * Board constructor.
     */
    public function __construct()
    {
        $this->columns = new ArrayCollection();
        $this->labels = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Board
     */
    public function setName($name): Board
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Board
     */
    public function setDescription($description): Board
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return boolean
     */
    public function isArchived(): bool
    {
        return $this->archived;
    }

    /**
     * @param boolean $archived
     */
    public function setArchived(bool $archived)
    {
        $this->archived = $archived;
    }

    /**
     * Add column
     *
     * @param Column $column
     *
     * @return Board
     */
    public function addColumn(Column $column): Board
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * Remove column
     *
     * @param Column $column
     */
    public function removeColumn(Column $column)
    {
        $this->columns->removeElement($column);
    }

    /**
     * Get columns
     *
     * @return Collection
     */
    public function getColumns(): Collection
    {
        return $this->columns;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Board $this
     */
    public function setProject(Project $project = null): Board
    {
        if($this->project !== null) {
            $this->project->removeBoard($this);
        }

        if ($project !== null) {
            $this->project = $project;

            if(!$project->getBoards()->contains($this)) {
                $project->addBoard($this);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    /**
     * Add a label
     *
     * @param Label $label
     *
     * @return Board
     */
    public function addLabel(Label $label): Board
    {
        $this->labels->add($label);
        $label->setBoard($this);

        return $this;
    }

    /**
     * Remove a label
     *
     * @param Label $label
     */
    public function removeLabel(Label $label)
    {
        $this->columns->removeElement($label);
    }

}
