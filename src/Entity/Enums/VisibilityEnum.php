<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 18/03/2017
 * Time: 14:28
 */

namespace App\Entity\Enums;

abstract class VisibilityEnum
{

    CONST VISIBILITY_PRIVATE = 'VISIBILITY_PRIVATE';
    CONST VISIBILITY_PUBLIC = 'VISIBILITY_PUBLIC';

    /** @var array  */
    protected static $visibilityName = [
        self::VISIBILITY_PRIVATE => 'Private',
        self::VISIBILITY_PUBLIC => 'Public',

    ];

    /**
     * @param $visibilityShortName
     * @return mixed|string
     * @internal param $roleShortName
     */
    public static function getVisibilityName($visibilityShortName){
        if(!isset(static::$visibilityName[$visibilityShortName])){
            return "Unknown role ($visibilityShortName)";
        }

        return static::$visibilityName[$visibilityShortName];
    }

    public static function getAvailableVisibility(){
        return [
            self::VISIBILITY_PRIVATE,
            self::VISIBILITY_PUBLIC,
        ];
    }

}
