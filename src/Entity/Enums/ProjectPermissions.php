<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 12:02
 */

namespace App\Entity\Enums;

/**
 * Class ProjectPermissions
 * List the various operations that can be performed inside a project.
 *
 * @package App\Entity\Enums
 */
class ProjectPermissions
{

    /*
     * Manage Project description
     */
    CONST UPDATE_PROJECT = 'UPDATE_PROJECT';
    CONST DELETE_PROJECT = 'DELETE_PROJECT';

    /*
     * Manage Project roles
     */
    CONST MANAGE_ROLE = 'MANAGE_ROLE';

    /*
     * Manage members
     */
    CONST MANAGE_MEMBERSHIP = 'MANAGE_MEMBERSHIP';

    /*
     * Manage posts
     */
    CONST MANAGE_POST = 'MANAGE_POST';

    /*
     * Manage candidacies
     */
    CONST MANAGE_CANDIDACIES = 'MANAGE_CANDIDACIES';

    /*
     * Manage invitations
     */
    CONST MANAGE_INVITATIONS = 'MANAGE_INVITATIONS';

    /*
     * Manage Documents
     */
    CONST MANAGE_DOCUMENT = 'MANAGE_DOCUMENT';

    /*
     * Manage Boards
     */
    CONST MANAGE_KANBAN = 'MANAGE_KANBAN';

    /*
     * Manage Crowdfunding campaigns
     */
    CONST MANAGE_CROWD_FUND = 'MANAGE_CROWD_FUND';

    /*
     * Manage jobs and applications.
     */
    CONST MANAGE_JOBS = 'MANAGE_JOBS';

    /*
     * Manage brainstorming sessions
     */
    CONST MANAGE_BRAINSTORMING_SESSION = 'MANAGE_BRAINSTORMING_SESSION';
    CONST MANAGE_BRAINSTORMING_IDEAS = 'MANAGE_BRAINSTORMING_IDEAS';

    /** @var array  */
    protected static $permissionName = [
        self::UPDATE_PROJECT => 'Update Project',
        self::DELETE_PROJECT => 'Delete Project',
        self::MANAGE_ROLE => 'Manage Role',
        self::MANAGE_MEMBERSHIP => 'Manage Membership',
        self::MANAGE_POST => 'Manage Post',
        self::MANAGE_CANDIDACIES => 'Manage Candidacies',
        self::MANAGE_INVITATIONS => 'Manage Invitations',
        self::MANAGE_DOCUMENT => 'Manage Document',
        self::MANAGE_KANBAN => 'Manage Board',
        self::MANAGE_CROWD_FUND => 'Manage Crowdfunding Campaigns',
        self::MANAGE_JOBS => 'Manage Job',
        self::MANAGE_BRAINSTORMING_SESSION => 'Manage Brainstorming session',
        self::MANAGE_BRAINSTORMING_IDEAS => 'Manage Brainstorming ideas',
    ];

    /**
     * @param $permissionShortName
     * @return mixed|string
     * @internal param $roleShortName
     */
    public static function getPermissionName($permissionShortName): string
    {
        if(!isset(static::$permissionName[$permissionShortName])){
            return "Unknown permission ($permissionShortName)";
        }

        return static::$permissionName[$permissionShortName];
    }

    public static function getAvailablePermissions(): array
    {
        return [
            self::UPDATE_PROJECT,
            self::DELETE_PROJECT,
            self::MANAGE_ROLE,
            self::MANAGE_MEMBERSHIP,
            self::MANAGE_POST,
            self::MANAGE_CANDIDACIES,
            self::MANAGE_INVITATIONS,
            self::MANAGE_DOCUMENT,
            self::MANAGE_KANBAN,
            self::MANAGE_CROWD_FUND,
            self::MANAGE_JOBS,
            self::MANAGE_BRAINSTORMING_SESSION,
            self::MANAGE_BRAINSTORMING_IDEAS
        ];
    }

    public static function getAllPermissions()
    {
        return self::$permissionName;
    }

}
