<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * MailDomain are used:
 *   - In public organizations, to prevent some domains from being used for user registration, and avoid spam
 *   - In private organizations, to allow users with a specific mail domain to register
 *
 * @ORM\Table(name="mail_domain")
 * @ORM\Entity(repositoryClass="App\Repository\MailDomainRepository")
 */
class MailDomain
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255, unique=true, nullable=false)
     */
    private $domain;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }

}
