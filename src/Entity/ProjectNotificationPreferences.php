<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 22:45
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectNotificationPreferencesRepository")
 * @ORM\Table(name="project_notification_preferences")
 */
class ProjectNotificationPreferences
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()CommunityNotificationPreferences
     */
    private $id;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new task is created in the project.
     * @ORM\Column(name="on_new_task", type="boolean")
     */
    private $onNewTask = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when the user is assigned a new task.
     * @ORM\Column(name="on_assigned_task", type="boolean")
     */
    private $onAssignedTask = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when the user supervises a task that has just been submitted.
     * @ORM\Column(name="on_task_submission", type="boolean")
     * been submitted
     */
    private $onTaskSubmission = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a task submitted by the user is approved,
     * decline, or received feedback.
     * @ORM\Column(name="on_task_approval_or_refusal", type="boolean")
     */
    private $onTaskApprovalOrRefusal = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when the project is forked.
     * @ORM\Column(name="on_fork", type="boolean")
     */
    private $onFork = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when someones candidates to the project.
     * @ORM\Column(name="on_candidacy", type="boolean")
     */
    private $onCandidacy = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new post is created.
     * @ORM\Column(name="on_post", type="boolean")
     */
    private $onPost = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new document is created in the project.
     * @ORM\Column(name="on_new_document", type="boolean")
     */
    private $onNewDocument = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a user accepts an invitation to the project.
     * @ORM\Column(name="on_invitation_accepted", type="boolean")
     */
    private $onInvitationAccepted = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a user rejects an invitation to the project.
     * @ORM\Column(name="on_invitation_rejected", type="boolean")
     */
    private $onInvitationRejected = true;

    /**
     * @var ProjectMembership
     *
     * Membership those preferences are linked to.
     *
     * @ORM\OneToOne(targetEntity="App\Entity\ProjectMembership", mappedBy="preferences")
     * @JMS\Exclude()
     */
    private $projectMembership;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isOnNewTask(): bool
    {
        return $this->onNewTask;
    }

    /**
     * @param bool $onNewTask
     */
    public function setOnNewTask(bool $onNewTask): void
    {
        $this->onNewTask = $onNewTask;
    }

    /**
     * @return bool
     */
    public function isOnAssignedTask(): bool
    {
        return $this->onAssignedTask;
    }

    /**
     * @param bool $onAssignedTask
     */
    public function setOnAssignedTask(bool $onAssignedTask): void
    {
        $this->onAssignedTask = $onAssignedTask;
    }

    /**
     * @return bool
     */
    public function isOnTaskSubmission(): bool
    {
        return $this->onTaskSubmission;
    }

    /**
     * @param bool $onTaskSubmission
     */
    public function setOnTaskSubmission(bool $onTaskSubmission): void
    {
        $this->onTaskSubmission = $onTaskSubmission;
    }

    /**
     * @return bool
     */
    public function isOnTaskApprovalOrRefusal(): bool
    {
        return $this->onTaskApprovalOrRefusal;
    }

    /**
     * @param bool $onTaskApprovalOrRefusal
     */
    public function setOnTaskApprovalOrRefusal(bool $onTaskApprovalOrRefusal
    ): void {
        $this->onTaskApprovalOrRefusal = $onTaskApprovalOrRefusal;
    }

    /**
     * @return bool
     */
    public function isOnFork(): bool
    {
        return $this->onFork;
    }

    /**
     * @param bool $onFork
     */
    public function setOnFork(bool $onFork): void
    {
        $this->onFork = $onFork;
    }

    /**
     * @return bool
     */
    public function isOnCandidacy(): bool
    {
        return $this->onCandidacy;
    }

    /**
     * @param bool $onCandidacy
     */
    public function setOnCandidacy(bool $onCandidacy): void
    {
        $this->onCandidacy = $onCandidacy;
    }

    /**
     * @return bool
     */
    public function isOnPost(): bool
    {
        return $this->onPost;
    }

    /**
     * @param bool $onPost
     */
    public function setOnPost(bool $onPost): void
    {
        $this->onPost = $onPost;
    }

    /**
     * @return bool
     */
    public function isOnNewDocument(): bool
    {
        return $this->onNewDocument;
    }

    /**
     * @param bool $onNewDocument
     */
    public function setOnNewDocument(bool $onNewDocument): void
    {
        $this->onNewDocument = $onNewDocument;
    }

    /**
     * @return ProjectMembership
     */
    public function getProjectMembership(): ProjectMembership
    {
        return $this->projectMembership;
    }

    /**
     * @param ProjectMembership $projectMembership
     */
    public function setProjectMembership(ProjectMembership $projectMembership
    ): void {
        $this->projectMembership = $projectMembership;
        if($projectMembership->getPreferences() == null) {
            $projectMembership->setPreferences($this);
        }
    }

    /**
     * @return bool
     */
    public function isOnInvitationAccepted(): bool
    {
        return $this->onInvitationAccepted;
    }

    /**
     * @param bool $onInvitationAccepted
     */
    public function setOnInvitationAccepted(bool $onInvitationAccepted): void
    {
        $this->onInvitationAccepted = $onInvitationAccepted;
    }

    /**
     * @return bool
     */
    public function isOnInvitationRejected(): bool
    {
        return $this->onInvitationRejected;
    }

    /**
     * @param bool $onInvitationRejected
     */
    public function setOnInvitationRejected(bool $onInvitationRejected): void
    {
        $this->onInvitationRejected = $onInvitationRejected;
    }

}
