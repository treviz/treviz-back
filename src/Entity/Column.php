<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * List
 *
 * @ORM\Table(name="board_column")
 * @ORM\Entity(repositoryClass="App\Repository\ColumnRepository")
 */
class Column extends Hashable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Board
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Board", inversedBy="columns")
     * @JMS\Groups({"job"})
     */
    private $board;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="column", cascade={"persist", "remove"})
     * @JMS\Groups({"columns"})
     * @JMS\MaxDepth(5)
     */
    private $tasks;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Column
     */
    public function setName($name): Column
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    /**
     * Set board
     *
     * @param Board $board
     *
     * @return Column
     */
    public function setBoard(Board $board = null): ?Column
    {
        $this->board = $board;
        if (!$board->getColumns()->contains($this)) {
            $board->addColumn($this);
        }

        return $this;
    }

    /**
     * Get board
     *
     * @return Board
     */
    public function getBoard(): ?Board
    {
        return $this->board;
    }

    /**
     * Add task
     *
     * @param Task $task
     *
     * @return Column
     */
    public function addTask(Task $task): ?Column
    {
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param Task $task
     */
    public function removeTask(Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return Collection
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

}
