<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ProjectJob
 *
 * @ORM\Table(name="project_job")
 * @ORM\Entity(repositoryClass="App\Repository\ProjectJobRepository")
 */
class ProjectJob extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @JMS\Groups({"job"})
     */
    private $description;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="jobs")
     * @JMS\Groups({"job", "jobs"})
     * @JMS\MaxDepth(2)
     */
    private $project;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="job")
     * @JMS\Groups({"job"})
     */
    private $tasks;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @JMS\Groups({"job"})
     */
    private $contact;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @JMS\Groups({"job", "jobs"})
     */
    private $holder;

    /**
     * @var ArrayCollection<Skill>
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Skill", cascade={"persist"})
     * @ORM\JoinTable(name="job_skills")
     */
    private $skills;

    /**
     * @var ArrayCollection<Tag>
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", cascade={"persist"})
     * @ORM\JoinTable(name="job_tags")
     */
    private $tags;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ProjectCandidacy", mappedBy="job", cascade={"remove"})
     * @JMS\Exclude()
     */
    private $applications;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="job", cascade={"persist", "remove"})
     * @JMS\MaxDepth(7) In order to return posts and the users who posted them.
     */
    private $posts;

    /**
     * ProjectJob constructor.
     */
    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->applications = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProjectJob
     */
    public function setName($name): ProjectJob
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProjectJob
     */
    public function setDescription($description): ProjectJob
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return ProjectJob
     */
    public function setProject(Project $project): ProjectJob
    {
        $this->project = $project;
        if (!$project->getJobs()->contains($this)) {
            $project->addJob($this);
        }


        return $this;
    }

    /**
     * @return Collection<Task>
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    /**
     * @param ArrayCollection $tasks
     * @return ProjectJob
     */
    public function setTasks(ArrayCollection $tasks): ProjectJob
    {
        $this->tasks = $tasks;

        return $this;
    }

    /**
     * @param Task $task
     * @return ProjectJob
     */
    public function addTask(Task $task): ProjectJob
    {
        $this->tasks->add($task);
        if ($task->getJob() == null) {
            $task->setJob($this);
        }

        return $this;
    }

    /**
     * @param Task $task
     * @return ProjectJob
     */
    public function removeTask(Task $task): ProjectJob
    {
        $this->tasks->removeElement($task);

        return $this;
    }

    /**
     * @return User
     */
    public function getContact(): ?User
    {
        return $this->contact;
    }

    /**
     * @param User $contact
     * @return ProjectJob
     */
    public function setContact(User $contact): ProjectJob
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return User
     */
    public function getHolder(): ?User
    {
        return $this->holder;
    }

    /**
     * @param User $holder
     * @return ProjectJob
     */
    public function setHolder(?User $holder): ProjectJob
    {
        $this->holder = $holder;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    /**
     * @param ArrayCollection $skills
     */
    public function setSkills(ArrayCollection $skills)
    {
        $this->skills = $skills;
    }

    /**
     * @param Skill $skill
     * @return ProjectJob
     */
    public function addSkill(Skill $skill): ProjectJob
    {
        $this->skills->add($skill);

        return $this;
    }

    /**
     * @param Skill $skill
     * @return ProjectJob
     */
    public function removeSkill(Skill $skill): ProjectJob
    {
        $this->skills->removeElement($skill);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $tags
     */
    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @param Tag $tag
     * @return ProjectJob
     */
    public function addTag(Tag $tag): ProjectJob
    {
        $this->tags->add($tag);

        return $this;
    }

    /**
     * @param Tag $tag
     * @return ProjectJob
     */
    public function removeTag(Tag $tag): ProjectJob
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    /**
     * @param ArrayCollection $applications
     * @return ProjectJob
     */
    public function setApplications(ArrayCollection $applications): ProjectJob
    {
        $this->applications = $applications;

        return $this;
    }

    /**
     * @param ProjectCandidacy $application
     * @return ProjectJob
     */
    public function addApplication(ProjectCandidacy $application): ProjectJob
    {
        if (!$this->applications->contains($application)) {
            $this->applications->add($application);
        }

        return $this;
    }

    /**
     * @param ProjectCandidacy $application
     * @return ProjectJob
     */
    public function removeApplication(ProjectCandidacy $application): ProjectJob
    {
        if ($this->applications->contains($application)) {
            $this->applications->removeElement($application);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
        if ($post->getJob() == null) {
            $post->setJob($this);
        }
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }


    /**
     * @JMS\VirtualProperty
     */
    public function getNbApplications()
    {
        return $this->applications->count();
    }

}

