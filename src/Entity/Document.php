<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, unique=true)
     */
    private $hash;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", nullable=true)
     * @Assert\File(maxSize="20M")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="file_url", type="string", nullable=true)
     */
    private $fileUrl;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", nullable=true)
     */
    private $extension;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @JMS\MaxDepth(2)
     */
    private $owner;

    /**
     * Directories are regarded as documents that have "children"
     *
     * @var boolean
     *
     * @ORM\Column(name="directory", type="boolean")
     */
    private $directory = false;

    /**
     * Parent directory

     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Document")
     */
    private $parent;

    /**
     * @var Room
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="documents")
     * @JMS\Exclude()
     */
    private $room;

    /**
     * @var Community
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Community", inversedBy="documents")
     * @JMS\Exclude()
     */
    private $community;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="documents",)
     * @JMS\Exclude()
     */
    private $project;

    /**
     * @var Idea
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Idea", inversedBy="documents")
     * @JMS\Exclude()
     */
    private $idea;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="document", cascade={"persist", "remove"})
     * @JMS\MaxDepth(7) In order to return posts and the users who posted them.
     */
    private $posts;

    public function __construct()
    {
        $this->date = new DateTime();
        $this->posts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFileUrl(): ?string
    {
        return $this->fileUrl;
    }

    /**
     * @param string $fileUrl
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
    }

    /**
     * @return User
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return Room
     */
    public function getRoom(): ?Room
    {
        return $this->room;
    }

    /**
     * @param Room $room
     */
    public function setRoom(Room $room)
    {
        $this->room = $room;
        if (!$room->getDocuments()->contains($this)) {
            $room->addDocument($this);
        }
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community)
    {
        $this->community = $community;
        if (!$community->getDocuments()->contains($this)) {
            $community->addDocument($this);
        }
    }

    public function getRelatedCommunity(): ?Community
    {
        if (null !== $this->community) {
            return $this->community;
        }

        if (null !== $this->idea) {
            return $this->idea->getSession()->getCommunity();
        }

        return null;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
        if (!$project->getDocuments()->contains($this)) {
            $project->addDocument($this);
        }
    }

    /**
     * @return Idea
     */
    public function getIdea(): ?Idea
    {
        return $this->idea;
    }

    /**
     * @param Idea $idea
     */
    public function setIdea(Idea $idea)
    {
        $this->idea = $idea;
        if (!$idea->getDocuments()->contains($this)) {
            $idea->addDocument($this);
        }
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return boolean
     */
    public function isDirectory(): ?bool
    {
        return $this->directory;
    }

    /**
     * @param boolean $directory
     */
    public function setDirectory(bool $directory)
    {
        $this->directory = $directory;
    }

    /**
     * @return Document
     */
    public function getParent(): ?Document
    {
        return $this->parent;
    }

    /**
     * @param Document $parent
     */
    public function setParent(Document $parent)
    {
        $this->parent = $parent;
    }


    /**
     * @return Collection
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
        if ($post->getDocument() == null) {
            $post->setDocument($this);
        }
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

}

