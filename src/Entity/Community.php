<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Community
 *
 * If the community is sent with others (use case: search), only few elements will be serialized.
 *
 * @ORM\Table(name="community")
 * @ORM\Entity(repositoryClass="App\Repository\CommunityRepository")
 */
class Community extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * Visible communities can be found by anyone
     * Private communities are closed by default.
     *
     * @var boolean $visible
     * @ORM\Column(name="visible", type="boolean")
     * @JMS\Groups({"community"})
     * @JMS\SerializedName("isVisible")
     */
    private $visible;

    /**
     * An open community can be joined by anyone, without any approval.
     *
     * @ORM\Column(name="open", type="boolean")
     * @var bool
     *
     * @JMS\Groups({"community"})
     */
    private $open;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @JMS\Groups({"community"})
     */
    private $description;

    /**
     * @var ArrayCollection<CommunityMembership>
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityMembership", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $memberships;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityRole", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $roles;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityInvitation", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    protected $invitations;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityCandidacy", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    protected $candidacies;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="text", nullable=true)
     * @JMS\Groups({"community"})
     */
    private $website;

    /**
     * @ORM\Column(name="logo", type="string", nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/svg+xml"}, maxSize="1M")
     * @JMS\Exclude()
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_url", type="string", nullable=true)
     */
    private $logoUrl;

    /**
     * @ORM\Column(name="background_image", type="string", nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/svg+xml"}, maxSize="1M")
     * @JMS\Exclude()
     */
    private $backgroundImage;

    /**
     * @var string
     *
     * @ORM\Column(name="background_image_url", type="string", nullable=true)
     * @JMS\Groups({"community"})
     */
    private $backgroundImageUrl;

    /**
     * @Orm\OneToMany(targetEntity="App\Entity\Post", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $posts;

    /**
     * @Orm\ManyToMany(targetEntity="App\Entity\Project", mappedBy="communities")
     * @JMS\Exclude()
     */
    private $projects;

    /**
     * @Orm\OneToMany(targetEntity="App\Entity\Room", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $rooms;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Session", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $brainstormingSessions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="community", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $documents;

    /**
     * @ORM\Column(name="creation_date", type="datetime")
     * @JMS\Groups({"project"})
     */
    private $creationDate;

    /**
     * Community constructor.
     */
    public function __construct()
    {
        $this->memberships = new ArrayCollection();
        $this->invitations = new ArrayCollection();
        $this->candidacies = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->rooms = new ArrayCollection();
        $this->brainstormingSessions = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->creationDate = new DateTime();

        //On Project creation, create a room #general
        $defaultRoom = new Room();
        $defaultRoom->setName('general');
        $defaultRoom->setDescription('General discussion regarding the community');
        $defaultRoom->setHash(hash('sha256', random_bytes(256) . 'general'));
        $this->addRoom($defaultRoom);

        //Also create default member role.
        $role = new CommunityRole();
        $role->setName('Member');
        $role->setDefaultMember(true);
        $role->setHash(hash('sha256', random_bytes(256)));
        $this->roles->add($role);
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Community
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function isVisible(): ?bool
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Community
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Collection<CommunityMember>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    /**
     * @param ArrayCollection $memberships
     */
    public function setMemberships($memberships)
    {
        $this->memberships = $memberships;
    }

    /**
     * @param CommunityMembership $membership
     */
    public function addMembership(CommunityMembership $membership)
    {
        $this->memberships->add($membership);
        if ($membership->getCommunity() == null) {
            $membership->setCommunity($this);
        }
    }

    /**
     * @param CommunityMembership $user
     */
    public function removeMembership(CommunityMembership $user)
    {
        $this->memberships->removeElement($user);
    }


    /**
     * @return Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * @param ArrayCollection $roles
     */
    public function setRoles(ArrayCollection $roles)
    {
        $this->roles = $roles;
    }

    public function addRole(CommunityRole $role)
    {
        $this->roles->add($role);
        if ($role->getCommunity() == null) {
            $role->setCommunity($this);
        }
    }

    public function removeRole(CommunityRole $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * @return Collection
     */
    public function getInvitations(): Collection
    {
        return $this->invitations;
    }

    /**
     * @param ArrayCollection $invitations
     */
    public function setInvitations(ArrayCollection $invitations)
    {
        $this->invitations = $invitations;
    }

    /**
     * @param CommunityInvitation $communityInvitation
     */
    public function addInvitation(CommunityInvitation $communityInvitation)
    {
        $this->invitations->add($communityInvitation);
        if ($communityInvitation->getCommunity() == null) {
            $communityInvitation->setCommunity($this);
        }
    }

    /**
     * @param CommunityInvitation $communityInvitation
     */
    public function removeInvitation(CommunityInvitation $communityInvitation)
    {
        $this->invitations->removeElement($communityInvitation);
    }

    /**
     * @return Collection
     */
    public function getCandidacies(): Collection
    {
        return $this->candidacies;
    }

    /**
     * @param ArrayCollection $candidacies
     */
    public function setCandidacies(ArrayCollection $candidacies)
    {
        $this->candidacies = $candidacies;
    }

    /**
     * @param CommunityCandidacy $candidacy
     */
    public function addCandidacy(CommunityCandidacy $candidacy)
    {
        $this->candidacies->add($candidacy);
        if ($candidacy->getCommunity() == null) {
            $candidacy->setCommunity($this);
        }
    }

    /**
     * @param CommunityCandidacy $candidacy
     */
    public function removeCandidacy(CommunityCandidacy $candidacy)
    {
        $this->candidacies->removeElement($candidacy);
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return Collection
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
        if ($post->getCommunity() == null) {
            $post->setCommunity($this);
        }
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @return Collection
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @param ArrayCollection $projects
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
    }

    /**
     * @param Project $project
     */
    public function addProject(Project $project)
    {
        $this->projects->add($project);
        if (!$project->getCommunities()->contains($this)) {
            $project->addCommunity($this);
        }
    }

    /**
     * @param Project $project
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
        if ($project->getCommunities()->contains($this)) {
            $project->removeCommunity($this);
        }
    }

    /**
     * @return string
     */
    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    /**
     * @param string $logoUrl
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    }

    /**
     * @return mixed
     */
    public function getBackgroundImage()
    {
        return $this->backgroundImage;
    }

    /**
     * @param mixed $backgroundImage
     */
    public function setBackgroundImage($backgroundImage)
    {
        $this->backgroundImage = $backgroundImage;
    }

    /**
     * @return string
     */
    public function getBackgroundImageUrl(): ?string
    {
        return $this->backgroundImageUrl;
    }

    /**
     * @param string $backgroundImageUrl
     */
    public function setBackgroundImageUrl(string $backgroundImageUrl)
    {
        $this->backgroundImageUrl = $backgroundImageUrl;
    }

    /**
     * @return string
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return boolean
     */
    public function isOpen(): ?bool
    {
        return $this->open;
    }

    /**
     * @param boolean $open
     */
    public function setOpen($open)
    {
        $this->open = $open;
    }

    /**
     * @return Collection
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    /**
     * @param ArrayCollection $rooms
     */
    public function setRooms(ArrayCollection $rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @param Room $room
     */
    public function addRoom(Room $room) {
        $this->rooms->add($room);
        if (!$room->getCommunity()) {
            $room->setCommunity($this);
        }
    }

    /**
     * @param Room $room
     */
    public function removeRoom(Room $room) {
        $this->rooms->removeElement($room);
    }

    /**
     * @return Collection
     */
    public function getBrainstormingSessions(): Collection
    {
        return $this->brainstormingSessions;
    }

    /**
     * @param ArrayCollection $brainstormingSessions
     */
    public function setBrainstormingSessions(ArrayCollection $brainstormingSessions)
    {
        $this->brainstormingSessions = $brainstormingSessions;
    }

    /**
     * @param Session $session
     */
    public function addBrainstormingSession(Session $session)
    {
        $this->brainstormingSessions->add($session);
        if ($session->getCommunity() == null) {
            $session->setCommunity($this);
        }
    }

    /**
     * @param Session $session
     * @return $this
     */
    public function removeBrainstormingSession(Session $session)
    {
        $this->brainstormingSessions->removeElement($session);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        $this->documents->add($document);
        if ($document->getCommunity() == null) {
            $document->setCommunity($this);
        }
    }

    /**
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * @return DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }


    /**
     * @JMS\VirtualProperty()
     */
    public function getNbMembers()
    {
        return $this->getMemberships()->count();
    }

    /**
     * @JMS\VirtualProperty()
     */
    public function getNbProjects()
    {
        return $this->getProjects()->count();
    }

}

