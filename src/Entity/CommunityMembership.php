<?php

namespace App\Entity;

use App\Entity\Enums\CommunityPermissions;
use App\Entity\Superclass\Hashable;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * CommunityMembership
 *
 * @ORM\Table(name="community_membership")
 * @ORM\Entity(repositoryClass="App\Repository\CommunityMembershipRepository")
 */
class CommunityMembership extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var CommunityRole $role
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CommunityRole", cascade={"persist"})
     */
    private $role;

    /**
     * @var Community $community
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Community", inversedBy="memberships")
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"user"})
     */
    private $community;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="communitiesMemberships")
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"community"})
     */
    private $user;

    /**
     * @var CommunityNotificationPreferences
     *
     * @ORM\OneToOne(targetEntity="App\Entity\CommunityNotificationPreferences", inversedBy="communityMembership", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $preferences;

    /**
     * CommunityMembership constructor.
     */
    public function __construct()
    {
        $this->preferences = new CommunityNotificationPreferences();
        $this->preferences->setCommunityMembership($this);
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return CommunityRole
     */
    public function getRole(): ?CommunityRole
    {
        return $this->role;
    }

    /**
     * @param CommunityRole $role
     */
    public function setRole(CommunityRole $role): void
    {
        $this->role = $role;
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community): void
    {
        $this->community = $community;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return CommunityNotificationPreferences
     */
    public function getPreferences(): CommunityNotificationPreferences
    {
        return $this->preferences;
    }

    /**
     * @param CommunityNotificationPreferences $preferences
     */
    public function setPreferences(CommunityNotificationPreferences $preferences
    ): void {
        $this->preferences = $preferences;
    }

    public function canManageRoles()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_ROLE);
    }

    public function canManageMemberships()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_MEMBERSHIP);
    }

    public function canManageCandidacies()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_CANDIDACIES);
    }

    public function canManageInvitations()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_INVITATIONS);
    }

    public function canManageDocuments()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_DOCUMENT);
    }

    public function canManagePosts()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_POST);
    }

    public function canUpdateCommunity()
    {
        return $this->hasRole(CommunityPermissions::UPDATE_COMMUNITY);
    }

    public function canDeleteCommunity()
    {
        return $this->hasRole(CommunityPermissions::DELETE_COMMUNITY);
    }

    public function canManageBrainstormingIdeas()
    {
        return $this->hasRole(CommunityPermissions::MANAGE_BRAINSTORMING_IDEAS);
    }

    public function hasRole(string $role): bool
    {
        $permissions = $this->role->getPermissions();
        return in_array($role, $permissions, true);
    }

}

