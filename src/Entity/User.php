<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 10/02/2017
 * Time: 10:15
 */

namespace App\Entity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users", indexes={@ORM\Index(name="user_idx", columns={"username"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{

    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Groups({"user"})
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Skill", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="user_skills")
     */
    private $skills;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="user_interests")
     */
    private $interests;

    /**
     * @ORM\Column(name="avatar", type="string", nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg", "image/jpg", "image/png"}, maxSize="1M")
     * @JMS\Exclude()
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar_url", type="string", nullable=true)
     */
    private $avatarUrl;

    /**
     * @ORM\Column(name="background_image", type="string", nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg", "image/png"}, maxSize="1M")
     * @JMS\Exclude()
     */
    private $backgroundImage;

    /**
     * @var string
     *
     * @ORM\Column(name="background_image_url", type="string", nullable=true)
     * @JMS\Groups({"user"})
     */
    private $backgroundImageUrl;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityMembership", mappedBy="user", cascade={"persist", "remove"}),
     * @JMS\Exclude()
     */
    private $communitiesMemberships;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityInvitation", mappedBy="user", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $communitiesInvitations;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommunityCandidacy", mappedBy="user", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $communitiesCandidacies;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Room", mappedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="user_room")
     * @JMS\Exclude()
     */
    private $rooms;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectMembership", mappedBy="user", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $projectsMemberships;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectInvitation", mappedBy="user", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $projectsInvitations;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectCandidacy", mappedBy="user", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $projectsCandidacies;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Idea", mappedBy="author", cascade={"persist"})
     * @JMS\Exclude()
     */
    private $ideas;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\IdeaEnhancement", mappedBy="author", cascade={"persist"})
     * @JMS\Exclude()
     */
    private $ideasEnhancements;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="lastChangelogDisplay", type="datetime")
     */
    private $lastChangelogDisplay;

    /**
     * @var UserPreferences
     *
     * @ORM\OneToOne(targetEntity="App\Entity\UserPreferences", inversedBy="user", cascade={"persist", "remove"})
     */
    private $preferences;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="user", cascade={"persist", "remove"})
     */
    private $votes;

    /**
     * @var bool
     *
     * @ORM\Column(name="welcome", type="boolean")
     */
    private $welcome = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="aggreed_with_terms", type="boolean")
     */
    private $agreedWithTerms = false;

    /**
     * @var Organization | null
     *
     * @JMS\Exclude()
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="users", cascade={"persist"})
     */
    private $organization;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->skills = new ArrayCollection();
        $this->interests = new ArrayCollection();
        $this->communitiesMemberships = new ArrayCollection();
        $this->projectsMemberships = new ArrayCollection();
        $this->rooms = new ArrayCollection();
        $this->ideas = new ArrayCollection();
        $this->ideasEnhancements = new ArrayCollection();
        $this->projectsMemberships = new ArrayCollection();
        $this->projectsCandidacies = new ArrayCollection();
        $this->preferences = new UserPreferences();
        $this->votes = new ArrayCollection();
        $this->lastChangelogDisplay = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get Skills the user wants to develop
     *
     * @return Collection<Skill>
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    /**
     * @param ArrayCollection $skills
     * @return User
     */
    public function setSkills($skills): User
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Add a skill the user wants to develop
     *
     * @param Skill $skill
     * @return User
     */
    public function addSkill(Skill $skill): User
    {
        $this->skills->add($skill);

        return $this;
    }

    /**
     * Remove a skill
     *
     * @param Skill $skill
     * @return User
     */
    public function removeSkill(Skill $skill): User
    {
        $this->skills->removeElement($skill);

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return User
     */
    public function setDescription($description): User
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     * @return User
     */
    public function setAvatar($avatar): User
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatarUrl(): ?string
    {
        return $this->avatarUrl;
    }

    /**
     * @param string $avatarUrl
     * @return User
     */
    public function setAvatarUrl($avatarUrl): User
    {
        $this->avatarUrl = $avatarUrl;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBackgroundImage()
    {
        return $this->backgroundImage;
    }

    /**
     * @param mixed $backgroundImage
     * @return User
     */
    public function setBackgroundImage($backgroundImage): User
    {
        $this->backgroundImage = $backgroundImage;

        return $this;
    }

    /**
     * @return string
     */
    public function getBackgroundImageUrl(): ?string
    {
        return $this->backgroundImageUrl;
    }

    /**
     * @param string $backgroundImageUrl
     * @return User
     */
    public function setBackgroundImageUrl(string $backgroundImageUrl): User
    {
        $this->backgroundImageUrl = $backgroundImageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName): User
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName): User
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection<Tag>
     */
    public function getInterests(): Collection
    {
        return $this->interests;
    }

    /**
     * @param ArrayCollection $interests
     * @return User
     */
    public function setInterests($interests): User
    {
        $this->interests = $interests;

        return $this;
    }

    /**
     * @param Tag $interest
     * @return User
     */
    public function addInterest(Tag $interest): User
    {
        if (!$this->interests->contains($interest)) {
            $this->interests->add($interest);
        }

        return $this;
    }

    /**
     * @param Tag $interest
     * @return User
     */
    public function removeInterest(Tag $interest): User
    {
        if ($this->interests->contains($interest)) {
            $this->interests->removeElement($interest);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCommunitiesMemberships(): Collection
    {
        return $this->communitiesMemberships;
    }

    /**
     * @param ArrayCollection $communitiesMemberships
     * @return User
     */
    public function setCommunitiesMemberships(ArrayCollection $communitiesMemberships): User
    {
        $this->communitiesMemberships = $communitiesMemberships;

        return $this;
    }

    /**
     * @param CommunityMembership $membership
     * @return User
     */
    public function addCommunityMembership(CommunityMembership $membership): User
    {
        $this->communitiesMemberships->add($membership);
        if ($membership->getUser() === null) {
            $membership->setUser($this);
        }

        return $this;
    }

    /**
     * @param CommunityMembership $membership
     * @return User
     */
    public function removeCommunityMembership(CommunityMembership $membership): User
    {
        $this->communitiesMemberships->removeElement($membership);

        return $this;
    }


    /**
     * @return Collection
     */
    public function getCommunitiesInvitations(): Collection
    {
        return $this->communitiesInvitations;
    }

    /**
     * @param ArrayCollection $projectsInvitations
     * @return User
     */
    public function setCommunitiesInvitations(ArrayCollection $projectsInvitations): User
    {
        $this->communitiesInvitations = $projectsInvitations;

        return $this;
    }

    /**
     * @param CommunityInvitation $invitation
     * @return User
     */
    public function addCommunityInvitation(CommunityInvitation $invitation): User
    {
        $this->communitiesInvitations->add($invitation);
        if ($invitation->getUser() === null) {
            $invitation->setUser($this);
        }

        return $this;
    }

    /**
     * @param CommunityInvitation $invitation
     * @return User
     */
    public function removeCommunityInvitation(CommunityInvitation $invitation): User
    {
        $this->communitiesInvitations->removeElement($invitation);

        return $this;

    }

    /**
     * @return Collection
     */
    public function getCommunitiesCandidacies(): Collection
    {
        return $this->communitiesCandidacies;
    }

    /**
     * @param ArrayCollection $communitiesCandidacies
     * @return User
     */
    public function setCommunitiesCandidacies(ArrayCollection $communitiesCandidacies): User
    {
        $this->communitiesCandidacies = $communitiesCandidacies;

        return $this;
    }

    /**
     * @param CommunityCandidacy $candidacy
     * @return User
     */
    public function addCommunityCandidacy(CommunityCandidacy $candidacy): User
    {
        $this->communitiesCandidacies->add($candidacy);
        if ($candidacy->getUser() === null) {
            $candidacy->setUser($this);
        }

        return $this;
    }

    /**
     * @param CommunityCandidacy $candidacy
     * @return User
     */
    public function removeCommunityCandidacy(CommunityCandidacy $candidacy): User
    {
        $this->communitiesCandidacies->removeElement($candidacy);

        return $this;

    }


    /**
     * @return Collection
     */
    public function getProjectsMemberships(): Collection
    {
        return $this->projectsMemberships;
    }

    /**
     * @param ArrayCollection $projectsMemberships
     * @return User
     */
    public function setProjectsMemberships(ArrayCollection $projectsMemberships): User
    {
        $this->projectsMemberships = $projectsMemberships;

        return $this;
    }

    /**
     * @param ProjectMembership $membership
     * @return User
     */
    public function addProjectMembership(ProjectMembership $membership): User
    {
        $this->projectsMemberships->add($membership);
        if ($membership->getUser() === null) {
            $membership->setUser($this);
        }

        return $this;
    }

    /**
     * @param ProjectMembership $membership
     * @return User
     */
    public function removeProjectMembership(ProjectMembership $membership): User
    {
        $this->communitiesMemberships->removeElement($membership);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProjectsInvitations(): Collection
    {
        return $this->projectsInvitations;
    }

    /**
     * @param ArrayCollection $projectsInvitations
     * @return User
     */
    public function setProjectsInvitations(ArrayCollection $projectsInvitations): User
    {
        $this->projectsInvitations = $projectsInvitations;

        return $this;
    }

    /**
     * @param ProjectInvitation $invitation
     * @return User
     */
    public function addProjectInvitation(ProjectInvitation $invitation): User
    {
        $this->projectsInvitations->add($invitation);
        if ($invitation->getUser() === null) {
            $invitation->setUser($this);
        }

        return $this;
    }

    /**
     * @param ProjectInvitation $invitation
     * @return User
     */
    public function removeProjectInvitation(ProjectInvitation $invitation): User
    {
        $this->projectsInvitations->removeElement($invitation);

        return $this;

    }

    /**
     * @return Collection
     */
    public function getProjectsCandidacies(): Collection
    {
        return $this->projectsCandidacies;
    }

    /**
     * @param ArrayCollection $projectsCandidacies
     * @return User
     */
    public function setProjectsCandidacies(ArrayCollection $projectsCandidacies): User
    {
        $this->projectsCandidacies = $projectsCandidacies;

        return $this;
    }

    /**
     * @param ProjectCandidacy $candidacy
     * @return User
     */
    public function addProjectCandidacy(ProjectCandidacy $candidacy): User
    {
        $this->projectsCandidacies->add($candidacy);
        if ($candidacy->getUser() === null) {
            $candidacy->setUser($this);
        }

        return $this;
    }

    /**
     * @param ProjectCandidacy $candidacy
     * @return User
     */
    public function removeProjectCandidacy(ProjectCandidacy $candidacy): User
    {
        $this->projectsCandidacies->removeElement($candidacy);

        return $this;

    }

    /**
     * @return Collection
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    /**
     * @param ArrayCollection $rooms
     * @return User
     */
    public function setRooms(ArrayCollection $rooms): User
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * @param Room $room
     * @return User
     */
    public function addRoom(Room $room): User
    {
        $this->rooms->add($room);
        if(!$room->getUsers()->contains($this)){
            $room->addUser($this);
        }

        return $this;
    }

    /**
     * @param Room $room
     * @return User
     */
    public function removeRoom(Room $room): User
    {
        $this->rooms->removeElement($room);
        if ($room->getUsers()->contains($this)) {
            $room->removeUser($this);
        }

        return $this;
    }

    /**
     * @param Room $room
     * @return bool
     */
    public function isRoomMember(Room $room): ?bool
    {
        return $this->rooms->contains($room);
    }

    /**
     * @return Collection
     */
    public function getIdeas(): Collection
    {
        return $this->ideas;
    }

    /**
     * @param ArrayCollection $ideas
     * @return User
     */
    public function setIdeas(ArrayCollection $ideas): User
    {
        $this->ideas = $ideas;

        return $this;
    }

    /**
     * @param Idea $idea
     * @return User
     */
    public function addIdea(Idea $idea): User
    {
        $this->ideas->add($idea);

        return $this;
    }

    /**
     * @param Idea $idea
     * @return User
     */
    public function removeIdea(Idea $idea): User
    {
        $this->ideas->removeElement($idea);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getIdeasEnhancements(): Collection
    {
        return $this->ideasEnhancements;
    }

    /**
     * @param ArrayCollection $ideasEnhancements
     * @return User
     */
    public function setIdeasComment(ArrayCollection $ideasEnhancements): User
    {
        $this->ideasEnhancements = $ideasEnhancements;

        return $this;
    }

    /**
     * @param IdeaEnhancement $ideaEnhancement
     * @return User
     */
    public function addIdeaEnhancement(IdeaEnhancement $ideaEnhancement): User
    {
        $this->ideasEnhancements->add($ideaEnhancement);
        if ($ideaEnhancement->getAuthor() === null) {
            $ideaEnhancement->setAuthor($this);
        }

        return $this;
    }

    /**
     * @param IdeaEnhancement $ideaEnhancement
     * @return User
     */
    public function removeIdeaEnhancement(IdeaEnhancement $ideaEnhancement): User
    {
        $this->ideasEnhancements->removeElement($ideaEnhancement);

        return $this;
    }


    /**
     * @return DateTime
     */
    public function getLastChangelogDisplay(): DateTime
    {
        return $this->lastChangelogDisplay;
    }

    /**
     * @param DateTime $lastChangelogDisplay
     */
    public function setLastChangelogDisplay(DateTime $lastChangelogDisplay
    ): void {
        $this->lastChangelogDisplay = $lastChangelogDisplay;
    }

    /**
     * @return UserPreferences
     */
    public function getPreferences(): UserPreferences
    {
        return $this->preferences;
    }

    /**
     * @param UserPreferences $preferences
     */
    public function setPreferences(UserPreferences $preferences): void
    {
        $this->preferences = $preferences;
    }

    /**
     * @return ArrayCollection
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    /**
     * @param ArrayCollection $votes
     */
    public function setVotes(ArrayCollection $votes): void
    {
        $this->votes = $votes;
    }


    /**
     * @param Vote $vote
     */
    public function addVote(Vote $vote): void
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
        }

        if ($vote->getUser() === null) {
            $vote->setUser($this);
        }
    }

    /**
     * @param Vote $vote
     */
    public function removeVote(Vote $vote): void
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
        }
    }

    /**
     * @return bool
     */
    public function isWelcome(): bool
    {
        return $this->welcome;
    }

    /**
     * @param bool $welcome
     * @return null|User
     */
    public function setWelcome(bool $welcome): ?User
    {
        $this->welcome = $welcome;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAgreedWithTerms(): bool
    {
        return $this->agreedWithTerms;
    }

    /**
     * @param bool $agreedWithTerms
     */
    public function setAgreedWithTerms(bool $agreedWithTerms): void
    {
        $this->agreedWithTerms = $agreedWithTerms;
    }

    /**
     * @return null|Organization
     */
    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    /**
     * @param null|Organization $organization
     */
    public function setOrganization(?Organization $organization): void
    {
        $this->organization = $organization;
    }

    /**
     * Virtual property: we do not want to send all the organization's information, just its name.
     *
     * @return string
     * @JMS\VirtualProperty
     * @JMS\SerializedName("organization")
     */
    public function getOrganizationName(): ?string {
        if ($this->organization !== null) {
            return $this->organization->getName();
        }

        return null;
    }

    public function isAdmin()
    {
        return $this->hasRole(static::ROLE_ADMIN);
    }

}
