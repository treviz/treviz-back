<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Idea
 * @ORM\Table(name="idea")
 * @ORM\Entity(repositoryClass="App\Repository\IdeaRepository")
 */
class Idea extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var Session | null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Session", inversedBy="ideas")
     * @JMS\Groups({"idea"})
     */
    private $session;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\IdeaEnhancement", mappedBy="idea", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $enhancements;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     * @ORM\JoinTable(name="idea_liked")
     * @JMS\MaxDepth(3)
     */
    private $liked;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="idea")
     * @JMS\MaxDepth(3)
     */
    private $forked;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ideas")
     * @JMS\MaxDepth(3)
     */
    private $author;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="idea", cascade={"persist", "remove"})
     */
    private $documents;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="idea", cascade={"persist", "remove"})
     * @JMS\MaxDepth(4)
     */
    private $votes;

    public function __construct()
    {
        $this->enhancements = new ArrayCollection();
        $this->liked = new ArrayCollection();
        $this->forked = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->votes = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Idea
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return Session
     */
    public function getSession(): ?Session
    {
        return $this->session;
    }

    /**
     * @param null|Session $session
     */
    public function setSession(?Session $session)
    {
        $this->session = $session;
        if ($session != null && !$session->getIdeas()->contains($this)) {
            $session->addIdea($this);
        }
    }

    /**
     * @return Collection
     */
    public function getEnhancements(): Collection
    {
        return $this->enhancements;
    }

    /**
     * @param ArrayCollection $enhancements
     */
    public function setEnhancements($enhancements)
    {
        $this->enhancements = $enhancements;
    }

    /**
     * @param IdeaEnhancement $enhancement
     */
    public function addEnhancement(IdeaEnhancement $enhancement)
    {
        $this->enhancements->add($enhancement);

        if ($enhancement->getIdea() == null) {
            $enhancement->setIdea($this);
        }
    }

    public function removeEnhancement(IdeaEnhancement $enhancement)
    {
        $this->enhancements->removeElement($enhancement);
    }

    /**
     * @return Collection
     */
    public function getLiked(): Collection
    {
        return $this->liked;
    }

    /**
     * @param mixed $liked
     */
    public function setLiked($liked)
    {
        $this->liked = $liked;
    }

    public function addLiked(User $user)
    {
        $this->liked->add($user);
    }

    public function removeLiked(User $user)
    {
        $this->liked->removeElement($user);
    }

    /**
     * @return Collection
     */
    public function getForked(): Collection
    {
        return $this->forked;
    }

    /**
     * @param mixed $forked
     */
    public function setForked($forked)
    {
        $this->forked = $forked;
    }

    public function addForked(Project $project)
    {
        $this->forked->add($project);
        if ($project->getIdea() == null) {
            $project->setIdea($this);
        }
    }

    public function removeForked(Project $project)
    {
        $this->forked->remove($project);
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        if(!$author->getIdeas()->contains($this)){
            $author->addIdea($this);
        }
        $this->author = $author;
    }

    /**
     * @return Collection
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    /**
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        $this->documents->add($document);
    }

    /**
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * @return ArrayCollection
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    /**
     * @param ArrayCollection $votes
     */
    public function setVotes(ArrayCollection $votes): void
    {
        $this->votes = $votes;
    }

    /**
     * @param Vote $vote
     */
    public function addVote(Vote $vote): void
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
        }

        if ($vote->getIdea() == null) {
            $vote->setIdea($this);
        }
    }

    /**
     * @param Vote $vote
     */
    public function removeVote(Vote $vote): void
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
        }
    }

    /**
     * @return int
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("score");
     */
    public function getScore(): int
    {
        $score = 0;
        /** @var Vote $vote */
        foreach ($this->votes as $vote) {
            $score += $vote->getScore();
        }
        return $score;
    }

}
