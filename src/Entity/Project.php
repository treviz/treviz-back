<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Project
 *
 * If the project is sent alongside others, for a research for instance, only few attributes must be serialized:
 * its name, short description, logo, funding figures and tags.
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @JMS\Groups({"project"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=140)
     */
    private $shortDescription;

    /**
     * @var ArrayCollection<Skill>
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Skill", cascade={"persist"})
     * @ORM\JoinTable(name="required_skill")
     */
    private $skills;

    /**
     * @var ArrayCollection<Tag>
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", cascade={"persist"})
     * @ORM\JoinTable(name="project_tags")
     */
    private $tags;

    /**
     * @var ArrayCollection<ProjectMember>
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectMembership", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $memberships;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectRole", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $roles;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectInvitation", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    protected $invitations;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectCandidacy", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    protected $candidacies;

    /**
     * @ORM\Column(name="creation_date", type="datetime")
     * @JMS\Groups({"project"})
     */
    private $creationDate;

    /**
     * @var ArrayCollection<Post>
     *
     * @Orm\OneToMany(targetEntity="App\Entity\Post", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $posts;

    /**
     * @var ArrayCollection<Community>
     *
     * @Orm\ManyToMany(targetEntity="App\Entity\Community", inversedBy="projects")
     * @JMS\Exclude()
     */
    private $communities;

    /**
     * @var ArrayCollection<Room>
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Room", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $rooms;

    /**
     * A visible project can be seen by anyone
     * A private project can only be seen by registered members of its communities.
     *
     * @var boolean $visible
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     * @JMS\Groups({"project"})
     * @JMS\SerializedName("isVisible")
     */
    private $visible;

    /**
     * @ORM\Column(name="avatar", type="string", nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/svg+xml"}, maxSize="1M")
     * @JMS\Exclude()
     */
    private $logo;

    /**
     * @ORM\Column(name="logo_url", type="string", nullable=true)
     */
    private $logoUrl;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idea", inversedBy="forked")
     * @JMS\Groups({"project"})
     */
    private $idea;

    /**
     * @var ArrayCollection<Session>
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Session", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $brainstormingSessions;

    /**
     * @var ArrayCollection<Document>
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $documents;

    /**
     * Kanban boards of the project.
     *
     * @var ArrayCollection<Board>
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Board", mappedBy="project", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $boards;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ProjectJob", mappedBy="project", cascade={"remove"})
     * @JMS\Exclude()
     */
    private $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="parent")
     * @JMS\Groups({"project"})
     */
    private $forks;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="forks")
     * @JMS\Groups({"project"})
     */
    private $parent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open", type="boolean")
     * @JMS\Groups({"project"})
     * @JMS\SerializedName("isOpen")
     */
    private $open = true;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->memberships = new ArrayCollection();
        $this->invitations = new ArrayCollection();
        $this->candidacies = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->creationDate = new DateTime();
        $this->posts = new ArrayCollection();
        $this->communities = new ArrayCollection();
        $this->rooms = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->brainstormingSessions = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->boards = new ArrayCollection();
        $this->forks = new ArrayCollection();

        //On Project creation, create a room #General
        $defaultRoom = new Room();
        $defaultRoom->setName('general');
        $defaultRoom->setDescription('General discussion regarding the project');
        $defaultRoom->setHash(hash('sha256', random_bytes(256) . 'general'));
        $this->addRoom($defaultRoom);

        //Also create default member role.
        $role = new ProjectRole();
        $role->setName('Member');
        $role->setDefaultMember(true);
        $role->setHash(hash('sha256', random_bytes(256)));
        $this->addRole($role);
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set skills
     *
     * @param Skill $skill
     */
    public function addSkill(Skill $skill)
    {
        $this->skills[] = $skill;
    }

    /**
     * Remove a skill
     *
     * @param Skill $skill
     */
    public function removeSkill(Skill $skill){
        $this->skills->removeElement($skill);
    }

    /**
     * Get skills
     *
     * @return Collection<Skill>
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    /**
     * @return Collection<Tag>
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $tags
     */
    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag){
        $this->tags->removeElement($tag);
    }

    /**
     * @param ArrayCollection $skills
     */
    public function setSkills(ArrayCollection $skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return Collection
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    /**
     * @param ArrayCollection $memberships
     */
    public function setMemberships(ArrayCollection $memberships)
    {
        $this->memberships = $memberships;
    }

    /**
     * @param ProjectMembership $membership
     */
    public function addMembership(ProjectMembership $membership){
        $this->memberships->add($membership);
        if ($membership->getProject() == null) {
            $membership->setProject($this);
        }
    }

    /**
     * @param ProjectMembership $member
     */
    public function removeMembership(ProjectMembership $member){
        $this->memberships->removeElement($member);
    }

    /**
     * @return Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * @param ArrayCollection $roles
     */
    public function setRoles(ArrayCollection $roles)
    {
        $this->roles = $roles;
    }

    public function addRole(ProjectRole $role)
    {
        $this->roles->add($role);
        if ($role->getProject() == null) {
            $role->setProject($this);
        }
    }

    public function removeRole(ProjectRole $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * @return Collection
     */
    public function getInvitations(): Collection
    {
        return $this->invitations;
    }

    /**
     * @param ArrayCollection $invitations
     */
    public function setInvitations(ArrayCollection $invitations)
    {
        $this->invitations = $invitations;
    }

    /**
     * @param ProjectInvitation $projectInvitation
     */
    public function addInvitation(ProjectInvitation $projectInvitation)
    {
        $this->invitations->add($projectInvitation);
        if ($projectInvitation->getProject() == null) {
            $projectInvitation->setProject($this);
        }
    }

    /**
     * @param ProjectInvitation $projectInvitation
     */
    public function removeInvitation(ProjectInvitation $projectInvitation)
    {
        $this->invitations->removeElement($projectInvitation);
    }

    /**
     * @return Collection
     */
    public function getCandidacies(): Collection
    {
        return $this->candidacies;
    }

    /**
     * @param ArrayCollection $candidacies
     */
    public function setCandidacies(ArrayCollection $candidacies)
    {
        $this->candidacies = $candidacies;
    }

    /**
     * @param ProjectCandidacy $candidacy
     */
    public function addCandidacy(ProjectCandidacy $candidacy)
    {
        $this->candidacies->add($candidacy);
        if ($candidacy->getProject() == null) {
            $candidacy->setProject($this);
        }
    }

    /**
     * @param ProjectCandidacy $candidacy
     */
    public function removeCandidacy(ProjectCandidacy $candidacy)
    {
        $this->candidacies->removeElement($candidacy);
    }

    /**
     * @return Collection
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    /**
     * @param mixed $jobs
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;
    }

    /**
     * @param ProjectJob $job
     */
    public function addJob(ProjectJob $job)
    {
        $this->jobs->add($job);
        if ($job->getProject() == null) {
            $job->setProject($this);
        }
    }

    /**
     * @param ProjectJob $job
     */
    public function removeJob(ProjectJob $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return Collection<Tag>
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     */
    public function setPosts(ArrayCollection $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return Collection<Community>
     */
    public function getCommunities(): Collection
    {
        return $this->communities;
    }

    /**
     * @param ArrayCollection $communities
     */
    public function setCommunities(ArrayCollection $communities)
    {
        $this->communities = $communities;
    }

    /**
     * @param Community $community
     */
    public function addCommunity(Community $community)
    {
        $this->communities->add($community);
        if (!$community->getProjects()->contains($this)) {
            $community->addProject($this);
        }
    }

    /**
     * @param Community $community
     */
    public function removeCommunity(Community $community)
    {
        $this->communities->removeElement($community);
        if ($community->getProjects()->contains($this)) {
            $community->removeProject($this);
        }
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    /**
     * @param string $logoUrl
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    }

    /**
     * @return string
     */
    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return boolean
     */
    public function isVisible(): ?bool
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return Collection
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    /**
     * @param ArrayCollection $rooms
     */
    public function setRooms(ArrayCollection $rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @param Room $room
     */
    public function addRoom(Room $room) {
        $this->rooms->add($room);
        if (!$room->getProject()) {
            $room->setProject($this);
        }
    }

    /**
     * @param Room $room
     */
    public function removeRoom(Room $room) {
        $this->rooms->removeElement($room);
    }

    /**
     * @param Idea|null $idea
     */
    public function setIdea(?Idea $idea)
    {
        $this->idea = $idea;

        if ($idea != null && !$idea->getForked()->contains($this)) {
            $idea->addForked($this);
        }
    }

    public function getIdea(): ?Idea
    {
        return $this->idea;
    }

    /**
     * @return Collection<Session>
     */
    public function getBrainstormingSessions(): Collection
    {
        return $this->brainstormingSessions;
    }

    /**
     * @param ArrayCollection $brainstormingSessions
     */
    public function setBrainstormingSessions(ArrayCollection $brainstormingSessions)
    {
        $this->brainstormingSessions = $brainstormingSessions;
    }

    /**
     * @param Session $session
     */
    public function addBrainstormingSession(Session $session)
    {
        $this->brainstormingSessions->add($session);
        if ($session->getProject() == null) {
            $session->setProject($this);
        }
    }

    /**
     * @param Session $session
     */
    public function removeBrainstormingSession(Session $session)
    {
        $this->brainstormingSessions->removeElement($session);
    }

    /**
     * @return Collection
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     */
    public function setDocuments(ArrayCollection $documents)
    {
        $this->documents = $documents;
    }

    /**
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        $this->documents->add($document);
    }

    /**
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("nbMembers")
     */
    public function getNbMembers()
    {
        return $this->getMemberships()->count();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("nbJobsOpen")
     */
    public function getNbOpenJobs() {
        $i = 0;
        foreach ($this->jobs as $job) {
            if ($job->getHolder() === null) {
                $i++;
            }
        }
        return $i;
    }

    /**
     * @return Collection
     */
    public function getBoards(): Collection
    {
        return $this->boards;
    }

    /**
     * @param ArrayCollection $boards
     */
    public function setBoards(ArrayCollection $boards)
    {
        $this->boards = $boards;
    }

    /**
     * @param Board $board
     */
    public function addBoard(Board $board)
    {
        $this->boards->add($board);
    }

    public function removeBoard(Board $board)
    {
        $this->boards->removeElement($board);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("nbBoards")
     */
    public function getNbBoards()
    {
        return $this->boards->count();
    }

    /**
     * @return ArrayCollection
     */
    public function getForks(): Collection
    {
        return $this->forks;
    }

    /**
     * @param ArrayCollection $forks
     */
    public function setForks(ArrayCollection $forks): void
    {
        $this->forks = $forks;
    }

    /**
     * @param Project $project
     */
    public function addFork(Project $project): void
    {
        $this->forks->add($project);
        if($project->getParent() === null) {
            $project->setParent($this);
        }
    }

    /**
     * @param Project $project
     */
    public function removeFork(Project $project): void
    {
        if ($this->forks->contains($project)) {
            $this->forks->removeElement($project);
        }
    }

    /**
     * @return Project
     */
    public function getParent(): ?Project
    {
        return $this->parent;
    }

    /**
     * @param Project $parent
     */
    public function setParent(Project $parent): void
    {
        $this->parent = $parent;
        if (!$parent->getForks()->contains($this)) {
            $parent->addFork($this);
        }
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->open;
    }

    /**
     * @param bool $open
     */
    public function setOpen(bool $open): void
    {
        $this->open = $open;
    }

}



