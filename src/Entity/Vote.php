<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 24/08/2018
 * Time: 21:24
 */

namespace App\Entity;


use App\Entity\Superclass\Hashable;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Vote
 * @package App\Entity
 *
 * @ORM\Table(name="brainstorming_votes")
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 */
class Vote extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="votes")
     * @JMS\Groups({"idea"})
     */
    private $user;

    /**
     * @var Idea
     * @ORM\ManyToOne(targetEntity="App\Entity\Idea", inversedBy="votes")
     * @JMS\Groups({"user"})
     */
    private $idea;

    /**
     * @var int
     *
     * Score can only be -1, 0 or 1
     * @ORM\Column(type="string")
     *
     * @Assert\Range(
     *     min="-1",
     *     max="1",
     *     minMessage="Score cannot be lower than -1",
     *     maxMessage="Score cannot be higher than 1"
     * )
     */
    private $score = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
        if(!$user->getVotes()->contains($this)) {
            $user->addVote($this);
        }
    }

    /**
     * @return mixed
     */
    public function getIdea(): ?Idea
    {
        return $this->idea;
    }

    /**
     * @param Idea $idea
     */
    public function setIdea(Idea $idea): void
    {
        $this->idea = $idea;
        if (!$idea->getVotes()->contains($this)) {
            $idea->addVote($this);
        }
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score): void
    {
        $this->score = $score;
    }
}
