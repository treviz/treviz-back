<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Posts can be used to discuss around projects or communities.
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @JMS\MaxDepth(1)
     */
    private $author;

    /**
     *  Projects are only (partly) serialized if the post is returned inside a Notification
     *
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="posts", cascade={"persist"})
     * @JMS\MaxDepth(2)
     * @JMS\Groups({"notification", "GeneralPosts"})
     */
    private $project;

    /**
     * Communities are only (partly) serialized if the post is returned inside a Notification
     *
     * @var Community
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Community", inversedBy="posts", cascade={"persist"})
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"notification", "GeneralPosts"})
     */
    private $community;

    /**
     * @var Task
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="posts", cascade={"persist"})
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"notification", "GeneralPosts"})
     */
    private $task;

    /**
     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Document", inversedBy="posts", cascade={"persist"})
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"notification", "GeneralPosts"})
     */
    private $document;

    /**
     * @var ProjectJob
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ProjectJob", inversedBy="posts", cascade={"persist"})
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"notification", "GeneralPosts"})
     */
    private $job;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="post", cascade={"persist", "remove"})
     * @JMS\MaxDepth(7) In order to return comments and the users who posted them.
     */
    private $comments;

    /**
     * @ORM\Column(name="publication_date", type="datetime")
     */
    private $publicationDate;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->publicationDate = new DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Post
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Post
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * Returns the project the post could be linked to
     * @return Project|null
     */
    public function getRelatedProject(): ?Project
    {
        $project = $this->getProject();
        if (null !== $project) {
            return $this->getProject();
        }

        $job = $this->getJob();
        if (null !== $job) {
            return $job->getProject();
        }

        $task = $this->getTask();
        if (null !== $task) {
            return $task->getColumn()->getBoard()->getProject();
        }

        $document = $this->getDocument();
        if (null !== $document) {
            return $document->getProject();
        }

        return null;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param Comment $comment
     */
    public function addComment(Comment $comment)
    {
        $this->comments->add($comment);
        if ($comment->getPost() == null) {
            $comment->setPost($this);
        }
    }

    /**
     * @param Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity($community)
    {
        $this->community = $community;
        if(!$community->getPosts()->contains($this)){
            $community->addPost($this);
        }
    }

    /**
     * Returns the community a post could be linked to
     * @return Community|null
     */
    public function getRelatedCommunity(): ?Community
    {
        $community = $this->getCommunity();
        if (null !== $community) {
            return $community;
        }

        $document = $this->getDocument();
        if (null !== $document) {
            return $document->getCommunity();
        }

        return null;
    }

    /**
     * @return Task
     */
    public function getTask(): ?Task
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        if (!$task->getPosts()->contains($this)) {
            $task->addPost($this);
        }
    }

    /**
     * @return Document
     */
    public function getDocument(): ?Document
    {
        return $this->document;
    }

    /**
     * @param Document $document
     */
    public function setDocument(Document $document)
    {
        $this->document = $document;
        if (!$document->getPosts()->contains($this)) {
            $document->addPost($this);
        }
    }

    /**
     * @return ProjectJob
     */
    public function getJob(): ?ProjectJob
    {
        return $this->job;
    }

    /**
     * @param ProjectJob $job
     */
    public function setJob(ProjectJob $job)
    {
        $this->job = $job;
        if (!$job->getPosts()->contains($this)) {
            $job->addPost($this);
        }
    }

    /**
     * @return mixed
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * @param mixed $publicationDate
     */
    public function setPublicationDate($publicationDate): void
    {
        $this->publicationDate = $publicationDate;
    }

}

