<?php

namespace App\Entity\Superclass;

use Doctrine\ORM\Mapping as ORM;

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 28/10/2017
 * Time: 14:49
 */
class Hashable
{

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, unique=true)
     */
    protected $hash;

    /**
     * @return string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }

}
