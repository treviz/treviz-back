<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 23:00
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommunityNotificationPreferencesRepository")
 * @ORM\Table(name="community_notification_preferences")
 */
class CommunityNotificationPreferences
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new project is linked to the community.
     * @ORM\Column(name="on_new_project", type="boolean")
     */
    private $onNewProject = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a brainstorming session is created.
     * @ORM\Column(name="on_new_brainstorming", type="boolean")
     */
    private $onNewBrainstorming = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when someones candidates to the project.
     * @ORM\Column(name="on_candidacy", type="boolean")
     */
    private $onCandidacy = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new post is created.
     * @ORM\Column(name="on_post", type="boolean")
     */
    private $onPost = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new document is created in the project.
     * @ORM\Column(name="on_new_document", type="boolean")
     */
    private $onNewDocument = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a user accepts an invitation to the project.
     * @ORM\Column(name="on_invitation_accepted", type="boolean")
     */
    private $onInvitationAccepted = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a user rejects an invitation to the project.
     * @ORM\Column(name="on_invitation_rejected", type="boolean")
     */
    private $onInvitationRejected = true;

    /**
     * @var CommunityMembership
     *
     * Membership those preferences are linked to.
     *
     * @ORM\OneToOne(targetEntity="App\Entity\CommunityMembership", mappedBy="preferences")
     * @JMS\Exclude()
     */
    private $communityMembership;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isOnNewProject(): bool
    {
        return $this->onNewProject;
    }

    /**
     * @param bool $onNewProject
     */
    public function setOnNewProject(bool $onNewProject): void
    {
        $this->onNewProject = $onNewProject;
    }

    /**
     * @return bool
     */
    public function isOnNewBrainstorming(): bool
    {
        return $this->onNewBrainstorming;
    }

    /**
     * @param bool $onNewBrainstorming
     */
    public function setOnNewBrainstorming(bool $onNewBrainstorming): void
    {
        $this->onNewBrainstorming = $onNewBrainstorming;
    }

    /**
     * @return bool
     */
    public function isOnCandidacy(): bool
    {
        return $this->onCandidacy;
    }

    /**
     * @param bool $onCandidacy
     */
    public function setOnCandidacy(bool $onCandidacy): void
    {
        $this->onCandidacy = $onCandidacy;
    }

    /**
     * @return bool
     */
    public function isOnPost(): bool
    {
        return $this->onPost;
    }

    /**
     * @param bool $onPost
     */
    public function setOnPost(bool $onPost): void
    {
        $this->onPost = $onPost;
    }

    /**
     * @return bool
     */
    public function isOnNewDocument(): bool
    {
        return $this->onNewDocument;
    }

    /**
     * @param bool $onNewDocument
     */
    public function setOnNewDocument(bool $onNewDocument): void
    {
        $this->onNewDocument = $onNewDocument;
    }

    /**
     * @return CommunityMembership
     */
    public function getCommunityMembership(): CommunityMembership
    {
        return $this->communityMembership;
    }

    /**
     * @param CommunityMembership $communityMembership
     */
    public function setCommunityMembership(CommunityMembership $communityMembership): void {
        $this->communityMembership = $communityMembership;
        if ($communityMembership->getPreferences() == null) {
            $communityMembership->setPreferences($this);
        }
    }

    /**
     * @return bool
     */
    public function isOnInvitationAccepted(): bool
    {
        return $this->onInvitationAccepted;
    }

    /**
     * @param bool $onInvitationAccepted
     */
    public function setOnInvitationAccepted(bool $onInvitationAccepted): void
    {
        $this->onInvitationAccepted = $onInvitationAccepted;
    }

    /**
     * @return bool
     */
    public function isOnInvitationRejected(): bool
    {
        return $this->onInvitationRejected;
    }

    /**
     * @param bool $onInvitationRejected
     */
    public function setOnInvitationRejected(bool $onInvitationRejected): void
    {
        $this->onInvitationRejected = $onInvitationRejected;
    }

}
