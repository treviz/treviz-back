<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 22:47
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPreferencesRepository")
 * @ORM\Table(name="user_preferences")
 */
class UserPreferences
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a the user is invited.
     * @ORM\Column(name="on_invitation", type="boolean")
     */
    private $onInvitation = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when one of the user's candidacy is accepted or rejected.
     * @ORM\Column(name="on_candidacy_change", type="boolean")
     */
    private $onCandidacyChange = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a message is sent to the user.
     * @ORM\Column(name="on_direct_message", type="boolean")
     */
    private $onDirectMessage = false;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new message is sent in a chat room the user is member of.
     * @ORM\Column(name="on_chat_room_message", type="boolean")
     */
    private $onChatRoomMessage = false;

    /**
     * @var boolean
     *
     * Use light or dark theme for the UI.
     * @ORM\Column(name="light_theme", type="boolean")
     */
    private $lightTheme = true;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="preferences")
     * @JMS\Exclude()
     */
    private $user;

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isOnInvitation(): bool
    {
        return $this->onInvitation;
    }

    /**
     * @param bool $onInvitation
     */
    public function setOnInvitation(bool $onInvitation): void
    {
        $this->onInvitation = $onInvitation;
    }

    /**
     * @return bool
     */
    public function isOnCandidacyChange(): bool
    {
        return $this->onCandidacyChange;
    }

    /**
     * @param bool $onCandidacyChange
     */
    public function setOnCandidacyChange(bool $onCandidacyChange): void
    {
        $this->onCandidacyChange = $onCandidacyChange;
    }

    /**
     * @return bool
     */
    public function isLightTheme(): bool
    {
        return $this->lightTheme;
    }

    /**
     * @param bool $lightTheme
     */
    public function setLightTheme(bool $lightTheme): void
    {
        $this->lightTheme = $lightTheme;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isOnDirectMessage(): bool
    {
        return $this->onDirectMessage;
    }

    /**
     * @param bool $onDirectMessage
     */
    public function setOnDirectMessage(bool $onDirectMessage): void
    {
        $this->onDirectMessage = $onDirectMessage;
    }

    /**
     * @return bool
     */
    public function isOnChatRoomMessage(): bool
    {
        return $this->onChatRoomMessage;
    }

    /**
     * @param bool $onChatRoomMessage
     */
    public function setOnChatRoomMessage(bool $onChatRoomMessage): void
    {
        $this->onChatRoomMessage = $onChatRoomMessage;
    }

}
