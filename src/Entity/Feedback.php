<?php

namespace App\Entity;

use App\Entity\Superclass\Hashable;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Feedback
 *
 * @ORM\Table(name="feedback")
 * @ORM\Entity(repositoryClass="App\Repository\FeedbackRepository")
 */
class Feedback extends Hashable
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text")
     */
    private $feedback;

    /**
     * @var bool
     *
     * @ORM\Column(name="praise", type="boolean")
     */
    private $praise = false;

    /**
     * The assignee is the one who is to perform the task
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @JMS\MaxDepth(1)
     */
    private $receiver;

    /**
     * The supervisor determine if the task was correctly performed, by who, and in which proportions.
     * His·er opinion is used to send the reward of the task, and archive it.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @JMS\MaxDepth(1)
     */
    private $giver;

    /**
     * The task for which the feedback was given.
     *
     * @var Task
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="feedbacks")
     * @JMS\MaxDepth(1)
     */
    private $task;

    /**
     * @ORM\Column(name="publication_date", type="datetime", nullable=true)
     */
    private $publicationDate;

    public function __construct()
    {
        $this->publicationDate = new DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set feedback
     *
     * @param string $feedback
     *
     * @return Feedback
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * Get feedback
     *
     * @return string
     */
    public function getFeedback(): ?string
    {
        return $this->feedback;
    }

    /**
     * Set praise
     *
     * @param boolean $praise
     *
     * @return Feedback
     */
    public function setPraise($praise)
    {
        $this->praise = $praise;

        return $this;
    }

    /**
     * Get praise
     *
     * @return bool
     */
    public function getPraise(): bool
    {
        return $this->praise;
    }

    /**
     * @return Task
     */
    public function getTask(): ?Task
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask(Task $task)
    {
        if (!$task->getFeedbacks()->contains($this)) {
            $task->addFeedback($this);
        }
        $this->task = $task;
    }

    /**
     * @return User
     */
    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    /**
     * @param User $receiver
     */
    public function setReceiver(User $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return User
     */
    public function getGiver(): ?User
    {
        return $this->giver;
    }

    /**
     * @param User $giver
     */
    public function setGiver(User $giver)
    {
        $this->giver = $giver;
    }

}

