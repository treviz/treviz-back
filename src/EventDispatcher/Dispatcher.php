<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 30/06/2018
 * Time: 23:32
 */

namespace App\EventDispatcher;

use App\EventSubscriber\MessageEventSubscriber;
use App\EventSubscriber\RoomEventSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\Event;

class Dispatcher
{

    private $dispatcher;

    public function __construct(
        MessageEventSubscriber $messageEventSubscriber,
        RoomEventSubscriber $roomEventSubscriber
    )
    {
        $this->dispatcher = new EventDispatcher();

        $this->dispatcher->addSubscriber($messageEventSubscriber);
        $this->dispatcher->addSubscriber($roomEventSubscriber);
    }

    public function dispatch(string $name, Event $event): void
    {
        $this->dispatcher->dispatch($event, $name);
    }

}
