<?php


namespace App\Utils;


use \Doctrine\Persistence\ObjectRepository;
use Symfony\Component\HttpFoundation\File\File;

/**
 * This utility classes generates a unique ID for an object
 *
 * Class HashGeneratorService
 * @package App\Utils
 */
class HashGeneratorService
{

    public static function generateHash(ObjectRepository $repository, string $dataToHash = null) {
        $hash = hash('sha256', $dataToHash ?? random_bytes(256));

        if ($repository->findOneBy(array('hash' => $hash))) {
            return HashGeneratorService::generateHash($repository);
        }

        return $hash;
    }

    public static function generateFileHashName(File $file)
    {
        $name = hash('sha256',$file->getFilename() . random_bytes(256));
        return $name . '.' . $file->guessExtension();
    }

}
