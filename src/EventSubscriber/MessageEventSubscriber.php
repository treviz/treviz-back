<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 15/10/2018
 * Time: 22:43
 */

namespace App\EventSubscriber;


use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Entity\Message;
use App\Events\MessageEvent;
use App\Service\RabbitMQService;

/**
 * Class MessageEventSubscriber
 *
 * Listens to message creations and update, to trigger the appropriate
 * notifications to be sent.
 *
 * @package App\EventSubscriber
 */
class MessageEventSubscriber implements EventSubscriberInterface
{

    /** @var RabbitMQService */
    private $notificationService;
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(RabbitMQService $notificationService, SerializerInterface $serializer)
    {
        $this->notificationService = $notificationService;
        $this->serializer = $serializer;
    }

    /**
     * Returns the events to which this class has subscribed.
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            MessageEvent::CREATED => 'onMessageCreated',
            MessageEvent::UPDATED => 'onMessageUpdated',
            MessageEvent::DELETED => 'onMessageDeleted',
        );
    }

    public function onMessageCreated(MessageEvent $event): void
    {
        $this->sendNotificationToMembers(
            $event->getMessage(),
            MessageEvent::CREATED
        );
    }

    public function onMessageUpdated(MessageEvent $event): void
    {
        $this->sendNotificationToMembers(
            $event->getMessage(),
            MessageEvent::UPDATED
        );
    }

    public function onMessageDeleted(MessageEvent $event): void
    {
        $this->sendNotificationToMembers(
            $event->getMessage(),
            MessageEvent::DELETED
        );
    }

    /**
     * Sends a notification to all the members of a chat room using RabbitMQ
     *
     * @param Message $message
     * @param string  $eventType
     */
    private function sendNotificationToMembers(Message $message, string $eventType): void
    {
        if (null !== $message->getRoom()) {
            $roomUsers = $message->getRoom()->getUsers();
            $msg = array(
                'type' => $eventType,
                'content' => $message
            );

            foreach ($roomUsers as $roomUser) {
                if ($roomUser !== $message->getOwner()) {
                    $this->notificationService
                        ->sendNotificationToUser(
                            $roomUser,
                            $this->serializer->serialize($msg, 'json')
                        );
                }
            }
        }
    }
}
