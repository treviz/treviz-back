<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 15/10/2018
 * Time: 22:43
 */

namespace App\EventSubscriber;


use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Entity\Room;
use App\Events\RoomEvent;
use App\Entity\User;
use App\Service\RabbitMQService;

/**
 * Class RoomEventSubscriber
 *
 * Listens to message creations and update, to trigger the appropriate
 * notifications to be sent.
 *
 * @package App\EventSubscriber
 */
class RoomEventSubscriber implements EventSubscriberInterface
{

    /** @var RabbitMQService */
    private $notificationService;
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(RabbitMQService $notificationService, SerializerInterface $serializer)
    {
        $this->notificationService = $notificationService;
        $this->serializer = $serializer;
    }

    /**
     * Returns the events to which this class has subscribed.
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            RoomEvent::CREATED => 'onRoomCreated',
            RoomEvent::UPDATED => 'onRoomUpdated',
            RoomEvent::DELETED => 'onRoomDeleted',
        );
    }

    public function onRoomCreated(RoomEvent $event): void
    {
        $this->sendNotificationToMembers(
            $event->getRoom(),
            $event->getAuthor(),
            RoomEvent::CREATED
        );
    }

    public function onRoomUpdated(RoomEvent $event): void
    {
        $this->sendNotificationToMembers(
            $event->getRoom(),
            $event->getAuthor(),
            RoomEvent::DELETED
        );
    }

    public function onRoomDeleted(RoomEvent $event): void
    {
        $this->sendNotificationToMembers(
            $event->getRoom(),
            $event->getAuthor(),
            RoomEvent::DELETED
        );
    }

    /**
     * Sends a notification to all the members of a chat room using RabbitMQ
     *
     * @param Room   $room
     * @param User   $author
     * @param string $eventType
     */
    private function sendNotificationToMembers(Room $room, User $author, string $eventType): void
    {
        $roomUsers = $room->getUsers();
        $msg = array(
            'type' => $eventType,
            'content' => $room
        );

        foreach ($roomUsers as $roomUser) {
            if ($roomUser != $author) {
                $this->notificationService
                    ->sendNotificationToUser(
                        $roomUser,
                        $this->serializer->serialize($msg, 'json')
                    );
            }
        }
    }
}
