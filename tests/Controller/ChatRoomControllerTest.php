<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 30/06/2019
 * Time: 14:49
 */

namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;


class ChatRoomControllerTest extends WebTestCaseWithFixtures
{
    public function testAnonymousUserCannotFetchRooms()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/rooms'
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchOwnRooms($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/rooms'
        );
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testCommunityCreationGeneratesRoom()
    {
        // Create a community
        $client = self::createAuthenticatedClient('dolivaw');
        $community = array(
            'name' => 'Anacreon',
            'description' => 'An other kingdom',
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg'
        );
        $client->request(
            'POST',
            "/v1/communities",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($community)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $createdCommunity = json_decode($response->getContent(), true);
        $hash = $createdCommunity['hash'];

        // Check a room was created too
        $client->request(
            'GET',
            "/v1/rooms?community=$hash"
        );
        $response= $client->getResponse();
        $rooms = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $rooms);
    }

    public function testProjectCreationGeneratesRoom()
    {
        // Create a project
        $client = self::createAuthenticatedClient('dolivaw');
        $project = array(
            'name' => 'Project 1',
            'description' => 'A small project',
            'shortDescription' => 'A small project',
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg',
            'isOpen' => 'true',
            'isVisible' => 'true'
        );
        $client->request(
            'POST',
            "/v1/projects",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($project)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $createdProject = json_decode($response->getContent(), true);
        $hash = $createdProject['hash'];

        // Check a room was created too
        $client->request(
            'GET',
            "/v1/rooms?project=$hash"
        );
        $response= $client->getResponse();
        $rooms = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $rooms);
    }

    public function testAnonymousCannotCreateDirectRoom()
    {
        $roomToCreate = array(
            'name' => 'Just a chatroom',
            'description' => 'Let\'s talk about anything',
            'users' => ['gdornick', 'shardin']
        );
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/rooms',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function testUserCanCreateDirectRoom()
    {
        $name = 'Just a chatroom';
        $description= 'Let\'s talk about anything';
        $roomToCreate = array(
            'name' => $name,
            'description' => $description,
            'users' => ['gdornick', 'shardin']
        );
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'POST',
            '/v1/rooms',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $response = $client->getResponse();
        $room = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($name, $room['name']);
        $this->assertEquals($description, $room['description']);
        $this->assertCount(3, $room['users']);
    }

    public function testUserCannotCreateRoomInOtherProject()
    {
        $roomToCreate = array(
            'name' => 'Just a chatroom',
            'description' => 'Let\'s talk about anything',
            'users' => ['gdornick', 'shardin'],
            'project' => 'nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3'
        );
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/rooms',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testMemberCanCreateRoomInProject()
    {
        $name = 'Next Edition';
        $description= 'Chatroom to discuss the next edition of the encyclopedia';
        $roomToCreate = array(
            'name' => $name,
            'description' => $description,
            'users' => ['gdornick', 'shardin'],
            'project' => 'nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3'
        );
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/rooms',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $response = $client->getResponse();
        $room = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($name, $room['name']);
        $this->assertEquals($description, $room['description']);
        $this->assertCount(3, $room['users']);
    }

    public function testUserCannotCreateRoomInOtherCommunity()
    {
        $roomToCreate = array(
            'name' => 'Just a chatroom',
            'description' => 'Let\'s talk about anything',
            'users' => ['gdornick', 'shardin'],
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'POST',
            '/v1/rooms',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testMemberCanCreateRoomInCommunity()
    {
        $name = 'The fall of Trantor';
        $description= 'When is this supposed to happend ?';
        $roomToCreate = array(
            'name' => $name,
            'description' => $description,
            'users' => ['hseldon'],
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/rooms',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $response = $client->getResponse();
        $room = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($name, $room['name']);
        $this->assertEquals($description, $room['description']);
        $this->assertCount(2, $room['users']);
    }


    public function testUserCannotInviteMember()
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/rooms/ongnzeig2R124unifueg98/remove',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array("hseldon"))
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testMemberCanInviteUser()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/rooms/ongnzeig2R124unifueg98/invite',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array("jpelorat"))
        );
        $response = $client->getResponse();
        $room = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $room['users']);
    }

    public function testUserCannotRemoveMember()
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/rooms/ongnzeig2R124unifueg98/remove',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array("gdornick"))
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testMemberCanRemoveUser()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/rooms/ongnzeig2R124unifueg98/remove',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array("jpelorat"))
        );
        $response = $client->getResponse();
        $room = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $room['users']);
    }

    public function testUserCannotUpdateRoom()
    {
        $name = 'First Room (Updated)';
        $description= 'This is the first (updated) room';
        $roomToCreate = array(
            'name' => $name,
            'description' => $description,
            'users' => ['hseldon'],
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'PUT',
            '/v1/rooms/ongnzeig2R124unifueg98',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testMemberCanUpdateRoom()
    {
        $name = 'First Room (Updated)';
        $description= 'This is the first (updated) room';
        $roomToCreate = array(
            'name' => $name,
            'description' => $description,
            'users' => ['hseldon', 'gdornick'],
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/rooms/ongnzeig2R124unifueg98',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($roomToCreate)
        );
        $response = $client->getResponse();
        $room = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($name, $room['name']);
        $this->assertEquals($description, $room['description']);
        $this->assertCount(2, $room['users']);
    }

    public function testUserCannotDeleteRoom()
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'DELETE',
            '/v1/rooms/ongnzeig2R124unifueg98'
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testMemberCanDeleteRoom()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/rooms/ongnzeig2R124unifueg98'
        );
        $response = $client->getResponse();
        echo $response->getContent();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
    }
}
