<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class ProjectInvitationControllerTest extends WebTestCaseWithFixtures
{
    /**
     * A non-authenticated user should be return a 401 Unauthorized error code
     */
    public function testGetProjectInvitationsWithoutToken(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/projects/invitations?user=gdornick'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can fetch his or her own invitations
     *
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchHisOwnInvitations($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/projects/invitations?user=$username"
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /**
     * A authorized user can create a invitation for a project he/she is part of.
     */
    public function testUserCanCreateProjectInvitation(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );

        $response = $client->getResponse();

        /** Assert the response contains the created invitation */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * A user cannot create a invitation for a project he/she is already candidating to.
     */
    public function testUserCannotCreateSameInvitationTwice(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CONFLICT, $response->getStatusCode());
    }

    public function testUnauthorizedUserCannotAcceptInvitation(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'POST',
            '/v1/projects/invitations/1hre68h1e98z1gze8ghre198h1e8/accept'
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUnauthorizedUserCannotRemoveInvitation(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'DELETE',
            '/v1/projects/invitations/1hre68h1e98z1gze8ghre198h1e8'
        );

        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUserCanAcceptInvitation(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/projects/invitations/1hre68h1e98z1gze8ghre198h1e8/accept'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * A user can delete his/her own invitation.
     */
    public function testUserCanDeclineInvitation(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "hmallow"}'
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $invitation = json_decode($response->getContent(), true);

            $client = self::createAuthenticatedClient('hmallow');
            $client->request(
                'DELETE',
                "/v1/projects/invitations/{$invitation['hash']}"
            );
            $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
        }
    }
}
