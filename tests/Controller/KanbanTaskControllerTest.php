<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 05/04/2019
 * Time: 14:51
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class KanbanTaskControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchTask() {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her'
        );

        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode(),
            'A anonymous user cannot fetch a task'
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchAvailableTasks($username) {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her'
        );
        $response = $client->getResponse();
        $code = $response->getStatusCode();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $code);
        $this->assertEquals('A test task', $content['name']);
    }

    public function testUnknownTaskResultsIn404() {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/ze3rg1er6hj1r61dd23fg'
        );
        $this->assertEquals(
            Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode(),
            'Trying to fetch an unexisting task should result in 404'
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanOnlyFetchAuthorizedTasks($username) {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/ertuzg4z816htrj94arfd'
        );
        $response = $client->getResponse();
        $code = $response->getStatusCode();
        if ($username === 'admin' or $username === 'jpelorat') {
            $content = json_decode($response->getContent(), true);
            $this->assertEquals(Response::HTTP_OK, $code);
            $this->assertEquals('A test task', $content['name']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $code);
        }
    }

    public function testUserCanCreateTask() {
        $client = self::createAuthenticatedClient('gdornick');

        $column = array(
            "name" => "A new task",
            "position" => 2000
        );
        $client->request(
            'POST',
            '/v1/boards/columns/ehr4hgsd86v16z8r4ge9g4azeg/tasks',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($column)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals("A new task", $content["name"]);
    }

    public function testUserCannotCreateTaskInOtherProject() {
        $client = self::createAuthenticatedClient('gdornick');

        $column = array(
            "name" => "A new task",
            "position" => 2000
        );
        $client->request(
            'POST',
            '/v1/boards/columns/ze63g4ze68hr4eh8er4hj9tj4rtj/tasks',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($column)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testOwnerCanUpdateTask() {
        $client = self::createAuthenticatedClient('gdornick');

        $task = array(
            "name" => "A new updated task",
            "description" => "The description of this updated task",
            "position" => 1500,
            "supervisor" => "gdornick",
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals("A new updated task", $content["name"]);
        $this->assertEquals("The description of this updated task", $content["description"]);
    }

    public function testOwnerCanAssignTask() {
        $client = self::createAuthenticatedClient('gdornick');

        $task = array(
            "name" => "A new assigned task",
            "description" => "The description of this new affected task",
            "position" => 1500,
            "supervisor" => "gdornick",
            "assignee" => "hseldon"
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals("A new assigned task", $content["name"]);
        $this->assertEquals("The description of this new affected task", $content["description"]);
        $this->assertEquals("hseldon", $content["assignee"]["username"]);
    }

    public function testOwnerCanMoveTask() {
        $client = self::createAuthenticatedClient('gdornick');

        $task = array(
            "name" => "A new updated task",
            "description" => "The description of this new task",
            "supervisor" => "gdornick",
            "assignee" => "hseldon",
            "column" => "tyki98zf41c8qv4be9h4zs8g",
            "position" => 2000
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testAssigneeUpdateTask()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $task = array(
            "name" => "A new updated task updated by its assignee",
            "description" => "The description of this new task",
            "supervisor" => "gdornick",
            "assignee" => "hseldon",
            "column" => "djhsr9y84zqC9SF45U3K126",
            "position" => 2000
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testAssigneeCanSubmitTask()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $client->request(
            'POST',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her/submit'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testAssigneeCannotArchiveTask()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $task = array(
            "name" => "A new updated task",
            "description" => "The description of this new task",
            "supervisor" => "gdornick",
            "position" => 1500,
            "assignee" => "hseldon",
            "archived" => "true"
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testNonMemberCannotUpdateTask()
    {
        $client = self::createAuthenticatedClient('jpelorat');

        $task = array(
            "name" => "A new task updated by someone else",
            "description" => "The description of this new task",
            "supervisor" => "gdornick",
            "position" => 1500,
            "assignee" => "hseldon",
            "archived" => "true"
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testOwnerCanArchiveTask()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $task = array(
            "name" => "A new updated task",
            "description" => "The description of this new task",
            "supervisor" => "gdornick",
            "position" => 1500,
            "assignee" => "hseldon",
            "archived" => "true"
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($task)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(true, $content["archived"]);
    }

    public function testAssigneeCannotDeleteTask()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $client->request(
            'DELETE',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testOwnerCanDeleteTask() {
        $client = self::createAuthenticatedClient('gdornick');

        $client->request(
            'DELETE',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

}
