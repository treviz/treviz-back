<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class PostControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchPosts(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/posts'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchPosts($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/posts'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        switch ($username) {
            case 'admin':
            case 'hmallow':
                $this->assertCount(5, $content);
                $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content[4]['hash']);
                $this->assertEquals('6w4ve9br8h4r9e', $content[3]['hash']);
                $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[2]['hash']);
                $this->assertEquals('35s4vge9r8h1z6eg', $content[1]['hash']);
                $this->assertEquals('bfgh5j4er9hrz', $content[0]['hash']);
                break;
            case 'gdornick':
                $this->assertCount(7, $content);
                $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content[6]['hash']);
                $this->assertEquals('6w4ve9br8h4r9e', $content[5]['hash']);
                $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[4]['hash']);
                $this->assertEquals('35s4vge9r8h1z6eg', $content[3]['hash']);
                $this->assertEquals('bfgh5j4er9hrz', $content[2]['hash']);
                $this->assertEquals('fgk1ui9m41hg6s1rge8', $content[1]['hash']);
                $this->assertEquals('tdkty35k4t86hj4rtjk', $content[0]['hash']);
                break;
            case 'hseldon':
                $this->assertCount(8, $content);
                $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content[7]['hash']);
                $this->assertEquals('6w4ve9br8h4r9e', $content[6]['hash']);
                $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[5]['hash']);
                $this->assertEquals('35s4vge9r8h1z6eg', $content[4]['hash']);
                $this->assertEquals('bfgh5j4er9hrz', $content[3]['hash']);
                $this->assertEquals('sty61eh498reh', $content[2]['hash']);
                $this->assertEquals('tdkty35k4t86hj4rtjk', $content[1]['hash']);
                $this->assertEquals('neziugnz983223ijifnzeige', $content[0]['hash']);
                break;
            case 'jpelorat':
                $this->assertCount(8, $content);
                $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content[7]['hash']);
                $this->assertEquals('6w4ve9br8h4r9e', $content[6]['hash']);
                $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[5]['hash']);
                $this->assertEquals('35s4vge9r8h1z6eg', $content[4]['hash']);
                $this->assertEquals('dez68g4hre9hq', $content[3]['hash']);
                $this->assertEquals('bfgh5j4er9hrz', $content[2]['hash']);
                $this->assertEquals('hgjfd68sg84zer8j4t9j', $content[1]['hash']);
                $this->assertEquals('rjyt8y4k9uy84l', $content[0]['hash']);
                break;
            case 'shardin':
                $this->assertCount(5, $content);
                $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content[4]['hash']);
                $this->assertEquals('6w4ve9br8h4r9e', $content[3]['hash']);
                $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[2]['hash']);
                $this->assertEquals('35s4vge9r8h1z6eg', $content[1]['hash']);
                $this->assertEquals('bfgh5j4er9hrz', $content[0]['hash']);
                break;
            case 'dolivaw':
                $this->assertCount(6, $content);
                $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content[5]['hash']);
                $this->assertEquals('6w4ve9br8h4r9e', $content[4]['hash']);
                $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[3]['hash']);
                $this->assertEquals('35s4vge9r8h1z6eg', $content[2]['hash']);
                $this->assertEquals('bfgh5j4er9hrz', $content[1]['hash']);
                $this->assertEquals('sty61eh498reh', $content[0]['hash']);
                break;
        }
    }

    public function testUserCanFetchProjectsPost(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/posts?project=nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('35s4vge9r8h1z6eg', $content[0]['hash']);
        $this->assertEquals('vs5x3w4v9zreh41r9eh', $content[1]['hash']);
    }

    public function testUsersCanFetchCommunitiesPosts(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/posts?community=ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('bfgh5j4er9hrz', $content[0]['hash']);
    }

    public function testUsersCanFetchTasksPosts(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/posts?task=oeh51erg16her76her68her'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('tdkty35k4t86hj4rtjk', $content[0]['hash']);
    }

    public function testUsersCanFetchDocumentPosts(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/posts?document=z1h68jty1t8k9u'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('fgk1ui9m41hg6s1rge8', $content[0]['hash']);
    }

    public function testUsersCanFetchJobPosts(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/posts?job=rk541tkl8ty4kty68lk'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('hgjfd68sg84zer8j4t9j', $content[0]['hash']);
    }

    public function testUserCannotFetchProtectedProjectsPost(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/posts?project=z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(0, $content);
    }

    public function testUsersCannotFetchProtectedCommunitiesPosts(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/posts?community=gez3g5hergf6vcx1vbvghyuo7ythrtg'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(0, $content);
    }

    public function testUsersCannotFetchProtectedTasksPosts(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/posts?task=ertuzg4z816htrj94arfd'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(0, $content);
    }

    public function testUsersCannotFetchProtectedDocumentPosts(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/posts?document=kjgbeziubzib763T2582'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(0, $content);
    }

    public function testUsersCannotFetchProtectedJobPosts(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/posts?job=rk541tkl8ty4kty68lk'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(0, $content);
    }

    public function testUsersCanFetchPublicPostDetail(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/posts/eh4jr6n1fg6j4ty86k4ty'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('eh4jr6n1fg6j4ty86k4ty', $content['hash']);
        $this->assertEquals('Lorem ipsum dolor sit amet, consectetur adipiscing elit.', $content['message']);
    }

    public function testFetchUnavailablePostResultsIn404(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/posts/6z1ghe8r6j4y9k8t4yk9yu4aze4'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCanCreateGeneralPost(): void
    {
        $post = array(
            'message' => 'Nunc ultrices gravida vehicula'
        );
        $this->createPostAndAssertMatches($post, 'gdornick');
    }

    public function testUserCanCreateProjectPost(): void
    {
        $post = array(
            'message' => 'Pellentesque vitae massa nulla',
            'project' => 'nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3'
        );
        $this->createPostAndAssertMatches($post, 'gdornick');
    }

    public function testUserCanCreateCommunityPost(): void
    {
        $post = array(
            'message' => 'Maecenas id lacus a nisl maximus dignissim',
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $this->createPostAndAssertMatches($post, 'gdornick');
    }

    public function testUserCanCreateTaskPost(): void
    {
        $post = array(
            'message' => 'Cras ultrices rhoncus tortor, ac sagittis nibh rhoncus non.',
            'task' => 'oeh51erg16her76her68her'
        );
        $this->createPostAndAssertMatches($post, 'gdornick');
    }

    public function testUserCanCreateDocumentPost(): void
    {
        $post = array(
            'message' => 'Donec quis hendrerit sapien, eget faucibus risus.',
            'document' => 'z1h68jty1t8k9u'
        );
        $this->createPostAndAssertMatches($post, 'gdornick');
    }

    public function testUserCanCreateJobPost(): void
    {
        $post = array(
            'message' => 'Ut sit amet turpis arcu. Integer sit amet viverra dolor.',
            'job' => 'rk541tkl8ty4kty68lk'
        );
        $this->createPostAndAssertMatches($post, 'jpelorat');
    }

    public function testUserCannotCreateProtectedProjectPost(): void
    {
        $post = array(
            'message' => 'Ut sit amet turpis arcu. Integer sit amet viverra dolor.',
            'project' => 'z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19'
        );
        $this->createPostAndAssertForbidden($post, 'hmallow');
    }

    public function testUserCannotCreateProtectedCommunityPost(): void
    {
        $post = array(
            'message' => 'Ut sit amet turpis arcu. Integer sit amet viverra dolor.',
            'project' => 'z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19'
        );
        $this->createPostAndAssertForbidden($post, 'hmallow');
    }

    public function testUserCannotCreateProtectedTaskPost(): void
    {
        $post = array(
            'message' => 'Ut sit amet turpis arcu. Integer sit amet viverra dolor.',
            'task' => 'ertuzg4z816htrj94arfd'
        );
        $this->createPostAndAssertForbidden($post, 'hmallow');
    }

    public function testUserCannotCreateProtectedDocumentPost(): void
    {
        $post = array(
            'message' => 'Ut sit amet turpis arcu. Integer sit amet viverra dolor.',
            'document' => 'kjgbeziubzib763T2582'
        );
        $this->createPostAndAssertForbidden($post, 'shardin');
    }

    public function testUserCannotCreateProtectedJobPost(): void
    {
        $post = array(
            'message' => 'Ut sit amet turpis arcu. Integer sit amet viverra dolor.',
            'job' => 'rk541tkl8ty4kty68lk'
        );
        $this->createPostAndAssertForbidden($post, 'hmallow');
    }

    public function testUserCanUpdateOwnPost(): void
    {
        $post = array(
            'message' => 'Sed sed lorem vulputate, pharetra est a, sollicitudin ante.'
        );
        $this->updatePostAndAssertMatches(
            'eh4jr6n1fg6j4ty86k4ty',
            $post,
            'gdornick'
        );
    }

    public function testUserCannotUpdateSomeoneElsePost(): void
    {
        $post = array(
            'message' => 'Proin urna lectus, luctus sit amet rutrum in.'
        );
        $this->updatePostAndAssertForbidden(
            'eh4jr6n1fg6j4ty86k4ty',
            $post,
            'hmallow'
        );
    }

    public function testProjectAdminCanUpdatePost(): void
    {
        $post = array(
            'message' => 'Integer tempor est nec nisl finibus tempor.',
            'project' => 'nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3'
        );
        $this->updatePostAndAssertMatches(
            '35s4vge9r8h1z6eg',
            $post,
            'gdornick'
        );
    }

    public function testCommunityAdminCanUpdatePost(): void
    {
        $post = array(
            'message' => 'Aliquam erat volutpat.',
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $this->updatePostAndAssertMatches(
            'bfgh5j4er9hrz',
            $post,
            'gdornick'
        );
    }

    public function testUserCanDeleteOwnPost(): void
    {
        $this->deletePostAndAssertOK(
            'eh4jr6n1fg6j4ty86k4ty',
            'gdornick'
        );
    }

    public function testUserCannotDeleteSomeoneElsePost(): void
    {
        $this->deletePostAndAssertForbidden(
            '35s4vge9r8h1z6eg',
            'hmallow'
        );
    }

    public function testProjectAdminCanDeletePost(): void
    {
        $this->deletePostAndAssertOK(
            '35s4vge9r8h1z6eg',
            'gdornick'
        );
    }

    public function testCommunityAdminCanDeletePost(): void
    {
        $this->deletePostAndAssertOK(
            'bfgh5j4er9hrz',
            'gdornick'
        );
    }

    // Helper functions

    private function createPostAndAssertMatches($post, $username): void {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'POST',
            '/v1/posts',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($post)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($post['message'], $content['message']);
    }

    private function createPostAndAssertForbidden($post, $username): void {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'POST',
            '/v1/posts',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($post)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    private function updatePostAndAssertMatches($postHash, $post, $username): void {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'PUT',
            "/v1/posts/$postHash",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($post)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($post['message'], $content['message']);
    }

    private function updatePostAndAssertForbidden($postHash, $post, $username): void {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'PUT',
            "/v1/posts/$postHash",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($post)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    private function deletePostAndAssertOK($postHash, $username): void {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'DELETE',
            "/v1/posts/$postHash"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    private function deletePostAndAssertForbidden($postHash, $username): void {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'DELETE',
            "/v1/posts/$postHash"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

}
