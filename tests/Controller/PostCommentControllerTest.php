<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class PostCommentControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousUserCannotPostComment()
    {
        $comment = array(
            "message" => "This is a comment"
        );
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/posts/eh4jr6n1fg6j4ty86k4ty/comments',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($comment)
        );
        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testUserCannotCommentPostHeCannotSee()
    {
        $comment = array(
            "message" => "This is a comment"
        );
        $client = self::createAuthenticatedClient("gdornick");
        $client->request(
            'POST',
            '/v1/posts/dez68g4hre9hq/comments',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($comment)
        );
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testUserCanCommentVisiblePost()
    {
        $comment = array(
            "message" => "This is a comment"
        );
        $client = self::createAuthenticatedClient("jpelorat");
        $client->request(
            'POST',
            '/v1/posts/dez68g4hre9hq/comments',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($comment)
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_CREATED,
            $response->getStatusCode()
        );
        $createdPost = json_decode($response->getContent(), true);
        $this->assertEquals($comment["message"], $createdPost["message"]);
        $this->assertEquals("jpelorat", $createdPost["author"]["username"]);
    }

    public function testUserCannotUpdateOtherPost()
    {
        $comment = array(
            "message" => "This is an updated comment"
        );
        $client = self::createAuthenticatedClient("jpelorat");
        $client->request(
            'PUT',
            '/v1/posts/comments/iuzebfgz29Abuogé',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($comment)
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testOwnerCanUpdatePost()
    {
        $comment = array(
            "message" => "This is an updated comment"
        );
        $client = self::createAuthenticatedClient("hseldon");
        $client->request(
            'PUT',
            '/v1/posts/comments/iuzebfgz29Abuogé',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($comment)
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
        $updatedComment = json_decode($response->getContent(), true);
        $this->assertEquals(
            "This is an updated comment",
            $updatedComment["message"]
        );
        $this->assertEquals(
            "hseldon",
            $updatedComment["author"]["username"]
        );
    }

    public function testAdminCanUpdatePost()
    {
        $comment = array(
            "message" => "This is a comment updated by an admin"
        );
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'PUT',
            '/v1/posts/comments/iuzebfgz29Abuogé',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($comment)
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
        $updatedComment = json_decode($response->getContent(), true);
        $this->assertEquals(
            "This is a comment updated by an admin",
            $updatedComment["message"]
        );
        $this->assertEquals(
            "hseldon",
            $updatedComment["author"]["username"]
        );
    }

    public function testUserCannotDeleteOtherPost()
    {
        $client = self::createAuthenticatedClient("jpelorat");
        $client->request(
            'DELETE',
            '/v1/posts/comments/iuzebfgz29Abuogé'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testOwnerCanDeletePost()
    {
        $client = self::createAuthenticatedClient("hseldon");
        $client->request(
            'DELETE',
            '/v1/posts/comments/iuzebfgz29Abuogé'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }

    public function testAdminCanDeletePost()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'DELETE',
            '/v1/posts/comments/zeoignez0912non124iuB124'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }

}
