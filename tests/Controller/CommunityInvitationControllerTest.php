<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class CommunityInvitationControllerTest extends WebTestCaseWithFixtures
{
    /**
     * A non-authenticated user should be return a 401 Unauthorized error code
     */
    public function testGetCommunityInvitationsWithoutToken(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/communities/invitations?user=gdornick'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can fetch his or her own invitations
     *
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchHisOwnInvitations($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/communities/invitations?user=$username"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * A authorized user can create a invitation for a community he/she is part of.
     */
    public function testUserCanCreateCommunityInvitation(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/communities/gedhgrsdv68c1xvb68tyjk8t9j4hfgfh/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();

        /** Assert the response contains the created invitation */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * A user cannot create a invitation for a community he/she is already candidating to.
     */
    public function testUserCannotCreateSameInvitationTwice(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/communities/gedhgrsdv68c1xvb68tyjk8t9j4hfgfh/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CONFLICT, $response->getStatusCode());
    }

    public function testUnauthorizedUserCannotAcceptInvitation(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/communities/invitations/235noiN53f2362yryhrz0FEZG9/accept'
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUnauthorizedUserCannotRemoveInvitation(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/communities/invitations/235noiN53f2362yryhrz0FEZG9'
        );

        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUserCanAcceptInvitation(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/communities/invitations/235noiN53f2362yryhrz0FEZG9/accept'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * A user can delete his/her own invitation.
     */
    public function testUserCanDeclineInvitation(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/communities/gedhgrsdv68c1xvb68tyjk8t9j4hfgfh/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "hseldon"}'
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $invitation = json_decode($response->getContent(), true);

            $client = self::createAuthenticatedClient('hseldon');
            $client->request(
                'DELETE',
                "/v1/communities/invitations/{$invitation['hash']}"
            );
            $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
        }
    }
}
