<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class SkillControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchSkills()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/skills'
        );
        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode(),
            'An anonymous user cannot fetch the skills'
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchSkills($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/skills'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'An connected user cann fetch the skills'
        );

        $this->assertCount(
            10,
            json_decode($response->getContent(), true),
            'All skills can be retrieved'
        );
    }

    public function testUserCanSearchSkillByName()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/skills?name=té'
        );
        $response = $client->getResponse();
        $skills = json_decode($response->getContent(), true);

        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'An connected user cann search the skills by name'
        );

        $this->assertEquals(
            array(
                array('name' => 'Comptabilité'),
                array('name' => 'Relativité générale'),
                array('name' => 'Science des matériaux')
            ),
            $skills,
            'All 3 skills are correctly fetched'
        );
    }

    public function testUserCanCreateNewSkill()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $skill = array('name' => 'Engineering');
        $client->request(
            'POST',
            '/v1/skills',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($skill)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(
            Response::HTTP_CREATED,
            $response->getStatusCode(),
            'Skill creation results in 201 status code'
        );

        $this->assertEquals(
            $skill,
            $content,
            'Created skill matches the request that was made'
        );
    }

    public function testUserCannotUpdateSkill()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $skill = array('name' => 'Design Thinking');
        $client->request(
            'PUT',
            '/v1/skills/Design',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($skill)
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode(),
            'Attempt to update a skill results in 403 for a non-admin'
        );
    }

    public function testAdminCanUpdateSkill()
    {
        $client = self::createAuthenticatedClient('admin');

        $skill = array("name" => "Design Thinking");
        $client->request(
            'PUT',
            '/v1/skills/Design',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($skill)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'Skill update results in 200 status code'
        );

        $this->assertEquals(
            $skill,
            $content,
            'Created skill matches the request that was made'
        );
    }

    public function testUserCannotDeleteSkill()
    {

        $client = self::createAuthenticatedClient('gdornick');

        $client->request(
            'DELETE',
            '/v1/skills/Pitch',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode(),
            'Attempt to delete a skill results in 403 for a non-admin'
        );
    }

    public function testAdminCanDeleteSkill()
    {

        $client = self::createAuthenticatedClient('admin');

        $client->request(
            'DELETE',
            '/v1/skills/Pitch',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode(),
            'Skill deletion results in 204'
        );
    }
}
