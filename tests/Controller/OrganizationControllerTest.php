<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 07/01/2019
 * Time: 20:59
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class OrganizationControllerTest extends WebTestCaseWithFixtures
{

    public function testUserMustBeAuthenticate()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/organizations'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testConnectedUserCanCreateOrganization()
    {
        $organization = array(
            'name' => "Treviz"
        );
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/organizations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($organization)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchOrganizations($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/organizations'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testOrganizationNameIsUnique()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'POST',
            '/v1/organizations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(["name" => "First Empire"])
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CONFLICT, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCannotUpdateOrganization($username)
    {
        if ($username !== "admin") {
            $client = self::createAuthenticatedClient($username);
            $client->request(
                'PUT',
                '/v1/organizations/Treviz',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json'),
                json_encode([ "name" => "treviz" ])
            );
            $response = $client->getResponse();
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        } else {
            $this->assertTrue(true);
        }
    }

    public function testAdminCanUpdateOrganization()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'PUT',
            '/v1/organizations/Treviz',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode([ "name" => "treviz" ])
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testUserCanAssignOwnOrganisation()
    {
        $updatedUser = array(
            "firstName" => "Janov",
            "lastName" => "Pelorat",
            "username" => "jpelorat",
            "description" => "Lorem ipsum",
            "organization" => "First Empire"
        );

        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'PUT',
            '/v1/users/jpelorat',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedUser)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('First Empire', $content['organization']);
    }

    public function testUserCantAssignElseOrganization()
    {
        $updatedUser = array(
            "firstName" => "Salvor",
            "lastName" => "Hardin",
            "username" => "shardin",
            "organization" => "First Empire"
        );

        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'PUT',
            '/v1/users/shardin',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedUser)
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAdminCanAssignOrganization()
    {
        $updatedUser = array(
            "firstName" => "Salvor",
            "lastName" => "Hardin",
            "username" => "shardin",
            "organization" => "treviz"
        );

        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'PUT',
            '/v1/users/shardin',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedUser)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals( 'treviz', $content['organization']);
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCannotDeleteOrganization($username)
    {
        if ($username !== "admin") {
            $client = self::createAuthenticatedClient($username);
            $client->request(
                'DELETE',
                '/v1/organizations/treviz',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json'),
                json_encode([ "name" => "treviz" ])
            );
            $response = $client->getResponse();
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        } else {
            $this->assertTrue(true);
        }
    }

    public function testAdminCanDeleteOrganization()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'DELETE',
            '/v1/organizations/treviz',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode([ "name" => "treviz" ])
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
