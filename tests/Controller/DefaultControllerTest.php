<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends WebTestCase
{

    public function testUserCanFetchInstanceData()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $expectedContent = array(
            'host'           => 'Treviz Local',
            'termsOfService' => 'https://treviz.xyz/terms-of-service',
            'privacyNotice'  => 'https://treviz.xyz/privacy-notice',
            'open'           => true,
            'documentation'  => '/v1/doc',
            'version'        => '0.7.2'
        );
        $this->assertEquals($expectedContent, $content);
    }

}
