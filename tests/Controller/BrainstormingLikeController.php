<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class BrainstormingLikeController extends WebTestCaseWithFixtures
{

    public function testAnonymousUserCannotLikeIdea()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/like'
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUsernames
     * @param $username
     */
    public function testOnlyMemberCanLikeIdea($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/like'
        );

        if ('shardin' === $username) {
            $this->assertEquals(
                Response::HTTP_CONFLICT,
                $client->getResponse()->getStatusCode(),
                'Trying to like an already liked idea results in 409 (Conflict)'
            );
        } elseif ('admin' === $username || 'gdornick' === $username) {
            $this->assertEquals(
                Response::HTTP_OK,
                $client->getResponse()->getStatusCode(),
                'A member (or admin) can like an idea'
            );
        } else {
            $this->assertEquals(
                Response::HTTP_FORBIDDEN,
                $client->getResponse()->getStatusCode(),
                'Only a community member can like an idea (or an admin)'
            );
        }
    }

    public function testUserCanUnlikeIdea()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/unlike'
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            'A user can stop liking an idea he/she has already liked'
        );
    }

    public function testUserCannotUnlikeIdeaTwice()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/unlike'
        );
        $this->assertEquals(
            Response::HTTP_CONFLICT,
            $client->getResponse()->getStatusCode(),
            'A user cannot unlike an idea he or she does not like'
        );
    }
}
