<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 26/12/2018
 * Time: 11:49
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

/**
 * Class ProjectCandidacyControllerTest
 * @package Tests\ProjectBundle\Controller
 */
class ProjectCandidacyControllerTest extends WebTestCaseWithFixtures
{

    /**
     * A non-authenticated user should be return a 401 Unauthorized error code
     */
    public function testGetProjectCandidaciesWithoutToken(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/projects/candidacies?user=gdornick'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can fetch his or her own candidacies
     *
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchHisOwnCandidacies($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/projects/candidacies?user=$username"
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can create a candidacy for a project he/she is not part of.
     */
    public function testUserCanCreateProjectCandidacy(): void
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world"}'
        );

        $response = $client->getResponse();

        /** Assert the response contains the created candidacy */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * A user cannot create a candidacy for a project he/she is already candidating to.
     */
    public function testUserCannotCreateSameCandidacyTwice(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world"}'
        );

        $this->assertEquals(Response::HTTP_CONFLICT, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can delete his/her own candidacy.
     */
    public function testCandidateCanRemoveHisOwnCandidacy(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world"}'
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $candidacy = json_decode($response->getContent(), true);

            $client->request(
                'DELETE',
                "/v1/projects/candidacies/{$candidacy['hash']}"
            );
            $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
        }
    }

    public function testUserCanCandidateToJob(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array('message' => 'hello world', 'job' => 'ziuegneziugezniug214ijb'))
        );

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }

    public function testUnauthorizedUserCannotAcceptCandidacy(): void
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/projects/candidacies/e56htrj9r1eg86qs1ghg6zre1ge6/accept'
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUnauthorizedUserCannotRemoveCandidacy(): void
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'DELETE',
            '/v1/projects/candidacies/e56htrj9r1eg86qs1ghg6zre1ge6'
        );

        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }


    /**
     * An project admin can accept one's candidacy for a project.
     */
    public function testAdminCanAcceptCandidacy(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/candidacies/e56htrj9r1eg86qs1ghg6zre1ge6/accept'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testAcceptingJobCandidacySpecifiesHolder()
    {
        $jobHash = 'ziuegneziugezniug214ijb';
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            "v1/projects/candidacies?job=$jobHash"
        );
        $response = $client->getResponse();
        $candidacy = json_decode($response->getContent(), true);
        $candidacyHash = $candidacy[0]['hash'];

        $client->request(
            'POST',
            "/v1/projects/candidacies/$candidacyHash/accept"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $client->request(
            'GET',
            "/v1/projects/jobs/$jobHash"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $job = json_decode($response->getContent(), true);
        $this->assertEquals('dolivaw', $job['holder']['username']);

    }

}
