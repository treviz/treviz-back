<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class CommunityMembershipControllerTest extends WebTestCaseWithFixtures
{
    public function testAnonymousCannotFetchMemberships(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/communities/memberships'
        );

        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testUserCanFetchMemberships(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/communities/memberships'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('[]', $response->getContent());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchCommunityMemberships($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/memberships'
        );
        $response = $client->getResponse();

        if ($username === 'hseldon' or $username === 'dolivaw' or $username === 'admin') {
            $content = json_decode($response->getContent(), true);
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
            $this->assertCount(2, $content);
            $this->assertEquals('h4df984bngyt94lkyu1s65g', $content[0]['hash']);
            $this->assertEquals('yuoliu57o7ezaf1sd6vb5df4h9re', $content[1]['hash']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    public function testFetchingMembershipsFromWrongCommunityResultIn404(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/communities/z5htr86j4tykyu89l4y9lui8rjh19/memberships'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCanFetchUserMemberships(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'GET',
            '/v1/communities/memberships?user=hseldon'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('h4df984bngyt94lkyu1s65g', $content[0]['hash']);
    }

    public function testInvitedUserCanFetchMemberships(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'GET',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/memberships'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, json_decode($response->getContent(), true));
    }

    public function testAcceptingCandidacyCreatesMembership(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/communities/candidacies/zjnziugb4eiu71funiuezh8G7365/accept'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotNull($content['hash']);
        $this->assertNotNull($content['role']);
        $this->assertEquals(['MANAGE_POST'], $content['role']['permissions']);
    }

    public function testAcceptingInvitationCreatesMembership(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/communities/invitations/235noiN53f2362yryhrz0FEZG9/accept'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotNull($content['hash']);
        $this->assertNotNull($content['role']);
    }

    public function testUserCanCreateMembershipInOpenCommunity()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf/memberships'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertNotNull($content['hash']);
        $this->assertNotNull($content['role']);
        $this->assertNotNull($content['role']['hash']);
    }

    public function testUserCannotUpdateOwnMembership(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'PUT',
            '/v1/communities/memberships/j7ty4kl869lk1g6n1ty68jsdbf',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'shardin',
                'role' => 'nignzeg619ez1gz4eg1fd321'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testUserCannotUpdateSomeoneEsleMembership(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'PUT',
            '/v1/communities/memberships/j7ty4kl869lk1g6n1ty68jsdbf',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'shardin',
                'role' => 'nignzeg619ez1gz4eg1fd321'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testAdminCanUpdateOwnMembership(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/communities/memberships/j7ty4kl869lk1g6n1ty68jsdbf',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'shardin',
                'role' => 'nignzeg619ez1gz4eg1fd321'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
    }

    public function testMembershipCannotBeUpdatedWithFalseRole(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/communities/memberships/j7ty4kl869lk1g6n1ty68jsdbf',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'shardin',
                'role' => 'vej1kt98t1y6legnzegre51jh9ze1ge'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );
    }


    public function testUserCannotRemoveOtherMembership(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'DELETE',
            '/v1/communities/memberships/h4df984bngyt94lkyu1s65g'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testCommunityAdminCanRemoveOtherMembership(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'DELETE',
            '/v1/communities/memberships/j687ty4j6h1fa68ze4a5a45e'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }

    public function testUserCanRemoveOwnMembership(): void
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'DELETE',
            '/v1/communities/memberships/yuoliu57o7ezaf1sd6vb5df4h9re'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }
}
