<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 05/04/2019
 * Time: 15:10
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class KanbanColumnControllerTest extends WebTestCaseWithFixtures
{

    private $column;

    public function testAnonymousCannotFetchColumns()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/boards/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/columns'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function test404Column()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/boards/nuyl5iop45kjhdfvc1v2x1z65etger56her6hr4eh/columns'
        );

        $this->assertEquals(
            Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode(),
            'Trying to fetch the columns of an unexisting board should result in 404'
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testOnlyAuthorizedUsersFetchColumns($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards/rtj3sj68n4fdhbr86tj4/columns'
        );
        $response = $client->getResponse();
        $code = $response->getStatusCode();

        if ($username === 'admin' or $username === 'jpelorat') {
            $this->assertEquals(Response::HTTP_OK,
                $code,
                'Only an authorized user can see the columns of a private project'
            );
            $content = json_decode($response->getContent(), true);
            $this->assertEquals(1,
                sizeof($content),
                "$username should see 1 column for this private project"
            );
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN,
                $code,
                'A user cannot se the columns of a private project he is not member of'
            );
        }
    }

    public function testAdminCanCreateColumn()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $column = array(
            "name" => "Test column"
        );
        $client->request(
            'POST',
            '/v1/boards/ipuhneh6rj16rtjr896tj/columns',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($column)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($content["name"], "Test column");
        $this->assertEquals($content["tasks"], []);
        $this->column = $content["hash"];
    }

    public function testAdminCanUpdateColumn()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $column = array(
            "name" => "Pending (updated)"
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/ehr4hgsd86v16z8r4ge9g4azeg',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($column)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($content["name"], "Pending (updated)");
    }

    public function testUserCannotUpdateColumn() {
        $client = self::createAuthenticatedClient('hseldon');

        $column = array(
            "name" => "Test column (updated)"
        );
        $client->request(
            'PUT',
            '/v1/boards/columns/ehr4hgsd86v16z8r4ge9g4azeg',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($column)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAdminCanDeleteColumn()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $client->request(
            'DELETE',
            '/v1/boards/columns/ehr4hgsd86v16z8r4ge9g4azeg'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

}
