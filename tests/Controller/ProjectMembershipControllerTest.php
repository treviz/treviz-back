<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class ProjectMembershipControllerTest extends WebTestCaseWithFixtures
{
    public function testAnonymousCannotFetchMemberships(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/projects/memberships'
        );

        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testUserCanFetchMemberships(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/projects/memberships'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('[]', $response->getContent());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchProjectMemberships($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19/memberships'
        );
        $response = $client->getResponse();

        if ($username === 'jpelorat' || $username === 'admin') {
            $content = json_decode($response->getContent(), true);
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertEquals('g96z8h4E98H4RE9HEZ654GH', $content[0]['hash']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    public function testInvitedUserCanFetchMemberships(): void
    {
        /* Part 1 : create the invitation */
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();

        /** Assert the response contains the created invitation */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        /* Part 2 : check the invited user can access the project */
        $newClient = self::createAuthenticatedClient('dolivaw');
        $newClient ->request(
            'GET',
            "/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19/memberships"
        );
        $response = $newClient->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
    }

    public function testFetchingMembershipsFromWrongProjectResultIn404(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/projects/z5htr86j4tykyu89l4y9lui8rjh19/memberships'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCanFetchUserMemberships(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/projects/memberships?user=hseldon'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('32058h29tug68zi3tjegnie4gh86zeg4ze', $content[0]['hash']);
        $this->assertEquals('h4df984bngyt94lkyu1s65g', $content[1]['hash']);
    }

    public function testAcceptingCandidacyCreatesMembership(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/candidacies/e56htrj9r1eg86qs1ghg6zre1ge6/accept'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotNull($content['hash']);
        $this->assertNotNull($content['role']);
        $this->assertEquals([], $content['role']['permissions']);
    }

    public function testAcceptingInvitationCreatesMembership(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/projects/invitations/1hre68h1e98z1gze8ghre198h1e8/accept'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotNull($content['hash']);
        $this->assertNotNull($content['role']);
    }

    public function testUserCannotUpdateOwnMembership(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'PUT',
            '/v1/projects/memberships/32058h29tug68zi3tjegnie4gh86zeg4ze',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'hseldon',
                'role' => 'z7g684h9re4h68464f56g62e1r315'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testUserCannotUpdateSomeoneEsleMembership(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'PUT',
            '/v1/projects/memberships/32058h29tug68zi3tjegnie4gh86zeg4ze',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'hseldon',
                'role' => 'z7g684h9re4h68464f56g62e1r315'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testAdminCanUpdateOwnMembership(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/projects/memberships/32058h29tug68zi3tjegnie4gh86zeg4ze',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'hseldon',
                'role' => 'z7g684h9re4h68464f56g62e1r315'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
    }

    public function testMembershipCannotBeUpdatedWithFalseRole(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/projects/memberships/32058h29tug68zi3tjegnie4gh86zeg4ze',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'user' => 'hseldon',
                'role' => 'vej1kt98t1y6re51jh9ze1ge'
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );
    }


    public function testUserCannotRemoveOtherMembership(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'DELETE',
            '/v1/projects/memberships/32058h29tug68zi3tjegnie4gh86zeg4ze'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testProjectAdminCanRemoveOtherMembership(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/projects/memberships/32058h29tug68zi3tjegnie4gh86zeg4ze'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }

    public function testUserCanRemoveOwnMembership(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/memberships'
        );
        $response = $client->getResponse();
        $memberships = json_decode($response->getContent(), true);
        $membershipHash = $memberships[1]['hash'];

        $client->request(
            'DELETE',
            "/v1/projects/memberships/$membershipHash"
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }
}
