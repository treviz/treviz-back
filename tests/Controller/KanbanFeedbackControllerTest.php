<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 02/07/2019
 * Time: 21:16
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class KanbanFeedbackControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchFeedback()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/feedback'
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchReceivedFeedback($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/feedback?given=false'
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        if ($username === 'gdornick') {
            $this->assertCount(1, $feedback);
            $this->assertEquals('51feziuzebggne32095', $feedback[0]['hash']);
            $this->assertEquals('Nice !', $feedback[0]['feedback']);
            $this->assertEquals(false, $feedback[0]['praise']);
            $this->assertEquals('gdornick', $feedback[0]['receiver']['username']);
            $this->assertEquals('hseldon', $feedback[0]['giver']['username']);
            $this->assertEquals('rtj4r9t4rj6r1jrj161sd23vgs', $feedback[0]['task']['hash']);
        } else {
            $this->assertCount(0, $feedback);
        }
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchGivenFeedback($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/feedback?given=true'
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        if ($username === 'hseldon') {
            $this->assertCount(1, $feedback);
            $this->assertEquals('51feziuzebggne32095', $feedback[0]['hash']);
            $this->assertEquals('Nice !', $feedback[0]['feedback']);
            $this->assertEquals(false, $feedback[0]['praise']);
            $this->assertEquals('gdornick', $feedback[0]['receiver']['username']);
            $this->assertEquals('hseldon', $feedback[0]['giver']['username']);
            $this->assertEquals('rtj4r9t4rj6r1jrj161sd23vgs', $feedback[0]['task']['hash']);
        } else {
            $this->assertCount(0, $feedback);
        }
    }

    public function testUserCanFetchReceivedFeedbackForTask()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/feedback?given=false&task=rtj4r9t4rj6r1jrj161sd23vgs'
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $feedback);
        $this->assertEquals('51feziuzebggne32095', $feedback[0]['hash']);
        $this->assertEquals('Nice !', $feedback[0]['feedback']);
        $this->assertEquals(false, $feedback[0]['praise']);
        $this->assertEquals('gdornick', $feedback[0]['receiver']['username']);
        $this->assertEquals('hseldon', $feedback[0]['giver']['username']);
        $this->assertEquals('rtj4r9t4rj6r1jrj161sd23vgs', $feedback[0]['task']['hash']);
    }

    public function testFeedbackForWrongTaskIsEmpty()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/feedback?given=false&task=rgj4r9t4rj6r1jrj161sd23vgs'
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $feedback);
    }

    public function testUserCanFetchGivenFeedbackForTask()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/boards/columns/tasks/feedback?given=true&task=rtj4r9t4rj6r1jrj161sd23vgs'
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $feedback);
        $this->assertEquals('51feziuzebggne32095', $feedback[0]['hash']);
        $this->assertEquals('Nice !', $feedback[0]['feedback']);
        $this->assertEquals(false, $feedback[0]['praise']);
        $this->assertEquals('gdornick', $feedback[0]['receiver']['username']);
        $this->assertEquals('hseldon', $feedback[0]['giver']['username']);
        $this->assertEquals('rtj4r9t4rj6r1jrj161sd23vgs', $feedback[0]['task']['hash']);
    }

    public function testUserCannotGiveFeedback()
    {
        $feedbackText = 'This is a feedback';
        $feedbackToSend = array(
            'feedback' => $feedbackText,
            'praise' => false
        );
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her/feedback',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($feedbackToSend)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAssigneeCannotGiveFeedback()
    {
        $feedbackText = 'This is a feedback';
        $feedbackToSend = array(
            'feedback' => $feedbackText,
            'praise' => false
        );
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her/feedback',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($feedbackToSend)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());

    }

    public function testSupervisorCanGiveFeedback()
    {
        $feedbackText = 'This is a feedback';
        $feedbackToSend = array(
            'feedback' => $feedbackText,
            'praise' => false
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her/feedback',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($feedbackToSend)
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($feedbackText, $feedback['feedback']);
        $this->assertEquals(false, $feedback['praise']);
        $this->assertEquals('gdornick', $feedback['giver']['username']);
        $this->assertEquals('hseldon', $feedback['receiver']['username']);
    }

    public function testSupervisorCanGiveFeedbackAndPraise()
    {
        $feedbackText = 'This is a feedback';
        $feedbackToSend = array(
            'feedback' => $feedbackText,
            'praise' => true
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/boards/columns/tasks/oeh51erg16her76her68her/feedback',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($feedbackToSend)
        );
        $response = $client->getResponse();
        $feedback = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($feedbackText, $feedback['feedback']);
        $this->assertEquals(true, $feedback['praise']);
        $this->assertEquals('gdornick', $feedback['giver']['username']);
        $this->assertEquals('hseldon', $feedback['receiver']['username']);
    }

}
