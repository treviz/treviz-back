<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 30/06/2019
 * Time: 14:48
 */

namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class ChatMessageControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotGetMessages()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/rooms/iub21oi96Fezfzegezg8zezgb/messages'
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function testUserCanGetMessagesInRoomInWhichMember()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/rooms/iub21oi96Fezfzegezg8zezgb/messages'
        );
        $response = $client->getResponse();
        $messages = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $messages);
        $this->assertEquals('This is a message', $messages[0]['text']);
    }

    public function testUserCannotGetMessagesInRoomInWhichNotMember()
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'GET',
            '/v1/rooms/iub21oi96Fezfzegezg8zezgb/messages'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }
    public function testUserCanPostMessagesInRoomInWhichMember()
    {
        $text = 'This is another message';
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/rooms/iub21oi96Fezfzegezg8zezgb/messages',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['text' => $text])
        );
        $response = $client->getResponse();
        $message = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($text, $message['text']);
        $this->assertEquals('gdornick', $message['owner']['username']);
    }

    public function testUserCannotPostMessagesInRoomInWhichNotMember()
    {
        $text = 'This is another message';
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'POST',
            '/v1/rooms/iub21oi96Fezfzegezg8zezgb/messages',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['text' => $text])
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCanUpdateOwnMessage()
    {
        $text = 'This is an updated message';
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/rooms/messages/ozegbe04124njnOU532iubi53',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['text' => $text])
        );
        $response = $client->getResponse();
        $message = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($text, $message['text']);
        $this->assertEquals('gdornick', $message['owner']['username']);
    }

    public function testUserCannotUpdateOtherMessage()
    {
        $text = 'This is an updated message';
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'PUT',
            '/v1/rooms/messages/ozegbe04124njnOU532iubi53',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['text' => $text])
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }


    public function testUserCannotDeleteOtherMessage()
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'DELETE',
            '/v1/rooms/messages/ozegbe04124njnOU532iubi53'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCanDeleteOwnMessage()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/rooms/messages/ozegbe04124njnOU532iubi53'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
