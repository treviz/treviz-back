<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 24/06/2019
 * Time: 16:44
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class ProjectControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchProjects()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/projects'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchProjects($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/projects?nb=20&offset=0"
        );

        $response = $client->getResponse();
        $projects = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        if ($username == 'jpelorat') {
            $this->assertCount(3, $projects);
        } else {
            $this->assertCount(2, $projects);
        }
    }

    public function testUserCanSearchProjectByTag()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects?tags[]=Numérique&nb=20&offset=0"
        );

        $response = $client->getResponse();
        $projects = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $projects);
    }

    public function testUserCanSearchProjectBySkill()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects?skills[]=Développement+Mobile&nb=20&offset=0"
        );

        $response = $client->getResponse();
        $projects = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $projects);
    }

    public function testUserCanSearchProjectBySkillAndTag()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects?tags[]=Environnement&skills[]=Développement+Mobile&skills[]=Science+des+matériaux&nb=20&offset=0"
        );

        $response = $client->getResponse();
        $projects = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $projects);
        $this->assertEquals('Psychohistoire', $projects[0]['name']);
    }


    public function testUserCanSearchProjectByCommunity()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects?community=gez3g5hergf6vcx1vbvghyuo7ythrtg&nb=20&offset=0"
        );

        $response = $client->getResponse();
        $projects = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $projects);
        $this->assertEquals('Encyclopedia Galactica', $projects[0]['name']);
    }

    public function testUserCanSearchProjectsInWhichIsMember()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects?user=hseldon"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('Encyclopedia Galactica', $content[0]['name']);
        $this->assertEquals('Psychohistoire', $content[1]['name']);
    }

    public function testUserCannotSeePrivateProjects()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            "/v1/projects"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('Encyclopedia Galactica', $content[0]['name']);
        $this->assertEquals('Psychohistoire', $content[1]['name']);
    }

    public function testUserCanSeeOwnPrivateProjects()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects?user=jpelorat"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('FTL', $content[0]['name']);
    }

    public function testUserCanFetchProjectDetail()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/projects/gtr57hujtyuk61jh5gcsx1cxw21gtr45j6t684651d231596"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Psychohistoire', $content['name']);
        $this->assertEquals('Science permettant de prédire le comportement de larges groupes de personnes', $content['shortDescription']);
    }

    public function testUserCannotFetchWrongProjectDetail()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/projects/zlkhnezioihernhoernherinh"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCannotFetchPrivateProjectDetails()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            "/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCanFetchProjectDetailInWhichMember()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('FTL', $content['name']);
        $this->assertEquals('Création d\'un moteur permettant de voyager plus vite que la lumière', $content['description']);
    }

    public function testUserCanFetchProjectDetailInWhichInvited()
    {
        /* Part 1 : create the invitation */
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'POST',
            '/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();

        /** Assert the response contains the created invitation */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        /* Part 2 : check the invited user can access the project */
        $newClient = self::createAuthenticatedClient('dolivaw');
        $newClient ->request(
            'GET',
            "/v1/projects/z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19"
        );
        $response = $newClient->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('FTL', $content['name']);
        $this->assertEquals('Création d\'un moteur permettant de voyager plus vite que la lumière', $content['description']);
    }

    public function testUserCanCreateProject()
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $project = array(
            'name' => 'Commercial Agreement',
            'shortDescription' => 'A commercial deal between nations',
            'description' => 'A commercial deal between nations',
            'isVisible' => false,
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg',
            'skills' => ['deal-making', 'diplomacy']
        );
        $client->request(
            'POST',
            "/v1/projects",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($project)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testUserCannotUpdateProject()
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $updatedProject = array(
            'name' => 'Encyclopedia Galactica v2',
            'shortDescription' => 'New edition of the universally acclaimed encyclopedia',
            'description' => 'New edition of the universally acclaimed encyclopedia, with 2000 more articles !',
            'isVisible' => "true",
            'skills' => ['Relativité générale', 'Design'],
            'tags' => ['Environnement', 'Santé'],
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg'
        );
        $client->request(
            'PUT',
            "/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedProject)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAdminCanUpdateProject()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $updatedProject = array(
            'name' => 'Encyclopedia Galactica v2',
            'shortDescription' => 'New edition of the universally acclaimed encyclopedia',
            'description' => 'New edition of the universally acclaimed encyclopedia, with 2000 more articles !',
            'isVisible' => 'true',
            'skills' => ['Relativité générale', 'Design'],
            'tags' => ['Environnement', 'Santé'],
        );
        $client->request(
            'PUT',
            "/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedProject)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Encyclopedia Galactica v2', $content['name']);
        $this->assertEquals('New edition of the universally acclaimed encyclopedia', $content['shortDescription']);
    }

    public function testUserCannotDeleteProject()
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $client->request(
            'DELETE',
            "/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testProjectAdminCanDeleteProject()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            "/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testPlatformAdminCanDeleteProject()
    {
        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'DELETE',
            "/v1/projects/gtr57hujtyuk61jh5gcsx1cxw21gtr45j6t684651d231596"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

}
