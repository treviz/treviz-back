<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class BrainstormingIdeaControllerTest extends WebTestCaseWithFixtures
{
    public function testAnonymousCannotFetchIdeas() {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4/ideas'
        );
        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode()
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testOnlyMembersCanSeeIdeas($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4/ideas'
        );
        $response = $client->getResponse();
        if (in_array($username, array('admin', 'gdornick', 'shardin'))) {
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
            $ideas = json_decode($response->getContent(), true);
            $this->assertCount(2, $ideas);
            $this->assertEquals("niuniu12onoivryy698fQFQS", $ideas[0]["hash"]);
            $this->assertEquals("A first idea", $ideas[0]["content"]);
            $this->assertEquals("gdornick", $ideas[0]["author"]["username"]);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testOnlyMembersCanPostIdeas($username)
    {
        $ideaToPost = array("content" => "A new idea");
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4/ideas',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($ideaToPost)
        );
        $response = $client->getResponse();
        if (in_array($username, array('admin', 'gdornick', 'shardin'))) {
            $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
            $idea = json_decode($response->getContent(), true);
            $this->assertEquals($ideaToPost["content"], $idea["content"]);
            $this->assertEquals($username, $idea["author"]["username"]);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    public function testMembersCannotPostIdeaInClosedSession()
    {
        $ideaToPost = array("content" => "A new idea");
        $client = self::createAuthenticatedClient("gdornick");
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/1egkzg1b2zafa2bnç2hRII21TH98/ideas',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($ideaToPost)
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUserCannotUpdateIdeaIfNorAuthorNorAdmin()
    {
        $ideaToPost = array("content" => "An updated idea");
        $client = self::createAuthenticatedClient("hseldon");
        $client->request(
            'PUT',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($ideaToPost)
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testMemberCanUpdateOwnIdea()
    {
        $author = 'gdornick';
        $updatedIdea = array("content" => "An updated idea");
        $client = self::createAuthenticatedClient($author);
        $client->request(
            'PUT',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($updatedIdea)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $idea = json_decode($response->getContent(), true);
        $this->assertEquals($updatedIdea['content'], $idea['content']);
        $this->assertEquals($author, $idea['author']['username']);
    }

    public function testAdminCanUpdateAnyIdea()
    {
        $author = 'gdornick';
        $updatedIdea = array("content" => "An idea updated by an admin");
        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'PUT',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($updatedIdea)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $idea = json_decode($response->getContent(), true);
        $this->assertEquals($updatedIdea['content'], $idea['content']);
        $this->assertEquals($author, $idea['author']['username']);
    }

    public function testUserCannotDeleteIdea()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'DELETE',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS'
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testOwnerCanDeleteIdea()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testAdminCanDeleteIdea()
    {
        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'DELETE',
            '/v1/brainstorming-sessions/ideas/ubgfiyezbgeziug35oin2341ubuaz09'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
