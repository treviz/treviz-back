<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class ProjectJobControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchJobs()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/projects/jobs'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchJobs($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/projects/jobs?nb=20&offset=0"
        );

        $response = $client->getResponse();
        $jobs = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        if ('jpelorat' === $username) {
            $this->assertCount(2, $jobs);
        } else {
            $this->assertCount(1, $jobs);
        }
    }

    public function testUserCanFetchProjectJobs()
    {
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'GET',
            "/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/jobs"
        );

        $response = $client->getResponse();
        $jobs = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $jobs);
    }

    public function testUserCanSearchJobsByName()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects/jobs?nb=20&offset=0&name=cyclo"
        );

        $response = $client->getResponse();
        $jobs = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $jobs);

        $client->request(
            'GET',
            "/v1/projects/jobs?nb=20&offset=0&name=azerty"
        );
        $response = $client->getResponse();
        $jobs2 = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(0, $jobs2);
    }

    public function testUserCanSeeOwnJobs()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/projects/jobs?nb=20&offset=0&attributed=true&holder=jpelorat"
        );

        $response = $client->getResponse();
        $jobs = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $jobs);
        $this->assertEquals('Mayor', $jobs[0]['name']);
        $this->assertEquals('rk541tkl8ty4kty68lk', $jobs[0]['hash']);
    }

    public function testAdminCanCreateJob()
    {
        $jobToCreate = array(
            'name' => 'Psychohistorian',
            'description' => 'Some kind of description',
            'contact' => 'gdornick'
        );
        $projectHash = 'nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3';
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            "/v1/projects/$projectHash/jobs",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($jobToCreate)
        );

        $response = $client->getResponse();
        $job = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($jobToCreate['name'], $job['name']);
        $this->assertEquals($jobToCreate['description'], $job['description']);
        $this->assertEquals('gdornick', $job['contact']['username']);
    }

    public function testAdminCanUpdateJob()
    {
        $updatedJob = array(
            'name' => 'Psychohistorian',
            'description' => 'Lorem ipsum',
            'contact' => 'gdornick',
            'holder' => 'hseldon'
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/projects/jobs/ziuegneziugezniug214ijb',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedJob)
        );

        $response = $client->getResponse();
        $job = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($updatedJob['name'], $job['name']);
        $this->assertEquals($updatedJob['description'], $job['description']);
        $this->assertEquals('gdornick', $job['contact']['username']);
        $this->assertEquals($updatedJob['holder'], $job['holder']['username']);
    }

    public function testUserCannotUpdateJob()
    {
        $updatedJob = array(
            'name' => 'Psychohistorian',
            'description' => 'Lorem ipsum',
            'contact' => 'gdornick',
            'holder' => 'hseldon'
        );
        $client = self::createAuthenticatedClient('hmallow');
        $client->request(
            'PUT',
            '/v1/projects/jobs/ziuegneziugezniug214ijb',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedJob)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCannotQuitOtherJob()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/jobs/ziuegneziugezniug214ijb/quit'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testJobHolderCanQuit()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/projects/jobs/ziuegneziugezniug214ijb/quit'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testUserCannotDeleteJob()
    {
        echo PHP_EOL . "ProjetJobControllerTest::testUserCannotDeleteJob()" . PHP_EOL;
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'DELETE',
            '/v1/projects/jobs/ziuegneziugezniug214ijb'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAdminCanDeleteJob()
    {
        echo PHP_EOL . "ProjetJobControllerTest::testAdminCanDeleteJob()" . PHP_EOL;
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/projects/jobs/ziuegneziugezniug214ijb'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

}
