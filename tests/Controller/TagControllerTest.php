<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class TagControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchTags()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/tags'
        );
        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode(),
            'An anonymous user cannot fetch the tags'
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchTags($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/tags'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'An connected user cann fetch the tags'
        );

        $this->assertCount(
            7,
            json_decode($response->getContent(), true),
            'All tags can be retrieved'
        );
    }

    public function testUserCanSearchTagByName()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/tags?name=ri'
        );
        $response = $client->getResponse();
        $tags = json_decode($response->getContent(), true);

        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'An connected user cann search the tags by name'
        );

        $this->assertEquals(
            array(
                array('name' => 'Nourriture'),
                array('name' => 'Numérique')
            ),
            $tags,
            'All 3 tags are correctly fetched'
        );
    }

    public function testUserCanCreateNewTag()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $tag = array('name' => 'Engineering');
        $client->request(
            'POST',
            '/v1/tags',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($tag)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(
            Response::HTTP_CREATED,
            $response->getStatusCode(),
            'Tag creation results in 201 status code'
        );

        $this->assertEquals(
            $tag,
            $content,
            'Created tag matches the request that was made'
        );
    }

    public function testUserCannotUpdateTag()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $tag = array('name' => 'Digital');
        $client->request(
            'PUT',
            '/v1/tags/Numérique',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($tag)
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode(),
            'Attempt to update a tag results in 403 for a non-admin'
        );
    }

    public function testAdminCanUpdateTag()
    {
        $client = self::createAuthenticatedClient('admin');

        $tag = array('name' => 'Digital');
        $client->request(
            'PUT',
            '/v1/tags/Numérique',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($tag)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'Tag update results in 200 status code'
        );

        $this->assertEquals(
            $tag,
            $content,
            'Created tag matches the request that was made'
        );
    }

    public function testUserCannotDeleteTag()
    {

        $client = self::createAuthenticatedClient('gdornick');

        $client->request(
            'DELETE',
            '/v1/tags/Santé',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode(),
            'Attempt to delete a tag results in 403 for a non-admin'
        );
    }

    public function testAdminCanDeleteTag()
    {

        $client = self::createAuthenticatedClient('admin');

        $client->request(
            'DELETE',
            '/v1/tags/Santé',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode(),
            'Tag deletion results in 204'
        );
    }
}
