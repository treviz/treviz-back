<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class BrainstormingSessionControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchBrainstormingSession()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/brainstorming-sessions'
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUsernames
     * @param $username
     */
    public function testMembersOrAdminCanFetchCommunitySessions($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/brainstorming-sessions?community=ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $sessions = json_decode($response->getContent(), true);
        if (in_array($username, array('admin', 'gdornick', 'shardin'))) {
            $this->assertCount(2, $sessions);
            $this->assertEquals('iuabauribi21412NiunIU2UB12I4', $sessions[0]['hash']);
            $this->assertEquals('Brainstorming 1', $sessions[0]['name']);
            $this->assertEquals('The first brainstorming session', $sessions[0]['description']);
        } else {
            $this->assertEquals([], $sessions);
        }
    }
    /**
     * @dataProvider getUsernames
     * @param $username
     */
    public function testMembersOrAdminCanFetchCommunitySessionDetail($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4'
        );
        $response = $client->getResponse();
        if (in_array($username, array('admin', 'gdornick', 'shardin'))) {
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
            $session = json_decode($response->getContent(), true);
            $this->assertEquals('iuabauribi21412NiunIU2UB12I4', $session['hash']);
            $this->assertEquals('Brainstorming 1', $session['name']);
            $this->assertEquals('The first brainstorming session', $session['description']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    /**
     * @dataProvider getUsernames
     * @param $username
     */
    public function testOnlyAdminOrManagersCanCreateSession($username)
    {
        $sessionToCreate = array(
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf',
            'name' => 'New brainstorming session',
            'description' => 'Some description'
        );
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'POST',
            '/v1/brainstorming-sessions?community=',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($sessionToCreate)
        );
        $response = $client->getResponse();
        if (in_array($username, array('admin', 'gdornick'))) {
            $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
            $session = json_decode($response->getContent(), true);
            $this->assertEquals($sessionToCreate['name'], $session['name']);
            $this->assertEquals($sessionToCreate['description'], $session['description']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    /**
     * @dataProvider getUsernames
     * @param $username
     */
    public function testOnlyMemberAndAdminCanUpdateSession($username)
    {
        $updates = array(
            'community' => 'ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf',
            'name' => "A session updated by $username",
            'description' => 'Some description',
            'isOpen' => false
        );
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'PUT',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($updates)
        );
        $response = $client->getResponse();
        if (in_array($username, array('admin', 'gdornick'))) {
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
            $session = json_decode($response->getContent(), true);
            $this->assertEquals($updates['name'], $session['name']);
            $this->assertEquals($updates['description'], $session['description']);
            $this->assertEquals($updates['isOpen'], $session['isOpen']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    public function testUserCannotDeleteSession()
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'DELETE',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testManagerCanDeleteSession()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/brainstorming-sessions/iuabauribi21412NiunIU2UB12I4'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testAdminCanDeleteSession()
    {
        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'DELETE',
            '/v1/brainstorming-sessions/1egkzg1b2zafa2bnç2hRII21TH98'
        );
        $response = $client->getResponse();
        var_dump($response->getContent());
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
