<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 01/01/2019
 * Time: 21:18
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class UserControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousUserCanCreateAccount()
    {
        $user = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'username' => 'jdoe',
            'email' => 'contact@treviz.xyz',
            'password' => 'changeit!'
        );

        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($content['firstName'], 'John');
        $this->assertEquals($content['lastName'], 'Doe');
        $this->assertEquals($content['username'], 'jdoe');
    }

    public function testAccountEmailMustBeValid()
    {
        $user = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'username' => 'jdoe2',
            'email' => 'contact@treviz.xyz@test.com',
            'password' => 'changeit!'
        );

        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }

    public function testAccountUsernameCannotContainDots()
    {
        $user = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'username' => 'john.doe',
            'email' => 'contact3@treviz.xyz',
            'password' => 'changeit!'
        );

        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }

    public function testAccountUsernameCannotContainSlash()
    {
        $user = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'username' => 'j/oe',
            'email' => 'contact4@treviz.xyz',
            'password' => 'changeit!'
        );

        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }
    public function testAccountUsernameCannotContainSpecialCharacters()
    {
        $user = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'username' => 'j@oe!',
            'email' => 'contact5@treviz.xyz',
            'password' => 'changeit!'
        );

        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }


    public function testAdminCanCreateAccount()
    {
        $user = array(
            'firstName' => 'John',
            'lastName'  => 'Doe',
            'username'  => 'jdoe3',
            'email'     => 'contact6@treviz.xyz',
            'password'  => 'changeit!'
        );

        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertEquals($content['firstName'], 'John');
        $this->assertEquals($content['lastName'], 'Doe');
        $this->assertEquals($content['username'], 'jdoe3');

    }

    public function testNonAdminCannotCreateAccount()
    {
        $user = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'username' => 'jdoe4',
            'email' => 'contact7@treviz.xyz',
            'password' => 'changeit!'
        );

        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($user)
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testConnectedUserCanFetchUsers($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/users'
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testUserCanSearchUsersByName()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/users?name=har'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(2, count($content));
        $this->assertEquals('hseldon', $content[0]['username']);
        $this->assertEquals('shardin', $content[1]['username']);
    }

    public function testUserCanSearchUsersBySkill()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/users?skills[]=Développement Web'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(4, count($content));
        $this->assertEquals('gdornick', $content[0]['username']);
        $this->assertEquals('hseldon', $content[1]['username']);
        $this->assertEquals('shardin', $content[2]['username']);
        $this->assertEquals('dolivaw', $content[3]['username']);
    }

    public function testUserCanSearchUsersByInterest()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/users?tags[]=Transports'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(2, count($content));
        $this->assertEquals('hseldon', $content[0]['username']);
        $this->assertEquals('jpelorat', $content[1]['username']);
    }

    public function testUserCanSearchUsersByOrganization()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/users?organization=First Empire'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(1, count($content));
        $this->assertEquals('gdornick', $content[0]['username']);
    }

    public function testUserCanSearchUsersSetOffsetAndLimit()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/users?nb=3&offset=2'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(3, count($content));
        $this->assertEquals('hseldon', $content[0]['username']);
        $this->assertEquals('jpelorat', $content[1]['username']);
        $this->assertEquals('shardin', $content[2]['username']);
    }

    public function testUserCanDoComplexSearch()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            '/v1/users?skills[]=Développement Web&tags[]=Transports&offset=0&nb=3'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(1, count($content));
        $this->assertEquals('hseldon', $content[0]['username']);
    }

    public function testAnonymousUserCannotFetchUsers()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/users'
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testAnonymousCannotFetchSpecificUser($username)
    {
        $client = self::createClient();
        $client->request(
            'GET',
            "/v1/users/$username"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }


    public function testUserCanFetchSpecificUser()
    {
        $client = self::createAuthenticatedClient('jpelorat');

        foreach ($this->getUserNames() as [$userName]) {
            $client->request(
                'GET',
                "/v1/users/$userName"
            );

            $response = $client->getResponse();
            $content = json_decode($response->getContent(), true);
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
            $this->assertEquals($userName, $content['username']);
        }
    }

    public function testFalseUsernameResultIn404()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/users/azerty'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCanUpdateOwnAccount()
    {
        $updatedUser = array(
            'firstName' => 'Janov',
            'lastName' => 'Pelorat',
            'username' => 'jpelorat',
            'description' => 'Lorem ipsum',
        );

        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'PUT',
            '/v1/users/jpelorat',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedUser)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($content['firstName'], 'Janov');
        $this->assertEquals($content['lastName'], 'Pelorat');
        $this->assertEquals($content['description'], 'Lorem ipsum');
    }

    public function testAdminCanUpdateOtherAccount()
    {
        $updatedUser = array(
            'firstName' => 'Janov',
            'lastName' => 'Pelorat',
            'username' => 'jpelorat',
            'description' => 'Lorem ipsum dolor sit amet'
        );

        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'PUT',
            '/v1/users/jpelorat',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedUser)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($content['description'], 'Lorem ipsum dolor sit amet');
    }

    public function testUserCannotUpdateSomeoneElseAccount()
    {
        $updatedUser = array(
            'firstName' => 'Janov',
            'lastName' => 'Pelorat',
            'username' => 'jpelorat',
            'description' => 'Lorem ipsum dolor sit amet'
        );

        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/users/jpelorat',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedUser)
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }


    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchOwnArchive($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/users/$username/archive"
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testUserCannotFetchSomeoneElseArchive()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            '/v1/users/hseldon/archive'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCanDeleteOwnAccount()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'DELETE',
            '/v1/users/jpelorat'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());


        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'GET',
            '/v1/users/jpelorat'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testAdminCanDeleteAnyAccount()
    {
        $client = self::createAuthenticatedClient('admin');
        $client->request(
            'DELETE',
            '/v1/users/gdornick'
        );
        $response = $client->getResponse();
        echo $response->getContent();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        $client->request(
            'GET',
            '/v1/users/gdornick'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCannotDeleteSomeoneElseAccount()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'DELETE',
            '/v1/users/shardin'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

}
