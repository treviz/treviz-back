<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 05/04/2019
 * Time: 15:10
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class KanbanBoardControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchBoards()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/boards'
        );

        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode(),
            'A user that is not logged in cannot fetch any boards'
        );
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchAvailableBoards($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'A logged user can fetch boards'
        );
        if ($username === 'admin' or $username === 'jpelorat') {
            $this->assertEquals(
                3,
                sizeof($content),
                "$username should see all 3 boards"
            );
        } else {
            $this->assertEquals(
                2,
                sizeof($content),
                "$username should see the 2 boards of the Encyclopedia Galactica project"
            );
        }
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanSearchUserBoards($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards?user=gdornick'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'A logged user can search the boards of someone'
        );

        $this->assertEquals(
            2,
            sizeof($content),
            "$username should see the 2 boards of this user"
        );

    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanSearchProjectBoards($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/boards?project=z61her7jtrzj7g5zg8erj4h6reh1t8j6rt78jt98rjh19'
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'A logged user can search the boards of a project'
        );

        if ($username === 'admin'
            or $username === 'jpelorat'
        ) {
            $this->assertEquals(
                1,
                sizeof($content),
                "$username should see the board of this project"
            );
        } else {
            $this->assertEquals(
                0,
                sizeof($content),
                "$username should not see any board for this project"
            );
        }
    }

    public function testUserCanCreateBoard()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $board = array(
            "name" => "Test board",
            "description" => "A test board",
            "project" => "nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3"
        );
        $client->request(
            'POST',
            '/v1/boards',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($board)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($content["name"], "Test board");
        $this->assertEquals($content["description"], "A test board");
    }

    public function testUserCannotCreateBoardIfUnauthorized()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $board = array(
            "name" => "Test board",
            "description" => "A test board",
            "project" => "nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3"
        );
        $client->request(
            'POST',
            '/v1/boards',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($board)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testBoardOwnerCanUpdateIt()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $board = array(
            "name" => "1st Edition updated",
            "description" => "A test board (updated)",
            "archived" => "false"
        );
        $client->request(
            'PUT',
            '/v1/boards/ipuhneh6rj16rtjr896tj',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($board)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($content["name"], "1st Edition updated");
        $this->assertEquals($content["description"], "A test board (updated)");
    }

    public function testUserCannotUpdateOtherBoard()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $board = array(
            "name" => "1st Edition updated",
            "description" => "A test board (updated)",
            "archived" => "false"
        );
        $client->request(
            'PUT',
            '/v1/boards/ipuhneh6rj16rtjr896tj',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($board)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCannotDeleteOtherBoard()
    {
        $client = self::createAuthenticatedClient('hseldon');

        $client->request(
            'DELETE',
            '/v1/boards/ipuhneh6rj16rtjr896tj'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testOwnerCanDeleteBoard()
    {
        $client = self::createAuthenticatedClient('gdornick');

        $client->request(
            'DELETE',
            '/v1/boards/ipuhneh6rj16rtjr896tj'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        $client->request(
            'GET',
            '/v1/boards/ipuhneh6rj16rtjr896tj'
        );
        $responseAlt = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $responseAlt->getStatusCode());
    }

}
