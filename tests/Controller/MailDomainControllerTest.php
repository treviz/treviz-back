<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 07/01/2019
 * Time: 20:59
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class MailDomainControllerTest extends WebTestCaseWithFixtures
{

    public function testUserMustBeAuthenticate()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/platform/mail-domains'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testOnlyAdminCanFetchMailDomains($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/platform/mail-domains'
        );
        $response = $client->getResponse();

        if ($username === "admin") {
            $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    public function testAdminCanCreateDomain()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'POST',
            '/v1/platform/mail-domains',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"domain":"acme.org"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testNonAdminCannotCreateDomain()
    {
        $client = self::createAuthenticatedClient("gdornick");
        $client->request(
            'POST',
            '/v1/platform/mail-domains',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"domain": "acme.com"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testDomainNameIsUnique()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'POST',
            '/v1/platform/mail-domains',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"domain": "acme.org"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CONFLICT, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCannotUpdateDomain($username)
    {
        if ($username !== "admin") {
            $client = self::createAuthenticatedClient($username);
            $client->request(
                'PUT',
                "/v1/platform/mail-domains/acme.org",
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json'),
                '{"domain": "acme.com"}'
            );
            $response = $client->getResponse();
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        } else {
            $this->assertTrue(true);
        }
    }

    public function testAdminCanUpdateDomain()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'PUT',
            '/v1/platform/mail-domains/acme.org',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"domain": "acme.com"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCannotDeleteDomain($username)
    {
        if ($username !== "admin") {
            $client = self::createAuthenticatedClient($username);
            $client->request(
                'DELETE',
                '/v1/platform/mail-domains/acme.com'
            );
            $response = $client->getResponse();
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        } else {
            $this->assertTrue(true);
        }
    }

    public function testAdminCanDeleteDomain()
    {
        $client = self::createAuthenticatedClient("admin");
        $client->request(
            'DELETE',
            '/v1/platform/mail-domains/acme.com'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
