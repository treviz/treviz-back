<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class AccountControllerTest extends WebTestCaseWithFixtures
{
    public function testAccountConfirmationFailsWithWrongUsername()
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users/le_mulet/confirm',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"token": "wrongToken"}'
        );
        $this->assertEquals(
            Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode(),
            'Trying to enable a non-existing user results in 404'
        );
    }

    public function testAccountConfirmationFailsWithWrongToken()
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users/the_mule/confirm',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
            ),
            '{"token": "wrongToken"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode(),
            'Trying to active a user with the wrong token results in 403'
        );
    }

    public function testDisabledUserCannotLogin()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/login_check',
            array(
                'username' => 'the_mule',
                'password' => 'the_mule',
            )
        );
        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode(),
            'Trying to update a user with the wrong token results in 403'
        );
    }

    public function testAccountConfirmationSucceedsWithActualToken()
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users/the_mule/confirm',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"token":"someT0k3n"}'
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            'User can be enabled with the right status code'
        );
    }

    public function testAccountCannotBeEnabledTwice()
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/users/the_mule/confirm',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"token":"someT0k3n"}'
        );
        $this->assertEquals(
            Response::HTTP_CONFLICT,
            $client->getResponse()->getStatusCode(),
            'User cannot be enabled twice'
        );
    }

    public function testEnabledUserCanLogin()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/login_check',
            array(
                'username' => 'the_mule',
                'password' => 'the_mule',
            )
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            'Logging in with an enabled user succeeds'
        );
    }

    public function testUserCanResetPassword()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/users/hmallow/reset',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                "token" => "someConfirmationToken",
                "password" => "mynewpassword"
            ))
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            'A user can reset his/her password'
        );
    }

    public function testUserCanAskToResetPassword()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/v1/reset-password?email=the.mule@acme.org'
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            'User can reset his or her password'
        );
    }

    public function testUserCanRetrieveOwnPersonalData()
    {
        $client = static::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/users/gdornick/archive'
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode(),
            'User can reset his or her password'
        );
    }

    public function testUserCanRetrieveOtherPersonalData()
    {
        $client = static::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/users/hseldon/archive'
        );
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $client->getResponse()->getStatusCode(),
            'User can reset his or her password'
        );
    }

}
