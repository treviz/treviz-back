<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 24/06/2019
 * Time: 16:44
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class CommunityControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousCannotFetchCommunities()
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/communities'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testUserCanFetchCommunities($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/communities"
        );

        $response = $client->getResponse();
        $communities = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        if ($username === 'hseldon' or $username === 'dolivaw') {
            $this->assertCount(3, $communities);
            $this->assertEquals('Trantor', $communities[0]['name']);
            $this->assertEquals('Terminus', $communities[1]['name']);
            $this->assertEquals('Gaia', $communities[2]['name']);
        } else {
            $this->assertCount(2, $communities);
            $this->assertEquals('Trantor', $communities[0]['name']);
            $this->assertEquals('Gaia', $communities[1]['name']);
        }
    }

    public function testUserCanSearchCommunitiesInWhichIsMember()
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'GET',
            "/v1/communities?user=gdornick"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('Trantor', $content[0]['name']);
    }


    public function testUserCanSearchCommunitiesInWhichNotMember()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/communities?exclude=gdornick"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('Terminus', $content[0]['name']);
        $this->assertEquals('Gaia', $content[1]['name']);

        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            "/v1/communities?exclude=gdornick"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('Gaia', $content[0]['name']);
    }

    public function testPaginationWorks()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/communities?offset=1"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('Terminus', $content[0]['name']);
        $this->assertEquals('Gaia', $content[1]['name']);
    }

    public function testUserCannotSeePrivateCommunities()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            "/v1/communities"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(2, $content);
        $this->assertEquals('Trantor', $content[0]['name']);
        $this->assertEquals('Gaia', $content[1]['name']);
    }

    public function testUserCanSeeOwnPrivateCommunities()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/communities?user=hseldon"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(1, $content);
        $this->assertEquals('Terminus', $content[0]['name']);
    }

    public function testUserCanFetchCommunityDetail()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Trantor', $content['name']);
        $this->assertEquals('Center of the galaxy', $content['description']);
    }

    public function testUserCannotFetchWrongCommunityDetail()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxzlgnzeugnzn"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUserCannotFetchPrivateCommunityDetails()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            "/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUserCanFetchCommunityDetailInWhichMember()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'GET',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf"
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Trantor', $content['name']);
        $this->assertEquals('Center of the galaxy', $content['description']);
    }

    public function testUserCanFetchCommunityDetailInWhichInvited()
    {
        /* Part 1 : create the invitation */
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/invitations',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world", "user": "dolivaw"}'
        );
        $response = $client->getResponse();

        /** Assert the response contains the created invitation */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        /* Part 2 : check the invited user can access the community */
        $newClient = self::createAuthenticatedClient('dolivaw');
        $newClient ->request(
            'GET',
            "/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg"
        );
        $response = $newClient->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Terminus', $content['name']);
        $this->assertEquals('Capital planet of the first foundation', $content['description']);
    }

    public function testUserCanCreateCommunity()
    {
        $client = self::createAuthenticatedClient('dolivaw');
        $community = array(
            'name' => 'Anacreon',
            'description' => 'An other kingdom',
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg'
        );
        $client->request(
            'POST',
            "/v1/communities",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($community)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testUserCannotUpdateCommunity()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $updatedCommunity = array(
            'name' => 'Trantor (updated)',
            'description' => 'Actual Center of the galaxy',
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg'
        );
        $client->request(
            'PUT',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedCommunity)
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAdminCanUpdateCommunity()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $updatedCommunity = array(
            'name' => 'Trantor (updated)',
            'description' => 'Actual Center of the galaxy',
            'logoUrl' => 'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg'
        );
        $client->request(
            'PUT',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($updatedCommunity)
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('Trantor (updated)', $content['name']);
        $this->assertEquals('Actual Center of the galaxy', $content['description']);
        $this->assertEquals(
            'http://upload.wikimedia.org/wikipedia/commons/4/46/Trantor-Coruscant.jpg',
            $content['logoUrl']
        );
    }

    public function testUserCannotDeleteCommunity()
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'DELETE',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testAdminCanDeleteCommunity()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            "/v1/communities/ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

}
