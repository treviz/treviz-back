<?php
/**
 * Created by IntelliJ IDEA.
 * User: huber
 * Date: 26/12/2018
 * Time: 11:49
 */

 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

/**
 * Class CommunityCandidacyControllerTest
 * @package Tests\CommunityBundle\Controller
 */
class CommunityCandidacyControllerTest extends WebTestCaseWithFixtures
{

    /**
     * A non-authenticated user should be return a 401 Unauthorized error code
     */
    public function testGetCommunityCandidaciesWithoutToken(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/communities/candidacies?user=gdornick'
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can fetch his or her own candidacies
     *
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchHisOwnCandidacies($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            "/v1/communities/candidacies?user=$username"
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /**
     * A user can create a candidacy for a community he/she is not part of.
     */
    public function testUserCanCreateCommunityCandidacy(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world"}'
        );

        $response = $client->getResponse();

        /** Assert the response contains the created candidacy */
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * A user cannot create a candidacy for a community he/she is already candidating to.
     */
    public function testUserCannotCreateSameCandidacyTwice(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world"}'
        );

        $this->assertEquals(Response::HTTP_CONFLICT, $client->getResponse()->getStatusCode());
    }

    public function testUnauthorizedUserCannotAcceptCandidacy(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'POST',
            '/v1/communities/candidacies/zjnziugb4eiu71funiuezh8G7365/accept'
        );
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testUnauthorizedUserCannotRemoveCandidacy(): void
    {
        $client = self::createAuthenticatedClient('shardin');
        $client->request(
            'DELETE',
            '/v1/communities/candidacies/zjnziugb4eiu71funiuezh8G7365'
        );

        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }


    /**
     * An community admin can accept one's candidacy for a community.
     */
    public function testAdminCanAcceptCandidacy(): void
    {
        $client = self::createAuthenticatedClient('hseldon');
        $client->request(
            'POST',
            '/v1/communities/candidacies/zjnziugb4eiu71funiuezh8G7365/accept'
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * A user can delete his/her own candidacy.
     */
    public function testCandidateCanRemoveHisOwnCandidacy(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/communities/gez3g5hergf6vcx1vbvghyuo7ythrtg/candidacies',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"message":"Hello world"}'
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $candidacy = json_decode($response->getContent(), true);

            $client->request(
                'DELETE',
                "/v1/communities/candidacies/{$candidacy['hash']}"
            );
            $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
        }
    }

}
