<?php


namespace App\Tests\Controller;


use App\Tests\WebTestCaseWithFixtures;
use Symfony\Component\HttpFoundation\Response;

class BrainstormingVoteControllerTest extends WebTestCaseWithFixtures
{

    public function testAnonymousUserCannotVote()
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/votes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array('score' => 1))
        );
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testOnlyMembersCanVote($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/votes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array('score' => 1))
        );
        if (in_array($username, array('gdornick', 'shardin', 'admin'))) {
            $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
        }
    }

    /**
     * @dataProvider getUserNames
     * @param $username
     */
    public function testOnlyMembersCanDownVote($username)
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/votes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array('score' => -1))
        );
        if (in_array($username, array('gdornick', 'shardin', 'admin'))) {
            $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
        }
    }

    public function testMemberCannotGrantMoreThan1Point()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/votes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array('score' => 2))
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }

    public function testMemberCannotRemoveMoreThan1Point()
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/brainstorming-sessions/ideas/niuniu12onoivryy698fQFQS/votes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(array('score' => -2))
        );
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }
}
