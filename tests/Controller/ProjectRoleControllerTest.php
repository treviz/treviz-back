<?php


 namespace App\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;
use App\Tests\WebTestCaseWithFixtures;

class ProjectRoleControllerTest extends WebTestCaseWithFixtures
{
    public function testAnonymousCannotFetchRoles(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/roles'
        );

        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED,
            $client->getResponse()->getStatusCode()
        );
    }

    /**
     * @dataProvider getUserNames
     * @param string $username
     */
    public function testUserCanFetchProjectRoles($username): void
    {
        $client = self::createAuthenticatedClient($username);
        $client->request(
            'GET',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/roles'
        );
        $response = $client->getResponse();
        $roles = json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertCount(3, $roles);
    }

    public function testFetchingRolesFromWrongProjectResultIn404(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'GET',
            '/v1/projects/erh8tj4tr86j16az1gh9erh1re6/roles'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testAdminCanCreateRole(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'POST',
            '/v1/projects/nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3/roles',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'name' => 'role_test',
                'permissions' => array(
                    'MANAGE_DOCUMENT',
                    'MANAGE_POST'
                )
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_CREATED,
            $response->getStatusCode()
        );
    }

    public function testAdminCanUpdateRoleWithMorePermissions(): void
    {
        $updatedName = 'Admin (updated)';
        $updatedPermissions = array(
            'MANAGE_DOCUMENT',
            'MANAGE_POST',
            'MANAGE_MEMBERSHIP',
            'MANAGE_INVITATIONS',
            'MANAGE_CANDIDACIES'
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/projects/roles/erhez6t1ze65rhj16gz18r98e',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'name' => $updatedName,
                'permissions' => $updatedPermissions
            ))
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
        $this->assertEquals($updatedName, $content['name']);
        $this->assertCount(count($updatedPermissions), $content['permissions']);
        foreach ($content["permissions"] as $permission) {
            $this->assertTrue(in_array($permission, $updatedPermissions));
        }
    }

    public function testAdminCanUpdateRoleWithLessPermissions(): void
    {
        $updatedName = 'Admin (updated)';
        $updatedPermissions = array(
            'MANAGE_DOCUMENT',
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/projects/roles/erhez6t1ze65rhj16gz18r98e',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'name' => $updatedName,
                'permissions' => $updatedPermissions
            ))
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
        $this->assertEquals($updatedName, $content['name']);
        $this->assertCount(count($updatedPermissions), $content['permissions']);
        foreach ($content["permissions"] as $permission) {
            $this->assertTrue(in_array($permission, $updatedPermissions));
        }
    }

    public function testRoleCannotBeUpdatedWithFalsePermissions(): void
    {
        $updatedName = 'Admin (updated)';
        $updatedPermission = array(
            'MANAGE_BANANAS',
        );
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'PUT',
            '/v1/projects/roles/erhez6t1ze65rhj16gz18r98e',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'name' => $updatedName,
                'permissions' => $updatedPermission
            ))
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );
    }

    public function testUserCannotRemoveOtherRole(): void
    {
        $client = self::createAuthenticatedClient('jpelorat');
        $client->request(
            'DELETE',
            '/v1/projects/roles/erhez6t1ze65rhj16gz18r98e'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    public function testProjectAdminCanRemoveRole(): void
    {
        $client = self::createAuthenticatedClient('gdornick');
        $client->request(
            'DELETE',
            '/v1/projects/roles/erhez6t1ze65rhj16gz18r98e'
        );
        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }

}
