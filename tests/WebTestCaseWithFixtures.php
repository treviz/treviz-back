<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 01/01/2019
 * Time: 21:19
 */

namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WebTestCaseWithFixtures extends WebTestCase
{

    private static $USERS = array(
        'admin'    => array('password' => 'admin', 'token' => ''),
        'gdornick' => array('password' => 'gdornick', 'token' => ''),
        'hseldon'  => array('password' => 'hseldon', 'token' => ''),
        'jpelorat' => array('password' => 'jpelorat', 'token' => ''),
        'shardin'  => array('password' => 'shardin', 'token' => ''),
        'dolivaw'  => array('password' => 'dolivaw', 'token' => ''),
        'hmallow'  => array('password' => 'hmallow', 'token' => '')
    );

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::fetchTokens();
    }

    private static function fetchTokens(): bool
    {
        echo 'Fetching tokens...'.PHP_EOL;
        foreach (self::$USERS as $username => $userData) {
            echo '   - Fetching token for '.$username.PHP_EOL;
            $token = self::fetchClientToken($username, $userData['password']);
            echo '     Token fetched'.PHP_EOL;
            self::$USERS[$username]['token'] = $token;
        }

        return true;
    }

    private static function fetchClientToken(string $username, string $password): string
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/login_check',
            array(
                'username' => $username,
                'password' => $password,
            )
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        return $content['token'];
    }

    public static function getUsers(): array
    {
        return self::$USERS;
    }

    public function getUserNames()
    {
        $userNames = array_keys(self::$USERS);
        return array_map( static function ($username) {
            return array($username);
        }, $userNames);
    }

    public static function createAuthenticatedClient($username)
    {
        $token = self::getUsers()[$username]['token'];
        $client = self::createClient();
        $client->setServerParameter('HTTP_AUTHORIZATION', "Bearer $token");
        return $client;
    }
}
