FROM php:7.4.26-fpm-alpine3.14

WORKDIR /var/www/api

ENV COMPOSER_VERSION 76a7060ccb93902cd7576b67264ad91c8a2700e2
ENV APP_ENV=prod

# Default environment variables
# Those must be set for symfony scripts to run
ENV DATABASE_URL="mysql://treviz:password@127.0.0.1:3306/treviz?serverVersion=5.7"
ENV MAILER_URL=null://localhost
ENV RABBITMQ_HOST=rabbitmq
ENV RABBITMQ_PORT=5672
ENV RABBITMQ_DEFAULT_USER=guest

ENV RABBITMQ_DEFAULT_PASS=guest
ENV FRONTEND_URL=app.treviz.org
ENV BACKEND_HOST=api.treviz.org
ENV RSA_PASSPHRASE=passphrase
ENV OPEN_ORGANIZATION=true
ENV PLATFORM_NAME=Treviz

# Instal php extensions
RUN apk --update upgrade && \
    apk add --update --no-cache \
        icu \
        pwgen \
        openssl \
        wget \
        libzip-dev \
        libssh-dev \
        icu-dev \
        supervisor \
        autoconf \
        build-base \
        rabbitmq-c-dev \
        git \
    && docker-php-ext-configure intl \
    && docker-php-ext-install zip pdo pdo_mysql bcmath sockets intl \
    && pecl install amqp \
    && docker-php-ext-enable amqp

# Instal Composer
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/${COMPOSER_VERSION}/web/installer -O - -q | php -- --quiet

COPY composer.json composer.lock .env ./

RUN php -d memory_limit=-1 composer.phar install --optimize-autoloader --no-interaction --no-ansi --no-dev --no-scripts

COPY . ./

RUN mkdir -p public/upload/projects/logos \
    public/upload/communities/logos \
    public/upload/communities/backgrounds \
    public/upload/users/avatars \
    public/upload/users/backgrounds \
    /etc/supervisor.d && \
    apk del git wget && \
    rm -rf /tmp/* /var/cache/apk/* && \
    mv webserver/php.ini $PHP_INI_DIR/conf.d/ && \
    rm -rf webserver && \
    mv /var/www/api/supervisor/messenger-worker.ini /etc/supervisor.d/messenger-worker.ini && \
    APP_ENV=prod php composer.phar auto-scripts --no-dev && \
    APP_ENV=prod php composer.phar dump-autoload --no-dev --classmap-authoritative && \
    APP_ENV=prod php bin/console cache:warmup && \
    chown -R www-data:www-data var && \
    rm composer.phar

CMD ["/bin/sh", "/var/www/api/entrypoint.sh"]