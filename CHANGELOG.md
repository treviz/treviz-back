# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.8.2] - 2022-01-16
### Changed
- Remove some elements from the basic notification preferences
- Uses an async channel to deliver mails

# [0.8.1] - 2021-02-10
### Added
- Inline style attributes in mail for improved mail client support

# [0.8.0] - 2020-12-31
### Refactor
- Migrate the API to Symfony 4
- Translates the mail in French
- Add a link to the notification preferences page in every mail
- Specify the mail's object when sending an update notification

## [0.7.0] - 2020-08-22
### Added
- Allow users to quit their jobs
- Allow users to close brainstorming sessions

### Fixed
- Mails notifying a task rejection are no longer sent when tasks are approved
- Stop creating new roles each time a user joins an open community
- Allow invited users to see private project members if invited.

## [0.6.0] - 2020-06-23
### Added
- Communities now also send the number of linked projects
- Tests for project jobs

## Changed
- Notification mails for new candidacies and invitations approval are only sent to the users who can manage it

### Removed
- Removed references to crypto assets and rewards

## [0.5.6] - (2020-04-27)
## Fixed
- Resetting the password now activates the user

## [0.5.5] - (2020-04-20)
## Added
- Send notification on message update or deletion
- Send notification on room creation, update or deletion

## Fix
- Correctly send job candidacies

## [0.5.4] - (2019-10-27)
### Fixed
- Jobs can correctly be deleted
- Reset password link in mail

## [0.5.3] (2019-09-14)
### Refactor
- Use specific exchange for RabbitMQ to enhance message delivery
- Use Gitlab CI/CD to build and push images to registry

## [0.5.2] (2019-07-29)
### Changed
- Assignee can move their tasks
- It is now possible to exclude communities in which a user is member from a search.

### Chore
- Test cases for Kanban bundle added
- Test cases for Post management added
- Test cases for Mail Domain management added
- Test cases for User management added

## [0.5.1] (2019-03-24)
## Changed
- Docker now uses an Alpine-based image

### Fixed
- Correctly handle offset on user search

## [0.5.0] - (2019-02-10)
### Added
- Send update mail on task update, project fork, community candidacy and invitations
- Add a base structure to users
- Allow users to fetch an archive containing all their data
- Organizations can be open (anyone can create an account) or closed (admin only)
- Manage the terms of use and privacy notice of the platform
- Users can be linked to "organizations" (like schools or companies)
- Mail domains can be white or black listed

### Removed
- Removed administration bundle as it becomes part of the frontend

## [0.4.6] - (2018-12-03)
### Fixed
- Change the nginx conf to allow large file uploads
- Increase the picture size upload from 500kB to 1MB
- Increase the maximum file size from 10MB to 20MB
- Persist the boolean specifying if the user should see a welcome screen

## [0.4.5] - 2018-11-11
#### Fixed
- Changes entity mapping to allow community and projects to be deleted

## [0.4.4] - 2018-11-02
### Fixed
- Use a RabbitMQ instance to trigger server-sent events instead of relying on Ratchet

## [0.4.3] - 2018-10-07
### Added
- Allow users to delete their account or claim their data

### Fixed
- Only return the posts a user can see
- User can update community and project memberships

## [0.4.2] - 2018-05-01
### Added (1 change)
- Add a Changelog
- Projects can be open, closed, and forked from.

### Security
- Tasks can only be deleted by their supervisors or the projects's managers
- Changed md5 to sha-256 for creating hashes
- Protect API resources
