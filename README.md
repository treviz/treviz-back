Treviz Backend
========================

![Treviz Logo](public/images/treviz-logo.png)

## Description

Treviz is an open-source web platform that helps you set up and join open, collaborative organizations, in which you 
can work on the projects you want and get rewarded for your work.

This repository contains the code of the backend of the platform. It is developed around [Symfony 3.4](https://symfony.com)

## Features
The backend grants access to all the features of a Treviz Node, including:
* User registration and account management
* Manage skills and domains of interests (referred to as "tags")
* Launch communities in which users can discuss, share documents and ideas
* Create brainstorming sessions
* Launch and manage projects
* Manages tasks thanks to a Kanban
* Create "jobs" linked to projects
* Post news and comments
* Send real-time notifications
* Chat

Treviz is still in beta, so use it carefully.

## Installation

See our [documentation](https://doc.treviz.xyz) to learn more about how to install Treviz on your
own server.

If you do not have a web server you can host treviz on, feel free to check out [our hosting offers](https://treviz.xyz/#four)
and [contact us](https://treviz.xyz/contact) if you are interested. We'll grant you with a 30 day free trial.

## Develop locally

The requirements for developing the Treviz backend are the same as those for the production platform, except you may
not need nginx as a webserver. Indeed, you can simply start the built-in web server included in Symfony by prompting:

```
$ symfony server:start
```

## Contributing

Check out our [Contributing Guide](./CONTRIBUTING.md) if you are interested in joining the development team, if you want
to submit a bug, etc.

## Public instances

The list of public Treviz instances is available on [our website](https://treviz.xyz/instances). If you want to
list your, simply add it to the form, we'll check it out and display it!

## Privacy

Privacy is one of our main concerns when it comes to developing Treviz. We do not sell any information about our users, nor
do we try to analyze their behaviour. If a security breach was to be found, we would hunt it down and fix it as soon as we could.

## Contact

You can learn more about Treviz on:
* our website: [treviz.xyz](https://treviz.xyz)
* our documentation [doc.treviz.xyz](https://doc.treviz.xyz)
* our blog [blog.treviz.xyz](https://blog.treviz.xyz)

Feel free to [contact us](https://treviz.xyz/contact) if you have any question.

## To Do:

* Implements access control and rules for the notifications. For instance, when the ratchet instance receives an input
  specifying that a new message has been posted in a chatroom, and should be dispatched to users, check out the sender
  has the rights to do so.
* Write test cases
* Remove dependency towards FosUserBundle
* Implement OAuth 2.0 with OpenID Connect frameworks for Identification, authentication and authorization.
* Enrich the administration module with every possible entity
* Document every route of the API. We rely on [Nelmio API Doc Bundle](https://github.com/nelmio/NelmioApiDocBundle) in that regard.
