#!/bin/sh

# Migrate the app database
php /var/www/api bin/console doctrine:database:create --if-not-exists
php /var/www/api/bin/console doctrine:schema:update --force

# Make sure to have a clean start
php /var/www/api/bin/console cache:clear
php /var/www/api/bin/console cache:warmup
chmod 777 -R /var/www/api/var

supervisord -c /etc/supervisord.conf

php-fpm